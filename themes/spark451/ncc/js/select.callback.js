(function ($) {
	Drupal.behaviors.nccSelectCallback = {
		attach: function (context, settings) {
			var el;
			if(!settings['nccinit']){
				el = $('select', context).not('.ncc-modal select');
				settings['nccinit'] = true;
			}else{
				el = $('select', context);
			}
			el.once('ncc-select-callback').each(function (index, _el) {
				if (typeof Select === 'function' && typeof _el.selectInstance === 'undefined') {
					new Select({
						el: _el,
						className: 'select-theme-default'
					})
				}
			});
		}
	};
})(jQuery);