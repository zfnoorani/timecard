// Helpers
function nccHide(el) {
    el.classList.add('hidden');
}

function nccShow(el) {
    el.classList.remove('hidden');
}

function findAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
}

function findAncestorByTag(el, tag) {
    while ((el = el.parentElement) && el.tagName != tag);
    return el;
}

function getElementIndex(currentNode) {
    var index = 0;
    while ((currentNode = currentNode.previousElementSibling) != null) {
        index++;
    }
    return index;
}

function getPointerEvent(event) {
    if (event.originalEvent && event.originalEvent.targetTouches && event.originalEvent.targetTouches[0]) {
        return event.originalEvent.targetTouches[0];
    }
    return event;
};

function getSlideIndex(currentNode) {
    if (currentNode.classList.contains('ncc-slider-bg')) {
        currentNode = currentNode.parentElement;
    }
    return getElementIndex(currentNode);
}

function throttle(delay, fn) {
    var last, deferTimer;
    return function () {
        var context = this,
            args = arguments,
            now = +new Date;
        if (last && now < last + delay) {
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, delay);
        } else {
            last = now;
            fn.apply(context, args);
        }
    };
};

function isRetina() {
    var mediaQuery = "(-webkit-min-device-pixel-ratio: 1.5),\
              (min--moz-device-pixel-ratio: 1.5),\
              (-o-min-device-pixel-ratio: 3/2),\
              (min-resolution: 1.5dppx)";
    return ((window.devicePixelRatio > 1) || (window.matchMedia && window.matchMedia(mediaQuery).matches));
}

var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
var resize;

function resizedw() {
    windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
}
window.addEventListener("resize", function (event) {
    windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    clearTimeout(resize);
    resize = setTimeout(resizedw, 100);
});

function isMobileLayout() {
    //alert("isMobileLayout");
    return (windowWidth < 1024);
}

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        }, wait);
        if (immediate && !timeout) func.apply(context, args);
    };
};

var navOffset = 0;

// #### BEGIN: Detect iOS devices and set html class if detected
(function () {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    //console.log("ipad?");
    if (iOS) {
        //console.log("this is ipad");
        document.documentElement.classList.add("ios-device");
    }
})();
// #### END: Detect iOs

// #### BEGIN: Detect android devices and set html class if detected
(function () {
    var android = /Android/.test(navigator.userAgent);
    //console.log("ipad?");
    if (android) {
        //console.log("this is ipad");
        document.documentElement.classList.add("android-device");
    }
})();
// #### END: Detect iOs

// #### BEGIN: Navigation Component
if (!isMobileLayout()) {
    jQuery("#main-nav-inner li").click(function (event) {
        event.stopImmediatePropagation();
        var target = jQuery(event.target);
        var iOS = jQuery("html").hasClass("ios-device");
        var android = jQuery("html").hasClass("android-device");
        var tablet = false;
        if (iOS || android) {
            tablet = true;
        }
        var menuClicked = "";
        var typeOfMenu = "";

        //console.log("clicked");
        //console.log("vvvvv-target-vvvvv");
        //console.log(target);

        if (target.parent().parent().hasClass("main-nav-inner")) {
            //console.log("has main-nav-inner");
            menuClicked = target.parent(); //main-nav-inner li item
            typeOfMenu = "topMenu";
        } else {
            //console.log("no main-nav-inner");
            menuClicked = target.parent().parent().parent();//div class=ncc-submenu
            typeOfMenu = "subMenu";
        }

        //console.log(menuClicked);
        if (tablet && typeOfMenu == "topMenu") {
            //possible to add backgrount to current page topMenu item .css('background-color', 'rgba(53,57,60,0.3)');
            //console.log("topMenu");
            if (menuClicked.hasClass("is-active")) {
                //console.log("current topMenu is active");
                //should follow the topMenu link if it is active unless it is the first time the page is loaded.
                if (!target.siblings(".ncc-submenu").hasClass("is-active")) {
                    //console.log("current subMenu was not active so set it active");
                    event.preventDefault();
                    target.siblings(".ncc-submenu").addClass("is-active");
                }
            } else {
                //console.log("current topMenu is not active");
                //don't go to the link if it isn't active
                event.preventDefault();
                //remove is-active classes from all of the topMenu
                jQuery(".main-nav-inner li").removeClass("is-active");
                //remove is-active class from every subMenu
                jQuery(".main-nav-inner li div").removeClass("is-active");
                //set topMenu as active
                menuClicked.addClass("is-active");
                //set subMenu as active
                target.siblings(".ncc-submenu").addClass("is-active");

            }
        } else if (tablet && typeOfMenu == "subMenu") {
            //console.log("subMenu");
            if (menuClicked.hasClass("is-active")) {
                //console.log("current subMenu is active");
            } else {
                //console.log("current subMenu is not active");
            }
        }

    });
}
// BEGIN: Search Bar
(function (event) {
    var searchApiUrl = '/api/search';

    var nav = document.getElementById('ncc-navigation');
    //console.log("nav in search")
    if (nav != null) {
        //console.log("nav no null in search bar code");
        var search = document.getElementById('ncc-search');

        if (search != null) {
            var searchBar = document.getElementById('search-area');
            var searchBarOpen = document.getElementById('ncc-search-open');
            var searchBarClose = document.getElementById('ncc-search-close');
            var searchResultsWrapper = document.getElementById("ncc-live-search");
            var searchViewAll = document.getElementById('ncc-search-all');

            if (searchBarOpen != null) {
                searchBarOpen.addEventListener("click", openSearch);
                searchBarOpen.addEventListener("touchstart", openSearch);
            }

            var searchBarStickyOpen = document.getElementById('ncc-search-open-sticky');
            searchBarStickyOpen.addEventListener("click", openSearch);
            searchBarStickyOpen.addEventListener("touchstart", openSearch);

            searchBarClose.addEventListener("click", closeSearch);
            searchBarClose.addEventListener("touchstart", closeSearch);

            // search.addEventListener("keyup", doSearch);
            search.addEventListener("keyup", debounce(function (event) {
                doSearch(event);
            }, 500));
        }
    }

    function openSearch(event) {
        event.preventDefault();
        nccShow(searchBar);
        search.focus();
    }

    function closeSearch(event) {
        event.preventDefault();
        search.value = "";
        nccHide(searchBar);
        nccHide(searchResultsWrapper);
    }

    function loadSearchResults() {
        if (search.value) {
            var searchResults = document.getElementById("ncc-live-search-results");
            var spinnerHtml = '<div class="ncc-search-spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';

            searchResults.innerHTML = spinnerHtml;
            searchApiUrlWithKeys = searchApiUrl + "?keys=" + search.value;

            var xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = function () {
                var searchResultsHtml = '';

                if (xmlhttp.readyState === XMLHttpRequest.DONE) {
                    if (xmlhttp.status === 200) {
                        var responseJson = JSON.parse(xmlhttp.responseText);
                        var additionalClasses = '';

                        if (responseJson.length) {
                            for (var i = 0; i < responseJson.length; i++) {
                                additionalClasses = (i >= 3 ? 'ncc-hidden ncc-show-gt-sm' : '');

                                searchResultsHtml += '<div class="ncc-col-12 ncc-col-gt-sm-6 ncc-search-result-item ' + additionalClasses + '">\n';
                                searchResultsHtml += responseJson[i].item + '\n';
                                searchResultsHtml += '</div>\n';
                            }
                        } else {
                            searchResultsHtml = '<p>No results.</p>';
                        }

                        searchResults.innerHTML = searchResultsHtml;
                    } else if (xmlhttp.status === 400) {
                        console.error('Error', xmlhttp.statusText);
                    } else {
                        console.error('Error:', xmlhttp.statusText);
                    }
                }
            };

            if (searchApiUrl === '/api/search.json') {
                setTimeout(function () {
                    xmlhttp.open("GET", searchApiUrlWithKeys, true);
                    xmlhttp.send();
                }, 500);
            } else {
                xmlhttp.open("GET", searchApiUrlWithKeys, true);
                xmlhttp.send();
            }
        }
    }

    function doSearch(event) {
        if (!event.target.value.length) {
            nccHide(searchResultsWrapper);
        } else {
            var searchHref = searchViewAll.href.replace(/(keys=)[^\&]+/, '$1' + event.target.value);
            searchViewAll.href = searchHref;
            nccShow(searchResultsWrapper);
            loadSearchResults();
        }
    }
    // END: Search Bar

    // BEGIN: Menu
    //console.log("nav in menu");
    //console.log(nav);
    // alert("menu");
    if (nav != null) {
        var mainNav = document.getElementById('main-nav-wrapper');
        var mainNavInner = document.getElementById('main-nav-inner');

        var overlay = document.getElementById('ncc-navigation-overlay');
        overlay.addEventListener("click", closeMobileMenu);
        overlay.addEventListener("touchstart", closeMobileMenu);

        var mobileMenu = document.getElementById('ncc-mobile-menu');
        mobileMenu.addEventListener("click", openMobileMenu);
        mobileMenu.addEventListener("touchstart", openMobileMenu);
        //console.log("ncc-mobile-menu");

        var mobileMenuItems = document.getElementsByClassName('ncc-menu-children');
        for (var i = 0; i < mobileMenuItems.length; i++) {
            mobileMenuItems[i].addEventListener('click', showSubmenus);
            mobileMenuItems[i].addEventListener('touchstart', showSubmenus);
        }

        //var mainNavInner = document.getElementById('main-nav-inner');
        for (var i = 0; i < mainNavInner.children.length; i++) {
            setMenuClasses(mainNavInner.children[i]);
        }

        var importantLinks = document.getElementById('important-links');
        if (importantLinks != null) {
            importantLinks.addEventListener('click', showSubmenus);
            importantLinks.addEventListener("touchstart", showSubmenus);
        }
    }

    function adjustDoubleRowTitles(parent) {
        [].slice.call(parent.querySelectorAll('.ncc-submenu-items a.ncc-body-2 span.text-inner')).forEach(function (el) {
            // if submenu name is spanning into two lines, add extra class to its parent
            if (el.offsetHeight > 22) {
                el.parentElement.parentElement.classList.add('ncc-long-menu-item');
            }
        });
    }

    function setMenuClasses(submenu) {
        //console.log("setMenuClasses");
        var hasActive = false;
        var subitems = submenu.lastElementChild;

        // add small timeout so we are sure that flex childrens has fit in properly
        setTimeout(function () {
            adjustDoubleRowTitles(subitems);
        }, 250);

        if (subitems.children.length) {
            subitems = subitems.children[0];
            for (var i = 0; i < subitems.children.length; i++) {
                if (subitems.children[i].classList.contains('is-active')) {
                    hasActive = true;
                    break;
                }
            }
            //console.log(hasActive);
            if (hasActive) {
                submenu.classList.add('has-active-children');
                //console.log("has-active-children");
                if (isMobileLayout()) {
                    //console.log("has-active-children mobile layout truez");
                    submenu.classList.add('ncc-nav-hover');
                }
            } else {
                submenu.classList.add('no-active-children');
            }
        }
    }

    function closeMobileMenu(event) {
        //      event.preventDefault();
        //console.log("closeMobileMenu");
        mobileMenu.classList.remove('ncc-expanded');
        openMobileMenu(event);
    }

    function getActiveFirstLevel() {
        /*event.preventDefault();*/

        //console.log("getActiveFirstLevel");
        /*need to check if this is a root level page*/
        for (var i = 0; i < mainNavInner.children.length; i++) {
            if (mainNavInner.children[i].classList.contains('is-active')) {
                return mainNavInner.children[i];
            }
        }

        return false;
    }

    function openMobileMenu(event) {
        //console.log("openMobileMenu");
        //alert("openMobileMenu");
        event.preventDefault();
        if (isMobileLayout()) {
            overlay.classList.add('active');
            document.body.classList.add('noscroll');
            if (mobileMenu.classList.contains('ncc-expanded')) {
                mobileMenu.classList.remove('ncc-expanded');
                mainNav.classList.add('ncc-show');
                mainNav.classList.remove('ncc-nav-hover');
                var hovered = document.getElementsByClassName('ncc-nav-hover');
                if (hovered.length) {
                    hovered[0].classList.remove('ncc-nav-hover');
                }
            } else {
                if (mobileMenu.classList.contains('ncc-open')) {
                    overlay.classList.remove('active');
                    document.body.classList.remove('noscroll');
                    mobileMenu.classList.remove('ncc-open');
                } else {
                    mobileMenu.classList.add('ncc-open');
                    var active = getActiveFirstLevel();
                    if (active) {
                        //console.log(active);
                        setMenuClasses(active);
                        mobileMenu.classList.add('ncc-expanded');
                        mainNav.classList.add('ncc-nav-hover');
                        active.classList.add('ncc-nav-hover');
                    }
                }
                if (mainNav.classList.contains('ncc-show')) {
                    mainNav.classList.remove('ncc-show');
                } else {
                    mainNav.classList.add('ncc-show');
                }
            }
        }
    }

    function showSubmenus(event) {
        console.log("showSubmenus");
        //alert("showSubmenus")
        event.preventDefault();
        if (isMobileLayout()) {
            //console.log("ncc-nav-hover");
            this.parentElement.classList.toggle('ncc-nav-hover');
            mainNav.classList.add('ncc-nav-hover');
            adjustDoubleRowTitles(this.parentElement);
            mobileMenu.classList.add('ncc-expanded');
        }
    }

})();
// END: Menu

// #### END: Navigation Compoment

// #### BEGIN: Tabs Component
(function () {
    [].slice.call(document.querySelectorAll('.ncc-tabs')).forEach(function (el) {

        // Check how many tabs are in the wrapper, and add proper class for animations
        var wrapper = el.getElementsByClassName('ncc-tabs-wrapper')[0];
        var container = el.getElementsByTagName('ul')[0];
        var nav = el.getElementsByClassName('tabs-navigation')[0];
        var tabItems = container.getElementsByTagName('LI');
        var urlHash = window.location.hash;

        var minWidth = 0;
        var startPosition = 0;
        for (var i = 0; i <= tabItems.length - 1; i++) {
            // Check if we have tab ID in URL hash and change startPosition parameter according to that
            var tabID = tabItems[i].getAttribute('data-id');
            if (urlHash && (tabID == urlHash)) {
                startPosition = i;
            }
            // Get tab text width
            var elWidth = tabItems[i].getElementsByTagName('span')[0].offsetWidth;
            var icon = tabItems[i].getElementsByTagName('svg');
            // Add icon width to text width if exists
            elWidth += (icon && icon.length) ? icon[0].clientWidth : 0;
            // Increment minWidth
            minWidth += elWidth;
        };

        // Init Tabs
        var tabs = new CBPFWTabs(el, { start: startPosition });

        // Move by
        var moveOffset = minWidth / tabItems.length;

        var scrollTimeout;
        nav.addEventListener('click', function (event) {
            event.preventDefault();
            smoothScroll(0, 15, moveOffset);
        });

        function smoothScroll(leftOffset, moveBy, moveLimit) {
            scrollTimeout = setTimeout(function () {
                if (leftOffset >= moveLimit) {
                    clearTimeout(scrollTimeout);
                } else {
                    wrapper.scrollLeft += moveBy;
                    leftOffset += moveBy;
                    smoothScroll(leftOffset, moveBy, moveLimit);
                }
            }, 20);
        };

        var tabResize;

        function tabResizeListener() {
            if (wrapper.offsetWidth < minWidth) {
                container.style.width = minWidth + 'px';
                if (!wrapper.parentElement.classList.contains("tabs-breakpoint")) {
                    wrapper.parentElement.classList.add("tabs-breakpoint");
                }
            } else if (wrapper.parentElement.classList.contains("tabs-breakpoint")) {
                container.style = "";
                wrapper.parentElement.classList.remove("tabs-breakpoint");
            }
            if (container.classList.contains('animation-initialized')) {
                tabs.animation.style.width = tabItems[tabs.current].clientWidth + 'px';
                tabs.animation.style.transform = "translate(" + tabItems[tabs.current].offsetLeft + "px,0)";
            }
        }

        tabResizeListener();
        window.addEventListener("resize", function (event) {
            clearTimeout(tabResize);
            tabResize = setTimeout(tabResizeListener, 100);
        });
    });
})();
// #### END: Tabs Component

// #### BEGIN: Modal

var nccModal = (function () {
    //console.log("nccModal");
    //alert("modal");
    var modal = {},
        showModalButtons = document.getElementsByClassName('ncc-modal-btn');

    modal.init = function () {
        var touchStarted = false, // detect if a touch event is sarted
            currX = 0,
            currY = 0,
            cachedX = 0,
            cachedY = 0;

        for (var i = 0; i < showModalButtons.length; i++) {
            (function (showModalButtons, i) {
                showModalButtons[i].addEventListener('click', function (event) {
                    modal.displayModal(event, showModalButtons[i].getAttribute('data-target'));
                });

                showModalButtons[i].addEventListener('touchstart', function (event) {
                    var pointer = getPointerEvent(event);
                    // caching the current x
                    cachedX = currX = pointer.pageX;
                    // caching the current y
                    cachedY = currY = pointer.pageY;
                    // a touch event is detected
                    touchStarted = true;
                    // detecting if after 200ms the finger is still in the same position
                    setTimeout(function () {
                        if ((cachedX === currX) && !touchStarted && (cachedY === currY) && isMobileLayout()) {
                            modal.displayModal(event, showModalButtons[i].getAttribute('data-target'));
                        }
                    }, 200);

                });
            })(showModalButtons, i);
        }
    };

    if (showModalButtons) {
        modal.init();
    }

    modal.displayModal = function (event, modalContentId) {
        if (event) {
            event.preventDefault();
        }

        var dialog = document.getElementById(modalContentId);

        if (!dialog) {
            console.error('Modal Content #' + modalContentId + ' doesn\'t exist');
            return;
        }

        liteModal.open(modalContentId);
        this.initializeGallery(dialog);
    }

    // #### END: Modal

    // ### BEGIN: Initialize Gallery in modal

    modal.initializeGallery = function (dialog) {

        var gallery = dialog.getElementsByClassName('ncc-gallery')[0];

        if (gallery != null) {

            var slider = gallery.getElementsByClassName('gallery-top')[0];
            var thumbs = gallery.getElementsByClassName('gallery-thumbs')[0];
            var nextSlide = slider.getElementsByClassName('swiper-button-next')[0];
            var prevSlide = slider.getElementsByClassName('swiper-button-prev')[0];
            var pagination = gallery.getElementsByClassName('swiper-pagination')[0];

            var description = slider.getElementsByClassName('ncc-gallery-description');
            for (var i = 0; i < description.length; i++) {
                description[i].addEventListener('click', modal.showSlideDescription);
            }

            var galleryTop = new Swiper(slider, {
                nextButton: nextSlide,
                prevButton: prevSlide,
                pagination: pagination,
                paginationType: 'fraction',
                spaceBetween: 0
                // autoplay: 5000,
                // loop: true
            });

            var galleryThumbs = new Swiper(thumbs, {
                spaceBetween: 1,
                centeredSlides: true,
                grabCursor: true,
                slidesPerView: 10,
                slideToClickedSlide: true
            });

            galleryTop.params.control = galleryThumbs;
            galleryThumbs.params.control = galleryTop;
        }
    };

    modal.showSlideDescription = function (event) {
        event.preventDefault();
        if (isMobileLayout()) {
            var parent = event.target.parentElement;
            if (parent.classList.contains('expanded')) {
                parent.classList.add('collapsed');
                parent.classList.remove('expanded');
            } else {
                parent.classList.add('expanded');
                parent.classList.remove('collapsed');
            }
        }
    };

    // ### END: Initialize Gallery in modal

    return modal;
}());

// #### END: Modal

// #### START: Gallery
(function () {
    // Get all galleries on the page
    var galleries = document.getElementsByClassName('ncc-gallery-wrapper');
    if (galleries.length) {
        for (var i = 0; i < galleries.length; i++) {
            // Skip gallery initialization if it's in modal, no need for that until modal is triggered
            if (!galleries[i].parentElement.classList.contains('ncc-modal-content')) {
                nccModal.initializeGallery(galleries[i]);
            }
        }
    }
})();
// #### END: Gallery

// #### START: Slider

(function () {

    var sliders = document.getElementsByClassName('ncc-slider');
    for (var i = 0; i < sliders.length; i++) {
        var children = sliders[i].children[0].children[0].children;
        var parent = sliders[i];
        var navSlides = parent.getElementsByClassName('ncc-slider-indicators')[0];
        if (children.length) {
            // Set default active slide
            var active = (children.length > 2) ? 1 : 0;
            children[active].classList.add('active');
            if (children[active].previousElementSibling != null) {
                children[active].previousElementSibling.classList.add('pre-active');
            }
            if (children[active].nextElementSibling != null) {
                children[active].nextElementSibling.classList.add('post-active');
            }
            navSlides.children[active].classList.add('active');

            for (var j = 0; j < children.length; j++) {
                children[j].addEventListener('click', function (event) {
                    if (event.target.tagName.toLowerCase() == 'img') {
                        event.preventDefault();
                        var index = getSlideIndex(event.target.parentElement);
                        toggleSlider(index, parent, navSlides, false);
                    }
                })
            }
            if (children.length > 1) {
                initilizePagination(parent, navSlides, active);
            } else {
                parent.getElementsByClassName('ncc-slider-pagination')[0].classList.add('hidden');
            }
        }
    }

    function toggleSlider(index, parent, navSlides, isPagination) {
        var outerWrapper = parent.children[0].children[0];
        var el = (isPagination) ? outerWrapper.querySelectorAll("[data-index='" + index + "']")[0] : outerWrapper.children[index];

        if (el.classList.contains('active')) {
            return;
        }

        for (var i = 0; i < el.parentElement.children.length; i++) {
            var currentChild = el.parentElement.children[i];
            if (currentChild.classList.contains('active')) {
                currentChild.classList.remove('active');
            }
            if (currentChild.classList.contains('pre-active')) {
                currentChild.classList.remove('pre-active');
            }
            if (currentChild.classList.contains('post-active')) {
                currentChild.classList.remove('post-active');
            }
        }

        adjustDOM(el);

        for (var i = 0; i < navSlides.children.length; i++) {
            navSlides.children[i].classList.remove('active');
        }
        navSlides.children[el.getAttribute('data-index')].classList.add('active');
    }

    function adjustDOM(el) {
        var parent = el.parentElement;
        var newEl = parent.children[0];
        if (el == newEl) {
            newEl = parent.children[parent.children.length - 1];
            parent.removeChild(parent.children[parent.children.length - 1]);
            parent.insertBefore(newEl, el);
        } else {
            var toAppend = [];
            while ((prev = el.previousElementSibling.previousElementSibling) != null) {
                toAppend.push(prev);
                parent.removeChild(prev);
            }
            for (var i = toAppend.length - 1; i >= 0; i--) {
                parent.appendChild(toAppend[i]);
            }
        }
        setTimeout(function () {
            setActiveClasses(el);
        }, 10);
    }

    function setActiveClasses(el) {
        el.classList.add('active');
        if (el.previousElementSibling != null) {
            el.previousElementSibling.classList.add('pre-active');
        }
        if (el.nextElementSibling != null) {
            el.nextElementSibling.classList.add('post-active');
        }
    }

    // ## BEGIN: Slider Pagination Controls

    function initilizePagination(slider, navSlides, active) {
        var pagination = slider.getElementsByClassName('ncc-slider-pagination')[0];
        // Controls
        var navPrev = pagination.getElementsByClassName('ncc-slider-prev')[0];
        var navNext = pagination.getElementsByClassName('ncc-slider-next')[0];

        navPrev.addEventListener('click', function (event) {
            event.preventDefault();
            sliderGoToIndex(slider, navSlides, -1);
        });
        navNext.addEventListener('click', function (event) {
            event.preventDefault();
            sliderGoToIndex(slider, navSlides, 1);
        });
        if (navSlides.children.length) {
            for (var i = 0; i < navSlides.children.length; i++) {
                navSlides.children[i].addEventListener('click', function (event) {
                    var index = getSlideIndex(event.target);
                    toggleSlider(index, slider, navSlides, true);
                });
            }
            navSlides.children[active].classList.add('active');
        }
    }

    // Handle Prev/Next Pagination clicks
    function sliderGoToIndex(slider, navSlides, shift) {
        var current = navSlides.getElementsByClassName('active')[0];
        var index = getSlideIndex(current);
        index += shift;
        if (index >= 0 && index < navSlides.children.length) {
            toggleSlider(index, slider, navSlides, true);
        }
    }
    // ## END: Slider Pagination Controls

})();

// #### END: Slider


// #### BEGIN: Search Filter

(function () {
    var editKeys = document.getElementById('edit-keys'),
        facetCount = document.getElementsByClassName('facet-count');

    // Add placeholder on input element
    if (editKeys) {
        editKeys.placeholder = 'Search';
    }

    // Remove brackets and add dash on each facet count
    if (facetCount.length) {
        for (var i = 0; i < facetCount.length; i++) {
            facetCount[i].innerHTML = '- ' + facetCount[i].innerHTML.substring(1, facetCount[i].innerHTML.length - 1);
        }
    }
})();

// #### END: Search Filter


// #### BEGIN: Social Share

(function () {

    // Initialize Social Share Kit library
    var shareBox = document.getElementsByClassName('ncc-sharing-box')[0];
    if (shareBox != null) {

        SocialShareKit.init({
            selector: '.ncc-sharing-box .ssk'
        });

        var shareBoxHeight = shareBox.offsetHeight;

	//If we are midway down the window
	window.addEventListener('scroll', throttle(300, function () {
	    if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight/2 - shareBoxHeight) {
		//check if we have hidden social share
		if (!shareBox.classList.contains('slide-out')) {
		   shareBox.classList.remove('active');
		   shareBox.classList.add('slide-out');
		}
	    //scrolled up
	    }else{
		    shareBox.classList.remove('slide-out');
		    shareBox.classList.add('active');
	    }
	}));
    }

})();

// #### END: Social Share


// #### START: Tray
var nccTray = (function () {
    var tray = {},
        bodyEl = document.body,
        pageWrapper = document.getElementById('page-wrapper'),
        trayEl = document.getElementsByClassName('ncc-tray'),
        trayContent = document.getElementById('ncc-tray-content'),
        trayOpenBtn = document.getElementById('ncc-tray-open-btn'),
        trayCloseBtn = document.getElementById('ncc-tray-close-btn'),
        openTray = document.getElementsByClassName('ncc-open-tray'),
        isTrayOpen = false,
        modalBackdrop;

    tray.init = function () {
        if (trayEl) {
            if (trayOpenBtn) {
                trayOpenBtn.addEventListener('click', tray.toggleTray);
            }

            if (trayCloseBtn) {
                trayCloseBtn.addEventListener('click', tray.toggleTray);
            }

            for (var i = 0; i < openTray.length; i++) {
                (function (openTray, i) {
                    openTray[i].addEventListener('click', function (event) {
                        tray.toggleTray(event, openTray[i].getAttribute('rel'));
                    });
                })(openTray, i)
            }
        }
    }

    tray.toggleTray = function (event, trayContentId) {
        event.preventDefault();

        var trayContentNew;

        if (trayContentId) {
            trayContentNew = document.getElementById(trayContentId);

            if (trayContentNew) {
                trayContent.innerHTML = trayContentNew.innerHTML;
            } else {
                console.error('Tray Content #' + trayContentId + ' doesn\'t exist');
                return;
            }
        }

        if (isTrayOpen) {
            modalBackdrop = document.getElementsByClassName('modal-backdrop');;
            
            if (!('remove' in Element.prototype)) {
                Element.prototype.remove = function() {
                    if (this.parentNode) {
                        this.parentNode.removeChild(this);
                    }
                };
            }

            if (modalBackdrop[0]) {
                modalBackdrop[0].remove();
            }

            bodyEl.classList.remove('ncc-tray-show');

        } else {
            modalBackdrop = document.createElement("div");
            modalBackdrop.classList.add("modal-backdrop");
            modalBackdrop.addEventListener('click', tray.toggleTray);
            pageWrapper.appendChild(modalBackdrop);

            bodyEl.classList.add('ncc-tray-show');
        }

        isTrayOpen = !isTrayOpen;
    }

    tray.init();

    return tray;
}());
// #### END: Tray

// #### BEGIN: Object Fit css selector polyfill for IE (mostly)

if (typeof objectFitImages == 'function') {
    objectFitImages();
}

// #### END: Object Fit css selector polyfill for IE (mostly)

// #### BEGIN: Marketing Form

(function () {
    document.addEventListener('DOMContentLoaded', function () {
        setTimeout(function () {
            billboard = document.getElementById('ncc-billboard-marketing'),
                modal = billboard ? billboard.getElementsByClassName('ncc-modal-form')[0] : null
            if (billboard && modal) {
                nccModal.displayModal(null, modal.id);
            }
        });
    });
}());

// #### END: Marketing Form

// #### BEGIN: DatePicker

(function () {
    if (window.jQuery) {
        jQuery(document).ready(function(){
            [].slice.call(document.querySelectorAll('input[data-ncc-date-format]')).forEach(function (el) {
                // Fetch default date format for field
                var format = jQuery(el).attr('data-ncc-date-format');
                console.log(format);
                console.log(jQuery(el));
                jQuery(el).kendoDatePicker({
                    format: (format) ? format : 'yyyy-MM-dd'
                });
            })
        });
    } else {
        console.warn("jQuery is not defined");
    }
}());

// #### END: DatePicker

// #### BEGIN: Drupal workbench moderation form fix

(function () {
    var localTasks = document.getElementById('block-ncc-local-tasks'),
        workbenchModerationForm = document.getElementById('workbench-moderation-entity-moderation-form'),
        userid = drupalSettings.user.uid, 
        marketingPage = document.body.classList.contains('marketing');
        
    //if marketing page and not logged in then turn off display of the admin menu    
    if(marketingPage && userid === 0){        
        localTasks.style.display = "none";                
    }    

    if (localTasks && workbenchModerationForm) {        
        workbenchModerationForm.style.bottom = localTasks.offsetHeight + 'px';

        window.addEventListener("resize", function (event) {
            workbenchModerationForm.style.bottom = localTasks.offsetHeight + 'px';
        });
    }

}());

// #### END: Drupal workbench moderation form fix

// #### START: Alert
(function () {
    // Get Alert Component
    var alert = document.getElementById('ncc-alert');
    if (alert != null) {
        var closeAlert = document.getElementById('ncc-alert-close-btn');
        var alertType = document.getElementById('ncc-alert-type');
        var alertContent = document.getElementById('ncc-alert-content');

        var nav = document.getElementById('ncc-navigation');
        //console.log("nav in START: alert");

        // Trigger close Alert
        var triggerClose = function () {
            alert.classList.add('ncc-alert-closed');
            if (nav != null) {
                navOffset = nav.offsetHeight;
                if (nav.parentElement.classList.contains('ncc-navigation-placeholder-active')) {
                    nav.parentElement.style.paddingTop = navOffset + 'px';
                }
            }
        }
        // Get Alert node ID
        var alertID = alert.getAttribute('data-node');
        // If set cookie that user closed alert with that ID before, initially close it
        var isCollapsed = Cookies.get('ncc-alert-' + alertID);
        if (isCollapsed) {
            triggerClose();
        }

        closeAlert.addEventListener('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            // When user close specific alert, set cookie so on reload that alert will start as collapsed
            Cookies.set('ncc-alert-' + alertID, 1, { expires: 7 });
            triggerClose();
        });

        alert.addEventListener('mouseup', function (event) {
            event.stopPropagation();
            // Only if alert is collapsed
            if (alert.classList.contains('ncc-alert-closed')) {
                alert.classList.remove('ncc-alert-closed');
                // When user expands alert, remove cookie that says it's collapsed
                Cookies.remove('ncc-alert-' + alertID);
                if (nav != null) {
                    navOffset = nav.offsetHeight;
                }
                if (nav.parentElement.classList.contains('ncc-navigation-placeholder-active')) {
                    nav.parentElement.style.paddingTop = nav.offsetHeight + 'px';
                }
            }
        });
    }
})();
// #### END: Alert

// BEGIN: Sticky Navigation
(function (document, window, index) {
    //console.log("sticky navigation");
    'use strict';
    var elSelector = '.ncc-navigation',
        elClassHidden = 'ncc-navigation--hidden',
        throttleTimeout = 200,
        element = document.querySelector(elSelector),
        alert = document.getElementById('ncc-alert'),
        breadcrumbs = document.querySelector('.ncc-breadcrumbs');

    if (!element) return true;

    var dHeight = 0,
        wHeight = 0,
        wScrollCurrent = 0,
        wScrollBefore = 0,
        wScrollDiff = 0,
        collapsedAlertHeight = 40,

        hasElementClass = function (element, className) {
            return element.classList ? element.classList.contains(className) : new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
        },
        addElementClass = function (element, className) {
            element.classList ? element.classList.add(className) : element.className += ' ' + className;
        },
        removeElementClass = function (element, className) {
            element.classList ? element.classList.remove(className) : element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        };

    navOffset = element.offsetHeight;

    window.addEventListener("resize", throttle(throttleTimeout, function () {
        navOffset = (element.getBoundingClientRect().top + element.offsetHeight);
        element.parentElement.style.paddingTop = 0;
    }));

    window.addEventListener('scroll', throttle(throttleTimeout, function () {
        dHeight = document.body.offsetHeight;
        wHeight = window.innerHeight;
        wScrollCurrent = window.pageYOffset;
        wScrollDiff = wScrollBefore - wScrollCurrent;

        // scrolled to the very top; element sticks to the top
        if (wScrollCurrent <= navOffset) {
            if (element.classList.contains('ncc-navigation-sticky')) {
                element.classList.remove('ncc-navigation-sticky');
                element.parentElement.classList.remove('ncc-navigation-placeholder-active');
                element.parentElement.style.paddingTop = 0;
            }
            removeElementClass(element, elClassHidden);
        } else if (wScrollDiff > 0 && hasElementClass(element, elClassHidden)) { // scrolled up; element slides in
            removeElementClass(element, elClassHidden);
        }
        // scrolled down
        else if (wScrollDiff < 0) {
            if (!element.classList.contains('ncc-navigation-sticky')) {
                element.classList.add('no-transition');
                setTimeout(function () {
                    element.classList.remove('no-transition');
                }, 100);
                element.classList.add('ncc-navigation-sticky');
                element.parentElement.classList.add('ncc-navigation-placeholder-active');
                element.parentElement.style.paddingTop = navOffset + 'px';
            }
            addElementClass(element, elClassHidden);
        }
        wScrollBefore = wScrollCurrent;
    }));

}(document, window, 0));
  // END: Sticky Navigation
