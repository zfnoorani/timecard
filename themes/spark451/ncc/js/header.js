// Init libraries (Typekit and svg4everybody).
try {
    // Execute svg4everybody.
    svg4everybody();

    // Load Typekit font(s).
    Typekit.load({async: true});
} catch (e) {
    console.error(e.stack, e);
}
