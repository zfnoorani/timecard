import {Component, ElementRef} from '@angular/core';

@Component({
    moduleId: __moduleName,
    selector: 'ng2-ncc-stack',
    template: `<template [nccDynamicHTML]="stack"></template>`
})
export class Ng2NccStackApp {
    protected stack: string;

    constructor(private _elementRef: ElementRef) {
        var component = drupalSettings.pdb.ng2.components[_elementRef.nativeElement.id];
        this.stack = component.template;
    }
}
