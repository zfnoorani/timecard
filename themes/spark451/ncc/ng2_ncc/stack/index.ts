/// <reference path="../../typings/index.d.ts" />
/// <reference path="../../app/core/typings.d.ts" />

// Modules
import { NgModule }      from '@angular/core';
import { HttpModule }     from '@angular/http';
import { CommonModule } from '@angular/common';

// external imports
import {LazyLoadComponent} from 'helpers/lazy-load-component';

// Components
import { Ng2NccStackApp }  from './ng2-ncc-stack.component';
import { NccDynamicHtmlModule } from '../ncc-dynamic-html.module';

// exports
export * from './globals';

@NgModule({
  imports: [                  // module dependencies
      HttpModule,
      NccDynamicHtmlModule
  ],
  declarations: [             // components and directives
    Ng2NccStackApp,
  ],  
  entryComponents: [                 // root component
    Ng2NccStackApp 
  ],
  providers: [                      // services
    { provide: LazyLoadComponent, useValue: Ng2NccStackApp }
  ],
  exports: [
    //
  ]
})

export class Ng2NccStack { }
