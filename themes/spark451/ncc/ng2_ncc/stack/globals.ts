/**
 * @module Ng2NccStack
 */ /** */

// external imports
import {mergeGlobals} from 'helpers/globals';

export const Ng2NccStackGlobals = mergeGlobals({}, []);