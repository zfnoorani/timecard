System.register(["@angular/core", "@angular/common", "./components/ncc-dynamic-html/ncc-dynamic-html.directive", "./components/ncc-fit-text/ncc-fit-text.directive", "./components/ncc-pipes/ncc-slice.pipe", "./components/ncc-pipes/ncc-safeurl.pipe", "./components/ncc-stack/ncc-stack.component", "./components/ncc-card/ncc-card.component", "./components/ncc-social-feed/ncc-social-feed.component"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, common_1, ncc_dynamic_html_directive_1, ncc_fit_text_directive_1, ncc_slice_pipe_1, ncc_safeurl_pipe_1, ncc_stack_component_1, ncc_card_component_1, ncc_card_component_2, ncc_card_component_3, ncc_card_component_4, ncc_card_component_5, ncc_card_component_6, ncc_card_component_7, ncc_card_component_8, ncc_card_component_9, ncc_card_component_10, ncc_card_component_11, ncc_card_component_12, ncc_card_component_13, ncc_card_component_14, ncc_card_component_15, ncc_card_component_16, ncc_card_component_17, ncc_card_component_18, ncc_card_component_19, ncc_card_component_20, ncc_social_feed_component_1, NccDynamicHtmlModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (ncc_dynamic_html_directive_1_1) {
                ncc_dynamic_html_directive_1 = ncc_dynamic_html_directive_1_1;
            },
            function (ncc_fit_text_directive_1_1) {
                ncc_fit_text_directive_1 = ncc_fit_text_directive_1_1;
            },
            function (ncc_slice_pipe_1_1) {
                ncc_slice_pipe_1 = ncc_slice_pipe_1_1;
            },
            function (ncc_safeurl_pipe_1_1) {
                ncc_safeurl_pipe_1 = ncc_safeurl_pipe_1_1;
            },
            function (ncc_stack_component_1_1) {
                ncc_stack_component_1 = ncc_stack_component_1_1;
            },
            function (ncc_card_component_1_1) {
                ncc_card_component_1 = ncc_card_component_1_1;
                ncc_card_component_2 = ncc_card_component_1_1;
                ncc_card_component_3 = ncc_card_component_1_1;
                ncc_card_component_4 = ncc_card_component_1_1;
                ncc_card_component_5 = ncc_card_component_1_1;
                ncc_card_component_6 = ncc_card_component_1_1;
                ncc_card_component_7 = ncc_card_component_1_1;
                ncc_card_component_8 = ncc_card_component_1_1;
                ncc_card_component_9 = ncc_card_component_1_1;
                ncc_card_component_10 = ncc_card_component_1_1;
                ncc_card_component_11 = ncc_card_component_1_1;
                ncc_card_component_12 = ncc_card_component_1_1;
                ncc_card_component_13 = ncc_card_component_1_1;
                ncc_card_component_14 = ncc_card_component_1_1;
                ncc_card_component_15 = ncc_card_component_1_1;
                ncc_card_component_16 = ncc_card_component_1_1;
                ncc_card_component_17 = ncc_card_component_1_1;
                ncc_card_component_18 = ncc_card_component_1_1;
                ncc_card_component_19 = ncc_card_component_1_1;
                ncc_card_component_20 = ncc_card_component_1_1;
            },
            function (ncc_social_feed_component_1_1) {
                ncc_social_feed_component_1 = ncc_social_feed_component_1_1;
            }
        ],
        execute: function () {
            NccDynamicHtmlModule = (function () {
                function NccDynamicHtmlModule() {
                }
                NccDynamicHtmlModule = __decorate([
                    core_1.NgModule({
                        imports: [
                            common_1.CommonModule
                        ],
                        declarations: [
                            ncc_dynamic_html_directive_1.NccDynamicHTML,
                            ncc_stack_component_1.NccStackComponent,
                            ncc_card_component_1.NccCardComponent,
                            ncc_card_component_2.NccCardCtaComponent,
                            ncc_card_component_3.NccCardCareerComponent,
                            ncc_card_component_4.NccCardCtaPhotoComponent,
                            ncc_card_component_5.NccCardFeatureComponent,
                            ncc_card_component_6.NccCardFactoidComponent,
                            ncc_card_component_7.NccCardImageComponent,
                            ncc_card_component_8.NccCardTitleComponent,
                            ncc_card_component_9.NccCardQuoteComponent,
                            ncc_card_component_10.NccCardEventComponent,
                            ncc_card_component_11.NccCardGalleryComponent,
                            ncc_card_component_12.NccCardProfileComponent,
                            ncc_card_component_13.NccCardVideoComponent,
                            ncc_card_component_14.NccCardStoryComponent,
                            ncc_card_component_15.NccCardNewsComponent,
                            ncc_card_component_16.NccCardProgramComponent,
                            ncc_card_component_17.NccCardMapComponent,
                            ncc_card_component_18.NccCardFacebookComponent,
                            ncc_card_component_19.NccCardInstagramComponent,
                            ncc_card_component_20.NccCardTwitterComponent,
                            ncc_social_feed_component_1.NccSocialFeedComponent,
                            ncc_fit_text_directive_1.NccFitTextDirective,
                            ncc_slice_pipe_1.NccSlicePipe,
                            ncc_safeurl_pipe_1.NccSafeUrlPipe,
                        ],
                        exports: [
                            ncc_dynamic_html_directive_1.NccDynamicHTML,
                            ncc_stack_component_1.NccStackComponent,
                            ncc_card_component_1.NccCardComponent,
                            ncc_card_component_2.NccCardCtaComponent,
                            ncc_card_component_3.NccCardCareerComponent,
                            ncc_card_component_4.NccCardCtaPhotoComponent,
                            ncc_card_component_5.NccCardFeatureComponent,
                            ncc_card_component_6.NccCardFactoidComponent,
                            ncc_card_component_7.NccCardImageComponent,
                            ncc_card_component_8.NccCardTitleComponent,
                            ncc_card_component_9.NccCardQuoteComponent,
                            ncc_card_component_10.NccCardEventComponent,
                            ncc_card_component_11.NccCardGalleryComponent,
                            ncc_card_component_12.NccCardProfileComponent,
                            ncc_card_component_13.NccCardVideoComponent,
                            ncc_card_component_14.NccCardStoryComponent,
                            ncc_card_component_15.NccCardNewsComponent,
                            ncc_card_component_16.NccCardProgramComponent,
                            ncc_card_component_17.NccCardMapComponent,
                            ncc_card_component_18.NccCardFacebookComponent,
                            ncc_card_component_19.NccCardInstagramComponent,
                            ncc_card_component_20.NccCardTwitterComponent,
                            ncc_fit_text_directive_1.NccFitTextDirective,
                            ncc_social_feed_component_1.NccSocialFeedComponent,
                            ncc_slice_pipe_1.NccSlicePipe,
                            ncc_safeurl_pipe_1.NccSafeUrlPipe
                        ]
                    })
                ], NccDynamicHtmlModule);
                return NccDynamicHtmlModule;
            }());
            exports_1("NccDynamicHtmlModule", NccDynamicHtmlModule);
        }
    };
});
//# sourceMappingURL=ncc-dynamic-html.module.js.map