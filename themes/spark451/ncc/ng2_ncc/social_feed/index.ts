// Modules
import { NgModule }      from '@angular/core';

// external imports
import {LazyLoadComponent} from 'helpers/lazy-load-component';

// Components
import { Ng2NccSocialFeedApp }  from './ng2-ncc-social-feed.component';
import { NccDynamicHtmlModule } from '../ncc-dynamic-html.module';

// exports
export * from './globals';

@NgModule({
    imports: [                        // module dependencies
        NccDynamicHtmlModule
    ],
    declarations: [                      // components, pipes and directives
        Ng2NccSocialFeedApp
    ],                
    entryComponents: [                   // root component
        Ng2NccSocialFeedApp
    ],
    exports: [
        //
    ],
    providers: [                         // providers/service
        { provide: LazyLoadComponent, useValue: Ng2NccSocialFeedApp }
    ]                    
})

export class Ng2NccSocialFeed { }