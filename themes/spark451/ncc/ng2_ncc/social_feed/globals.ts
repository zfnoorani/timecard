/**
 * @module Ng2NccSocial
 */ /** */

// external imports
import {mergeGlobals} from 'helpers/globals';

export const Ng2NccSocialFeedGlobals = mergeGlobals({}, []);