System.register(["@angular/core", "helpers/lazy-load-component", "./ng2-ncc-social-feed.component", "../ncc-dynamic-html.module", "./globals"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, lazy_load_component_1, ng2_ncc_social_feed_component_1, ncc_dynamic_html_module_1, Ng2NccSocialFeed;
    var exportedNames_1 = {
        "Ng2NccSocialFeed": true
    };
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default" && !exportedNames_1.hasOwnProperty(n)) exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (lazy_load_component_1_1) {
                lazy_load_component_1 = lazy_load_component_1_1;
            },
            function (ng2_ncc_social_feed_component_1_1) {
                ng2_ncc_social_feed_component_1 = ng2_ncc_social_feed_component_1_1;
            },
            function (ncc_dynamic_html_module_1_1) {
                ncc_dynamic_html_module_1 = ncc_dynamic_html_module_1_1;
            },
            function (globals_1_1) {
                exportStar_1(globals_1_1);
            }
        ],
        execute: function () {
            Ng2NccSocialFeed = (function () {
                function Ng2NccSocialFeed() {
                }
                Ng2NccSocialFeed = __decorate([
                    core_1.NgModule({
                        imports: [
                            ncc_dynamic_html_module_1.NccDynamicHtmlModule
                        ],
                        declarations: [
                            ng2_ncc_social_feed_component_1.Ng2NccSocialFeedApp
                        ],
                        entryComponents: [
                            ng2_ncc_social_feed_component_1.Ng2NccSocialFeedApp
                        ],
                        exports: [],
                        providers: [
                            { provide: lazy_load_component_1.LazyLoadComponent, useValue: ng2_ncc_social_feed_component_1.Ng2NccSocialFeedApp }
                        ]
                    })
                ], Ng2NccSocialFeed);
                return Ng2NccSocialFeed;
            }());
            exports_1("Ng2NccSocialFeed", Ng2NccSocialFeed);
        }
    };
});
//# sourceMappingURL=index.js.map