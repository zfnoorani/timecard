import { Component, ElementRef } from '@angular/core';

@Component({
    moduleId: __moduleName,
    selector: 'ng2-ncc-social-feed',
    template: `<template [nccDynamicHTML]="socialFeed"></template>`
})
export class Ng2NccSocialFeedApp {
    protected socialFeed: string;

    constructor(private _elementRef: ElementRef) {
        var component = drupalSettings.pdb.ng2.components[_elementRef.nativeElement.id];
        this.socialFeed = component.template;
    }
}