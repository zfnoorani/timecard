// Modules
import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';

// Directives
import { NccDynamicHTML } from './components/ncc-dynamic-html/ncc-dynamic-html.directive';
import { NccFitTextDirective } from './components/ncc-fit-text/ncc-fit-text.directive';
// Pipes
import { NccSlicePipe } from './components/ncc-pipes/ncc-slice.pipe';
import { NccSafeUrlPipe } from './components/ncc-pipes/ncc-safeurl.pipe';

// Components
import { NccStackComponent } from './components/ncc-stack/ncc-stack.component';
import { NccCardComponent } from './components/ncc-card/ncc-card.component';
import { NccCardCtaComponent } from './components/ncc-card/ncc-card.component';
import { NccCardCareerComponent } from './components/ncc-card/ncc-card.component';
import { NccCardCtaPhotoComponent } from './components/ncc-card/ncc-card.component';
import { NccCardFeatureComponent } from './components/ncc-card/ncc-card.component';
import { NccCardFactoidComponent } from './components/ncc-card/ncc-card.component';
import { NccCardImageComponent } from './components/ncc-card/ncc-card.component';
import { NccCardTitleComponent } from './components/ncc-card/ncc-card.component';
import { NccCardQuoteComponent } from './components/ncc-card/ncc-card.component';
import { NccCardEventComponent } from './components/ncc-card/ncc-card.component';
import { NccCardGalleryComponent } from './components/ncc-card/ncc-card.component';
import { NccCardProfileComponent } from './components/ncc-card/ncc-card.component';
import { NccCardVideoComponent } from './components/ncc-card/ncc-card.component';
import { NccCardStoryComponent } from './components/ncc-card/ncc-card.component';
import { NccCardNewsComponent } from './components/ncc-card/ncc-card.component';
import { NccCardProgramComponent } from './components/ncc-card/ncc-card.component';
import { NccCardMapComponent } from './components/ncc-card/ncc-card.component';
import { NccCardFacebookComponent } from './components/ncc-card/ncc-card.component';
import { NccCardInstagramComponent } from './components/ncc-card/ncc-card.component';
import { NccCardTwitterComponent } from './components/ncc-card/ncc-card.component';
import { NccSocialFeedComponent }  from './components/ncc-social-feed/ncc-social-feed.component';


@NgModule({
  imports: [                  // module dependencies
      CommonModule
  ],
  declarations: [             // components and directives
    // Components
    NccDynamicHTML,
    NccStackComponent,
    NccCardComponent,
    NccCardCtaComponent,
    NccCardCareerComponent,
    NccCardCtaPhotoComponent,
    NccCardFeatureComponent,
    NccCardFactoidComponent,
    NccCardImageComponent,
    NccCardTitleComponent,
    NccCardQuoteComponent,
    NccCardEventComponent,
    NccCardGalleryComponent,
    NccCardProfileComponent,
    NccCardVideoComponent,
    NccCardStoryComponent,
    NccCardNewsComponent,
    NccCardProgramComponent,
    NccCardMapComponent,
    NccCardFacebookComponent,
    NccCardInstagramComponent,
    NccCardTwitterComponent,
    NccSocialFeedComponent,
    // Directives
    NccFitTextDirective,
    // Pipes
    NccSlicePipe,
    NccSafeUrlPipe,
  ],  
  exports: [
  	// Components
    NccDynamicHTML,
    NccStackComponent,
    NccCardComponent,
    NccCardCtaComponent,
    NccCardCareerComponent,
    NccCardCtaPhotoComponent,
    NccCardFeatureComponent,
    NccCardFactoidComponent,
    NccCardImageComponent,
    NccCardTitleComponent,
    NccCardQuoteComponent,
    NccCardEventComponent,
    NccCardGalleryComponent,
    NccCardProfileComponent,
    NccCardVideoComponent,
    NccCardStoryComponent,
    NccCardNewsComponent,
    NccCardProgramComponent,
    NccCardMapComponent,
    NccCardFacebookComponent,
    NccCardInstagramComponent,
    NccCardTwitterComponent,
    // Directives
    NccFitTextDirective,
    NccSocialFeedComponent,
    // Pipes
    NccSlicePipe,
    NccSafeUrlPipe
  ]
})

export class NccDynamicHtmlModule { }
