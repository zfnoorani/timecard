System.register(["@angular/core", "@angular/common", "@angular/http", "helpers/lazy-load-component", "./ng2-ncc-calendar.component", "../components/ncc-calendar/ncc-calendar.component", "../components/ncc-calendar-tray/ncc-calendar-tray.component", "../components/ncc-calendar/ncc-calendar.service", "./globals"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, common_1, http_1, lazy_load_component_1, ng2_ncc_calendar_component_1, ncc_calendar_component_1, ncc_calendar_tray_component_1, ncc_calendar_service_1, Ng2NccCalendarModule;
    var exportedNames_1 = {
        "Ng2NccCalendarModule": true
    };
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default" && !exportedNames_1.hasOwnProperty(n)) exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (lazy_load_component_1_1) {
                lazy_load_component_1 = lazy_load_component_1_1;
            },
            function (ng2_ncc_calendar_component_1_1) {
                ng2_ncc_calendar_component_1 = ng2_ncc_calendar_component_1_1;
            },
            function (ncc_calendar_component_1_1) {
                ncc_calendar_component_1 = ncc_calendar_component_1_1;
            },
            function (ncc_calendar_tray_component_1_1) {
                ncc_calendar_tray_component_1 = ncc_calendar_tray_component_1_1;
            },
            function (ncc_calendar_service_1_1) {
                ncc_calendar_service_1 = ncc_calendar_service_1_1;
            },
            function (globals_1_1) {
                exportStar_1(globals_1_1);
            }
        ],
        execute: function () {
            Ng2NccCalendarModule = (function () {
                function Ng2NccCalendarModule() {
                }
                Ng2NccCalendarModule = __decorate([
                    core_1.NgModule({
                        imports: [
                            http_1.HttpModule,
                            common_1.CommonModule
                        ],
                        declarations: [
                            ng2_ncc_calendar_component_1.NccCalendarApp,
                            ncc_calendar_component_1.NccCalendarComponent,
                            ncc_calendar_tray_component_1.NccCalendarTrayComponent
                        ],
                        entryComponents: [
                            ng2_ncc_calendar_component_1.NccCalendarApp
                        ],
                        exports: [
                            ncc_calendar_component_1.NccCalendarComponent,
                            ncc_calendar_tray_component_1.NccCalendarTrayComponent
                        ],
                        providers: [
                            ncc_calendar_service_1.NccCalendarService,
                            { provide: ncc_calendar_service_1.EVENTS_API_URL, useValue: 'api/events' },
                            { provide: ncc_calendar_service_1.EVENTS_FILTERS_URL, useValue: 'api/event-filters' },
                            { provide: lazy_load_component_1.LazyLoadComponent, useValue: ng2_ncc_calendar_component_1.NccCalendarApp }
                        ]
                    })
                ], Ng2NccCalendarModule);
                return Ng2NccCalendarModule;
            }());
            exports_1("Ng2NccCalendarModule", Ng2NccCalendarModule);
        }
    };
});
//# sourceMappingURL=index.js.map