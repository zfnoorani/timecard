/// <reference path="../../typings/index.d.ts" />
/// <reference path="../../app/core/typings.d.ts" />

// Modules
import { NgModule }      from '@angular/core';
import { CommonModule }      from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

// external imports
import {LazyLoadComponent} from 'helpers/lazy-load-component';

// Components
import { NccCalendarApp }  from './ng2-ncc-calendar.component';
import { NccCalendarComponent } from '../components/ncc-calendar/ncc-calendar.component';
import { NccCalendarTrayComponent } from '../components/ncc-calendar-tray/ncc-calendar-tray.component';
import { EVENTS_API_URL, EVENTS_FILTERS_URL, NccCalendarService } from '../components/ncc-calendar/ncc-calendar.service';

// exports
export * from './globals';

@NgModule({
    imports: [                        // module dependencies
        HttpModule,
        CommonModule
    ],
    declarations: [                   // components, pipes and directives
        NccCalendarApp,
        NccCalendarComponent,
        NccCalendarTrayComponent
    ],                
    entryComponents: [                // root component
        NccCalendarApp 
    ],
    exports: [
        NccCalendarComponent,
        NccCalendarTrayComponent
    ],
    providers: [                      // providers/services
        NccCalendarService,
        { provide: EVENTS_API_URL, useValue: 'api/events' }, // Local JSON Data (dev data)
        { provide: EVENTS_FILTERS_URL, useValue: 'api/event-filters' }, // Local JSON Data (dev data)
        { provide: LazyLoadComponent, useValue: NccCalendarApp }

    ] 
})

export class Ng2NccCalendarModule{ }