/**
 * @module Ng2NccCalendar
 */ /** */

// external imports
import {mergeGlobals} from 'helpers/globals';

export const Ng2NccCalendarGlobals = mergeGlobals({}, []);