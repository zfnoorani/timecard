import {Component} from '@angular/core';

@Component({
    selector: 'ncc-app-5',
    template: `
        <ncc-calendar></ncc-calendar>
    `,
    styles: [`:host { display: block; }`],
})

export class NccCalendarApp {
    constructor() { }
}
