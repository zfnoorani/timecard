System.register(["@angular/core", "rxjs/Rx"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, Rx_1, NccFitTextDirective;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (Rx_1_1) {
                Rx_1 = Rx_1_1;
            }
        ],
        execute: function () {
            NccFitTextDirective = (function () {
                function NccFitTextDirective(_elementRef) {
                    var _this = this;
                    this._elementRef = _elementRef;
                    this.options = {
                        minFontSize: 8,
                        maxFontSize: 255,
                        adjustmentRatio: 1
                    };
                    Rx_1.Observable.fromEvent(window, 'resize')
                        .debounceTime(150)
                        .subscribe(function (event) {
                        _this.onResize(event);
                    });
                    Rx_1.Observable.fromEvent(window, 'orientationchange')
                        .debounceTime(150)
                        .subscribe(function (event) {
                        console.log('Event: orientationchange');
                        _this.onResize(event);
                    });
                }
                NccFitTextDirective.prototype.ngOnInit = function () {
                    if (typeof this.nccFitText !== 'undefined') {
                        this.options.minFontSize = this.nccFitText.minFontSize || this.options.minFontSize;
                        this.options.maxFontSize = this.nccFitText.maxFontSize || this.options.maxFontSize;
                        this.options.adjustmentRatio = this.nccFitText.adjustmentRatio || this.options.adjustmentRatio;
                    }
                };
                NccFitTextDirective.prototype.ngAfterViewInit = function () {
                    this.resizeFont();
                };
                NccFitTextDirective.prototype.resizeFont = function () {
                    var textElement = this._elementRef.nativeElement, currentFontSize = parseFloat(getComputedStyle(textElement).fontSize), parentElementWidth = textElement.parentElement.clientWidth, elementWidth = textElement.clientWidth, multiplier = parentElementWidth / elementWidth;
                    var newFontSize = Math.max(Math.min((currentFontSize * multiplier * this.options.adjustmentRatio), this.options.maxFontSize), this.options.minFontSize);
                    textElement.style.fontSize = newFontSize + 'px';
                };
                NccFitTextDirective.prototype.onResize = function (event) {
                    this.resizeFont();
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Object)
                ], NccFitTextDirective.prototype, "nccFitText", void 0);
                NccFitTextDirective = __decorate([
                    core_1.Directive({
                        selector: '[nccFitText]'
                    }),
                    __metadata("design:paramtypes", [typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object])
                ], NccFitTextDirective);
                return NccFitTextDirective;
                var _a;
            }());
            exports_1("NccFitTextDirective", NccFitTextDirective);
        }
    };
});
//# sourceMappingURL=ncc-fit-text.directive.js.map