import {Directive, Input, ElementRef, OnInit, AfterViewInit} from '@angular/core';
import {Observable} from 'rxjs/Rx';

export interface NccFitTextOptions {
    minFontSize: number,
    maxFontSize: number,
    adjustmentRatio: number
}

@Directive({
    selector: '[nccFitText]'
})

/**
 * Run nccFitText before anything that hides the element you're trying to size (e.g. before Carousels, Scrollers, Accordions, Tabs, etc). Hiding an element's container removes its width. It can't resize without a width.
 * Make sure your container has a width!
 *   - display: inline elements don't have a width. Use display: block OR display: inline-block + a specified width (i.e. width: 100%).
 *   - position: absolute elements need a specified width as well.
 * font-size property (default min font size) is important also because will be used in initial calculation
 * Tweak until you like it.
 * Set a No-JS fallback font-size in your CSS.
 *
 * ### Example
 *
 * <h3 nccFitText>Lorem ipsum dolor sit amet</h3>
 * <h3 [nccFitText]="{adjustmentRatio: 0.5}">Lorem ipsum dolor sit amet</h3>
 * <h3 [nccFitText]="{maxFontSize: 120, adjustmentRatio: 0.8}">Lorem ipsum dolor sit amet</h3>
 */

export class NccFitTextDirective implements OnInit, AfterViewInit {
    @Input() nccFitText: NccFitTextOptions;

    private options: NccFitTextOptions = {
        minFontSize: 8,
        maxFontSize: 255,
        adjustmentRatio: 1
    }

    constructor(private _elementRef: ElementRef) {
        Observable.fromEvent(window, 'resize')
            .debounceTime(150)
            .subscribe((event: any) => {
                // console.log('Event: resize');
                this.onResize(event);
            });

        Observable.fromEvent(window, 'orientationchange')
            .debounceTime(150)
            .subscribe((event: any) => {
                console.log('Event: orientationchange');
                this.onResize(event);
            });
    }

    ngOnInit() {
        if (typeof this.nccFitText !== 'undefined') {
            this.options.minFontSize = this.nccFitText.minFontSize || this.options.minFontSize;
            this.options.maxFontSize = this.nccFitText.maxFontSize || this.options.maxFontSize;
            this.options.adjustmentRatio = this.nccFitText.adjustmentRatio || this.options.adjustmentRatio;
        }
    }

    public ngAfterViewInit(): void {
        // Update font-size
        this.resizeFont();
    }

    public resizeFont(): void {
        // Text element and important variables
        var textElement = this._elementRef.nativeElement,
            currentFontSize = parseFloat(getComputedStyle(textElement).fontSize),
            parentElementWidth = textElement.parentElement.clientWidth,
            elementWidth = textElement.clientWidth,
            multiplier = parentElementWidth / elementWidth;

        // Calculate new font-size
        var newFontSize = Math.max(
            Math.min(
                // (currentFontSize * (multiplier - 0.1) * this.options.adjustmentRatio),
                (currentFontSize * multiplier * this.options.adjustmentRatio),
                this.options.maxFontSize),
            this.options.minFontSize
        );

        // Apply new font-size
        textElement.style.fontSize = newFontSize + 'px';
    }

    public onResize(event: any): void {
        // Update font-size
        this.resizeFont();
    }
}
