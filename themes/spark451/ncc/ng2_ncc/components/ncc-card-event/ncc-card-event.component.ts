import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-event',
    templateUrl: 'ncc-card-event.component.html',
    styleUrls: ['ncc-card-event.component.css']
})

export class NccCardEventComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() heading: string;
    @Input() title: string;
    @Input() description: string;
    @Input() timeStart: string;
    @Input() timeEnd: string;
    @Input() location: string;
    // @Input() linkLabel: string;
    @Input() linkUrl: string;
    @Input() shortDate: string;
    @Input() day: string;

    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;
    @ViewChild('elbutton') elbutton: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto',
            })

        if (this.elpreview && this.elcontent && this.elbutton) {
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
            contentAnimation.start(this.elbutton.nativeElement);
        }*/

        if (this.elpreview && this.elcontent && this.elbutton) {
            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';

            this.elbutton.nativeElement.style.opacity = '1';
            this.elbutton.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none',
            })

        if (this.elpreview && this.elcontent && this.elbutton) {
            contentAnimation.start(this.elcontent.nativeElement);
            contentAnimation.start(this.elbutton.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
        }*/

        if (this.elpreview && this.elcontent && this.elbutton) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elbutton.nativeElement.style.opacity = '0';
            this.elbutton.nativeElement.style.pointerEvents = 'none';

            this.elpreview.nativeElement.style.opacity = '1';
        }
    }

}
