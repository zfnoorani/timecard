import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-feature',
    templateUrl: 'ncc-card-feature.component.html',
    styleUrls: ['ncc-card-feature.component.css']
})

export class NccCardFeatureComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    @Input() image: string;
    @Input() linkUrl: string;
    // @Input() linkLabel: string;

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elimage && this.elcontent) {
            contentAnimation.start(this.elcontent.nativeElement)
            imageAnimation.start(this.elimage.nativeElement);
        }*/

        if (this.elimage && this.elcontent) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elimage && this.elcontent) {
            imageAnimation.start(this.elimage.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elimage && this.elcontent) {
            this.elimage.nativeElement.style.transform = 'scale(1.25)';

            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';
        }
    }

}
