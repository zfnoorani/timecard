import {Component, HostBinding, Input} from '@angular/core';

import {FilterCategory} from '../ncc-program-finder/ncc-program-finder.interface';
import {NccProgramFinderService} from '../ncc-program-finder/ncc-program-finder.service';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-program-tray',
    templateUrl: 'ncc-program-tray.component.html',
    styleUrls: ['ncc-program-tray.component.css'],
    inputs: ['filterCategories']
})

export class NccProgramTrayComponent {
    private filterCategories: FilterCategory[];
    private _isTrayOpen: boolean;

    @HostBinding('class.tray-open')
    @Input()
    public get isTrayOpen(): boolean {
        return this._isTrayOpen;
    }

    public set isTrayOpen(value: boolean) {
        this._isTrayOpen = value;
    }

    public filtersMatches: number;

    constructor(private _programFinderService: NccProgramFinderService) {
        this._programFinderService.filtersMatches.subscribe((value: number) => {
            this.filtersMatches = value;
        });
    }

    public toggleFilterByTid(filterTid: string): void {
        this._programFinderService.toggleFilterByTid(filterTid);
    }

    public clearAllFilters(event: MouseEvent): void {
        event.preventDefault();
        this._programFinderService.clearAllFilters();
    }

    public trayClose(): void {
        this._programFinderService.trayClose();
    }

}
