System.register(["@angular/core", "../ncc-program-finder/ncc-program-finder.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_program_finder_service_1, NccProgramTrayComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_program_finder_service_1_1) {
                ncc_program_finder_service_1 = ncc_program_finder_service_1_1;
            }
        ],
        execute: function () {
            NccProgramTrayComponent = (function () {
                function NccProgramTrayComponent(_programFinderService) {
                    var _this = this;
                    this._programFinderService = _programFinderService;
                    this._programFinderService.filtersMatches.subscribe(function (value) {
                        _this.filtersMatches = value;
                    });
                }
                Object.defineProperty(NccProgramTrayComponent.prototype, "isTrayOpen", {
                    get: function () {
                        return this._isTrayOpen;
                    },
                    set: function (value) {
                        this._isTrayOpen = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                NccProgramTrayComponent.prototype.toggleFilterByTid = function (filterTid) {
                    this._programFinderService.toggleFilterByTid(filterTid);
                };
                NccProgramTrayComponent.prototype.clearAllFilters = function (event) {
                    event.preventDefault();
                    this._programFinderService.clearAllFilters();
                };
                NccProgramTrayComponent.prototype.trayClose = function () {
                    this._programFinderService.trayClose();
                };
                __decorate([
                    core_1.HostBinding('class.tray-open'),
                    core_1.Input(),
                    __metadata("design:type", Boolean),
                    __metadata("design:paramtypes", [Boolean])
                ], NccProgramTrayComponent.prototype, "isTrayOpen", null);
                NccProgramTrayComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-program-tray',
                        templateUrl: 'ncc-program-tray.component.html',
                        styleUrls: ['ncc-program-tray.component.css'],
                        inputs: ['filterCategories']
                    }),
                    __metadata("design:paramtypes", [ncc_program_finder_service_1.NccProgramFinderService])
                ], NccProgramTrayComponent);
                return NccProgramTrayComponent;
            }());
            exports_1("NccProgramTrayComponent", NccProgramTrayComponent);
        }
    };
});
//# sourceMappingURL=ncc-program-tray.component.js.map