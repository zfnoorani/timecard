import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-map',
    templateUrl: 'ncc-card-map.component.html',
    styleUrls: ['ncc-card-map.component.css']
})

export class NccCardMapComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    @Input() googleMap: string;

    @ViewChild('elmap') elmap: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let mapAnimation = this._ab.css();
        mapAnimation
            .setDuration(500)
            .setFromStyles({
                'transform': 'scale(1.25)',
                'pointer-events': 'none'
            })
            .setToStyles({
                'transform': 'scale(1)',
                'pointer-events': 'auto'
            })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(400)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.elmap && this.elcontent) {
            mapAnimation.start(this.elmap.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elmap && this.elcontent) {
            this.elmap.nativeElement.style.transform = 'scale(1)';
            this.elmap.nativeElement.style.pointerEvents = 'auto';

            this.elcontent.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let mapAnimation = this._ab.css();
        mapAnimation
            .setDuration(500)
            .setFromStyles({
                'transform': 'scale(1)',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'transform': 'scale(1.25)',
                'pointer-events': 'none'
            })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(400)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        if (this.elmap && this.elcontent) {
            mapAnimation.start(this.elmap.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elmap && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';

            this.elmap.nativeElement.style.transform = 'scale(1.25)';
            this.elmap.nativeElement.style.pointerEvents = 'none';
        }
    }

}
