System.register(["@angular/core", "../ncc-card/ncc-card.base"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_card_base_1, NccCardMapComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_card_base_1_1) {
                ncc_card_base_1 = ncc_card_base_1_1;
            }
        ],
        execute: function () {
            NccCardMapComponent = (function (_super) {
                __extends(NccCardMapComponent, _super);
                function NccCardMapComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardMapComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elmap && this.elcontent) {
                        this.elmap.nativeElement.style.transform = 'scale(1)';
                        this.elmap.nativeElement.style.pointerEvents = 'auto';
                        this.elcontent.nativeElement.style.opacity = '1';
                    }
                };
                NccCardMapComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elmap && this.elcontent) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elmap.nativeElement.style.transform = 'scale(1.25)';
                        this.elmap.nativeElement.style.pointerEvents = 'none';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardMapComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardMapComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardMapComponent.prototype, "description", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardMapComponent.prototype, "googleMap", void 0);
                __decorate([
                    core_1.ViewChild('elmap'),
                    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
                ], NccCardMapComponent.prototype, "elmap", void 0);
                __decorate([
                    core_1.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
                ], NccCardMapComponent.prototype, "elcontent", void 0);
                NccCardMapComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-map',
                        templateUrl: 'ncc-card-map.component.html',
                        styleUrls: ['ncc-card-map.component.css']
                    }),
                    __metadata("design:paramtypes", [typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
                ], NccCardMapComponent);
                return NccCardMapComponent;
                var _a, _b, _c;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardMapComponent", NccCardMapComponent);
        }
    };
});
//# sourceMappingURL=ncc-card-map.component.js.map