import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

declare var nccModal: any;

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-video',
    templateUrl: 'ncc-card-video.component.html',
    styleUrls: ['ncc-card-video.component.css']
})

export class NccCardVideoComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() image: string;
    @Input() modalContentId: string;  // will be used for Modal content - trigger modal

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elicon') elicon: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(400)
            .setFromStyles({
                'bottom': '50%',
                'right': '50%',
                'transform': 'translate(50%, 50%)',
                'pointer-events': 'none'
            })
            .setToStyles({
                'bottom': '25px',
                'right': '17px',
                'transform': 'none',
                'pointer-events': 'auto'
            })

        if (this.elimage && this.elicon) {
            imageAnimation.start(this.elimage.nativeElement);
            iconAnimation.start(this.elicon.nativeElement);
        }*/

        if (this.elimage && this.elicon) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elicon.nativeElement.style.bottom = '25px';
            this.elicon.nativeElement.style.right = '17px';
            this.elicon.nativeElement.style.transform = 'none';
            this.elicon.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(400)
            .setFromStyles({
                'bottom': '25px',
                'right': '17px',
                'transform': 'none',
                'pointer-events': 'auto'
            })
            .setToStyles({

                'bottom': '50%',
                'right': '50%',
                'transform': 'translate(50%, 50%)',
                'pointer-events': 'none'
            })

        if (this.elimage && this.elicon) {
            imageAnimation.start(this.elimage.nativeElement);
            iconAnimation.start(this.elicon.nativeElement);
        }*/

        if (this.elimage && this.elicon) {
            this.elimage.nativeElement.style.transform = 'scale(1.25)';

            this.elicon.nativeElement.style.bottom = '50%';
            this.elicon.nativeElement.style.right = '50%';
            this.elicon.nativeElement.style.transform = 'translate(50%, 50%)';
            this.elicon.nativeElement.style.pointerEvents = 'none';
        }
    }

    public openModal(event: MouseEvent) {
        if (this.modalContentId) {
            event.preventDefault();
            nccModal.displayModal(event, this.modalContentId);
        }
    }

}
