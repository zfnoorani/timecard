System.register(["@angular/core", "../ncc-card/ncc-card.base"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_card_base_1, NccCardVideoComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_card_base_1_1) {
                ncc_card_base_1 = ncc_card_base_1_1;
            }
        ],
        execute: function () {
            NccCardVideoComponent = (function (_super) {
                __extends(NccCardVideoComponent, _super);
                function NccCardVideoComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardVideoComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage && this.elicon) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                        this.elicon.nativeElement.style.bottom = '25px';
                        this.elicon.nativeElement.style.right = '17px';
                        this.elicon.nativeElement.style.transform = 'none';
                        this.elicon.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardVideoComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage && this.elicon) {
                        this.elimage.nativeElement.style.transform = 'scale(1.25)';
                        this.elicon.nativeElement.style.bottom = '50%';
                        this.elicon.nativeElement.style.right = '50%';
                        this.elicon.nativeElement.style.transform = 'translate(50%, 50%)';
                        this.elicon.nativeElement.style.pointerEvents = 'none';
                    }
                };
                NccCardVideoComponent.prototype.openModal = function (event) {
                    if (this.modalContentId) {
                        event.preventDefault();
                        nccModal.displayModal(event, this.modalContentId);
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardVideoComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardVideoComponent.prototype, "image", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardVideoComponent.prototype, "modalContentId", void 0);
                __decorate([
                    core_1.ViewChild('elimage'),
                    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
                ], NccCardVideoComponent.prototype, "elimage", void 0);
                __decorate([
                    core_1.ViewChild('elicon'),
                    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
                ], NccCardVideoComponent.prototype, "elicon", void 0);
                NccCardVideoComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-video',
                        templateUrl: 'ncc-card-video.component.html',
                        styleUrls: ['ncc-card-video.component.css']
                    }),
                    __metadata("design:paramtypes", [typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
                ], NccCardVideoComponent);
                return NccCardVideoComponent;
                var _a, _b, _c;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardVideoComponent", NccCardVideoComponent);
        }
    };
});
//# sourceMappingURL=ncc-card-video.component.js.map