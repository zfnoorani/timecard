import {Component, OnInit, trigger, state, style, transition, animate} from '@angular/core';

import {FilterCategory, Program} from '../ncc-program-finder/ncc-program-finder.interface';
import {NccProgramFinderService} from '../ncc-program-finder/ncc-program-finder.service';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-program-finder',
    templateUrl: 'ncc-program-finder.component.html',
    styleUrls: ['ncc-program-finder.component.css'],
    host: {
        '(window:resize)': 'onResize($event)'
    },
    // TODO: improve toggle animations
    animations: [
        trigger('programStackState', [
            state('in', style({ transform: 'translateY(0)' })),
            transition('void => *', [
                style({ transform: 'translateY(-50%)' }),
                animate(250)
            ]),
            transition('* => void', [
                animate(350, style({ transform: 'translateY(-50%)' }))
            ])
        ])
    ]
    /*animations: [
        trigger('programStackState', [
            state('false', style({ maxHeight: '0' })),
            state('true', style({ maxHeight: '388px' })),
            transition('false => true', animate('450ms ease-in')),
            transition('true => false', animate('450ms ease-out'))
        ])
    ]*/
})

export class NccProgramFinderComponent implements OnInit {
    private programs: Program[];
    private filterCategories: FilterCategory[];
    private isTrayOpen: boolean;
    private isLoading: boolean = true;

    public toggleProgramDebounce: any;

    public constructor(private _programFinderService: NccProgramFinderService) {
        // prevent broken layout on multiple clicks
        this.toggleProgramDebounce = _.debounce(this.toggleProgram, 250);
    }

    ngOnInit(): void {
        let hash = window.location.hash;

        this._programFinderService.programs.subscribe((programs: Program[]) => {
            this.programs = programs;

            if (this.programs) {
                this.isLoading = false;
            }
        })

        this._programFinderService.filterCategories.subscribe((filterCategories: FilterCategory[]) => {
            this.filterCategories = filterCategories;
        })

        this._programFinderService.isTrayOpen.subscribe((value: boolean) => {
            this.isTrayOpen = value;
        });

        this._programFinderService.isDataReady.subscribe((value: boolean) => {
            if (value && hash.length) {
                let filtersGroup = hash.substr(2).split('&');

                filtersGroup = _.map(filtersGroup, (filterTid: string) => { return filterTid.substr(7); });

                console.log('hash filtersGroup: [' + filtersGroup + ']');

                this._programFinderService.filterByTids(filtersGroup);
            }
        })

    }

    public toggleProgram(programNid: string): void {
        this._programFinderService.toggleProgram(programNid);
    }

    public onResize(event: MouseEvent): void {
        this._programFinderService.deactivateAllPrograms();
    }

    public trayOpen(): void {
        this._programFinderService.trayOpen();
    }

}
