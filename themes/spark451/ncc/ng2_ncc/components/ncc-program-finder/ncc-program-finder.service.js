System.register(["@angular/core", "@angular/http", "rxjs/BehaviorSubject", "rxjs/add/operator/toPromise", "../ncc-element-query/ncc-breakpoints"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, BehaviorSubject_1, ncc_breakpoints_1, PROGRAM_FINDER_API_URL, NccProgramFinderService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (BehaviorSubject_1_1) {
                BehaviorSubject_1 = BehaviorSubject_1_1;
            },
            function (_1) {
            },
            function (ncc_breakpoints_1_1) {
                ncc_breakpoints_1 = ncc_breakpoints_1_1;
            }
        ],
        execute: function () {
            exports_1("PROGRAM_FINDER_API_URL", PROGRAM_FINDER_API_URL = '/api/program-finder.json');
            NccProgramFinderService = (function () {
                function NccProgramFinderService(apiUrl, _http) {
                    var _this = this;
                    this.apiUrl = apiUrl;
                    this._http = _http;
                    this.filtersGroupIsEmpty = true;
                    this.filtersMatches = new BehaviorSubject_1.BehaviorSubject(-1);
                    this.programs = new BehaviorSubject_1.BehaviorSubject(null);
                    this.filterCategories = new BehaviorSubject_1.BehaviorSubject(null);
                    this.isTrayOpen = new BehaviorSubject_1.BehaviorSubject(false);
                    this.isDataReady = new BehaviorSubject_1.BehaviorSubject(false);
                    this._http.get(this.apiUrl)
                        .toPromise()
                        .then(function (response) {
                        _this.programsStorage = response.json().programs;
                        _this.programsStorage.forEach(function (program, index) {
                            program.active = false;
                            program.darken = false;
                            program.order = index;
                            program.stackOrder = NccProgramFinderService_1.DEFAULT_STACK_ORDER;
                        });
                        _this.programs.next(_this.programsStorage);
                        _this.filterCategoriesStorage = response.json().categories;
                        _this.deactivateAllFilters();
                        _this.filterCategories.next(_this.filterCategoriesStorage);
                        setTimeout(function () {
                            _this.isDataReady.next(true);
                        });
                    })
                        .catch(this.handleError);
                }
                NccProgramFinderService_1 = NccProgramFinderService;
                NccProgramFinderService.prototype.handleError = function (error) {
                    var errMsg = (error.message) ? error.message :
                        error.status ? error.status + " - " + error.statusText : 'Server error';
                    console.error(errMsg);
                    return Promise.reject(errMsg);
                };
                NccProgramFinderService.prototype.getProgramStackOrder = function (programIndex) {
                    var programFinderColumns = 0;
                    var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                    if (windowWidth < ncc_breakpoints_1.LAYOUT_BREAKPOINTS.sm) {
                        programFinderColumns = 1;
                    }
                    else if (windowWidth < ncc_breakpoints_1.LAYOUT_BREAKPOINTS.md) {
                        programFinderColumns = 2;
                    }
                    else if (windowWidth < ncc_breakpoints_1.LAYOUT_BREAKPOINTS.lg) {
                        programFinderColumns = 3;
                    }
                    else {
                        programFinderColumns = 4;
                    }
                    var matrixRow = Math.ceil((programIndex + 1) / programFinderColumns);
                    return (programFinderColumns * matrixRow);
                };
                NccProgramFinderService.prototype.getFilteredPrograms = function () {
                    var _this = this;
                    if (this.filtersGroup.length) {
                        if (this.filtersGroupIsEmpty) {
                            this.filtersGroupIsEmpty = false;
                            this.programsStorageUnfiltered = this.programsStorage;
                        }
                        return _.filter(this.programsStorageUnfiltered, function (program) {
                            var filters;
                            if (program.filters) {
                                filters = _this.filtersGroup.length === _.intersection(_.map(program.filters, 'tid'), _this.filtersGroup).length;
                            }
                            return filters;
                        });
                    }
                    else {
                        this.filtersGroupIsEmpty = true;
                        this.programsStorage = this.programsStorageUnfiltered;
                        return this.programsStorage;
                    }
                };
                NccProgramFinderService.prototype.getUpdatedProgramsOrder = function (programs) {
                    programs.forEach(function (program, index) {
                        program.order = index;
                    });
                    return programs;
                };
                NccProgramFinderService.prototype.filterPrograms = function () {
                    this.programsStorage = this.getUpdatedProgramsOrder(this.getFilteredPrograms());
                    if (this.filtersGroup.length) {
                        this.filtersMatches.next(this.programsStorage.length);
                    }
                    else {
                        this.filtersMatches.next(-1);
                    }
                };
                NccProgramFinderService.prototype.deactivateAllFilters = function () {
                    this.filterCategoriesStorage.forEach(function (filterCategory) {
                        filterCategory.filters.forEach(function (filter) {
                            filter.choices.forEach(function (filterChoice) {
                                filterChoice.active = false;
                            });
                        });
                    });
                };
                NccProgramFinderService.prototype.clearFilterHash = function () {
                    window.location.hash = '';
                };
                NccProgramFinderService.prototype.toggleFilterHash = function (filterTid) {
                    var hash = window.location.hash;
                    if (hash.indexOf(filterTid) > -1) {
                        hash = hash.replace('&tids[]=' + filterTid, '');
                    }
                    else {
                        hash += '&tids[]=' + filterTid;
                    }
                    window.location.hash = hash;
                };
                NccProgramFinderService.prototype.toggleFiltersGroup = function (filterTid) {
                    this.toggleFilterChoice(filterTid);
                    this.toggleFilterHash(filterTid);
                    if (_.indexOf(this.filtersGroup, filterTid) === -1) {
                        this.filtersGroup = _.union(this.filtersGroup, [filterTid]);
                    }
                    else {
                        this.filtersGroup = _.without(this.filtersGroup, filterTid);
                    }
                };
                NccProgramFinderService.prototype.toggleFilterChoice = function (filterTid) {
                    this.filterCategoriesStorage.forEach(function (filterCategory) {
                        filterCategory.filters.forEach(function (filter) {
                            var currentFilterChoice = _.find(filter.choices, { tid: filterTid });
                            if (currentFilterChoice) {
                                currentFilterChoice.active = !currentFilterChoice.active;
                            }
                        });
                    });
                    this.filterCategories.next(this.filterCategoriesStorage);
                };
                NccProgramFinderService.prototype.darkenProgramsExcept = function (skipProgram) {
                    this.programsStorage.forEach(function (program, index) {
                        if (program === skipProgram) {
                            program.darken = false;
                        }
                        else {
                            program.darken = true;
                        }
                    });
                };
                NccProgramFinderService.prototype.undarkenPrograms = function () {
                    this.programsStorage.forEach(function (program, index) {
                        program.darken = false;
                    });
                };
                NccProgramFinderService.prototype.deactivateAllPrograms = function () {
                    var _this = this;
                    if (this.programsStorage) {
                        this.programsStorage.forEach(function (program) {
                            program.active = false;
                            program.darken = false;
                        });
                        this.programs.next(this.programsStorage);
                        setTimeout(function () {
                            _this.programsStorage.forEach(function (program) {
                                program.stackOrder = NccProgramFinderService_1.DEFAULT_STACK_ORDER;
                            });
                            _this.programs.next(_this.programsStorage);
                        }, NccProgramFinderService_1.COLLAPSE_TRANSITION_DURATION);
                    }
                };
                NccProgramFinderService.prototype.deactivateAllProgramsExcept = function (skipProgram) {
                    var _this = this;
                    this.programsStorage.forEach(function (program) {
                        if (program !== skipProgram) {
                            program.active = false;
                            program.darken = false;
                        }
                    });
                    this.programs.next(this.programsStorage);
                    setTimeout(function () {
                        _this.programsStorage.forEach(function (program) {
                            if (program !== skipProgram) {
                                program.stackOrder = NccProgramFinderService_1.DEFAULT_STACK_ORDER;
                            }
                        });
                        _this.programs.next(_this.programsStorage);
                    }, NccProgramFinderService_1.COLLAPSE_TRANSITION_DURATION);
                };
                NccProgramFinderService.prototype.toggleProgram = function (programNid) {
                    var _this = this;
                    var index = _.indexOf(this.programsStorage, _.find(this.programsStorage, { nid: programNid }));
                    if (index > -1 && this.programsStorage[index]) {
                        this.deactivateAllProgramsExcept(this.programsStorage[index]);
                        this.programsStorage[index].active = !this.programsStorage[index].active;
                        if (this.programsStorage[index].active) {
                            this.darkenProgramsExcept(this.programsStorage[index]);
                            this.programsStorage[index].stackOrder = this.getProgramStackOrder(index);
                            this.trayClose();
                        }
                        else {
                            this.undarkenPrograms();
                        }
                        this.programs.next(this.programsStorage);
                        if (!this.programsStorage[index].active) {
                            setTimeout(function () {
                                _this.programsStorage[index].stackOrder = NccProgramFinderService_1.DEFAULT_STACK_ORDER;
                                _this.programs.next(_this.programsStorage);
                            }, NccProgramFinderService_1.COLLAPSE_TRANSITION_DURATION);
                        }
                    }
                    else {
                        console.error("Program with nid: " + programNid + " doesn't exist.");
                    }
                };
                NccProgramFinderService.prototype.toggleFilterByTid = function (filterTid) {
                    this.toggleFiltersGroup(filterTid);
                    console.log('filtersGroup: [' + this.filtersGroup + ']');
                    this.filterPrograms();
                    this.programs.next(this.programsStorage);
                };
                NccProgramFinderService.prototype.filterByTids = function (filtersGroup) {
                    var _this = this;
                    this.filtersGroup = filtersGroup;
                    this.filtersGroup.forEach(function (filterTid) {
                        _this.toggleFilterChoice(filterTid);
                    });
                    this.filterPrograms();
                    this.programs.next(this.programsStorage);
                };
                NccProgramFinderService.prototype.clearAllFilters = function () {
                    this.filtersGroup = [];
                    this.clearFilterHash();
                    this.deactivateAllFilters();
                    this.filterPrograms();
                    this.programs.next(this.programsStorage);
                };
                NccProgramFinderService.prototype.trayOpen = function () {
                    this.deactivateAllPrograms();
                    this.isTrayOpen.next(true);
                };
                NccProgramFinderService.prototype.trayClose = function () {
                    this.isTrayOpen.next(false);
                };
                NccProgramFinderService.COLLAPSE_TRANSITION_DURATION = 450;
                NccProgramFinderService.DEFAULT_STACK_ORDER = 1000;
                NccProgramFinderService = NccProgramFinderService_1 = __decorate([
                    core_1.Injectable(),
                    __param(0, core_1.Inject(PROGRAM_FINDER_API_URL)),
                    __metadata("design:paramtypes", [String, typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
                ], NccProgramFinderService);
                return NccProgramFinderService;
                var NccProgramFinderService_1, _a;
            }());
            exports_1("NccProgramFinderService", NccProgramFinderService);
        }
    };
});
//# sourceMappingURL=ncc-program-finder.service.js.map