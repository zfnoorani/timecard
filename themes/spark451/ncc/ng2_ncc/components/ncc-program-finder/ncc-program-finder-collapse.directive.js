System.register(["@angular/core"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, NccProgramFinderCollapseDirective;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            }
        ],
        execute: function () {
            NccProgramFinderCollapseDirective = (function () {
                function NccProgramFinderCollapseDirective(_el, _renderer) {
                    this.collapsed = new core_1.EventEmitter(false);
                    this.expanded = new core_1.EventEmitter(false);
                    this.isExpanded = true;
                    this.isCollapsed = false;
                    this.isCollapse = true;
                    this.isCollapsing = false;
                    this.transitionDuration = 450;
                    this._el = _el;
                    this._renderer = _renderer;
                }
                Object.defineProperty(NccProgramFinderCollapseDirective.prototype, "nccProgramFinderCollapse", {
                    get: function () {
                        return this.isExpanded;
                    },
                    set: function (value) {
                        this.isExpanded = value;
                        this.toggle();
                    },
                    enumerable: true,
                    configurable: true
                });
                NccProgramFinderCollapseDirective.prototype.ngOnInit = function () {
                    this._el.nativeElement.style.transition = 'max-height ' + this.transitionDuration + 'ms ease, visibility ' + this.transitionDuration + 'ms ease';
                };
                NccProgramFinderCollapseDirective.prototype.toggle = function () {
                    if (this.isExpanded) {
                        this.hide();
                    }
                    else {
                        this.show();
                    }
                };
                NccProgramFinderCollapseDirective.prototype.hide = function () {
                    var _this = this;
                    this.isCollapse = false;
                    this.isCollapsing = true;
                    this.isExpanded = false;
                    this.isCollapsed = true;
                    this._el.nativeElement.style.maxHeight = '0px';
                    this._el.nativeElement.style.overflow = 'hidden';
                    setTimeout(function () {
                        _this.isCollapse = true;
                        _this.isCollapsing = false;
                        _this.collapsed.emit(event);
                    }, this.transitionDuration);
                };
                NccProgramFinderCollapseDirective.prototype.show = function () {
                    var _this = this;
                    this.isCollapse = false;
                    this.isCollapsing = true;
                    this.isExpanded = true;
                    this.isCollapsed = false;
                    this._el.nativeElement.style.maxHeight = '388px';
                    setTimeout(function () {
                        _this.isCollapse = true;
                        _this.isCollapsing = false;
                        _this._el.nativeElement.style.overflow = 'visible';
                        _this.expanded.emit(event);
                    }, this.transitionDuration);
                };
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", typeof (_a = typeof core_1.EventEmitter !== "undefined" && core_1.EventEmitter) === "function" && _a || Object)
                ], NccProgramFinderCollapseDirective.prototype, "collapsed", void 0);
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", typeof (_b = typeof core_1.EventEmitter !== "undefined" && core_1.EventEmitter) === "function" && _b || Object)
                ], NccProgramFinderCollapseDirective.prototype, "expanded", void 0);
                __decorate([
                    core_1.HostBinding('style.display'),
                    __metadata("design:type", String)
                ], NccProgramFinderCollapseDirective.prototype, "display", void 0);
                __decorate([
                    core_1.HostBinding('class.in'),
                    core_1.HostBinding('attr.aria-expanded'),
                    __metadata("design:type", Boolean)
                ], NccProgramFinderCollapseDirective.prototype, "isExpanded", void 0);
                __decorate([
                    core_1.HostBinding('attr.aria-hidden'),
                    __metadata("design:type", Boolean)
                ], NccProgramFinderCollapseDirective.prototype, "isCollapsed", void 0);
                __decorate([
                    core_1.HostBinding('class.collapse'),
                    __metadata("design:type", Boolean)
                ], NccProgramFinderCollapseDirective.prototype, "isCollapse", void 0);
                __decorate([
                    core_1.HostBinding('class.collapsing'),
                    __metadata("design:type", Boolean)
                ], NccProgramFinderCollapseDirective.prototype, "isCollapsing", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Number)
                ], NccProgramFinderCollapseDirective.prototype, "transitionDuration", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Boolean),
                    __metadata("design:paramtypes", [Boolean])
                ], NccProgramFinderCollapseDirective.prototype, "nccProgramFinderCollapse", null);
                NccProgramFinderCollapseDirective = __decorate([
                    core_1.Directive({
                        selector: '[nccProgramFinderCollapse]'
                    }),
                    __metadata("design:paramtypes", [typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object, typeof (_d = typeof core_1.Renderer !== "undefined" && core_1.Renderer) === "function" && _d || Object])
                ], NccProgramFinderCollapseDirective);
                return NccProgramFinderCollapseDirective;
                var _a, _b, _c, _d;
            }());
            exports_1("NccProgramFinderCollapseDirective", NccProgramFinderCollapseDirective);
        }
    };
});
//# sourceMappingURL=ncc-program-finder-collapse.directive.js.map