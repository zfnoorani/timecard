export interface FilterCategory {
    name: string;
    filters: Filter[];
}

export interface Filter {
    name: string;
    abbreviation: string;
    description: string;
    choices: FilterChoice[];
}

export interface FilterChoice {
    tid: string;
    filter: string;
    title: string;
    color: string;
    active?: boolean;
}

export interface Program {
    nid: string;
    title: string;
    url: string;
    level: string;
    color: string;
    summary: string;
    stack: string;
    stackOrder?: number;
    filters: FilterChoice[];
    active?: boolean;
    darken?: boolean;
    order?: number;
}
