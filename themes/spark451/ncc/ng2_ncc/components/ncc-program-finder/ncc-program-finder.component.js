System.register(["@angular/core", "../ncc-program-finder/ncc-program-finder.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_program_finder_service_1, NccProgramFinderComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_program_finder_service_1_1) {
                ncc_program_finder_service_1 = ncc_program_finder_service_1_1;
            }
        ],
        execute: function () {
            NccProgramFinderComponent = (function () {
                function NccProgramFinderComponent(_programFinderService) {
                    this._programFinderService = _programFinderService;
                    this.isLoading = true;
                    this.toggleProgramDebounce = _.debounce(this.toggleProgram, 250);
                }
                NccProgramFinderComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    var hash = window.location.hash;
                    this._programFinderService.programs.subscribe(function (programs) {
                        _this.programs = programs;
                        if (_this.programs) {
                            _this.isLoading = false;
                        }
                    });
                    this._programFinderService.filterCategories.subscribe(function (filterCategories) {
                        _this.filterCategories = filterCategories;
                    });
                    this._programFinderService.isTrayOpen.subscribe(function (value) {
                        _this.isTrayOpen = value;
                    });
                    this._programFinderService.isDataReady.subscribe(function (value) {
                        if (value && hash.length) {
                            var filtersGroup = hash.substr(2).split('&');
                            filtersGroup = _.map(filtersGroup, function (filterTid) { return filterTid.substr(7); });
                            console.log('hash filtersGroup: [' + filtersGroup + ']');
                            _this._programFinderService.filterByTids(filtersGroup);
                        }
                    });
                };
                NccProgramFinderComponent.prototype.toggleProgram = function (programNid) {
                    this._programFinderService.toggleProgram(programNid);
                };
                NccProgramFinderComponent.prototype.onResize = function (event) {
                    this._programFinderService.deactivateAllPrograms();
                };
                NccProgramFinderComponent.prototype.trayOpen = function () {
                    this._programFinderService.trayOpen();
                };
                NccProgramFinderComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-program-finder',
                        templateUrl: 'ncc-program-finder.component.html',
                        styleUrls: ['ncc-program-finder.component.css'],
                        host: {
                            '(window:resize)': 'onResize($event)'
                        },
                        animations: [
                            core_1.trigger('programStackState', [
                                core_1.state('in', core_1.style({ transform: 'translateY(0)' })),
                                core_1.transition('void => *', [
                                    core_1.style({ transform: 'translateY(-50%)' }),
                                    core_1.animate(250)
                                ]),
                                core_1.transition('* => void', [
                                    core_1.animate(350, core_1.style({ transform: 'translateY(-50%)' }))
                                ])
                            ])
                        ]
                    }),
                    __metadata("design:paramtypes", [ncc_program_finder_service_1.NccProgramFinderService])
                ], NccProgramFinderComponent);
                return NccProgramFinderComponent;
            }());
            exports_1("NccProgramFinderComponent", NccProgramFinderComponent);
        }
    };
});
//# sourceMappingURL=ncc-program-finder.component.js.map