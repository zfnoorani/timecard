import {Injectable, Inject} from '@angular/core';
import {Http, Response} from '@angular/http';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
// import 'rxjs/Rx'; // import all operators (dev only)
import 'rxjs/add/operator/toPromise';

declare var _: any;
// TODO: review this approach of including Lodash on Drupal side
// declare var _: any;

import {FilterCategory, Filter, FilterChoice, Program} from './ncc-program-finder.interface';
import {LAYOUT_BREAKPOINTS, ELEMENT_QUERY_BREAKPOINTS} from '../ncc-element-query/ncc-breakpoints';

export var PROGRAM_FINDER_API_URL: string = '/api/program-finder.json';

@Injectable()
export class NccProgramFinderService {
    // [nccCollapse] directive transition duration
    static COLLAPSE_TRANSITION_DURATION = 450;
    // static DEFAULT_STACK_ORDER = -1;
    // prevent broken layout when user click multiple times quickly on same program
    static DEFAULT_STACK_ORDER = 1000;

    // Service Programs/Filter Categories local storage
    private programsStorage: Program[];
    private programsStorageUnfiltered: Program[]; // programsStorage state before we apply filters
    private filterCategoriesStorage: FilterCategory[];

    // Filters Group (currently active filters)
    private filtersGroup: string[];
    private filtersGroupIsEmpty: boolean = true;
    public filtersMatches: BehaviorSubject<number> = new BehaviorSubject<number>(-1);

    // Observable Programs/Filter Categories
    public programs: BehaviorSubject<Program[]> = new BehaviorSubject<Program[]>(null);
    public filterCategories: BehaviorSubject<FilterCategory[]> = new BehaviorSubject<FilterCategory[]>(null);

    // Tray State
    public isTrayOpen: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    // Data State
    public isDataReady: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor( @Inject(PROGRAM_FINDER_API_URL) private apiUrl: string, private _http: Http) {
        this._http.get(this.apiUrl)
            .toPromise()
            .then((response: Response) => {
                // Programs
                this.programsStorage = response.json().programs;

                // Extend each Program
                this.programsStorage.forEach((program: Program, index: number) => {
                    program.active = false;
                    program.darken = false;
                    program.order = index;
                    program.stackOrder = NccProgramFinderService.DEFAULT_STACK_ORDER;
                });

                this.programs.next(this.programsStorage);

                // Filter Categories
                this.filterCategoriesStorage = response.json().categories;

                this.deactivateAllFilters();

                this.filterCategories.next(this.filterCategoriesStorage);

                // Update Data State
                setTimeout(() => {
                    this.isDataReady.next(true);
                });
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<void> {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';

        console.error(errMsg);

        return Promise.reject(errMsg);
    }

    private getProgramStackOrder(programIndex: number): number {
        let programFinderColumns = 0;

        let windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

        if (windowWidth < LAYOUT_BREAKPOINTS.sm) {
            programFinderColumns = 1;
        } else if (windowWidth < LAYOUT_BREAKPOINTS.md) {
            programFinderColumns = 2;
        } else if (windowWidth < LAYOUT_BREAKPOINTS.lg) {
            programFinderColumns = 3;
        } else {
            programFinderColumns = 4;
        }

        let matrixRow = Math.ceil((programIndex + 1) / programFinderColumns);

        return (programFinderColumns * matrixRow);
    }

    private getFilteredPrograms(): Program[] {
        if (this.filtersGroup.length) {
            if (this.filtersGroupIsEmpty) {
                this.filtersGroupIsEmpty = false;
                this.programsStorageUnfiltered = this.programsStorage
            }

            return _.filter(this.programsStorageUnfiltered, (program: Program) => {
                let filters: boolean;

                if (program.filters) {
                    // filters = !_.isEmpty(_.intersection(_.map(program.filters, 'tid'), this.filtersGroup));
                    filters = this.filtersGroup.length === _.intersection(_.map(program.filters, 'tid'), this.filtersGroup).length;
                }

                return filters;
            });
        } else {
            this.filtersGroupIsEmpty = true;
            this.programsStorage = this.programsStorageUnfiltered;

            return this.programsStorage;
        }
    }

    private getUpdatedProgramsOrder(programs: Program[]): Program[] {
        programs.forEach((program: Program, index: number) => {
            program.order = index;
        });

        return programs;
    }

    private filterPrograms(): void {
        this.programsStorage = this.getUpdatedProgramsOrder(this.getFilteredPrograms());
        // console.log('filteredPrograms:');
        // console.log(this.programsStorage);

        if (this.filtersGroup.length) {
            this.filtersMatches.next(this.programsStorage.length);
        } else {
            this.filtersMatches.next(-1);
        }
    }

    private deactivateAllFilters(): void {
        this.filterCategoriesStorage.forEach((filterCategory: FilterCategory) => {
            filterCategory.filters.forEach((filter: Filter) => {
                filter.choices.forEach((filterChoice: FilterChoice) => {
                    filterChoice.active = false;
                });
            });
        });
    }

    private clearFilterHash(): void {
        window.location.hash = '';
    }

    private toggleFilterHash(filterTid: string): void {
        let hash = window.location.hash;

        if (hash.indexOf(filterTid) > -1) {
            hash = hash.replace('&tids[]=' + filterTid, '');
        } else {
            hash += '&tids[]=' + filterTid;
        }

        window.location.hash = hash;
    }

    private toggleFiltersGroup(filterTid: string): void {
        this.toggleFilterChoice(filterTid);
        this.toggleFilterHash(filterTid);

        if (_.indexOf(this.filtersGroup, filterTid) === -1) {
            this.filtersGroup = _.union(this.filtersGroup, [filterTid]);
        } else {
            this.filtersGroup = _.without(this.filtersGroup, filterTid);
        }
    }

    private toggleFilterChoice(filterTid: string): void {
        this.filterCategoriesStorage.forEach((filterCategory: FilterCategory) => {
            filterCategory.filters.forEach((filter: Filter) => {
                let currentFilterChoice = _.find(filter.choices, { tid: filterTid });

                if (currentFilterChoice) {
                    currentFilterChoice.active = !currentFilterChoice.active;
                }
            });
        });

        this.filterCategories.next(this.filterCategoriesStorage);
    }

    private darkenProgramsExcept(skipProgram: Program): void {
        this.programsStorage.forEach((program: Program, index: number) => {
            if (program === skipProgram) {
                program.darken = false;
            } else {
                program.darken = true;
            }
        });
    }

    private undarkenPrograms(): void {
        this.programsStorage.forEach((program: Program, index: number) => {
            program.darken = false;
        });
    }

    /*public reversePrograms(): void {
        this.deactivateAllPrograms();

        this.programs.next(this.programsStorage)

        // TODO: after we update animation, review this:
        setTimeout(() => {
            this.programsStorage = _.reverse(this.programsStorage);
            this.programsStorage = this.getUpdatedProgramsOrder(this.programsStorage);
            this.programs.next(this.programsStorage);
        }, NccProgramFinderService.COLLAPSE_TRANSITION_DURATION)
    }*/

    public deactivateAllPrograms(): void {
        if (this.programsStorage) {
            this.programsStorage.forEach((program: Program) => {
                program.active = false;
                program.darken = false;
            });

            this.programs.next(this.programsStorage);

            // TODO: after we update animation, review this:
            setTimeout(() => {
                this.programsStorage.forEach((program: Program) => {
                    program.stackOrder = NccProgramFinderService.DEFAULT_STACK_ORDER;
                });
                this.programs.next(this.programsStorage);
            }, NccProgramFinderService.COLLAPSE_TRANSITION_DURATION);
        }
    }

    public deactivateAllProgramsExcept(skipProgram: Program): void {
        this.programsStorage.forEach((program: Program) => {
            if (program !== skipProgram) {
                program.active = false;
                program.darken = false;
            }
        });
        this.programs.next(this.programsStorage);

        // TODO: after we update animation, review this:
        setTimeout(() => {
            this.programsStorage.forEach((program: Program) => {
                if (program !== skipProgram) {
                    program.stackOrder = NccProgramFinderService.DEFAULT_STACK_ORDER;
                }
            });
            this.programs.next(this.programsStorage);
        }, NccProgramFinderService.COLLAPSE_TRANSITION_DURATION)
    }

    public toggleProgram(programNid: string): void {
        let index = _.indexOf(this.programsStorage, _.find(this.programsStorage, { nid: programNid }));

        if (index > -1 && this.programsStorage[index]) {
            this.deactivateAllProgramsExcept(this.programsStorage[index]);
            this.programsStorage[index].active = !this.programsStorage[index].active;
            if (this.programsStorage[index].active) {
                this.darkenProgramsExcept(this.programsStorage[index]);
                this.programsStorage[index].stackOrder = this.getProgramStackOrder(index);
                this.trayClose(); // close tray
            } else {
                this.undarkenPrograms();
            }

            this.programs.next(this.programsStorage);

            if (!this.programsStorage[index].active) {
                // TODO: after we update animation, review this:
                setTimeout(() => {
                    this.programsStorage[index].stackOrder = NccProgramFinderService.DEFAULT_STACK_ORDER;
                    this.programs.next(this.programsStorage);
                }, NccProgramFinderService.COLLAPSE_TRANSITION_DURATION)
            }
        } else {
            console.error("Program with nid: " + programNid + " doesn't exist.")
        }
    }

    public toggleFilterByTid(filterTid: string): void {
        this.toggleFiltersGroup(filterTid);
        console.log('filtersGroup: [' + this.filtersGroup + ']');

        this.filterPrograms();

        this.programs.next(this.programsStorage);
    }

    public filterByTids(filtersGroup: string[]): void {
        this.filtersGroup = filtersGroup;

        this.filtersGroup.forEach((filterTid: string) => {
            this.toggleFilterChoice(filterTid);
        });

        this.filterPrograms();

        this.programs.next(this.programsStorage);
    }

    public clearAllFilters(): void {
        this.filtersGroup = [];

        this.clearFilterHash();
        this.deactivateAllFilters();

        this.filterPrograms();

        this.programs.next(this.programsStorage);
    }

    public trayOpen(): void {
        this.deactivateAllPrograms();
        this.isTrayOpen.next(true);
    }

    public trayClose(): void {
        this.isTrayOpen.next(false);
    }

}
