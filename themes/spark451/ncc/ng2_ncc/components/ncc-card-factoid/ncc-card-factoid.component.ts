import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-factoid',
    templateUrl: 'ncc-card-factoid.component.html',
    styleUrls: ['ncc-card-factoid.component.css']
})

export class NccCardFactoidComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() fact: string;
    @Input() description: string;
    @Input() icon: string;
    @Input() source: string;

    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.elpreview && this.elcontent) {
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elpreview && this.elcontent) {
            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        if (this.elpreview && this.elcontent) {
            contentAnimation.start(this.elcontent.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
        }*/

        if (this.elpreview && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';

            this.elpreview.nativeElement.style.opacity = '1';
        }
    }

}
