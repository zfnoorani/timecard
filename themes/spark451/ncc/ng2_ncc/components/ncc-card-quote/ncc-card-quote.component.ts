import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';


@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-quote',
    templateUrl: 'ncc-card-quote.component.html',
    styleUrls: ['ncc-card-quote.component.css']
})

export class NccCardQuoteComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() quote: string;
    @Input() name: string;
    @Input() tagLabel: string;
    @Input() tagUrl: string;

    @ViewChild('elbackground') elbackground: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let backgroundAnimation = this._ab.css();
        backgroundAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.3' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elbackground && this.elcontent) {
            backgroundAnimation.start(this.elbackground.nativeElement)
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elbackground && this.elcontent) {
            this.elbackground.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let backgroundAnimation = this._ab.css();
        backgroundAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '0.3' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elbackground && this.elcontent) {
            backgroundAnimation.start(this.elbackground.nativeElement)
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elbackground && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elbackground.nativeElement.style.opacity = '0.3';
        }
    }

}
