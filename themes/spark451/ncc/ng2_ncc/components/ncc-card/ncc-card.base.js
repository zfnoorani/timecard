System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var NccCardBase;
    return {
        setters: [],
        execute: function () {
            NccCardBase = (function () {
                function NccCardBase() {
                }
                NccCardBase.prototype.ngOnChanges = function (changes) {
                    if (changes['cardState'].currentValue === 'active') {
                        this.activeStateContentAnimation();
                    }
                    else if (changes['cardState'].currentValue === 'inactive') {
                        this.inactiveStateContentAnimation();
                    }
                };
                NccCardBase.prototype.ngAfterViewInit = function () {
                    if (this.cardState === 'active') {
                        this.activeStateContentAnimation();
                    }
                };
                return NccCardBase;
            }());
            exports_1("NccCardBase", NccCardBase);
        }
    };
});
//# sourceMappingURL=ncc-card.base.js.map