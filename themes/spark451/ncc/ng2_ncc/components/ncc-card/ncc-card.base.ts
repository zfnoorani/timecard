import {OnChanges, SimpleChange, AfterViewInit} from '@angular/core';

export abstract class NccCardBase implements OnChanges, AfterViewInit {
    public cardState: string;

    public ngOnChanges(changes: { [cardState: string]: SimpleChange }): void {
        if (changes['cardState'].currentValue === 'active') {
            this.activeStateContentAnimation();
        } else if (changes['cardState'].currentValue === 'inactive') {
            this.inactiveStateContentAnimation();
        }
    }

    public ngAfterViewInit(): void {
        if (this.cardState === 'active') {
            this.activeStateContentAnimation();
        }
    }

    public abstract activeStateContentAnimation(): void;

    public abstract inactiveStateContentAnimation(): void;
}
