System.register(["@angular/core", "../ncc-stack/ncc-stack.component", "../ncc-card/ncc-card.base"], function(t, e) {
    "use strict";
    var n, i, o, l, p, a, r, c, s, d, y, m, v, u, f, h, g, E, C, S, b, R, I, w, j, O, A = this && this.__extends || (n = Object.setPrototypeOf || {
                __proto__: []
            }
            instanceof Array && function(t, e) {
                t.__proto__ = e
            } || function(t, e) {
                for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n])
            },
            function(t, e) {
                function i() {
                    this.constructor = t
                }
                n(t, e), t.prototype = null === e ? Object.create(e) : (i.prototype = e.prototype, new i)
            }),
        _ = this && this.__decorate || function(t, e, n, i) {
            var o, l = arguments.length,
                p = l < 3 ? e : null === i ? i = Object.getOwnPropertyDescriptor(e, n) : i;
            if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) p = Reflect.decorate(t, e, n, i);
            else
                for (var a = t.length - 1; a >= 0; a--)(o = t[a]) && (p = (l < 3 ? o(p) : l > 3 ? o(e, n, p) : o(e, n)) || p);
            return l > 3 && p && Object.defineProperty(e, n, p), p
        },
        V = this && this.__metadata || function(t, e) {
            if ("object" == typeof Reflect && "function" == typeof Reflect.metadata) return Reflect.metadata(t, e)
        },
        N = e && e.id;
    return {
        setters: [function(t) {
            i = t, p = t
        }, function(t) {
            o = t
        }, function(t) {
            a = t
        }],
        execute: function() {
            l = function() {
                function t(t, e) {
                    this.cards = t, this._el = e, this.cardState = "inactive", this._cardTransition = "all 0.5s cubic-bezier(0.35, 0, 0.25, 1)", this.cards = t
                }
                return Object.defineProperty(t.prototype, "isActive", {
                    get: function() {
                        return this._isActive
                    },
                    set: function(t) {
                        this._isActive = t, this._isActive ? this.cardState = "active" : this.cardState = "inactive"
                    },
                    enumerable: !0,
                    configurable: !0
                }), Object.defineProperty(t.prototype, "isPeekAdjacent", {
                    get: function() {
                        return this._isPeekAdjacent
                    },
                    set: function(t) {
                        this._isPeekAdjacent = t
                    },
                    enumerable: !0,
                    configurable: !0
                }), Object.defineProperty(t.prototype, "isPeek", {
                    get: function() {
                        return this._isPeek
                    },
                    set: function(t) {
                        this._isPeek = t
                    },
                    enumerable: !0,
                    configurable: !0
                }), t.prototype.ngOnInit = function() {
                    var t = this;
                    this.cardClass = this.cardClass || "card-default", this.cards.addCard(this), setTimeout(function() {
                        t._el.nativeElement.style.transition = t._cardTransition
                    })
                }, t.prototype.ngOnDestroy = function() {
                    this.cards.removeCard(this)
                }, t.prototype.activateCard = function(t) {
                    this.isActive || this.cards.activateCard(this)
                }, t.prototype.mouseEnter = function(t) {
                    this.isActive || this.cards.activateCard(this)
                }, t.prototype.mouseLeave = function(t) {}, t.prototype.updateZIndex = function(t) {
                    this._el.nativeElement.style.zIndex = t
                }, t.prototype.activeStateCardAnimation = function() {
                    this._el.nativeElement.style.marginLeft = "-5px", this._el.nativeElement.style.marginRight = "-5px"
                }, t.prototype.inactiveStateCardAnimation = function(t) {
                    this._el.nativeElement.style.marginLeft = "-5px", this._el.nativeElement.style.marginRight = "-5px", this._el.nativeElement.style.zIndex = t
                }, _([i.Input(), V("design:type", String)], t.prototype, "cardClass", void 0), _([i.HostBinding("class.active"), i.Input(), V("design:type", Boolean), V("design:paramtypes", [Boolean])], t.prototype, "isActive", null), _([i.HostBinding("class.peek-adjacent"), i.Input(), V("design:type", Boolean), V("design:paramtypes", [Boolean])], t.prototype, "isPeekAdjacent", null), _([i.HostBinding("class.peek"), i.Input(), V("design:type", Boolean), V("design:paramtypes", [Boolean])], t.prototype, "isPeek", null), t = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card",
                    template: '\n    <div [ngClass]="cardClass" (click)="activateCard($event)" (mouseenter)="mouseEnter($event)" (mouseleave)="mouseLeave($event)">\n        <ng-content></ng-content>\n    </div>\n    ',
                    styleUrls: ["ncc-card.component.css"]
                }), V("design:paramtypes", [o.NccStackComponent, "function" == typeof(e = void 0 !== i.ElementRef && i.ElementRef) && e || Object])], t);
                var e
            }(), t("NccCardComponent", l), r = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elgraphic && this.elcontent && (this.elgraphic.nativeElement.style.opacity = "0.1", this.elgraphic.nativeElement.style.color = "#35393c", this.elcontent.nativeElement.style.opacity = "1")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elgraphic && this.elcontent && (this.elgraphic.nativeElement.style.opacity = "0.7", this.elgraphic.nativeElement.style.color = "#689f46", this.elcontent.nativeElement.style.opacity = "0")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "description", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkLabel", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([p.ViewChild("elgraphic"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elgraphic", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-career",
                    templateUrl: "html/ncc-card-career.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardCareerComponent", r), c = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elcontent && this.elbutton && (this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto", this.elbutton.nativeElement.style.opacity = "1", this.elbutton.nativeElement.style.pointerEvents = "auto"), this.elicon && (this.elicon.nativeElement.style.opacity = "0")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elcontent && this.elbutton && (this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none", this.elbutton.nativeElement.style.opacity = "0", this.elbutton.nativeElement.style.pointerEvents = "none"), this.elicon && (this.elicon.nativeElement.style.opacity = "0.7")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "icon", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkLabel", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([p.ViewChild("elicon"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elicon", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), _([p.ViewChild("elbutton"), V("design:type", "function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object)], e.prototype, "elbutton", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-cta",
                    templateUrl: "html/ncc-card-cta.component.html"
                }), V("design:paramtypes", ["function" == typeof(a = void 0 !== i.ElementRef && i.ElementRef) && a || Object])], e);
                var n, o, l, a
            }(a.NccCardBase), t("NccCardCtaComponent", c), s = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elimage && this.elbutton && this.eltitle && (this.elimage.nativeElement.style.transform = "scale(1)", this.elbutton.nativeElement.style.top = "0", this.elbutton.nativeElement.style.right = "0", this.elbutton.nativeElement.style.width = "50px", this.elbutton.nativeElement.style.height = "100%", this.elbutton.nativeElement.style.border = "none", this.elbutton.nativeElement.style.transform = "none", this.elbutton.nativeElement.style.borderRadius = "0", this.elbutton.nativeElement.style.backgroundColor = "rgba(53,57,60,0.8)", this.elbutton.nativeElement.style.pointerEvents = "auto", this.eltitle.nativeElement.style.opacity = "1")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elimage && this.elbutton && this.eltitle && (this.eltitle.nativeElement.style.opacity = "0", this.elimage.nativeElement.style.transform = "scale(1.15)", this.elbutton.nativeElement.style.top = "50%", this.elbutton.nativeElement.style.right = "50%", this.elbutton.nativeElement.style.width = "55px", this.elbutton.nativeElement.style.height = "55px", this.elbutton.nativeElement.style.border = "3px solid rgba(255,192,59,0.7)", this.elbutton.nativeElement.style.transform = "translate(50%, -50%)", this.elbutton.nativeElement.style.borderRadius = "50%", this.elbutton.nativeElement.style.backgroundColor = "transparent", this.elbutton.nativeElement.style.pointerEvents = "none")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elimage", void 0), _([p.ViewChild("eltitle"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "eltitle", void 0), _([p.ViewChild("elbutton"), V("design:type", "function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object)], e.prototype, "elbutton", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-cta-photo",
                    templateUrl: "html/ncc-card-cta-photo.component.html"
                }), V("design:paramtypes", ["function" == typeof(a = void 0 !== i.ElementRef && i.ElementRef) && a || Object])], e);
                var n, o, l, a
            }(a.NccCardBase), t("NccCardCtaPhotoComponent", s), d = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elpreview && this.elcontent && this.elbutton && (this.elpreview.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto", this.elbutton.nativeElement.style.opacity = "1", this.elbutton.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elpreview && this.elcontent && this.elbutton && (this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none", this.elbutton.nativeElement.style.opacity = "0", this.elbutton.nativeElement.style.pointerEvents = "none", this.elpreview.nativeElement.style.opacity = "1")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "heading", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "description", void 0), _([i.Input(), V("design:type", String)], e.prototype, "timeStart", void 0), _([i.Input(), V("design:type", String)], e.prototype, "timeEnd", void 0), _([i.Input(), V("design:type", String)], e.prototype, "location", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([i.Input(), V("design:type", String)], e.prototype, "shortDate", void 0), _([i.Input(), V("design:type", String)], e.prototype, "day", void 0), _([p.ViewChild("elpreview"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elpreview", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), _([p.ViewChild("elbutton"), V("design:type", "function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object)], e.prototype, "elbutton", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-event",
                    templateUrl: "html/ncc-card-event.component.html"
                }), V("design:paramtypes", ["function" == typeof(a = void 0 !== i.ElementRef && i.ElementRef) && a || Object])], e);
                var n, o, l, a
            }(a.NccCardBase), t("NccCardEventComponent", d), y = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elwrapper && this.elpreview && this.elcontent && (this.elwrapper.nativeElement.style.backgroundColor = "#fff", this.elpreview.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elwrapper && this.elpreview && this.elcontent && (this.elwrapper.nativeElement.style.backgroundColor = "#3b5998", this.elpreview.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "icon", void 0), _([i.Input(), V("design:type", String)], e.prototype, "name", void 0), _([i.Input(), V("design:type", String)], e.prototype, "date", void 0), _([i.Input(), V("design:type", String)], e.prototype, "content", void 0), _([i.Input(), V("design:type", String)], e.prototype, "profileUrl", void 0), _([p.ViewChild("elwrapper"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elwrapper", void 0), _([p.ViewChild("elpreview"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elpreview", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-facebook",
                    templateUrl: "html/ncc-card-facebook.component.html"
                }), V("design:paramtypes", ["function" == typeof(a = void 0 !== i.ElementRef && i.ElementRef) && a || Object])], e);
                var n, o, l, a
            }(a.NccCardBase), t("NccCardFacebookComponent", y), m = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elpreview && this.elcontent && (this.elpreview.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elpreview && this.elcontent && (this.elcontent.nativeElement.style.opacity = "0", this.elpreview.nativeElement.style.opacity = "1")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "fact", void 0), _([i.Input(), V("design:type", String)], e.prototype, "description", void 0), _([i.Input(), V("design:type", String)], e.prototype, "icon", void 0), _([i.Input(), V("design:type", String)], e.prototype, "source", void 0), _([p.ViewChild("elpreview"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elpreview", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-factoid",
                    templateUrl: "html/ncc-card-factoid.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardFactoidComponent", m), v = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elimage && this.elcontent && (this.elimage.nativeElement.style.transform = "scale(1)", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elimage && this.elcontent && (this.elimage.nativeElement.style.transform = "scale(1.25)", this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "description", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elimage", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-feature",
                    templateUrl: "html/ncc-card-feature.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardFeatureComponent", v), u = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elimage && this.elicon && (this.elimage.nativeElement.style.transform = "scale(1)", this.elicon.nativeElement.style.bottom = "25px", this.elicon.nativeElement.style.right = "17px", this.elicon.nativeElement.style.transform = "none", this.elicon.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elimage && this.elicon && (this.elimage.nativeElement.style.transform = "scale(1.25)", this.elicon.nativeElement.style.bottom = "50%", this.elicon.nativeElement.style.right = "50%", this.elicon.nativeElement.style.transform = "translate(50%, 50%)", this.elicon.nativeElement.style.pointerEvents = "none")
                }, e.prototype.openModal = function(t) {
                    this.modalContentId && (t.preventDefault(), nccModal.displayModal(t, this.modalContentId))
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([i.Input(), V("design:type", String)], e.prototype, "modalContentId", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elimage", void 0), _([p.ViewChild("elicon"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elicon", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-gallery",
                    templateUrl: "html/ncc-card-gallery.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardGalleryComponent", u), f = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elimage && (this.elimage.nativeElement.style.transform = "scale(1)")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elimage && (this.elimage.nativeElement.style.transform = "scale(1.25)")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elimage", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-image",
                    templateUrl: "html/ncc-card-image.component.html"
                }), V("design:paramtypes", ["function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object])], e);
                var n, o
            }(a.NccCardBase), t("NccCardImageComponent", f), h = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elimage && (this.elimage.nativeElement.style.transform = "scale(1)")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elimage && (this.elimage.nativeElement.style.transform = "scale(1.25)")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "icon", void 0), _([i.Input(), V("design:type", String)], e.prototype, "name", void 0), _([i.Input(), V("design:type", String)], e.prototype, "date", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([i.Input(), V("design:type", String)], e.prototype, "profileUrl", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elimage", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-instagram",
                    templateUrl: "html/ncc-card-instagram.component.html"
                }), V("design:paramtypes", ["function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object])], e);
                var n, o
            }(a.NccCardBase), t("NccCardInstagramComponent", h), g = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elmap && this.elcontent && (this.elmap.nativeElement.style.transform = "scale(1)", this.elmap.nativeElement.style.pointerEvents = "auto", this.elcontent.nativeElement.style.opacity = "1")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elmap && this.elcontent && (this.elcontent.nativeElement.style.opacity = "0", this.elmap.nativeElement.style.transform = "scale(1.25)", this.elmap.nativeElement.style.pointerEvents = "none")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "description", void 0), _([i.Input(), V("design:type", String)], e.prototype, "googleMap", void 0), _([p.ViewChild("elmap"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elmap", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-map",
                    templateUrl: "html/ncc-card-map.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardMapComponent", g), E = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elpreview && this.elcontent && (this.elpreview.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elpreview && this.elcontent && (this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none", this.elpreview.nativeElement.style.opacity = "1")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "articleType", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "date", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([p.ViewChild("elpreview"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elpreview", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-news",
                    templateUrl: "html/ncc-card-news.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardNewsComponent", E), C = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elimage && this.elcontent && (this.elimage.nativeElement.style.transform = "scale(1)", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elimage && this.elcontent && (this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none", this.elimage.nativeElement.style.transform = "scale(1.25)")
                }, e.prototype.openProfile = function(t) {
                    this.trayContentId && (t.preventDefault(), nccTray.toggleTray(t, this.trayContentId))
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "name", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkLabel", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([i.Input(), V("design:type", String)], e.prototype, "trayContentId", void 0), _([i.Input(), V("design:type", String)], e.prototype, "tagLabel", void 0), _([i.Input(), V("design:type", String)], e.prototype, "tagUrl", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elimage", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-profile",
                    templateUrl: "html/ncc-card-profile.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardProfileComponent", C), S = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elpreview && this.elcontent && (this.elpreview.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elpreview && this.elcontent && (this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none", this.elpreview.nativeElement.style.opacity = "1")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "description", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([p.ViewChild("elpreview"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elpreview", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-program",
                    templateUrl: "html/ncc-card-program.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardProgramComponent", S), b = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elbackground && this.elcontent && (this.elbackground.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elbackground && this.elcontent && (this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none", this.elbackground.nativeElement.style.opacity = "0.3")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "quote", void 0), _([i.Input(), V("design:type", String)], e.prototype, "name", void 0), _([i.Input(), V("design:type", String)], e.prototype, "tagLabel", void 0), _([i.Input(), V("design:type", String)], e.prototype, "tagUrl", void 0), _([p.ViewChild("elbackground"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elbackground", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-quote",
                    templateUrl: "html/ncc-card-quote.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardQuoteComponent", b), R = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.ngOnInit = function() {
                    if(this.image == ""){
                        this.eltexture.nativeElement.style.opacity = '1';
                        this.elpreview.nativeElement.style.opacity = '1';
                    }
                }, e.prototype.activeStateContentAnimation = function() {
                    this.elimage && this.eltexture && this.elcontent && (this.elimage.nativeElement.style.transform = "scale(1)", this.eltexture.nativeElement.style.opacity = "1", this.eltexture.nativeElement.style.pointerEvents = "auto",this.elpreview.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elimage && this.eltexture && this.elcontent && (this.eltexture.nativeElement.style.opacity="0",this.elpreview.nativeElement.style.opacity="0",""!=this.image|| (this.eltexture.nativeElement.style.opacity="1",this.elpreview.nativeElement.style.opacity="1"), this.eltexture.nativeElement.style.pointerEvents = "none", this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none", this.elimage.nativeElement.style.transform = "scale(1.25)")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "name", void 0), _([i.Input(), V("design:type", String)], e.prototype, "tagUrl", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elimage", void 0), _([p.ViewChild("eltexture"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "eltexture", void 0),_([p.ViewChild("elpreview"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elpreview", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-story",
                    templateUrl: "html/ncc-card-story.component.html"
                }), V("design:paramtypes", ["function" == typeof(a = void 0 !== i.ElementRef && i.ElementRef) && a || Object])], e);
                var n, o, l, a
            }(a.NccCardBase), t("NccCardStoryComponent", R), I = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.eltitle && this.elcontent && (this.eltitle.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.eltitle && this.elcontent && (this.elcontent.nativeElement.style.opacity = "0", this.eltitle.nativeElement.style.opacity = "1")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "description", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkLabel", void 0), _([i.Input(), V("design:type", String)], e.prototype, "linkUrl", void 0), _([i.Input(), V("design:type", String)], e.prototype, "tagLabel", void 0), _([i.Input(), V("design:type", String)], e.prototype, "tagUrl", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([i.Input(), V("design:type", String)], e.prototype, "graphic", void 0), _([i.Input(), V("design:type", String)], e.prototype, "icon", void 0), _([p.ViewChild("eltitle"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "eltitle", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elimage", void 0), _([p.ViewChild("elgraphic"), V("design:type", "function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object)], e.prototype, "elgraphic", void 0), _([p.ViewChild("elicon"), V("design:type", "function" == typeof(a = void 0 !== i.ElementRef && i.ElementRef) && a || Object)], e.prototype, "elicon", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(r = void 0 !== i.ElementRef && i.ElementRef) && r || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-template",
                    templateUrl: "html/ncc-card-template.component.html"
                }), V("design:paramtypes", ["function" == typeof(c = void 0 !== i.ElementRef && i.ElementRef) && c || Object])], e);
                var n, o, l, a, r, c
            }(a.NccCardBase), t("NccCardTemplateComponent", I), w = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.eltitle1 && this.eltitle2 && (this.eltitle1.nativeElement.style.opacity = "0", this.eltitle2.nativeElement.style.opacity = "1"), this.elicon && (this.elicon.nativeElement.style.opacity = "1")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.eltitle1 && this.eltitle2 && (this.eltitle2.nativeElement.style.opacity = "0", this.eltitle1.nativeElement.style.opacity = "0.15"), this.elicon && (this.elicon.nativeElement.style.opacity = "0")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "title", void 0), _([i.Input(), V("design:type", String)], e.prototype, "icon", void 0), _([p.ViewChild("eltitle1"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "eltitle1", void 0), _([p.ViewChild("eltitle2"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "eltitle2", void 0), _([p.ViewChild("elicon"), V("design:type", "function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object)], e.prototype, "elicon", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-title",
                    templateUrl: "html/ncc-card-title.component.html"
                }), V("design:paramtypes", ["function" == typeof(a = void 0 !== i.ElementRef && i.ElementRef) && a || Object])], e);
                var n, o, l, a
            }(a.NccCardBase), t("NccCardTitleComponent", w), j = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elwrapper && this.elpreview && this.elcontent && (this.elwrapper.nativeElement.style.backgroundColor = "#fff", this.elpreview.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elwrapper && this.elpreview && this.elcontent && (this.elwrapper.nativeElement.style.backgroundColor = "#00aced", this.elpreview.nativeElement.style.opacity = "1", this.elcontent.nativeElement.style.opacity = "0", this.elcontent.nativeElement.style.pointerEvents = "none")
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "icon", void 0), _([i.Input(), V("design:type", String)], e.prototype, "name", void 0), _([i.Input(), V("design:type", String)], e.prototype, "account", void 0), _([i.Input(), V("design:type", String)], e.prototype, "date", void 0), _([i.Input(), V("design:type", String)], e.prototype, "content", void 0), _([i.Input(), V("design:type", String)], e.prototype, "profileUrl", void 0), _([p.ViewChild("elwrapper"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elwrapper", void 0), _([p.ViewChild("elpreview"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elpreview", void 0), _([p.ViewChild("elcontent"), V("design:type", "function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object)], e.prototype, "elcontent", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-twitter",
                    templateUrl: "html/ncc-card-twitter.component.html"
                }), V("design:paramtypes", ["function" == typeof(a = void 0 !== i.ElementRef && i.ElementRef) && a || Object])], e);
                var n, o, l, a
            }(a.NccCardBase), t("NccCardTwitterComponent", j), O = function(t) {
                function e(e) {
                    var n = t.call(this) || this;
                    return n._el = e, n
                }
                return A(e, t), e.prototype.activeStateContentAnimation = function() {
                    this.elimage && this.elicon && (this.elimage.nativeElement.style.transform = "scale(1)", this.elicon.nativeElement.style.bottom = "25px", this.elicon.nativeElement.style.right = "17px", this.elicon.nativeElement.style.transform = "none", this.elicon.nativeElement.style.pointerEvents = "auto")
                }, e.prototype.inactiveStateContentAnimation = function() {
                    this.elimage && this.elicon && (this.elimage.nativeElement.style.transform = "scale(1.25)", this.elicon.nativeElement.style.bottom = "50%", this.elicon.nativeElement.style.right = "50%", this.elicon.nativeElement.style.transform = "translate(50%, 50%)", this.elicon.nativeElement.style.pointerEvents = "none")
                }, e.prototype.openModal = function(t) {
                    this.modalContentId && (t.preventDefault(), nccModal.displayModal(t, this.modalContentId))
                }, _([i.Input(), V("design:type", String)], e.prototype, "cardState", void 0), _([i.Input(), V("design:type", String)], e.prototype, "image", void 0), _([i.Input(), V("design:type", String)], e.prototype, "modalContentId", void 0), _([p.ViewChild("elimage"), V("design:type", "function" == typeof(n = void 0 !== i.ElementRef && i.ElementRef) && n || Object)], e.prototype, "elimage", void 0), _([p.ViewChild("elicon"), V("design:type", "function" == typeof(o = void 0 !== i.ElementRef && i.ElementRef) && o || Object)], e.prototype, "elicon", void 0), e = _([i.Component({
                    moduleId: N,
                    selector: "ncc-card-video",
                    templateUrl: "html/ncc-card-video.component.html"
                }), V("design:paramtypes", ["function" == typeof(l = void 0 !== i.ElementRef && i.ElementRef) && l || Object])], e);
                var n, o, l
            }(a.NccCardBase), t("NccCardVideoComponent", O)
        }
    }
});