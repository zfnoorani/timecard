import {Component, OnInit, OnDestroy, Input, HostBinding, Inject, ElementRef} from '@angular/core';
import { NgClass } from '@angular/common';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccStackComponent} from '../ncc-stack/ncc-stack.component';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card',
    template: `
    <div [ngClass]="cardClass" (click)="activateCard($event)" (mouseenter)="mouseEnter($event)" (mouseleave)="mouseLeave($event)">
        <ng-content></ng-content>
    </div>
    `,
    styleUrls: ['ncc-card.component.css']
})

export class NccCardComponent implements OnInit, OnDestroy {
    @Input() public cardClass: string;

    @HostBinding('class.active')
    @Input()
    public get isActive(): boolean {
        return this._isActive;
    }

    public set isActive(value: boolean) {
        this._isActive = value;

        if (this._isActive) {
            this.cardState = 'active';
        } else {
            this.cardState = 'inactive';
        }
    }

    @HostBinding('class.peek-adjacent')
    @Input()
    public get isPeekAdjacent(): boolean {
        return this._isPeekAdjacent;
    }

    public set isPeekAdjacent(value: boolean) {
        this._isPeekAdjacent = value;
    }

    @HostBinding('class.peek')
    @Input()
    public get isPeek(): boolean {
        return this._isPeek;
    }

    public set isPeek(value: boolean) {
        this._isPeek = value;
    }

    public cardState: string = 'inactive';

    private _isActive: boolean;
    private _isPeekAdjacent: boolean;
    private _isPeek: boolean;
    // private cards: NccStackComponent;
    private _cardTransition: string = 'all 0.5s cubic-bezier(0.35, 0, 0.25, 1)';

    // public constructor( @Inject(NccStackComponent) cards: NccStackComponent, private _ab: AnimationBuilder, private _el: ElementRef) {
    public constructor( private cards: NccStackComponent, private _el: ElementRef) {
        this.cards = cards;
    }

    public ngOnInit(): void {
        this.cardClass = this.cardClass || 'card-default';
        this.cards.addCard(this);

        setTimeout(() => {
            this._el.nativeElement.style.transition = this._cardTransition;
        });
    }

    public ngOnDestroy(): void {
        this.cards.removeCard(this);
    }

    public activateCard(event: MouseEvent): void {
        if (!this.isActive) {
            this.cards.activateCard(this);
        }
    }

    public mouseEnter(event: MouseEvent): void {
        // this.cards.peekCard(this);
        if (!this.isActive) {
            this.cards.activateCard(this);
        }
    }

    public mouseLeave(event: MouseEvent): void {
        // this.cards.unpeekCard(this);
    }

    public updateZIndex(zIndex: number): void {
        /*let animation = this._ab.css();

        animation
            .setDuration(500)
            .setToStyles({ 'z-index': zIndex })

        animation.start(this._el.nativeElement)*/

        this._el.nativeElement.style.zIndex = zIndex;
    }

    public activeStateCardAnimation(): void {
        /*let animation = this._ab.css();

        animation
            .setDuration(500)
            .setFromStyles({
                'margin-left': '0',
                'margin-right': '0',
            })
            .setToStyles({
                'margin-left': '-5px',
                'margin-right': '-5px'
            })

        animation.start(this._el.nativeElement);*/

        this._el.nativeElement.style.marginLeft = '-5px';
        this._el.nativeElement.style.marginRight = '-5px';
    }

    public inactiveStateCardAnimation(zIndex: number): void {
        /*let animation = this._ab.css();

        animation
            .setDuration(500)
            .setFromStyles({
                'margin-left': '-5px',
                'margin-right': '-5px'
            })
            .setToStyles({
                'margin-left': '0',
                'margin-right': '0',
                'z-index': zIndex
            })

        animation.start(this._el.nativeElement);*/

        this._el.nativeElement.style.marginLeft = '-5px';
        this._el.nativeElement.style.marginRight = '-5px';
        this._el.nativeElement.style.zIndex = zIndex;
    }

}

import {ViewChild} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-career',
    templateUrl: 'html/ncc-card-career.component.html',
    //styleUrls: ['ncc-card-career.component.css']
})

export class NccCardCareerComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    @Input() linkLabel: string;
    @Input() linkUrl: string;

    @ViewChild('elgraphic') elgraphic: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let graphicAnimation = this._ab.css();
        graphicAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.7', 'color': '#689f46' })
            .setToStyles({ 'opacity': '0.1', 'color': '#35393c' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.elgraphic && this.elcontent) {
            graphicAnimation.start(this.elgraphic.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elgraphic && this.elcontent) {
            this.elgraphic.nativeElement.style.opacity = '0.1';
            this.elgraphic.nativeElement.style.color = '#35393c';

            this.elcontent.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let graphicAnimation = this._ab.css();
        graphicAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.1', 'color': '#35393c' })
            .setToStyles({ 'opacity': '0.7', 'color': '#689f46' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        if (this.elgraphic && this.elcontent) {
            graphicAnimation.start(this.elgraphic.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);

        }*/

        if (this.elgraphic && this.elcontent) {
            this.elgraphic.nativeElement.style.opacity = '0.7';
            this.elgraphic.nativeElement.style.color = '#689f46';

            this.elcontent.nativeElement.style.opacity = '0';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-cta',
    templateUrl: 'html/ncc-card-cta.component.html',
    //styleUrls: ['ncc-card-cta.component.css']
})

export class NccCardCtaComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() icon: string;
    @Input() linkLabel: string;
    @Input() linkUrl: string;

    @ViewChild('elicon') elicon: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;
    @ViewChild('elbutton') elbutton: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.7' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elicon && this.elcontent && this.elbutton) {
            iconAnimation.start(this.elicon.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
            contentAnimation.start(this.elbutton.nativeElement);
        }*/

        if (this.elcontent && this.elbutton) {
            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';

            this.elbutton.nativeElement.style.opacity = '1';
            this.elbutton.nativeElement.style.pointerEvents = 'auto';
        }

        if (this.elicon) {
            this.elicon.nativeElement.style.opacity = '0';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '0.7' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elicon && this.elcontent && this.elbutton) {
            iconAnimation.start(this.elicon.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
            contentAnimation.start(this.elbutton.nativeElement);
        }*/

        if (this.elcontent && this.elbutton) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elbutton.nativeElement.style.opacity = '0';
            this.elbutton.nativeElement.style.pointerEvents = 'none';
        }

        if (this.elicon) {
            this.elicon.nativeElement.style.opacity = '0.7';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-cta-photo',
    templateUrl: 'html/ncc-card-cta-photo.component.html',
    //styleUrls: ['ncc-card-cta-photo.component.css']
})

export class NccCardCtaPhotoComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() image: string;
    @Input() linkUrl: string;

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('eltitle') eltitle: ElementRef;
    @ViewChild('elbutton') elbutton: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(100)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let buttonAnimation = this._ab.css();
        buttonAnimation
            .setDuration(400)
            .setFromStyles({
                'top': '50%',
                'right': '50%',
                'width': '55px',
                'height': '55px',
                'border': '3px solid rgba(255,192,59,0.7)',
                'transform': 'translate(50%, -50%)',
                'border-radius': '50%',
                'background-color': 'none',
                'pointer-events': 'none'
            })
            .setToStyles({
                'top': '0',
                'right': '0',
                'width': '50px',
                'height': '100%',
                'border': 'none',
                'transform': 'none',
                'border-radius': '0',
                'background-color': 'rgba(53,57,60,0.8)',
                'pointer-events': 'auto'
            });

        if (this.elimage && this.elbutton && this.eltitle) {
            imageAnimation.start(this.elimage.nativeElement)
            buttonAnimation.start(this.elbutton.nativeElement)
                .onComplete(() => {
                    headingAnimation.start(this.eltitle.nativeElement);
                })
        }*/

        if (this.elimage && this.elbutton && this.eltitle) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elbutton.nativeElement.style.top = '0';
            this.elbutton.nativeElement.style.right = '0';
            this.elbutton.nativeElement.style.width = '50px';
            this.elbutton.nativeElement.style.height = '100%';
            this.elbutton.nativeElement.style.border = 'none';
            this.elbutton.nativeElement.style.transform = 'none';
            this.elbutton.nativeElement.style.borderRadius = '0';
            this.elbutton.nativeElement.style.backgroundColor = 'rgba(53,57,60,0.8)';
            this.elbutton.nativeElement.style.pointerEvents = 'auto';

            this.eltitle.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.15)' })

        let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(100)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let buttonAnimation = this._ab.css();
        buttonAnimation
            .setDuration(200)
            .setFromStyles({
                'top': '0',
                'right': '0',
                'width': '50px',
                'height': '100%',
                'border': 'none',
                'transform': 'none',
                'border-radius': 'none',
                'background-color': 'rgba(53,57,60,0.8)',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'top': '50%',
                'right': '50%',
                'width': '55px',
                'height': '55px',
                'border': '3px solid rgba(255,192,59,0.7)',
                'transform': 'translate(50%, -50%)',
                'border-radius': '50%',
                'background-color': 'transparent',
                'pointer-events': 'none'
            });


        if (this.elimage && this.elbutton && this.eltitle) {
            imageAnimation.start(this.elimage.nativeElement)
            headingAnimation.start(this.eltitle.nativeElement)
                .onComplete(() => {
                    buttonAnimation.start(this.elbutton.nativeElement);
                })
        }*/

        if (this.elimage && this.elbutton && this.eltitle) {
            this.eltitle.nativeElement.style.opacity = '0';

            this.elimage.nativeElement.style.transform = 'scale(1.15)';

            this.elbutton.nativeElement.style.top = '50%';
            this.elbutton.nativeElement.style.right = '50%';
            this.elbutton.nativeElement.style.width = '55px';
            this.elbutton.nativeElement.style.height = '55px';
            this.elbutton.nativeElement.style.border = '3px solid rgba(255,192,59,0.7)';
            this.elbutton.nativeElement.style.transform = 'translate(50%, -50%)';
            this.elbutton.nativeElement.style.borderRadius = '50%';
            this.elbutton.nativeElement.style.backgroundColor = 'transparent';
            this.elbutton.nativeElement.style.pointerEvents = 'none';
        }
    }

}

import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-event',
    templateUrl: 'html/ncc-card-event.component.html',
    //styleUrls: ['ncc-card-event.component.css']
})

export class NccCardEventComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() heading: string;
    @Input() title: string;
    @Input() description: string;
    @Input() timeStart: string;
    @Input() timeEnd: string;
    @Input() location: string;
    // @Input() linkLabel: string;
    @Input() linkUrl: string;
    @Input() shortDate: string;
    @Input() day: string;

    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;
    @ViewChild('elbutton') elbutton: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto',
            })

        if (this.elpreview && this.elcontent && this.elbutton) {
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
            contentAnimation.start(this.elbutton.nativeElement);
        }*/

        if (this.elpreview && this.elcontent && this.elbutton) {
            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';

            this.elbutton.nativeElement.style.opacity = '1';
            this.elbutton.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none',
            })

        if (this.elpreview && this.elcontent && this.elbutton) {
            contentAnimation.start(this.elcontent.nativeElement);
            contentAnimation.start(this.elbutton.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
        }*/

        if (this.elpreview && this.elcontent && this.elbutton) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elbutton.nativeElement.style.opacity = '0';
            this.elbutton.nativeElement.style.pointerEvents = 'none';

            this.elpreview.nativeElement.style.opacity = '1';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-facebook',
    templateUrl: 'html/ncc-card-facebook.component.html',
    //styleUrls: ['ncc-card-facebook.component.css']
})

export class NccCardFacebookComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() icon: string;
    @Input() name: string;
    @Input() date: string;
    @Input() content: string;
    @Input() profileUrl: string;

    @ViewChild('elwrapper') elwrapper: ElementRef;
    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let wrapperAnimation = this._ab.css();
        wrapperAnimation
            .setDuration(500)
            .setFromStyles({ 'background-color': '#3b5998' })
            .setToStyles({ 'background-color': '#fff' })

        let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elwrapper && this.elpreview && this.elcontent) {
            wrapperAnimation.start(this.elwrapper.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elwrapper && this.elpreview && this.elcontent) {
            this.elwrapper.nativeElement.style.backgroundColor = '#fff';

            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let wrapperAnimation = this._ab.css();
        wrapperAnimation
            .setDuration(500)
            .setFromStyles({ 'background-color': '#fff' })
            .setToStyles({ 'background-color': '#3b5998' })

        let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elwrapper && this.elpreview && this.elcontent) {
            wrapperAnimation.start(this.elwrapper.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elwrapper && this.elpreview && this.elcontent) {
            this.elwrapper.nativeElement.style.backgroundColor = '#3b5998';

            this.elpreview.nativeElement.style.opacity = '1';

            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-factoid',
    templateUrl: 'html/ncc-card-factoid.component.html',
    //styleUrls: ['ncc-card-factoid.component.css']
})

export class NccCardFactoidComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() fact: string;
    @Input() description: string;
    @Input() icon: string;
    @Input() source: string;

    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.elpreview && this.elcontent) {
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elpreview && this.elcontent) {
            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        if (this.elpreview && this.elcontent) {
            contentAnimation.start(this.elcontent.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
        }*/

        if (this.elpreview && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';

            this.elpreview.nativeElement.style.opacity = '1';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-feature',
    templateUrl: 'html/ncc-card-feature.component.html',
    //styleUrls: ['ncc-card-feature.component.css']
})

export class NccCardFeatureComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    @Input() image: string;
    @Input() linkUrl: string;
    // @Input() linkLabel: string;

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elimage && this.elcontent) {
            contentAnimation.start(this.elcontent.nativeElement)
            imageAnimation.start(this.elimage.nativeElement);
        }*/

        if (this.elimage && this.elcontent) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elimage && this.elcontent) {
            imageAnimation.start(this.elimage.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elimage && this.elcontent) {
            this.elimage.nativeElement.style.transform = 'scale(1.25)';

            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';
        }
    }

}

import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

declare var nccModal: any;

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-gallery',
    templateUrl: 'html/ncc-card-gallery.component.html',
    //styleUrls: ['ncc-card-gallery.component.css']
})

export class NccCardGalleryComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() image: string;
    @Input() modalContentId: string;  // will be used for Modal content - trigger modal

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elicon') elicon: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(400)
            .setFromStyles({
                'bottom': '50%',
                'right': '50%',
                'transform': 'translate(50%, 50%)',
                'pointer-events': 'none'
            })
            .setToStyles({
                'bottom': '25px',
                'right': '17px',
                'transform': 'none',
                'pointer-events': 'auto'
            })

        if (this.elimage && this.elicon) {
            imageAnimation.start(this.elimage.nativeElement);
            iconAnimation.start(this.elicon.nativeElement);
        }*/

        if (this.elimage && this.elicon) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elicon.nativeElement.style.bottom = '25px';
            this.elicon.nativeElement.style.right = '17px';
            this.elicon.nativeElement.style.transform = 'none';
            this.elicon.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(400)
            .setFromStyles({
                'bottom': '25px',
                'right': '17px',
                'transform': 'none',
                'pointer-events': 'auto'
            })
            .setToStyles({

                'bottom': '50%',
                'right': '50%',
                'transform': 'translate(50%, 50%)',
                'pointer-events': 'none'
            })

        if (this.elimage && this.elicon) {
            imageAnimation.start(this.elimage.nativeElement);
            iconAnimation.start(this.elicon.nativeElement);
        }*/

        if (this.elimage && this.elicon) {
            this.elimage.nativeElement.style.transform = 'scale(1.25)';

            this.elicon.nativeElement.style.bottom = '50%';
            this.elicon.nativeElement.style.right = '50%';
            this.elicon.nativeElement.style.transform = 'translate(50%, 50%)';
            this.elicon.nativeElement.style.pointerEvents = 'none';
        }
    }

    public openModal(event: MouseEvent) {
        if (this.modalContentId) {
            event.preventDefault();
            nccModal.displayModal(event, this.modalContentId);
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-image',
    templateUrl: 'html/ncc-card-image.component.html',
    //styleUrls: ['ncc-card-image.component.css']
})

export class NccCardImageComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() image: string;

    @ViewChild('elimage') elimage: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        if (this.elimage) {
            imageAnimation.start(this.elimage.nativeElement)
        }*/

        if (this.elimage) {
            this.elimage.nativeElement.style.transform = 'scale(1)';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        if (this.elimage) {
            imageAnimation.start(this.elimage.nativeElement)
        }*/

        if (this.elimage) {
            this.elimage.nativeElement.style.transform = 'scale(1.25)';
        }
    }

}

import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-instagram',
    templateUrl: 'html/ncc-card-instagram.component.html',
    //styleUrls: ['ncc-card-instagram.component.css']
})

export class NccCardInstagramComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() icon: string;
    @Input() name: string;
    @Input() date: string;
    @Input() image: string;
    @Input() profileUrl: string;

    @ViewChild('elimage') elimage: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        if (this.elimage) {
            imageAnimation.start(this.elimage.nativeElement);
        }*/

        if (this.elimage) {
            this.elimage.nativeElement.style.transform = 'scale(1)';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        if (this.elimage) {
            imageAnimation.start(this.elimage.nativeElement);
        }*/

        if (this.elimage) {
            this.elimage.nativeElement.style.transform = 'scale(1.25)';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-map',
    templateUrl: 'html/ncc-card-map.component.html',
    //styleUrls: ['ncc-card-map.component.css']
})

export class NccCardMapComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    @Input() googleMap: string;

    @ViewChild('elmap') elmap: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let mapAnimation = this._ab.css();
        mapAnimation
            .setDuration(500)
            .setFromStyles({
                'transform': 'scale(1.25)',
                'pointer-events': 'none'
            })
            .setToStyles({
                'transform': 'scale(1)',
                'pointer-events': 'auto'
            })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(400)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.elmap && this.elcontent) {
            mapAnimation.start(this.elmap.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elmap && this.elcontent) {
            this.elmap.nativeElement.style.transform = 'scale(1)';
            this.elmap.nativeElement.style.pointerEvents = 'auto';

            this.elcontent.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let mapAnimation = this._ab.css();
        mapAnimation
            .setDuration(500)
            .setFromStyles({
                'transform': 'scale(1)',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'transform': 'scale(1.25)',
                'pointer-events': 'none'
            })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(400)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        if (this.elmap && this.elcontent) {
            mapAnimation.start(this.elmap.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elmap && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';

            this.elmap.nativeElement.style.transform = 'scale(1.25)';
            this.elmap.nativeElement.style.pointerEvents = 'none';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-news',
    templateUrl: 'html/ncc-card-news.component.html',
    //styleUrls: ['ncc-card-news.component.css']
})

export class NccCardNewsComponent extends NccCardBase {
    @Input() cardState: string;
    @Input() articleType: string;

    @Input() title: string;
    @Input() date: string;
    // @Input() linkLabel: string;
    @Input() linkUrl: string;
    // @Input() linkLabel2: string;
    // @Input() linkUrl2: string;

    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elpreview && this.elcontent) {
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elpreview && this.elcontent) {
            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elpreview && this.elcontent) {
            contentAnimation.start(this.elcontent.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
        }*/

        if (this.elpreview && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elpreview.nativeElement.style.opacity = '1';
        }
    }

}

declare var nccTray: any;

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-profile',
    templateUrl: 'html/ncc-card-profile.component.html',
    //styleUrls: ['ncc-card-profile.component.css']
})

export class NccCardProfileComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() name: string;
    @Input() title: string; // title/major
    @Input() linkLabel: string;
    @Input() linkUrl: string; // will be used for Profile Full - link to the other page
    @Input() trayContentId: string;  // will be used for Profile Story - trigger tray content
    @Input() tagLabel: string;
    @Input() tagUrl: string;
    @Input() image: string;

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(400)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto',
            })

        if (this.elimage && this.elcontent) {
            imageAnimation.start(this.elimage.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elimage && this.elcontent) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(400)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none',
            })

        if (this.elimage && this.elcontent) {
            contentAnimation.start(this.elcontent.nativeElement);
            imageAnimation.start(this.elimage.nativeElement);
        }*/

        if (this.elimage && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elimage.nativeElement.style.transform = 'scale(1.25)';
        }
    }

    public openProfile(event: MouseEvent) {
        if (this.trayContentId) {
            event.preventDefault();
            // nccTray.init();
            nccTray.toggleTray(event, this.trayContentId);
        }
    }
}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-program',
    templateUrl: 'html/ncc-card-program.component.html',
    //styleUrls: ['ncc-card-program.component.css']
})

export class NccCardProgramComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    // @Input() linkLabel: string;
    @Input() linkUrl: string;

    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elpreview && this.elcontent) {
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elpreview && this.elcontent) {
            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elpreview && this.elcontent) {
            contentAnimation.start(this.elcontent.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
        }*/

        if (this.elpreview && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elpreview.nativeElement.style.opacity = '1';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-quote',
    templateUrl: 'html/ncc-card-quote.component.html',
    //styleUrls: ['ncc-card-quote.component.css']
})

export class NccCardQuoteComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() quote: string;
    @Input() name: string;
    @Input() tagLabel: string;
    @Input() tagUrl: string;

    @ViewChild('elbackground') elbackground: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let backgroundAnimation = this._ab.css();
        backgroundAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.3' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elbackground && this.elcontent) {
            backgroundAnimation.start(this.elbackground.nativeElement)
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elbackground && this.elcontent) {
            this.elbackground.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let backgroundAnimation = this._ab.css();
        backgroundAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '0.3' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elbackground && this.elcontent) {
            backgroundAnimation.start(this.elbackground.nativeElement)
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elbackground && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elbackground.nativeElement.style.opacity = '0.3';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-story',
    templateUrl: 'html/ncc-card-story.component.html',
    //styleUrls: ['ncc-card-story.component.css']
})

export class NccCardStoryComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() name: string;
    // @Input() tagLabel: string;
    @Input() tagUrl: string;
    @Input() image: string;

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('eltexture') eltexture: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public ngOnInit(): void {
        if(this.image != ""){
            this.eltexture.nativeElement.style.opacity = '1';
            this.elpreview.nativeElement.style.opacity = '1';
        }
    }

    public activeStateContentAnimation(): void {

        if (this.elimage && this.eltexture && this.elcontent) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.eltexture.nativeElement.style.opacity = '0.75';
            this.eltexture.nativeElement.style.pointerEvents = 'auto';

            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {

        if (this.elimage && this.eltexture && this.elcontent) {

            this.eltexture.nativeElement.style.opacity = '0';
            this.elpreview.nativeElement.style.opacity = '0';

            if(this.image != ""){
                this.eltexture.nativeElement.style.opacity = '1';
                this.elpreview.nativeElement.style.opacity = '1';
            }
            this.eltexture.nativeElement.style.pointerEvents = 'none';

            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elimage.nativeElement.style.transform = 'scale(1.25)';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-template', // element, [attribute], .class, and :not()
    // template: `Hello {{name}}`,
    templateUrl: 'html/ncc-card-template.component.html',
    //     styles: [`
    // :host {
    //     display: block;

    //     .primary {
    //         color: red;
    //     }
    // }
    //     `],
    //styleUrls: ['ncc-card-template.component.css'],
    // directives: [MyDirective, MyComponent],
    // pipes: [MyPipe, OtherPipe],
    // providers: [MyService, provide(...)],
})

export class NccCardTemplateComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    @Input() linkLabel: string;
    @Input() linkUrl: string;
    @Input() tagLabel: string;
    @Input() tagUrl: string;
    @Input() image: string;
    @Input() graphic: string;
    @Input() icon: string;

    @ViewChild('eltitle') eltitle: ElementRef;
    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elgraphic') elgraphic: ElementRef;
    @ViewChild('elicon') elicon: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.elcontent && this.eltitle) {
            headingAnimation.start(this.eltitle.nativeElement)
                .onComplete(() => {
                    contentAnimation.start(this.elcontent.nativeElement);
                });
        }*/

        if (this.eltitle && this.elcontent) {
            this.eltitle.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.opacity = '1';
        }

        // .onComplete(() => { })

        // 'pointer-events': 'none',
        // 'pointer-events': 'auto',
    }

    public inactiveStateContentAnimation(): void {
        /*let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({ 'top': '0' })
            .setToStyles({ 'top': '-500px' })

        let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(500)
            .setFromStyles({ 'bottom': '0' })
            .setToStyles({ 'bottom': '-55px' })

        if (this.elcontent && this.eltitle) {
            contentAnimation.start(this.elcontent.nativeElement)
                .onComplete(() => {
                    headingAnimation.start(this.eltitle.nativeElement);
                });
        }*/

        if (this.eltitle && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.eltitle.nativeElement.style.opacity = '1';
        }

        // .onComplete(() => { })

        // 'pointer-events': 'none',
        // 'pointer-events': 'auto',
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-title',
    templateUrl: 'html/ncc-card-title.component.html',
    //styleUrls: ['ncc-card-title.component.css']
})

export class NccCardTitleComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() icon: string;

    @ViewChild('eltitle1') eltitle1: ElementRef;
    @ViewChild('eltitle2') eltitle2: ElementRef;
    @ViewChild('elicon') elicon: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.15' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.eltitle1 && this.eltitle2 && this.elicon) {
            headingAnimation.start(this.eltitle1.nativeElement);
            contentAnimation.start(this.eltitle2.nativeElement);
            contentAnimation.start(this.elicon.nativeElement);
        }*/

        if (this.eltitle1 && this.eltitle2) {
            this.eltitle1.nativeElement.style.opacity = '0';
            this.eltitle2.nativeElement.style.opacity = '1';
        }

        if (this.elicon) {
            this.elicon.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '0.15' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        if (this.eltitle1 && this.eltitle2 && this.elicon) {
            contentAnimation.start(this.eltitle2.nativeElement);
            contentAnimation.start(this.elicon.nativeElement);
            headingAnimation.start(this.eltitle1.nativeElement);
        }*/

        if (this.eltitle1 && this.eltitle2) {
            this.eltitle2.nativeElement.style.opacity = '0';
            this.eltitle1.nativeElement.style.opacity = '0.15';
        }

        if (this.elicon) {
            this.elicon.nativeElement.style.opacity = '0';
        }
    }

}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-twitter',
    templateUrl: 'html/ncc-card-twitter.component.html',
    //styleUrls: ['ncc-card-twitter.component.css']
})

export class NccCardTwitterComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() icon: string;
    @Input() name: string;
    @Input() account: string;
    @Input() date: string;
    @Input() content: string;
    @Input() profileUrl: string;

    @ViewChild('elwrapper') elwrapper: ElementRef;
    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let wrapperAnimation = this._ab.css();
        wrapperAnimation
            .setDuration(500)
            .setFromStyles({ 'background-color': '#00aced' })
            .setToStyles({ 'background-color': '#fff' })

        let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elwrapper && this.elpreview && this.elcontent) {
            wrapperAnimation.start(this.elwrapper.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elwrapper && this.elpreview && this.elcontent) {
            this.elwrapper.nativeElement.style.backgroundColor = '#fff';

            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1'
            this.elcontent.nativeElement.style.pointerEvents = 'auto'
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let wrapperAnimation = this._ab.css();
        wrapperAnimation
            .setDuration(500)
            .setFromStyles({ 'background-color': '#fff' })
            .setToStyles({ 'background-color': '#00aced' })

        let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elwrapper && this.elpreview && this.elcontent) {
            wrapperAnimation.start(this.elwrapper.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elwrapper && this.elpreview && this.elcontent) {
            this.elwrapper.nativeElement.style.backgroundColor = '#00aced';

            this.elpreview.nativeElement.style.opacity = '1';

            this.elcontent.nativeElement.style.opacity = '0'
            this.elcontent.nativeElement.style.pointerEvents = 'none'
        }
    }

}

declare var nccModal: any;

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-video',
    templateUrl: 'html/ncc-card-video.component.html',
    //styleUrls: ['ncc-card-video.component.css']
})

export class NccCardVideoComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() image: string;
    @Input() modalContentId: string;  // will be used for Modal content - trigger modal

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elicon') elicon: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(400)
            .setFromStyles({
                'bottom': '50%',
                'right': '50%',
                'transform': 'translate(50%, 50%)',
                'pointer-events': 'none'
            })
            .setToStyles({
                'bottom': '25px',
                'right': '17px',
                'transform': 'none',
                'pointer-events': 'auto'
            })

        if (this.elimage && this.elicon) {
            imageAnimation.start(this.elimage.nativeElement);
            iconAnimation.start(this.elicon.nativeElement);
        }*/

        if (this.elimage && this.elicon) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elicon.nativeElement.style.bottom = '25px';
            this.elicon.nativeElement.style.right = '17px';
            this.elicon.nativeElement.style.transform = 'none';
            this.elicon.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(400)
            .setFromStyles({
                'bottom': '25px',
                'right': '17px',
                'transform': 'none',
                'pointer-events': 'auto'
            })
            .setToStyles({

                'bottom': '50%',
                'right': '50%',
                'transform': 'translate(50%, 50%)',
                'pointer-events': 'none'
            })

        if (this.elimage && this.elicon) {
            imageAnimation.start(this.elimage.nativeElement);
            iconAnimation.start(this.elicon.nativeElement);
        }*/

        if (this.elimage && this.elicon) {
            this.elimage.nativeElement.style.transform = 'scale(1.25)';

            this.elicon.nativeElement.style.bottom = '50%';
            this.elicon.nativeElement.style.right = '50%';
            this.elicon.nativeElement.style.transform = 'translate(50%, 50%)';
            this.elicon.nativeElement.style.pointerEvents = 'none';
        }
    }

    public openModal(event: MouseEvent) {
        if (this.modalContentId) {
            event.preventDefault();
            nccModal.displayModal(event, this.modalContentId);
        }
    }

}
