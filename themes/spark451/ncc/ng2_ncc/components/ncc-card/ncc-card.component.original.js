System.register(["@angular/core", "../ncc-stack/ncc-stack.component", "../ncc-card/ncc-card.base"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_stack_component_1, NccCardComponent, core_2, ncc_card_base_1, NccCardCareerComponent, NccCardCtaComponent, NccCardCtaPhotoComponent, NccCardEventComponent, NccCardFacebookComponent, NccCardFactoidComponent, NccCardFeatureComponent, NccCardGalleryComponent, NccCardImageComponent, NccCardInstagramComponent, NccCardMapComponent, NccCardNewsComponent, NccCardProfileComponent, NccCardProgramComponent, NccCardQuoteComponent, NccCardStoryComponent, NccCardTemplateComponent, NccCardTitleComponent, NccCardTwitterComponent, NccCardVideoComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
                core_2 = core_1_1;
            },
            function (ncc_stack_component_1_1) {
                ncc_stack_component_1 = ncc_stack_component_1_1;
            },
            function (ncc_card_base_1_1) {
                ncc_card_base_1 = ncc_card_base_1_1;
            }
        ],
        execute: function () {
            NccCardComponent = (function () {
                function NccCardComponent(cards, _el) {
                    this.cards = cards;
                    this._el = _el;
                    this.cardState = 'inactive';
                    this._cardTransition = 'all 0.5s cubic-bezier(0.35, 0, 0.25, 1)';
                    this.cards = cards;
                }
                Object.defineProperty(NccCardComponent.prototype, "isActive", {
                    get: function () {
                        return this._isActive;
                    },
                    set: function (value) {
                        this._isActive = value;
                        if (this._isActive) {
                            this.cardState = 'active';
                        }
                        else {
                            this.cardState = 'inactive';
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NccCardComponent.prototype, "isPeekAdjacent", {
                    get: function () {
                        return this._isPeekAdjacent;
                    },
                    set: function (value) {
                        this._isPeekAdjacent = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NccCardComponent.prototype, "isPeek", {
                    get: function () {
                        return this._isPeek;
                    },
                    set: function (value) {
                        this._isPeek = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                NccCardComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.cardClass = this.cardClass || 'card-default';
                    this.cards.addCard(this);
                    setTimeout(function () {
                        _this._el.nativeElement.style.transition = _this._cardTransition;
                    });
                };
                NccCardComponent.prototype.ngOnDestroy = function () {
                    this.cards.removeCard(this);
                };
                NccCardComponent.prototype.activateCard = function (event) {
                    if (!this.isActive) {
                        this.cards.activateCard(this);
                    }
                };
                NccCardComponent.prototype.mouseEnter = function (event) {
                    if (!this.isActive) {
                        this.cards.activateCard(this);
                    }
                };
                NccCardComponent.prototype.mouseLeave = function (event) {
                };
                NccCardComponent.prototype.updateZIndex = function (zIndex) {
                    this._el.nativeElement.style.zIndex = zIndex;
                };
                NccCardComponent.prototype.activeStateCardAnimation = function () {
                    this._el.nativeElement.style.marginLeft = '-5px';
                    this._el.nativeElement.style.marginRight = '-5px';
                };
                NccCardComponent.prototype.inactiveStateCardAnimation = function (zIndex) {
                    this._el.nativeElement.style.marginLeft = '-5px';
                    this._el.nativeElement.style.marginRight = '-5px';
                    this._el.nativeElement.style.zIndex = zIndex;
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardComponent.prototype, "cardClass", void 0);
                __decorate([
                    core_1.HostBinding('class.active'),
                    core_1.Input(),
                    __metadata("design:type", Boolean),
                    __metadata("design:paramtypes", [Boolean])
                ], NccCardComponent.prototype, "isActive", null);
                __decorate([
                    core_1.HostBinding('class.peek-adjacent'),
                    core_1.Input(),
                    __metadata("design:type", Boolean),
                    __metadata("design:paramtypes", [Boolean])
                ], NccCardComponent.prototype, "isPeekAdjacent", null);
                __decorate([
                    core_1.HostBinding('class.peek'),
                    core_1.Input(),
                    __metadata("design:type", Boolean),
                    __metadata("design:paramtypes", [Boolean])
                ], NccCardComponent.prototype, "isPeek", null);
                NccCardComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card',
                        template: "\n    <div [ngClass]=\"cardClass\" (click)=\"activateCard($event)\" (mouseenter)=\"mouseEnter($event)\" (mouseleave)=\"mouseLeave($event)\">\n        <ng-content></ng-content>\n    </div>\n    ",
                        styleUrls: ['ncc-card.component.css']
                    }),
                    __metadata("design:paramtypes", [ncc_stack_component_1.NccStackComponent, typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object])
                ], NccCardComponent);
                return NccCardComponent;
                var _a;
            }());
            exports_1("NccCardComponent", NccCardComponent);
            NccCardCareerComponent = (function (_super) {
                __extends(NccCardCareerComponent, _super);
                function NccCardCareerComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardCareerComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elgraphic && this.elcontent) {
                        this.elgraphic.nativeElement.style.opacity = '0.1';
                        this.elgraphic.nativeElement.style.color = '#35393c';
                        this.elcontent.nativeElement.style.opacity = '1';
                    }
                };
                NccCardCareerComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elgraphic && this.elcontent) {
                        this.elgraphic.nativeElement.style.opacity = '0.7';
                        this.elgraphic.nativeElement.style.color = '#689f46';
                        this.elcontent.nativeElement.style.opacity = '0';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCareerComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCareerComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCareerComponent.prototype, "description", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCareerComponent.prototype, "linkLabel", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCareerComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_2.ViewChild('elgraphic'),
                    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
                ], NccCardCareerComponent.prototype, "elgraphic", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object)
                ], NccCardCareerComponent.prototype, "elcontent", void 0);
                NccCardCareerComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-career',
                        templateUrl: 'html/ncc-card-career.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_d = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _d || Object])
                ], NccCardCareerComponent);
                return NccCardCareerComponent;
                var _b, _c, _d;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardCareerComponent", NccCardCareerComponent);
            NccCardCtaComponent = (function (_super) {
                __extends(NccCardCtaComponent, _super);
                function NccCardCtaComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardCtaComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elcontent && this.elbutton) {
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                        this.elbutton.nativeElement.style.opacity = '1';
                        this.elbutton.nativeElement.style.pointerEvents = 'auto';
                    }
                    if (this.elicon) {
                        this.elicon.nativeElement.style.opacity = '0';
                    }
                };
                NccCardCtaComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elcontent && this.elbutton) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                        this.elbutton.nativeElement.style.opacity = '0';
                        this.elbutton.nativeElement.style.pointerEvents = 'none';
                    }
                    if (this.elicon) {
                        this.elicon.nativeElement.style.opacity = '0.7';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "icon", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "linkLabel", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_2.ViewChild('elicon'),
                    __metadata("design:type", typeof (_e = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _e || Object)
                ], NccCardCtaComponent.prototype, "elicon", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_f = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _f || Object)
                ], NccCardCtaComponent.prototype, "elcontent", void 0);
                __decorate([
                    core_2.ViewChild('elbutton'),
                    __metadata("design:type", typeof (_g = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _g || Object)
                ], NccCardCtaComponent.prototype, "elbutton", void 0);
                NccCardCtaComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-cta',
                        templateUrl: 'html/ncc-card-cta.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_h = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _h || Object])
                ], NccCardCtaComponent);
                return NccCardCtaComponent;
                var _e, _f, _g, _h;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardCtaComponent", NccCardCtaComponent);
            NccCardCtaPhotoComponent = (function (_super) {
                __extends(NccCardCtaPhotoComponent, _super);
                function NccCardCtaPhotoComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardCtaPhotoComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage && this.elbutton && this.eltitle) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                        this.elbutton.nativeElement.style.top = '0';
                        this.elbutton.nativeElement.style.right = '0';
                        this.elbutton.nativeElement.style.width = '50px';
                        this.elbutton.nativeElement.style.height = '100%';
                        this.elbutton.nativeElement.style.border = 'none';
                        this.elbutton.nativeElement.style.transform = 'none';
                        this.elbutton.nativeElement.style.borderRadius = '0';
                        this.elbutton.nativeElement.style.backgroundColor = 'rgba(53,57,60,0.8)';
                        this.elbutton.nativeElement.style.pointerEvents = 'auto';
                        this.eltitle.nativeElement.style.opacity = '1';
                    }
                };
                NccCardCtaPhotoComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage && this.elbutton && this.eltitle) {
                        this.eltitle.nativeElement.style.opacity = '0';
                        this.elimage.nativeElement.style.transform = 'scale(1.15)';
                        this.elbutton.nativeElement.style.top = '50%';
                        this.elbutton.nativeElement.style.right = '50%';
                        this.elbutton.nativeElement.style.width = '55px';
                        this.elbutton.nativeElement.style.height = '55px';
                        this.elbutton.nativeElement.style.border = '3px solid rgba(255,192,59,0.7)';
                        this.elbutton.nativeElement.style.transform = 'translate(50%, -50%)';
                        this.elbutton.nativeElement.style.borderRadius = '50%';
                        this.elbutton.nativeElement.style.backgroundColor = 'transparent';
                        this.elbutton.nativeElement.style.pointerEvents = 'none';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaPhotoComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaPhotoComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaPhotoComponent.prototype, "image", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaPhotoComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_j = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _j || Object)
                ], NccCardCtaPhotoComponent.prototype, "elimage", void 0);
                __decorate([
                    core_2.ViewChild('eltitle'),
                    __metadata("design:type", typeof (_k = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _k || Object)
                ], NccCardCtaPhotoComponent.prototype, "eltitle", void 0);
                __decorate([
                    core_2.ViewChild('elbutton'),
                    __metadata("design:type", typeof (_l = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _l || Object)
                ], NccCardCtaPhotoComponent.prototype, "elbutton", void 0);
                NccCardCtaPhotoComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-cta-photo',
                        templateUrl: 'html/ncc-card-cta-photo.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_m = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _m || Object])
                ], NccCardCtaPhotoComponent);
                return NccCardCtaPhotoComponent;
                var _j, _k, _l, _m;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardCtaPhotoComponent", NccCardCtaPhotoComponent);
            NccCardEventComponent = (function (_super) {
                __extends(NccCardEventComponent, _super);
                function NccCardEventComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardEventComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elpreview && this.elcontent && this.elbutton) {
                        this.elpreview.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                        this.elbutton.nativeElement.style.opacity = '1';
                        this.elbutton.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardEventComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elpreview && this.elcontent && this.elbutton) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                        this.elbutton.nativeElement.style.opacity = '0';
                        this.elbutton.nativeElement.style.pointerEvents = 'none';
                        this.elpreview.nativeElement.style.opacity = '1';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "heading", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "description", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "timeStart", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "timeEnd", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "location", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "shortDate", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardEventComponent.prototype, "day", void 0);
                __decorate([
                    core_2.ViewChild('elpreview'),
                    __metadata("design:type", typeof (_o = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _o || Object)
                ], NccCardEventComponent.prototype, "elpreview", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_p = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _p || Object)
                ], NccCardEventComponent.prototype, "elcontent", void 0);
                __decorate([
                    core_2.ViewChild('elbutton'),
                    __metadata("design:type", typeof (_q = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _q || Object)
                ], NccCardEventComponent.prototype, "elbutton", void 0);
                NccCardEventComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-event',
                        templateUrl: 'html/ncc-card-event.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_r = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _r || Object])
                ], NccCardEventComponent);
                return NccCardEventComponent;
                var _o, _p, _q, _r;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardEventComponent", NccCardEventComponent);
            NccCardFacebookComponent = (function (_super) {
                __extends(NccCardFacebookComponent, _super);
                function NccCardFacebookComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardFacebookComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elwrapper && this.elpreview && this.elcontent) {
                        this.elwrapper.nativeElement.style.backgroundColor = '#fff';
                        this.elpreview.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardFacebookComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elwrapper && this.elpreview && this.elcontent) {
                        this.elwrapper.nativeElement.style.backgroundColor = '#3b5998';
                        this.elpreview.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFacebookComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFacebookComponent.prototype, "icon", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFacebookComponent.prototype, "name", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFacebookComponent.prototype, "date", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFacebookComponent.prototype, "content", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFacebookComponent.prototype, "profileUrl", void 0);
                __decorate([
                    core_2.ViewChild('elwrapper'),
                    __metadata("design:type", typeof (_s = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _s || Object)
                ], NccCardFacebookComponent.prototype, "elwrapper", void 0);
                __decorate([
                    core_2.ViewChild('elpreview'),
                    __metadata("design:type", typeof (_t = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _t || Object)
                ], NccCardFacebookComponent.prototype, "elpreview", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_u = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _u || Object)
                ], NccCardFacebookComponent.prototype, "elcontent", void 0);
                NccCardFacebookComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-facebook',
                        templateUrl: 'html/ncc-card-facebook.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_v = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _v || Object])
                ], NccCardFacebookComponent);
                return NccCardFacebookComponent;
                var _s, _t, _u, _v;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardFacebookComponent", NccCardFacebookComponent);
            NccCardFactoidComponent = (function (_super) {
                __extends(NccCardFactoidComponent, _super);
                function NccCardFactoidComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardFactoidComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elpreview && this.elcontent) {
                        this.elpreview.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.opacity = '1';
                    }
                };
                NccCardFactoidComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elpreview && this.elcontent) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elpreview.nativeElement.style.opacity = '1';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFactoidComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFactoidComponent.prototype, "fact", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFactoidComponent.prototype, "description", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFactoidComponent.prototype, "icon", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFactoidComponent.prototype, "source", void 0);
                __decorate([
                    core_2.ViewChild('elpreview'),
                    __metadata("design:type", typeof (_w = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _w || Object)
                ], NccCardFactoidComponent.prototype, "elpreview", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_x = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _x || Object)
                ], NccCardFactoidComponent.prototype, "elcontent", void 0);
                NccCardFactoidComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-factoid',
                        templateUrl: 'html/ncc-card-factoid.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_y = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _y || Object])
                ], NccCardFactoidComponent);
                return NccCardFactoidComponent;
                var _w, _x, _y;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardFactoidComponent", NccCardFactoidComponent);
            NccCardFeatureComponent = (function (_super) {
                __extends(NccCardFeatureComponent, _super);
                function NccCardFeatureComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardFeatureComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage && this.elcontent) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardFeatureComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage && this.elcontent) {
                        this.elimage.nativeElement.style.transform = 'scale(1.25)';
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFeatureComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFeatureComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFeatureComponent.prototype, "description", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFeatureComponent.prototype, "image", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardFeatureComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_z = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _z || Object)
                ], NccCardFeatureComponent.prototype, "elimage", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_0 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _0 || Object)
                ], NccCardFeatureComponent.prototype, "elcontent", void 0);
                NccCardFeatureComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-feature',
                        templateUrl: 'html/ncc-card-feature.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_1 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _1 || Object])
                ], NccCardFeatureComponent);
                return NccCardFeatureComponent;
                var _z, _0, _1;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardFeatureComponent", NccCardFeatureComponent);
            NccCardGalleryComponent = (function (_super) {
                __extends(NccCardGalleryComponent, _super);
                function NccCardGalleryComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardGalleryComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage && this.elicon) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                        this.elicon.nativeElement.style.bottom = '25px';
                        this.elicon.nativeElement.style.right = '17px';
                        this.elicon.nativeElement.style.transform = 'none';
                        this.elicon.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardGalleryComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage && this.elicon) {
                        this.elimage.nativeElement.style.transform = 'scale(1.25)';
                        this.elicon.nativeElement.style.bottom = '50%';
                        this.elicon.nativeElement.style.right = '50%';
                        this.elicon.nativeElement.style.transform = 'translate(50%, 50%)';
                        this.elicon.nativeElement.style.pointerEvents = 'none';
                    }
                };
                NccCardGalleryComponent.prototype.openModal = function (event) {
                    if (this.modalContentId) {
                        event.preventDefault();
                        nccModal.displayModal(event, this.modalContentId);
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardGalleryComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardGalleryComponent.prototype, "image", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardGalleryComponent.prototype, "modalContentId", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_2 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _2 || Object)
                ], NccCardGalleryComponent.prototype, "elimage", void 0);
                __decorate([
                    core_2.ViewChild('elicon'),
                    __metadata("design:type", typeof (_3 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _3 || Object)
                ], NccCardGalleryComponent.prototype, "elicon", void 0);
                NccCardGalleryComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-gallery',
                        templateUrl: 'html/ncc-card-gallery.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_4 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _4 || Object])
                ], NccCardGalleryComponent);
                return NccCardGalleryComponent;
                var _2, _3, _4;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardGalleryComponent", NccCardGalleryComponent);
            NccCardImageComponent = (function (_super) {
                __extends(NccCardImageComponent, _super);
                function NccCardImageComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardImageComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                    }
                };
                NccCardImageComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage) {
                        this.elimage.nativeElement.style.transform = 'scale(1.25)';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardImageComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardImageComponent.prototype, "image", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_5 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _5 || Object)
                ], NccCardImageComponent.prototype, "elimage", void 0);
                NccCardImageComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-image',
                        templateUrl: 'html/ncc-card-image.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_6 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _6 || Object])
                ], NccCardImageComponent);
                return NccCardImageComponent;
                var _5, _6;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardImageComponent", NccCardImageComponent);
            NccCardInstagramComponent = (function (_super) {
                __extends(NccCardInstagramComponent, _super);
                function NccCardInstagramComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardInstagramComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                    }
                };
                NccCardInstagramComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage) {
                        this.elimage.nativeElement.style.transform = 'scale(1.25)';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardInstagramComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardInstagramComponent.prototype, "icon", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardInstagramComponent.prototype, "name", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardInstagramComponent.prototype, "date", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardInstagramComponent.prototype, "image", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardInstagramComponent.prototype, "profileUrl", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_7 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _7 || Object)
                ], NccCardInstagramComponent.prototype, "elimage", void 0);
                NccCardInstagramComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-instagram',
                        templateUrl: 'html/ncc-card-instagram.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_8 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _8 || Object])
                ], NccCardInstagramComponent);
                return NccCardInstagramComponent;
                var _7, _8;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardInstagramComponent", NccCardInstagramComponent);
            NccCardMapComponent = (function (_super) {
                __extends(NccCardMapComponent, _super);
                function NccCardMapComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardMapComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elmap && this.elcontent) {
                        this.elmap.nativeElement.style.transform = 'scale(1)';
                        this.elmap.nativeElement.style.pointerEvents = 'auto';
                        this.elcontent.nativeElement.style.opacity = '1';
                    }
                };
                NccCardMapComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elmap && this.elcontent) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elmap.nativeElement.style.transform = 'scale(1.25)';
                        this.elmap.nativeElement.style.pointerEvents = 'none';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardMapComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardMapComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardMapComponent.prototype, "description", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardMapComponent.prototype, "googleMap", void 0);
                __decorate([
                    core_2.ViewChild('elmap'),
                    __metadata("design:type", typeof (_9 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _9 || Object)
                ], NccCardMapComponent.prototype, "elmap", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_10 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _10 || Object)
                ], NccCardMapComponent.prototype, "elcontent", void 0);
                NccCardMapComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-map',
                        templateUrl: 'html/ncc-card-map.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_11 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _11 || Object])
                ], NccCardMapComponent);
                return NccCardMapComponent;
                var _9, _10, _11;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardMapComponent", NccCardMapComponent);
            NccCardNewsComponent = (function (_super) {
                __extends(NccCardNewsComponent, _super);
                function NccCardNewsComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardNewsComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elpreview && this.elcontent) {
                        this.elpreview.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardNewsComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elpreview && this.elcontent) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                        this.elpreview.nativeElement.style.opacity = '1';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardNewsComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardNewsComponent.prototype, "articleType", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardNewsComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardNewsComponent.prototype, "date", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardNewsComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_2.ViewChild('elpreview'),
                    __metadata("design:type", typeof (_12 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _12 || Object)
                ], NccCardNewsComponent.prototype, "elpreview", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_13 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _13 || Object)
                ], NccCardNewsComponent.prototype, "elcontent", void 0);
                NccCardNewsComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-news',
                        templateUrl: 'html/ncc-card-news.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_14 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _14 || Object])
                ], NccCardNewsComponent);
                return NccCardNewsComponent;
                var _12, _13, _14;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardNewsComponent", NccCardNewsComponent);
            NccCardProfileComponent = (function (_super) {
                __extends(NccCardProfileComponent, _super);
                function NccCardProfileComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardProfileComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage && this.elcontent) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardProfileComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage && this.elcontent) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                        this.elimage.nativeElement.style.transform = 'scale(1.25)';
                    }
                };
                NccCardProfileComponent.prototype.openProfile = function (event) {
                    if (this.trayContentId) {
                        event.preventDefault();
                        nccTray.toggleTray(event, this.trayContentId);
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "name", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "linkLabel", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "trayContentId", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "tagLabel", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "tagUrl", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProfileComponent.prototype, "image", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_15 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _15 || Object)
                ], NccCardProfileComponent.prototype, "elimage", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_16 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _16 || Object)
                ], NccCardProfileComponent.prototype, "elcontent", void 0);
                NccCardProfileComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-profile',
                        templateUrl: 'html/ncc-card-profile.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_17 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _17 || Object])
                ], NccCardProfileComponent);
                return NccCardProfileComponent;
                var _15, _16, _17;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardProfileComponent", NccCardProfileComponent);
            NccCardProgramComponent = (function (_super) {
                __extends(NccCardProgramComponent, _super);
                function NccCardProgramComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardProgramComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elpreview && this.elcontent) {
                        this.elpreview.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardProgramComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elpreview && this.elcontent) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                        this.elpreview.nativeElement.style.opacity = '1';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProgramComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProgramComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProgramComponent.prototype, "description", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardProgramComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_2.ViewChild('elpreview'),
                    __metadata("design:type", typeof (_18 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _18 || Object)
                ], NccCardProgramComponent.prototype, "elpreview", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_19 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _19 || Object)
                ], NccCardProgramComponent.prototype, "elcontent", void 0);
                NccCardProgramComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-program',
                        templateUrl: 'html/ncc-card-program.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_20 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _20 || Object])
                ], NccCardProgramComponent);
                return NccCardProgramComponent;
                var _18, _19, _20;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardProgramComponent", NccCardProgramComponent);
            NccCardQuoteComponent = (function (_super) {
                __extends(NccCardQuoteComponent, _super);
                function NccCardQuoteComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardQuoteComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elbackground && this.elcontent) {
                        this.elbackground.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardQuoteComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elbackground && this.elcontent) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                        this.elbackground.nativeElement.style.opacity = '0.3';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardQuoteComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardQuoteComponent.prototype, "quote", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardQuoteComponent.prototype, "name", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardQuoteComponent.prototype, "tagLabel", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardQuoteComponent.prototype, "tagUrl", void 0);
                __decorate([
                    core_2.ViewChild('elbackground'),
                    __metadata("design:type", typeof (_21 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _21 || Object)
                ], NccCardQuoteComponent.prototype, "elbackground", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_22 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _22 || Object)
                ], NccCardQuoteComponent.prototype, "elcontent", void 0);
                NccCardQuoteComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-quote',
                        templateUrl: 'html/ncc-card-quote.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_23 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _23 || Object])
                ], NccCardQuoteComponent);
                return NccCardQuoteComponent;
                var _21, _22, _23;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardQuoteComponent", NccCardQuoteComponent);
            NccCardStoryComponent = (function (_super) {
                __extends(NccCardStoryComponent, _super);
                function NccCardStoryComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardStoryComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage && this.eltexture && this.elcontent) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                        this.eltexture.nativeElement.style.opacity = '1';
                        this.eltexture.nativeElement.style.pointerEvents = 'auto';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardStoryComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage && this.eltexture && this.elcontent) {
                        this.eltexture.nativeElement.style.opacity = '0';
                        this.eltexture.nativeElement.style.pointerEvents = 'none';
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                        this.elimage.nativeElement.style.transform = 'scale(1.25)';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardStoryComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardStoryComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardStoryComponent.prototype, "name", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardStoryComponent.prototype, "tagUrl", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardStoryComponent.prototype, "image", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_24 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _24 || Object)
                ], NccCardStoryComponent.prototype, "elimage", void 0);
                __decorate([
                    core_2.ViewChild('eltexture'),
                    __metadata("design:type", typeof (_25 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _25 || Object)
                ], NccCardStoryComponent.prototype, "eltexture", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_26 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _26 || Object)
                ], NccCardStoryComponent.prototype, "elcontent", void 0);
                NccCardStoryComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-story',
                        templateUrl: 'html/ncc-card-story.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_27 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _27 || Object])
                ], NccCardStoryComponent);
                return NccCardStoryComponent;
                var _24, _25, _26, _27;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardStoryComponent", NccCardStoryComponent);
            NccCardTemplateComponent = (function (_super) {
                __extends(NccCardTemplateComponent, _super);
                function NccCardTemplateComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardTemplateComponent.prototype.activeStateContentAnimation = function () {
                    if (this.eltitle && this.elcontent) {
                        this.eltitle.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.opacity = '1';
                    }
                };
                NccCardTemplateComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.eltitle && this.elcontent) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.eltitle.nativeElement.style.opacity = '1';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "description", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "linkLabel", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "tagLabel", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "tagUrl", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "image", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "graphic", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTemplateComponent.prototype, "icon", void 0);
                __decorate([
                    core_2.ViewChild('eltitle'),
                    __metadata("design:type", typeof (_28 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _28 || Object)
                ], NccCardTemplateComponent.prototype, "eltitle", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_29 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _29 || Object)
                ], NccCardTemplateComponent.prototype, "elimage", void 0);
                __decorate([
                    core_2.ViewChild('elgraphic'),
                    __metadata("design:type", typeof (_30 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _30 || Object)
                ], NccCardTemplateComponent.prototype, "elgraphic", void 0);
                __decorate([
                    core_2.ViewChild('elicon'),
                    __metadata("design:type", typeof (_31 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _31 || Object)
                ], NccCardTemplateComponent.prototype, "elicon", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_32 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _32 || Object)
                ], NccCardTemplateComponent.prototype, "elcontent", void 0);
                NccCardTemplateComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-template',
                        templateUrl: 'html/ncc-card-template.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_33 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _33 || Object])
                ], NccCardTemplateComponent);
                return NccCardTemplateComponent;
                var _28, _29, _30, _31, _32, _33;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardTemplateComponent", NccCardTemplateComponent);
            NccCardTitleComponent = (function (_super) {
                __extends(NccCardTitleComponent, _super);
                function NccCardTitleComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardTitleComponent.prototype.activeStateContentAnimation = function () {
                    if (this.eltitle1 && this.eltitle2) {
                        this.eltitle1.nativeElement.style.opacity = '0';
                        this.eltitle2.nativeElement.style.opacity = '1';
                    }
                    if (this.elicon) {
                        this.elicon.nativeElement.style.opacity = '1';
                    }
                };
                NccCardTitleComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.eltitle1 && this.eltitle2) {
                        this.eltitle2.nativeElement.style.opacity = '0';
                        this.eltitle1.nativeElement.style.opacity = '0.15';
                    }
                    if (this.elicon) {
                        this.elicon.nativeElement.style.opacity = '0';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTitleComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTitleComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTitleComponent.prototype, "icon", void 0);
                __decorate([
                    core_2.ViewChild('eltitle1'),
                    __metadata("design:type", typeof (_34 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _34 || Object)
                ], NccCardTitleComponent.prototype, "eltitle1", void 0);
                __decorate([
                    core_2.ViewChild('eltitle2'),
                    __metadata("design:type", typeof (_35 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _35 || Object)
                ], NccCardTitleComponent.prototype, "eltitle2", void 0);
                __decorate([
                    core_2.ViewChild('elicon'),
                    __metadata("design:type", typeof (_36 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _36 || Object)
                ], NccCardTitleComponent.prototype, "elicon", void 0);
                NccCardTitleComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-title',
                        templateUrl: 'html/ncc-card-title.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_37 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _37 || Object])
                ], NccCardTitleComponent);
                return NccCardTitleComponent;
                var _34, _35, _36, _37;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardTitleComponent", NccCardTitleComponent);
            NccCardTwitterComponent = (function (_super) {
                __extends(NccCardTwitterComponent, _super);
                function NccCardTwitterComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardTwitterComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elwrapper && this.elpreview && this.elcontent) {
                        this.elwrapper.nativeElement.style.backgroundColor = '#fff';
                        this.elpreview.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardTwitterComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elwrapper && this.elpreview && this.elcontent) {
                        this.elwrapper.nativeElement.style.backgroundColor = '#00aced';
                        this.elpreview.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTwitterComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTwitterComponent.prototype, "icon", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTwitterComponent.prototype, "name", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTwitterComponent.prototype, "account", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTwitterComponent.prototype, "date", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTwitterComponent.prototype, "content", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTwitterComponent.prototype, "profileUrl", void 0);
                __decorate([
                    core_2.ViewChild('elwrapper'),
                    __metadata("design:type", typeof (_38 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _38 || Object)
                ], NccCardTwitterComponent.prototype, "elwrapper", void 0);
                __decorate([
                    core_2.ViewChild('elpreview'),
                    __metadata("design:type", typeof (_39 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _39 || Object)
                ], NccCardTwitterComponent.prototype, "elpreview", void 0);
                __decorate([
                    core_2.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_40 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _40 || Object)
                ], NccCardTwitterComponent.prototype, "elcontent", void 0);
                NccCardTwitterComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-twitter',
                        templateUrl: 'html/ncc-card-twitter.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_41 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _41 || Object])
                ], NccCardTwitterComponent);
                return NccCardTwitterComponent;
                var _38, _39, _40, _41;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardTwitterComponent", NccCardTwitterComponent);
            NccCardVideoComponent = (function (_super) {
                __extends(NccCardVideoComponent, _super);
                function NccCardVideoComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardVideoComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage && this.elicon) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                        this.elicon.nativeElement.style.bottom = '25px';
                        this.elicon.nativeElement.style.right = '17px';
                        this.elicon.nativeElement.style.transform = 'none';
                        this.elicon.nativeElement.style.pointerEvents = 'auto';
                    }
                };
                NccCardVideoComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage && this.elicon) {
                        this.elimage.nativeElement.style.transform = 'scale(1.25)';
                        this.elicon.nativeElement.style.bottom = '50%';
                        this.elicon.nativeElement.style.right = '50%';
                        this.elicon.nativeElement.style.transform = 'translate(50%, 50%)';
                        this.elicon.nativeElement.style.pointerEvents = 'none';
                    }
                };
                NccCardVideoComponent.prototype.openModal = function (event) {
                    if (this.modalContentId) {
                        event.preventDefault();
                        nccModal.displayModal(event, this.modalContentId);
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardVideoComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardVideoComponent.prototype, "image", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardVideoComponent.prototype, "modalContentId", void 0);
                __decorate([
                    core_2.ViewChild('elimage'),
                    __metadata("design:type", typeof (_42 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _42 || Object)
                ], NccCardVideoComponent.prototype, "elimage", void 0);
                __decorate([
                    core_2.ViewChild('elicon'),
                    __metadata("design:type", typeof (_43 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _43 || Object)
                ], NccCardVideoComponent.prototype, "elicon", void 0);
                NccCardVideoComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-video',
                        templateUrl: 'html/ncc-card-video.component.html',
                    }),
                    __metadata("design:paramtypes", [typeof (_44 = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _44 || Object])
                ], NccCardVideoComponent);
                return NccCardVideoComponent;
                var _42, _43, _44;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardVideoComponent", NccCardVideoComponent);
        }
    };
});
//# sourceMappingURL=ncc-card.component.js.map