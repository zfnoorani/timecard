import {Component, Input} from '@angular/core';

// TODO: temp solution
// import {Program} from '../ncc-program-finder/ncc-program-finder.interface';
export interface FilterChoice {
    tid: string;
    filter: string;
    title: string;
    color: string;
    active?: boolean;
}

export interface Program {
    nid: string;
    title: string;
    url: string;
    level: string;
    color: string;
    summary: string;
    stack: string;
    stackOrder?: number;
    filters: FilterChoice[];
    active?: boolean;
    darken?: boolean;
    order?: number;
}

@Component({
    moduleId: __moduleName,
    selector: 'ncc-program-card',
    templateUrl: 'ncc-program-card.component.html',
    styleUrls: ['ncc-program-card.component.css']
})

export class NccProgramCardComponent {
    @Input() program: Program;

    public constructor() { }

}
