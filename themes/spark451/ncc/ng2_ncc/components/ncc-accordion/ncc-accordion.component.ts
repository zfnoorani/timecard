import {Component, Input, HostBinding} from '@angular/core';

import {NccAccordionGroupComponent} from './ncc-accordion-group.component';

@Component({
    selector: 'ncc-accordion',
    template: `<ng-content></ng-content>`
})

export class NccAccordionComponent {
    @Input() public closeOthers: boolean;

    @HostBinding('class.panel-group')
    private addClass: boolean = true;

    private groups: Array<NccAccordionGroupComponent> = [];

    public closeOtherPanels(openGroup: NccAccordionGroupComponent): void {
        if (!this.closeOthers) {
            return;
        }

        this.groups.forEach((group: NccAccordionGroupComponent) => {
            if (group !== openGroup) {
                group.isOpen = false;
            }
        });
    }

    public addGroup(group: NccAccordionGroupComponent): void {
        this.groups.push(group);
    }

    public removeGroup(group: NccAccordionGroupComponent): void {
        let index = this.groups.indexOf(group);
        if (index !== -1) {
            this.groups.splice(index, 1);
        }
    }
}
