System.register(["@angular/core", "./ncc-accordion.component"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_accordion_component_1, NccAccordionGroupComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_accordion_component_1_1) {
                ncc_accordion_component_1 = ncc_accordion_component_1_1;
            }
        ],
        execute: function () {
            NccAccordionGroupComponent = (function () {
                function NccAccordionGroupComponent(accordion) {
                    this.accordion = accordion;
                    this.accordion = accordion;
                }
                Object.defineProperty(NccAccordionGroupComponent.prototype, "isOpen", {
                    get: function () {
                        return this._isOpen;
                    },
                    set: function (value) {
                        this._isOpen = value;
                        if (value) {
                            this.accordion.closeOtherPanels(this);
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                NccAccordionGroupComponent.prototype.ngOnInit = function () {
                    this.panelClass = this.panelClass || 'panel-default';
                    this.accordion.addGroup(this);
                };
                NccAccordionGroupComponent.prototype.ngOnDestroy = function () {
                    this.accordion.removeGroup(this);
                };
                NccAccordionGroupComponent.prototype.toggleOpen = function (event) {
                    event.preventDefault();
                    if (!this.isDisabled) {
                        this.isOpen = !this.isOpen;
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccAccordionGroupComponent.prototype, "heading", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccAccordionGroupComponent.prototype, "panelClass", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Boolean)
                ], NccAccordionGroupComponent.prototype, "isDisabled", void 0);
                __decorate([
                    core_1.HostBinding('class.panel-open'),
                    core_1.Input(),
                    __metadata("design:type", Boolean),
                    __metadata("design:paramtypes", [Boolean])
                ], NccAccordionGroupComponent.prototype, "isOpen", null);
                NccAccordionGroupComponent = __decorate([
                    core_1.Component({
                        selector: 'ncc-accordion-group',
                        template: "\n    <div class=\"panel\" [ngClass]=\"panelClass\">\n      <div class=\"panel-heading\" (click)=\"toggleOpen($event)\">\n        <h4 class=\"panel-title\">\n          <a href tabindex=\"0\" class=\"accordion-toggle\">\n            <span *ngIf=\"heading\" [ngClass]=\"{'text-muted': isDisabled}\">{{heading}}</span>\n            <ng-content select=\"[accordion-heading]\"></ng-content>\n          </a>\n        </h4>\n      </div>\n      <div class=\"panel-collapse collapse\" [nccCollapse]=\"!isOpen\">\n        <div class=\"panel-body\">\n          <ng-content></ng-content>\n        </div>\n      </div>\n    </div>\n  "
                    }),
                    __metadata("design:paramtypes", [ncc_accordion_component_1.NccAccordionComponent])
                ], NccAccordionGroupComponent);
                return NccAccordionGroupComponent;
            }());
            exports_1("NccAccordionGroupComponent", NccAccordionGroupComponent);
        }
    };
});
//# sourceMappingURL=ncc-accordion-group.component.js.map