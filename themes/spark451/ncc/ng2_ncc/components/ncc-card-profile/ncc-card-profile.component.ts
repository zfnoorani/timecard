import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

declare var nccTray: any;

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-profile',
    templateUrl: 'ncc-card-profile.component.html',
    styleUrls: ['ncc-card-profile.component.css']
})

export class NccCardProfileComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() name: string;
    @Input() title: string; // title/major
    @Input() linkLabel: string;
    @Input() linkUrl: string; // will be used for Profile Full - link to the other page
    @Input() trayContentId: string;  // will be used for Profile Story - trigger tray content
    @Input() tagLabel: string;
    @Input() tagUrl: string;
    @Input() image: string;

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(400)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto',
            })

        if (this.elimage && this.elcontent) {
            imageAnimation.start(this.elimage.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elimage && this.elcontent) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(400)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none',
            })

        if (this.elimage && this.elcontent) {
            contentAnimation.start(this.elcontent.nativeElement);
            imageAnimation.start(this.elimage.nativeElement);
        }*/

        if (this.elimage && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elimage.nativeElement.style.transform = 'scale(1.25)';
        }
    }

    public openProfile(event: MouseEvent) {
        if (this.trayContentId) {
            event.preventDefault();
            // nccTray.init();
            nccTray.toggleTray(event, this.trayContentId);
        }
    }
}
