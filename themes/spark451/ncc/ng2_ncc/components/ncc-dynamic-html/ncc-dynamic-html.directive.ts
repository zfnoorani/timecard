import {
    Component, 
    NgModule, 
    Directive, 
    ModuleWithComponentFactories, 
    ComponentRef, 
    ComponentFactory, 
    Compiler, 
    Input, 
    ReflectiveInjector, 
    ViewContainerRef, 
    OnChanges, 
    OnDestroy
} from '@angular/core';

import { NccDynamicHtmlModule } from '../../ncc-dynamic-html.module';

export function createComponentFactory(compiler: Compiler, metadata: Component): Promise<ComponentFactory<any>> {
    const cmpClass = class DynamicComponent {};
    const decoratedCmp = Component(metadata)(cmpClass);

    @NgModule({ imports: [ NccDynamicHtmlModule ], declarations: [ decoratedCmp ] })
    class NccDynamicComponentModule { }

    return compiler.compileModuleAndAllComponentsAsync(NccDynamicComponentModule)
        .then((moduleWithComponentFactory: ModuleWithComponentFactories<any>) => {
            return moduleWithComponentFactory.componentFactories.find(x => x.componentType === decoratedCmp);
        });
}

@Directive({
    selector: '[nccDynamicHTML]',
})
export class NccDynamicHTML {
    @Input() nccDynamicHTML: string;
     private componentRef: ComponentRef<any>;

    constructor(private containerRef: ViewContainerRef, private compiler: Compiler) { }

    public ngOnChanges(): void {
        const nccDynamicHTML = this.nccDynamicHTML;
        if (!nccDynamicHTML) return;

        if(this.componentRef) {
            this.componentRef.destroy();
        }

        const metadata = new Component({
            selector: 'ncc-dynamic-html',
            template: nccDynamicHTML,
            styles: [`:host { display: block; }`]
        });

        createComponentFactory(this.compiler, metadata)
          .then((factory: ComponentFactory<any>) => {
            const injector = ReflectiveInjector.fromResolvedProviders([], this.containerRef.parentInjector);   
            this.componentRef = this.containerRef.createComponent(factory, 0, injector, []);
          });
    }

    ngOnDestroy() {
        if(this.componentRef) {
            this.componentRef.destroy();
        }    
    }
}
