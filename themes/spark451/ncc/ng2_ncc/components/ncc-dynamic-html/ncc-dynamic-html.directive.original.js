System.register(["@angular/core", "../../ncc-dynamic-html.module"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    function createComponentFactory(compiler, metadata) {
        var cmpClass = (function () {
            function DynamicComponent() {
            }
            return DynamicComponent;
        }());
        var decoratedCmp = core_1.Component(metadata)(cmpClass);
        var NccDynamicComponentModule = (function () {
            function NccDynamicComponentModule() {
            }
            NccDynamicComponentModule = __decorate([
                core_1.NgModule({ imports: [ncc_dynamic_html_module_1.NccDynamicHtmlModule], declarations: [decoratedCmp] })
            ], NccDynamicComponentModule);
            return NccDynamicComponentModule;
        }());
        return compiler.compileModuleAndAllComponentsAsync(NccDynamicComponentModule)
            .then(function (moduleWithComponentFactory) {
            return moduleWithComponentFactory.componentFactories.find(function (x) { return x.componentType === decoratedCmp; });
        });
    }
    exports_1("createComponentFactory", createComponentFactory);
    var core_1, ncc_dynamic_html_module_1, NccDynamicHTML;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_dynamic_html_module_1_1) {
                ncc_dynamic_html_module_1 = ncc_dynamic_html_module_1_1;
            }
        ],
        execute: function () {
            NccDynamicHTML = (function () {
                function NccDynamicHTML(containerRef, compiler) {
                    this.containerRef = containerRef;
                    this.compiler = compiler;
                }
                NccDynamicHTML.prototype.ngOnChanges = function () {
                    var _this = this;
                    var nccDynamicHTML = this.nccDynamicHTML;
                    if (!nccDynamicHTML)
                        return;
                    if (this.componentRef) {
                        this.componentRef.destroy();
                    }
                    var metadata = new core_1.Component({
                        selector: 'ncc-dynamic-html',
                        template: nccDynamicHTML,
                        styles: [":host { display: block; }"]
                    });
                    createComponentFactory(this.compiler, metadata)
                        .then(function (factory) {
                        var injector = core_1.ReflectiveInjector.fromResolvedProviders([], _this.containerRef.parentInjector);
                        _this.componentRef = _this.containerRef.createComponent(factory, 0, injector, []);
                    });
                };
                NccDynamicHTML.prototype.ngOnDestroy = function () {
                    if (this.componentRef) {
                        this.componentRef.destroy();
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccDynamicHTML.prototype, "nccDynamicHTML", void 0);
                NccDynamicHTML = __decorate([
                    core_1.Directive({
                        selector: '[nccDynamicHTML]',
                    }),
                    __metadata("design:paramtypes", [typeof (_a = typeof core_1.ViewContainerRef !== "undefined" && core_1.ViewContainerRef) === "function" && _a || Object, typeof (_b = typeof core_1.Compiler !== "undefined" && core_1.Compiler) === "function" && _b || Object])
                ], NccDynamicHTML);
                return NccDynamicHTML;
                var _a, _b;
            }());
            exports_1("NccDynamicHTML", NccDynamicHTML);
        }
    };
});
//# sourceMappingURL=ncc-dynamic-html.directive.js.map