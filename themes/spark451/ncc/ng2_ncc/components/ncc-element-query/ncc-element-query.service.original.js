System.register(["@angular/core", "./ncc-breakpoints"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_breakpoints_1, NccElementQueryService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_breakpoints_1_1) {
                ncc_breakpoints_1 = ncc_breakpoints_1_1;
            }
        ],
        execute: function () {
            NccElementQueryService = (function () {
                function NccElementQueryService(_el, _renderer) {
                    this._el = _el;
                    this._renderer = _renderer;
                }
                NccElementQueryService.prototype.setElement = function (element) {
                    this._el = element;
                };
                NccElementQueryService.prototype.updateMinMaxWidthAttributes = function () {
                    var elementWidth = this._el.nativeElement.getBoundingClientRect().width;
                    if (elementWidth < ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.sm) {
                        this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.xs + 'px');
                        this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.sm - 1) + 'px');
                    }
                    else if (elementWidth < ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.md) {
                        this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.sm + 'px');
                        this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.md - 1) + 'px');
                    }
                    else if (elementWidth < ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.lg) {
                        this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.sm + 'px ' + ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.md + 'px');
                        this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.lg - 1) + 'px');
                    }
                    else {
                        this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.sm + 'px ' + ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.md + 'px ' + ncc_breakpoints_1.ELEMENT_QUERY_BREAKPOINTS.lg + 'px');
                        this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', '');
                    }
                };
                NccElementQueryService = __decorate([
                    core_1.Injectable(),
                    __metadata("design:paramtypes", [typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object, typeof (_b = typeof core_1.Renderer !== "undefined" && core_1.Renderer) === "function" && _b || Object])
                ], NccElementQueryService);
                return NccElementQueryService;
                var _a, _b;
            }());
            exports_1("NccElementQueryService", NccElementQueryService);
        }
    };
});
//# sourceMappingURL=ncc-element-query.service.js.map