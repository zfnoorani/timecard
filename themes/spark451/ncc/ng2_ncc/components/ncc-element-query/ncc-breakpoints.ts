export const LAYOUT_BREAKPOINTS = {
    xs: 320,
    sm: 480,
    md: 768,
    lg: 1024
}

export const ELEMENT_QUERY_BREAKPOINTS = {
    xs: 0,
    sm: 451,
    md: 701,
    lg: 1025
}
