import {Injectable, ElementRef, Renderer} from '@angular/core';
// import {BrowserDomAdapter} from 'angular2/platform/browser';
// import {Ruler, Rectangle} from 'angular2/src/platform/browser/ruler';

import {ELEMENT_QUERY_BREAKPOINTS} from './ncc-breakpoints';

@Injectable()
export class NccElementQueryService {
    // private _browserDomAdapter = new BrowserDomAdapter();
    // private _ruler = new Ruler(this._browserDomAdapter);

    public constructor(private _el: ElementRef, private _renderer: Renderer) { }

    public setElement(element: ElementRef) {
        this._el = element;
    }

    public updateMinMaxWidthAttributes(): void {
        let elementWidth = this._el.nativeElement.getBoundingClientRect().width;

        // console.log('NccElementQueryService - component width: ' + elementWidth)

        if (elementWidth < ELEMENT_QUERY_BREAKPOINTS.sm) {
            this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ELEMENT_QUERY_BREAKPOINTS.xs + 'px');
            this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ELEMENT_QUERY_BREAKPOINTS.sm - 1) + 'px');
        } else if (elementWidth < ELEMENT_QUERY_BREAKPOINTS.md) {
            this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ELEMENT_QUERY_BREAKPOINTS.sm + 'px');
            this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ELEMENT_QUERY_BREAKPOINTS.md - 1) + 'px');
        } else if (elementWidth < ELEMENT_QUERY_BREAKPOINTS.lg) {
            this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ELEMENT_QUERY_BREAKPOINTS.sm + 'px ' + ELEMENT_QUERY_BREAKPOINTS.md + 'px');
            this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ELEMENT_QUERY_BREAKPOINTS.lg - 1) + 'px');
        } else {
            this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ELEMENT_QUERY_BREAKPOINTS.sm + 'px ' + ELEMENT_QUERY_BREAKPOINTS.md + 'px ' + ELEMENT_QUERY_BREAKPOINTS.lg + 'px');
            this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', '');
        }

        // Old Solution:
        // this._ruler.measure(this._el)
        //     .then((rect: Rectangle) => {
        //         // console.log('NccElementQueryService - component width: ' + rect.width)

        //         if (rect.width < ELEMENT_QUERY_BREAKPOINTS.sm) {
        //             this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ELEMENT_QUERY_BREAKPOINTS.xs + 'px');
        //             this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ELEMENT_QUERY_BREAKPOINTS.sm - 1) + 'px');
        //         } else if (rect.width < ELEMENT_QUERY_BREAKPOINTS.md) {
        //             this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ELEMENT_QUERY_BREAKPOINTS.sm + 'px');
        //             this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ELEMENT_QUERY_BREAKPOINTS.md - 1) + 'px');
        //         } else if (rect.width < ELEMENT_QUERY_BREAKPOINTS.lg) {
        //             this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ELEMENT_QUERY_BREAKPOINTS.sm + 'px ' + ELEMENT_QUERY_BREAKPOINTS.md + 'px');
        //             this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', (ELEMENT_QUERY_BREAKPOINTS.lg - 1) + 'px');
        //         } else {
        //             this._renderer.setElementAttribute(this._el.nativeElement, 'min-width', ELEMENT_QUERY_BREAKPOINTS.xs + 'px ' + ELEMENT_QUERY_BREAKPOINTS.sm + 'px ' + ELEMENT_QUERY_BREAKPOINTS.md + 'px ' + ELEMENT_QUERY_BREAKPOINTS.lg + 'px');
        //             this._renderer.setElementAttribute(this._el.nativeElement, 'max-width', '');
        //         }
        //     });
    }
}
