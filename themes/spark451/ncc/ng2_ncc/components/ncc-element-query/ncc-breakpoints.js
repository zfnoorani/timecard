System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var LAYOUT_BREAKPOINTS, ELEMENT_QUERY_BREAKPOINTS;
    return {
        setters: [],
        execute: function () {
            exports_1("LAYOUT_BREAKPOINTS", LAYOUT_BREAKPOINTS = {
                xs: 320,
                sm: 480,
                md: 768,
                lg: 1024
            });
            exports_1("ELEMENT_QUERY_BREAKPOINTS", ELEMENT_QUERY_BREAKPOINTS = {
                xs: 0,
                sm: 451,
                md: 701,
                lg: 1025
            });
        }
    };
});
//# sourceMappingURL=ncc-breakpoints.js.map