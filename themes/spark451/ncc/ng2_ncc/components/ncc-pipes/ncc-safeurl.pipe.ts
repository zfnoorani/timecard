import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

/**
 * Bypass security and trust the given value to be a safe resource URL
 *
 * ### Example
 *
 * <iframe [src]="url | nccsafeurl" ></iframe>
 *
 */
@Pipe({ name: 'nccsafeurl' })
export class NccSafeUrlPipe implements PipeTransform {
    sanitizer: any;

    constructor(sanitizer: DomSanitizer) {
        this.sanitizer = sanitizer;
    }

    transform(value: string): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(value);
    }
}
