System.register(["@angular/core", "@angular/platform-browser"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, platform_browser_1, NccSafeUrlPipe;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            }
        ],
        execute: function () {
            NccSafeUrlPipe = (function () {
                function NccSafeUrlPipe(sanitizer) {
                    this.sanitizer = sanitizer;
                }
                NccSafeUrlPipe.prototype.transform = function (value) {
                    return this.sanitizer.bypassSecurityTrustResourceUrl(value);
                };
                NccSafeUrlPipe = __decorate([
                    core_1.Pipe({ name: 'nccsafeurl' }),
                    __metadata("design:paramtypes", [typeof (_a = typeof platform_browser_1.DomSanitizer !== "undefined" && platform_browser_1.DomSanitizer) === "function" && _a || Object])
                ], NccSafeUrlPipe);
                return NccSafeUrlPipe;
                var _a;
            }());
            exports_1("NccSafeUrlPipe", NccSafeUrlPipe);
        }
    };
});
//# sourceMappingURL=ncc-safeurl.pipe.js.map