import {Pipe, PipeTransform} from '@angular/core';

/**
 * Ncc Slice
 *
 * ### Example
 *
 * expression | nccslice:chars
 */
@Pipe({ name: 'nccslice' })
export class NccSlicePipe implements PipeTransform {
    transform(value: string, chars: number = null): string {
        if (value === null) return value;

        if (typeof value !== 'string') {
            throw new Error(`Invalid argument '${value}' for pipe 'NccSlicePipe'`);
        }

        if (value.length > chars) {
            return value.slice(0, chars === null ? undefined : chars) + '...';
        }

        return value;
    }
}
