import {Component, ElementRef, ViewChild, OnInit, AfterViewInit} from '@angular/core';

import {Event, EventFilter, SelectedEvents} from '../ncc-calendar/ncc-calendar.interface';

import {NccCalendarService} from '../ncc-calendar/ncc-calendar.service';

// TODO: review this approach
declare var jQuery: any;
declare var kendo: any;
declare var nccTray: any;

@Component({
    moduleId: __moduleName,
    selector: 'ncc-calendar',
    templateUrl: 'ncc-calendar.component.html',
    styleUrls: ['ncc-calendar.component.css'],
    host: {
        '(window:resize)': 'onResize($event)'
    }
})

export class NccCalendarComponent implements OnInit, AfterViewInit {
    @ViewChild('elscheduler') elscheduler: ElementRef;
    @ViewChild('elselectedevents') elselectedevents: ElementRef;

    private events: Event[];
    private selectedEvents: SelectedEvents;
    private isLoading: boolean = true;
    private eventFilters: EventFilter[] = [];

    private monthNames: string[] = [];
    private currentMonth: string;
    private previousMonth: string;
    private nextMonth: string;

    private _kendoSchedulerOptions: any;
    private _kendoScheduler: any;
    private isMobile = false;
    private timezone: string;

    private _eventTemplate = (data: Event) => {

        var eventTime = this._calendarService.getEventStartEndTime(data.start, data.end);

        return `
            <div class="ncc-event-template" data-id="ncc-calendar-event-${data.id}">
                <p class="ncc-description no-margin">
                    <a href="${data.url}">${data.title}</a>
                </p>
                <div class="hidden">
                    <div class="ncc-calendar-tooltip" id="ncc-calendar-event-${data.id}">
                        <h3 class="ncc-subhead-1 no-margin">${data.title}</h3>
                        <hr class="default-margin bg-wolverine s2 short">
                        <p class="ncc-description no-margin">${data.address}<br />${eventTime.start} - ${eventTime.end}</p>
                    </div>
                </div>
            </div>`
    };

    public constructor(private _el: ElementRef, private _calendarService: NccCalendarService) {
        // console.log("NG2 Calendar - constructor()...");
        this.timezone = this._calendarService.getTimezone();

        this.isMobile = (window.innerWidth <= 1024);

        var context = this;
        // Get list of month names so we can fetch them by month index
        this.monthNames = this._calendarService.getMonthNames();
        // Extend kendo monthView and create new view
        var NccMonthView = this.initializeNccMonthView(context);
        // Kendo Scheduler options
        this._kendoSchedulerOptions = {};
        // Set Scheduler default view: Use NccMonthView that we created above
        this._kendoSchedulerOptions.views = [
            { type: NccMonthView, title: "NCC Month", eventHeight: 30 }
        ];
        // Don't allow events editing
        this._kendoSchedulerOptions.editable = false;
        // Set single event template ( include tooltip content )
        this._kendoSchedulerOptions.eventTemplate = this._eventTemplate;
        // Set default calendar timezone
        this._kendoSchedulerOptions.timezone = this.timezone;

        this._kendoSchedulerOptions.dataSource = [];
        // Fired when the user selects a cell or event in the scheduler.
        this._kendoSchedulerOptions.change = function(event: any) {
            var view = this.view();
            // Get events in selected slot
            var slot = this.slotByElement(view._current);
            var eventsInRange = event.sender.occurrencesInRange(slot.startDate, slot.endDate);
            // Set selected events
            if(context.isMobile){
                context._calendarService.setSelectedSlotEvents(eventsInRange, slot.startDate);
            }
        }
        // Fired before the widget binds to its data source.
        this._kendoSchedulerOptions.dataBinding = (event: any) => {
            // Set prev/next button labes
            this.setCalendarMonthLabels(event.sender);
        }

        // Fired when the widget is bound to data from its data source.
        this._kendoSchedulerOptions.dataBound = function(event: any) {
            var scheduler = this;
            var view = scheduler.view();
            var tds = view.content.find("td");
            // Go through all slots in calendar
            tds.each(function() {
                // get events in current slot
                var slot = scheduler.slotByElement(jQuery(this));
                var ocurrences = scheduler.occurrencesInRange(slot.startDate, slot.endDate);

                // If slot take place today, set its events as selected
                if(kendo.date.isToday(slot.startDate) && context.isMobile){
                    context._calendarService.setSelectedSlotEvents(ocurrences, slot.startDate);
                }
                // Mark slotas gray if they have any event
                if(!ocurrences.length && this.classList.contains('ncc-slot-has-events')){
                    this.classList.remove('ncc-slot-has-events');
                }
                if(ocurrences.length){
                    this.classList.add('ncc-slot-has-events');
                }
            });
        }

        this._kendoSchedulerOptions.navigate = (event: any) => {
            //
            if(event.action == 'changeView'){
                event.preventDefault();
                return false;
            }else if(event.action == 'next' || event.action == 'previous' || event.action == 'changeDate'){

                var kData = this._kendoScheduler.data('kendoScheduler');
                var view = kData.view();
                var time = event.date.getTime();

                if(!kendo.date.isInDateRange(event.date, view._firstDayOfMonth, view._lastDayOfMonth)){
                    kData.date(event.date);
                    var currentView = event.sender.view();
                    this._calendarService.getEvents(currentView._firstDayOfMonth, currentView._lastDayOfMonth);
                    setTimeout(() => {
                        context.adjustSelection(kData, time);
                    });
                }else{
                    // console.log("We are in the same view!");
                    context.adjustSelection(kData, time);
                    event.preventDefault();
                    return false;
                }
            }
        }
        // Remove footer from scheduler
        this._kendoSchedulerOptions.footer = false;
        // Set event slots selectable
        this._kendoSchedulerOptions.selectable = true;
        // Set min and max available dates in calendar ( +/- 10 years )
        var minDate =  new Date().setFullYear(new Date().getFullYear() - 10);
        var maxDate =  new Date().setFullYear(new Date().getFullYear() + 10);
        this._kendoSchedulerOptions.min = new Date(minDate);
        this._kendoSchedulerOptions.max = new Date(maxDate);
        // this._kendoSchedulerOptions.mobile = "phone";
    }

    // Extend "month" view and add prepare functions for "more events" button
    private initializeNccMonthView(context: NccCalendarComponent) {

        let MORE_BUTTON_TEMPLATE = kendo.template(
            `<div style="width:#=width#px;left:#=left#px;top:#=top#px" class="k-more-events ncc-more-events ncc-label">
            <a href="" style="font-size:8pt; margin-top: 0;"> #=hiddenevents# More Event#if(hiddenevents > 1){#s#}#</a>
            </div>`);

        return kendo.ui.MonthView.extend({
            _createRow: function (startDate: Date, startIdx: number, cellsPerRow: number, groupIndex: number) {

                var that = this;
                var min = that._firstDayOfMonth;
                var max = that._lastDayOfMonth;
                var content = that.dayTemplate;
                var classes = '';
                var html = '';
                var resources = function () {
                    return that._resourceBySlot({ groupIndex: groupIndex });
                };

                for (var cellIdx = 0; cellIdx < cellsPerRow; cellIdx++) {
                    var time = kendo.date.getDate(startDate).getTime();
                    classes = '';
                    if (kendo.date.isToday(startDate)) {
                        classes += 'k-today';
                    }
                    if (!kendo.date.isInDateRange(startDate, min, max)) {
                        classes += ' k-other-month';
                    }
                    html += '<td ';
                    if (classes !== '') {
                        html += 'class="' + classes + '"';
                    }

                    html += ' data-time="'+time+'"';

                    html += '>';
                    html += content({
                        date: startDate,
                        resources: resources
                    });
                    html += '</td>';
                    that._slotIndices[time] = startIdx + cellIdx;
                    startDate = kendo.date.nextDay(startDate);
                }
                return html;
            },
            _positionEvent: function(slotRange: any, element: any, group: any) {

                var eventHeight = this.options.eventHeight;
                var startSlot = slotRange.start;

                if (slotRange.start.offsetLeft > slotRange.end.offsetLeft) {
                    startSlot = slotRange.end;
                }

                var startIndex = slotRange.start.index;
                var endIndex = slotRange.end.index;
                var eventCount = startSlot.eventCount;
                var events = kendo.ui.SchedulerView.collidingEvents(slotRange.events(), startIndex, endIndex);
                var rightOffset = startIndex !== endIndex ? 5 : 4;

                // Should be multiday event
                if(startIndex != endIndex){
                    element[0].classList.add('ncc-multiday-event');
                }

                events.push({
                    element: element,
                    start: startIndex,
                    end: endIndex
                });

                var scheduler = jQuery(slotRange.start.element).closest("[data-role=scheduler]").getKendoScheduler();

                var rows = kendo.ui.SchedulerView.createRows(events);

                for (var idx = 0, length = Math.min(rows.length, eventCount); idx < length; idx++) {
                    var rowEvents = rows[idx].events;
                    var eventTop = startSlot.offsetTop + startSlot.firstChildHeight + idx * eventHeight + 3 * idx + startSlot.firstChildTop + "px";

                    for (var j = 0, eventLength = rowEvents.length; j < eventLength; j++) {
                        rowEvents[j].element[0].style.top = eventTop;
                    }
                }

                if (rows.length > eventCount) {
                    for (var slotIndex = startIndex; slotIndex <= endIndex; slotIndex++) {
                        var collection = slotRange.collection;

                        var slot = collection.at(slotIndex);

                        if (slot.more) {
                            return;
                        }

                        var eventsInRange = scheduler.occurrencesInRange(startSlot.startDate(), startSlot.endDate());
                        var hiddenEvents = ( eventsInRange.length - rows.length + 1 );

                        var options = {
                            startSlot: slotRange.start,
                            endSlot: slotRange.end,
                            rowsCount: rows.length,
                            ns: kendo.ns,
                            start: slotIndex,
                            end: slotIndex,
                            width: slot.clientWidth - 2,
                            left: slot.offsetLeft + 2,
                            top: slot.offsetTop + slot.firstChildHeight + eventCount * eventHeight + 3 * eventCount + slot.firstChildTop,
                            hiddenevents: hiddenEvents
                        };

                        if(hiddenEvents){
                            
                            slot.more = jQuery(MORE_BUTTON_TEMPLATE(options));

                            slot.more[0].addEventListener('click', (event: MouseEvent) => {
                                var slot = scheduler.slotByElement(event.target);
                                var occurences = scheduler.occurrencesInRange(slot.startDate, slot.endDate);
                                context._calendarService.setSelectedSlotEvents(occurences, slot.startDate);
                                // console.log("Events in range: %O", occurences);
                            });

                            this.content[0].appendChild(slot.more[0]);
                        }
                    }

                } else {
                    slotRange.addEvent({
                        element: element,
                        start: startIndex,
                        end: endIndex,
                        groupIndex: startSlot.groupIndex
                    });

                    element[0].style.width = slotRange.innerWidth() - rightOffset + "px";
                    element[0].style.left = startSlot.offsetLeft + 2 + "px";
                    element[0].style.height = eventHeight + "px";

                    group._continuousEvents.push({
                        element: element,
                        uid: element.attr(kendo.attr("uid")),
                        start: slotRange.start,
                        end: slotRange.end
                    });

                    element.appendTo(this.content);
                }
            },
            _layout: function () {
                var calendarInfo = this.calendarInfo();
                var weekDayNames = this._isMobile() ? calendarInfo.days.namesShort : calendarInfo.days.namesAbbr;
                var names = weekDayNames.slice(calendarInfo.firstDay).concat(weekDayNames.slice(0, calendarInfo.firstDay))
                var columns = jQuery.map(names, function (value: string) {
                    return { text: value };
                });

                var resources = this.groupedResources;
                var rows: any;
                if (resources.length) {
                    if (this._isVerticallyGrouped()) {
                        var inner: any[] = [];
                        for (var idx = 0; idx < 6; idx++) {
                            inner.push({
                                text: '<div>&nbsp;</div>',
                                className: 'k-hidden k-slot-cell'
                            });
                        }
                        rows = this._createRowsLayout(resources, inner, this.groupHeaderTemplate);
                    } else {
                        columns = this._createColumnsLayout(resources, columns, this.groupHeaderTemplate);
                    }
                }
                return {
                    columns: columns,
                    rows: rows
                };
            },
            _selectSlots: function (selection: any) {
                var isAllDay = selection.isAllDay;
                var group = this.groups[selection.groupIndex];
                if (!group.timeSlotCollectionCount()) {
                    isAllDay = true;
                }
                this._selectedSlots = [];
                var ranges = group.ranges(selection.start, selection.end, isAllDay, false);
                var element: HTMLTableCellElement;
                var slot: any;
                for (var rangeIndex = 0; rangeIndex < ranges.length; rangeIndex++) {
                    var range = ranges[rangeIndex];
                    var collection = range.collection;
                    slot = collection.at(range.start.index);
                    element = slot.element;
                    element.setAttribute('aria-selected', 'true');
                    element.className = element.className.replace(/\s*k-state-selected/, '') + ' k-state-selected';
                    this._selectedSlots.push({
                        start: slot.startDate(),
                        end: slot.endDate(),
                        element: element
                    });
                    break;
                }
                if (selection.backward) {
                    element = ranges[0].start.element;
                }
                this.current(element);
            }
        });
    }

    private onResize(event: MouseEvent){
        var target = <Window>event.target;
        this.isMobile = (target.innerWidth <= 1024);
    }

    // Format time with AM/PM indicators
    private formatUSLocaleTime(date: Date): string {
        return this._calendarService.formatUSLocaleTime(date);
    }

    // Initialize Kendo Scheduler
    private createKendoScheduler(options: any): void {
        // console.log("NG2 Calendar - createKendoScheduler(): %O", options);
        this._kendoScheduler = jQuery(this.elscheduler.nativeElement).kendoScheduler(options);
        var kData = this._kendoScheduler.data('kendoScheduler');
        this.setCalendarMonthLabels(kData);
        this.isLoading = false;
        setTimeout(() => {
            this.repositionCalendarPicker(kData);
        });
    }

    ngOnInit(): void {
        // Get list of available event filters
        this._calendarService.filters.subscribe((filters: any[]) => {
            this.eventFilters = filters;
        })
        // Subscribe to list of selected events
        this._calendarService.selectedEvents.subscribe((_selectedEvents: SelectedEvents) => {
            this.selectedEvents = _selectedEvents;
        });
    }

    ngAfterViewInit(): void {
        // Get list of available events for selected view range
        this._calendarService.events.subscribe((events: any[]) => {
            this.events = events;

            // console.log("events: %O", this.events);
            if (this.events) {
                // Get reference to the kendo.ui.Scheduler instance
                var scheduler = jQuery(this.elscheduler.nativeElement).data("kendoScheduler");

                if (typeof scheduler !== 'undefined') {
                    var dataSource = new kendo.data.SchedulerDataSource({
                        data: this.events
                    });

                    scheduler.setDataSource(dataSource);
                } else {
                    this._kendoSchedulerOptions.dataSource = this.events;
                    this.createKendoScheduler(this._kendoSchedulerOptions);
                }
            }
        });
    }

    public hasActiveFilters(): boolean {
        var hasActive = false;

        if(!this.eventFilters || !this.eventFilters.length){
            return hasActive;
        }

        for (var i = this.eventFilters.length - 1; i >= 0; i--) {
            if(this.eventFilters[i]['active']){
                return true;
            }
        }

        return hasActive;
    }

    // Filter events
    public toggleFilterById(event: MouseEvent, filterId: string): void {
        this._calendarService.toggleFilterById(filterId);
        var kData = this._kendoScheduler.data('kendoScheduler');
        var view = kData.view();
        if(view._selectedSlots.length && view._current){
            kData.trigger('change');
        }else{
            this._calendarService.setSelectedSlotEvents([], new Date());
        }
    }

    // Adjust selection
    private adjustSelection(that: any, time: number): number{
        var contentDiv = that.element.find("div.k-scheduler-content");
        var slotEl = contentDiv.find('td[data-time="'+time+'"]');
        if(slotEl.length){
            var slot = that.slotByElement(slotEl);
            that.select({start: slot.startDate, end: slot.endDate, isAllDay: slot.isDaySlot});
            that._adjustSelectedDate();
        }
        return slotEl.length;
    }

    // Move Calendar Picker to other container and initialize KendoUI tooltip widget
    public repositionCalendarPicker(kData: any){
        jQuery( kData.toolbar ).clone(true).appendTo( ".ncc-calendar-toolbar" );

        kData.toolbar.remove();
        if (kData.calendar) {
            kData.calendar.destroy();
            kData.popup.destroy();
        }

        kData.toolbar = jQuery('.ncc-calendar-toolbar');

        kData.element.kendoTooltip({
            filter: ".k-event:not(.k-event-drag-hint) > div, .k-task",
            position: "top",
            // autoHide: false,
            width: 210,
            animation: {
              close: {
                effects: "fade:out",
                duration: 200
              }
            },
            content: (data: any): HTMLElement => {
                return data.target[0].getElementsByClassName('ncc-calendar-tooltip')[0].cloneNode(true);
            }
        });
    }

    // Bind labels for previous, current and next month
    public setCalendarMonthLabels(kData: any): void{
        this.currentMonth = kData._model.formattedDate;
        this.nextMonth = this.monthNames[kData.view().nextDate().getMonth()];
        this.previousMonth = this.monthNames[kData.view().previousDate().getMonth()];

        jQuery('.k-nav-current .k-sm-date-format', kData.toolbar).html(this.currentMonth);
        jQuery('.k-nav-current .k-lg-date-format', kData.toolbar).html(this.currentMonth);
    }

    // Go to the next month
    public advanceToNextMonth(): void {
        var kData = this._kendoScheduler.data('kendoScheduler');
        var date = kData.view().nextDate();
        this.goToMonth('next', date, kData);
    }

    // Back to the previous month
    public backToPreviousMonth(): void {
        var kData = this._kendoScheduler.data('kendoScheduler');
        var date = kData.view().previousDate();
        this.goToMonth('previous', date, kData);
    }

    // Go to specific date
    private goToMonth(action: string, date: any, kData: any): void {
        if (!kData.trigger('navigate', {
            view: kData._selectedViewName,
            action: action,
            date: date
        })) {
            // kData.date(date);
        }
    }
}
