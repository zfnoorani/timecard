import {Injectable, Inject} from '@angular/core';
import {Http, Response, URLSearchParams} from '@angular/http';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
// import 'rxjs/Rx'; // import all operators (dev only)
import 'rxjs/add/operator/toPromise';

declare var _: any;
// TODO: review this approach of including Lodash on Drupal side
// declare var _: any;

import { Event, SelectedEvents } from './ncc-calendar.interface';

declare var kendo: any;

export var EVENTS_API_URL: string = '/api/events.json';
export var EVENTS_FILTERS_URL: string = '/api/event-filters.json';

@Injectable()
export class NccCalendarService {
    // Service Events local storage
    private eventsStorage: any[];
    private eventsStorageUnfiltered: any[];

    private filtersGroup: string[] = [];
    private filtersGroupIsEmpty: boolean = true;

    // Observable Events
    public events: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
    public selectedEvents: BehaviorSubject<SelectedEvents> = new BehaviorSubject<SelectedEvents>(null);
    public filters: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);

    private monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    private dateLocaleOptions = { hour: '2-digit', hour12: true, minute: '2-digit' };
    private timezone = 'America/Chicago';

    constructor( @Inject(EVENTS_API_URL) private apiUrl: string, @Inject(EVENTS_FILTERS_URL) private apiFiltersUrl: string, private _http: Http) {
        
        if(typeof drupalSettings !== 'undefined' && drupalSettings.timezone){
            this.timezone = drupalSettings.timezone;
            //console.log("DS timezone: " + this.timezone);
        }

        // Grab filters from API
        this.getFilters().then((res: Response) => {
            // Get current view date range
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            // Grab events for current view date range
            this.getEvents(firstDay, lastDay);
        });
    }
    // Get events by date range
    public getEvents(dateFrom: string | Date, dateTo: string | Date){
        let params = new URLSearchParams();

        if(dateFrom && dateTo){
            params.set('start_datetime', this.formatDateForApiSearch(dateFrom));
            params.set('end_datetime', this.formatDateForApiSearch(dateTo));
        }

        this._http.get(this.apiUrl, { search: params })
            .toPromise()
            .then((response: Response) => {
                var events = response.json();
                for (var i = events.length - 1; i >= 0; i--) {
                    events[i].startTimezone = this.timezone;
                    events[i].endTimezone = this.timezone;
                }
                this.eventsStorage = events;
                // console.log("Events: %O", eventsStorage);
                if(this.filtersGroup.length){
                    this.eventsStorageUnfiltered = events;
                    this.eventsStorage = this.getFilteredEvents();
                }
                this.events.next(this.eventsStorage);
            })
            .catch(this.handleError);
    }

    private getFilters(): Promise<any>{
        return this._http.get(this.apiFiltersUrl).toPromise().then((response: Response) => {
            // console.log("Filters: %O", response.json());
            var responseData = response.json();
            // Set filters as inactive by default
            for (var i = responseData.length - 1; i >= 0; i--) {
                if(responseData[i].selected === "1"){
                    responseData[i].active = true;
                    this.filtersGroup.push(responseData[i].id);
                }else{
                    responseData[i].active = false;
                }
            }
            this.filters.next(responseData);
        })
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<void> {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';

        console.error(errMsg);

        return Promise.reject(errMsg);
    }

    // Toggle filter active/inactive state
    private toggleFiltersGroup(filterId: string): void {
        this.filters.getValue().forEach((filter: any) => {
            if(filter.id == filterId){
                filter.active = !filter.active;
            }
        });

        if (_.indexOf(this.filtersGroup, filterId) === -1) {
            this.filtersGroup = _.union(this.filtersGroup, [filterId]);
        } else {
            this.filtersGroup = _.without(this.filtersGroup, filterId);
        }
    }
    // 'Unselect' all filters
    private resetFilters(): void {
        this.filters.getValue().forEach((filter: any) => {
            filter.active = false;
        });
        this.filtersGroup = [];
    }
    // Filter events by selected filters
    private getFilteredEvents(): Event[] {
        if (this.filtersGroup.length) {
            if (this.filtersGroupIsEmpty) {
                this.filtersGroupIsEmpty = false;
                this.eventsStorageUnfiltered = this.eventsStorage;
            }

            return _.filter(this.eventsStorageUnfiltered, (event: Event) => {
                let filters: boolean;

                if (event.categories) {
                    filters = this.filtersGroup.length === _.intersection(event.categories, this.filtersGroup).length;
                }

                return filters;
            });
        } else {
            this.filtersGroupIsEmpty = true;
            this.eventsStorage = this.eventsStorageUnfiltered;

            return this.eventsStorage;
        }
    }
    // Toggle single filter
    public toggleFilterById(filterId: string): void {
        if(filterId == '0'){
            this.resetFilters();
        }else{
            this.toggleFiltersGroup(filterId);
        }
        this.eventsStorage = this.getFilteredEvents();
        this.events.next(this.eventsStorage);
    }
    // Set currently selected events ( used by calendar Tray and listing below calendar )
    public setSelectedSlotEvents(_events: any[], startDate: Date) : void {
        this.selectedEvents.next({events: _events, date: startDate});
    }
    // Get private variable monthNames
    public getMonthNames(): string[]{
        return this.monthNames;
    }
    // Format date to YYYY-MM-DD format that API expects
    private formatDateForApiSearch(_date: string | Date){

        var date = (typeof _date === "string") ? new Date(_date) : _date;

        var month = '' + (date.getMonth() + 1),
            day = '' + date.getDate(),
            year = date.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
    }

    // Format dates for output on listing
    public formatEventDateOutput(startDate: Date, endDate: Date, current: Date){

        var date = startDate;
        if(kendo.toString(startDate, "d") != kendo.toString(current, "d")){
            date = current;
        }

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        var eventTime = this.getEventStartEndTime(startDate, endDate);

        return this.monthNames[monthIndex] + ' ' + day + ', ' + year + ', ' + eventTime.start + ' - ' + eventTime.end;
    }

    // Get USA Locale date range representation
    public getEventStartEndTime(_startDate: Date, _endDate: Date){
        return {
            start: this.formatUSLocaleTime(_startDate),
            end: this.formatUSLocaleTime(_endDate)
        }
    }

    // get USA local date representation
    public formatUSLocaleTime(date: Date): string {
        return date.toLocaleString('en-US', this.dateLocaleOptions);
    }

    public getTimezone(): string{
        return this.timezone;
    }
}
