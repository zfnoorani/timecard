System.register(["@angular/core", "@angular/http", "rxjs/BehaviorSubject", "rxjs/add/operator/toPromise"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, BehaviorSubject_1, EVENTS_API_URL, EVENTS_FILTERS_URL, NccCalendarService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (BehaviorSubject_1_1) {
                BehaviorSubject_1 = BehaviorSubject_1_1;
            },
            function (_1) {
            }
        ],
        execute: function () {
            exports_1("EVENTS_API_URL", EVENTS_API_URL = '/api/events.json');
            exports_1("EVENTS_FILTERS_URL", EVENTS_FILTERS_URL = '/api/event-filters.json');
            NccCalendarService = (function () {
                function NccCalendarService(apiUrl, apiFiltersUrl, _http) {
                    var _this = this;
                    this.apiUrl = apiUrl;
                    this.apiFiltersUrl = apiFiltersUrl;
                    this._http = _http;
                    this.filtersGroup = [];
                    this.filtersGroupIsEmpty = true;
                    this.events = new BehaviorSubject_1.BehaviorSubject(null);
                    this.selectedEvents = new BehaviorSubject_1.BehaviorSubject(null);
                    this.filters = new BehaviorSubject_1.BehaviorSubject(null);
                    this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    this.dateLocaleOptions = { hour: '2-digit', hour12: true, minute: '2-digit' };
                    this.timezone = 'America/Chicago';
                    if (typeof drupalSettings !== 'undefined' && drupalSettings.timezone) {
                        this.timezone = drupalSettings.timezone;
                        //console.log("DS timezone: " + this.timezone);
                    }
                    this.getFilters().then(function (res) {
                        var date = new Date();
                        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                        _this.getEvents(firstDay, lastDay);
                    });
                }
                NccCalendarService.prototype.getEvents = function (dateFrom, dateTo) {
                    var _this = this;
                    var params = new http_1.URLSearchParams();
                    if (dateFrom && dateTo) {
                        params.set('start_datetime', this.formatDateForApiSearch(dateFrom));
                        params.set('end_datetime', this.formatDateForApiSearch(dateTo));
                    }
                    this._http.get(this.apiUrl, { search: params })
                        .toPromise()
                        .then(function (response) {
                        var events = response.json();
                        for (var i = events.length - 1; i >= 0; i--) {
                            events[i].startTimezone = _this.timezone;
                            events[i].endTimezone = _this.timezone;
                        }
                        _this.eventsStorage = events;
                        if (_this.filtersGroup.length) {
                            _this.eventsStorageUnfiltered = events;
                            _this.eventsStorage = _this.getFilteredEvents();
                        }
                        _this.events.next(_this.eventsStorage);
                    })
                        .catch(this.handleError);
                };
                NccCalendarService.prototype.getFilters = function () {
                    var _this = this;
                    return this._http.get(this.apiFiltersUrl).toPromise().then(function (response) {
                        var responseData = response.json();
                        for (var i = responseData.length - 1; i >= 0; i--) {
                            if (responseData[i].selected === "1") {
                                responseData[i].active = true;
                                _this.filtersGroup.push(responseData[i].id);
                            }
                            else {
                                responseData[i].active = false;
                            }
                        }
                        _this.filters.next(responseData);
                    })
                        .catch(this.handleError);
                };
                NccCalendarService.prototype.handleError = function (error) {
                    var errMsg = (error.message) ? error.message :
                        error.status ? error.status + " - " + error.statusText : 'Server error';
                    console.error(errMsg);
                    return Promise.reject(errMsg);
                };
                NccCalendarService.prototype.toggleFiltersGroup = function (filterId) {
                    this.filters.getValue().forEach(function (filter) {
                        if (filter.id == filterId) {
                            filter.active = !filter.active;
                        }
                    });
                    if (_.indexOf(this.filtersGroup, filterId) === -1) {
                        this.filtersGroup = _.union(this.filtersGroup, [filterId]);
                    }
                    else {
                        this.filtersGroup = _.without(this.filtersGroup, filterId);
                    }
                };
                NccCalendarService.prototype.resetFilters = function () {
                    this.filters.getValue().forEach(function (filter) {
                        filter.active = false;
                    });
                    this.filtersGroup = [];
                };
                NccCalendarService.prototype.getFilteredEvents = function () {
                    var _this = this;
                    if (this.filtersGroup.length) {
                        if (this.filtersGroupIsEmpty) {
                            this.filtersGroupIsEmpty = false;
                            this.eventsStorageUnfiltered = this.eventsStorage;
                        }
                        return _.filter(this.eventsStorageUnfiltered, function (event) {
                            var filters;
                            if (event.categories) {
                                filters = _this.filtersGroup.length === _.intersection(event.categories, _this.filtersGroup).length;
                            }
                            return filters;
                        });
                    }
                    else {
                        this.filtersGroupIsEmpty = true;
                        this.eventsStorage = this.eventsStorageUnfiltered;
                        return this.eventsStorage;
                    }
                };
                NccCalendarService.prototype.toggleFilterById = function (filterId) {
                    if (filterId == '0') {
                        this.resetFilters();
                    }
                    else {
                        this.toggleFiltersGroup(filterId);
                    }
                    this.eventsStorage = this.getFilteredEvents();
                    this.events.next(this.eventsStorage);
                };
                NccCalendarService.prototype.setSelectedSlotEvents = function (_events, startDate) {
                    this.selectedEvents.next({ events: _events, date: startDate });
                };
                NccCalendarService.prototype.getMonthNames = function () {
                    return this.monthNames;
                };
                NccCalendarService.prototype.formatDateForApiSearch = function (_date) {
                    var date = (typeof _date === "string") ? new Date(_date) : _date;
                    var month = '' + (date.getMonth() + 1), day = '' + date.getDate(), year = date.getFullYear();
                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;
                    return [year, month, day].join('-');
                };
                NccCalendarService.prototype.formatEventDateOutput = function (startDate, endDate, current) {
                    var date = startDate;
                    if (kendo.toString(startDate, "d") != kendo.toString(current, "d")) {
                        date = current;
                    }
                    var day = date.getDate();
                    var monthIndex = date.getMonth();
                    var year = date.getFullYear();
                    var eventTime = this.getEventStartEndTime(startDate, endDate);
                    return this.monthNames[monthIndex] + ' ' + day + ', ' + year + ', ' + eventTime.start + ' - ' + eventTime.end;
                };
                NccCalendarService.prototype.getEventStartEndTime = function (_startDate, _endDate) {
                    return {
                        start: this.formatUSLocaleTime(_startDate),
                        end: this.formatUSLocaleTime(_endDate)
                    };
                };
                NccCalendarService.prototype.formatUSLocaleTime = function (date) {
                    return date.toLocaleString('en-US', this.dateLocaleOptions);
                };
                NccCalendarService.prototype.getTimezone = function () {
                    return this.timezone;
                };
                NccCalendarService = __decorate([
                    core_1.Injectable(),
                    __param(0, core_1.Inject(EVENTS_API_URL)), __param(1, core_1.Inject(EVENTS_FILTERS_URL)),
                    __metadata("design:paramtypes", [String, String, typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
                ], NccCalendarService);
                return NccCalendarService;
                var _a;
            }());
            exports_1("NccCalendarService", NccCalendarService);
        }
    };
});
//# sourceMappingURL=ncc-calendar.service.js.map