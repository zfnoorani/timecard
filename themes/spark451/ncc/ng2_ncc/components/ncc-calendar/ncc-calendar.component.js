System.register(["@angular/core", "../ncc-calendar/ncc-calendar.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_calendar_service_1, NccCalendarComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_calendar_service_1_1) {
                ncc_calendar_service_1 = ncc_calendar_service_1_1;
            }
        ],
        execute: function () {
            NccCalendarComponent = (function () {
                function NccCalendarComponent(_el, _calendarService) {
                    var _this = this;
                    this._el = _el;
                    this._calendarService = _calendarService;
                    this.isLoading = true;
                    this.eventFilters = [];
                    this.monthNames = [];
                    this.isMobile = false;
                    this._eventTemplate = function (data) {
                        var eventTime = _this._calendarService.getEventStartEndTime(data.start, data.end);
                        return "\n            <div class=\"ncc-event-template\" data-id=\"ncc-calendar-event-" + data.id + "\">\n                <p class=\"ncc-description no-margin\">\n                    <a href=\"" + data.url + "\">" + data.title + "</a>\n                </p>\n                <div class=\"hidden\">\n                    <div class=\"ncc-calendar-tooltip\" id=\"ncc-calendar-event-" + data.id + "\">\n                        <h3 class=\"ncc-subhead-1 no-margin\">" + data.title + "</h3>\n                        <hr class=\"default-margin bg-wolverine s2 short\">\n                        <p class=\"ncc-description no-margin\">" + data.address + "<br />" + eventTime.start + " - " + eventTime.end + "</p>\n                    </div>\n                </div>\n            </div>";
                    };
                    this.timezone = this._calendarService.getTimezone();
                    this.isMobile = (window.innerWidth <= 1024);
                    var context = this;
                    this.monthNames = this._calendarService.getMonthNames();
                    var NccMonthView = this.initializeNccMonthView(context);
                    this._kendoSchedulerOptions = {};
                    this._kendoSchedulerOptions.views = [
                        { type: NccMonthView, title: "NCC Month", eventHeight: 30 }
                    ];
                    this._kendoSchedulerOptions.editable = false;
                    this._kendoSchedulerOptions.eventTemplate = this._eventTemplate;
                    this._kendoSchedulerOptions.timezone = this.timezone;
                    this._kendoSchedulerOptions.dataSource = [];
                    this._kendoSchedulerOptions.change = function (event) {
                        var view = this.view();
                        var slot = this.slotByElement(view._current);
                        var eventsInRange = event.sender.occurrencesInRange(slot.startDate, slot.endDate);
                        if (context.isMobile) {
                            context._calendarService.setSelectedSlotEvents(eventsInRange, slot.startDate);
                        }
                    };
                    this._kendoSchedulerOptions.dataBinding = function (event) {
                        _this.setCalendarMonthLabels(event.sender);
                    };
                    this._kendoSchedulerOptions.dataBound = function (event) {
                        var scheduler = this;
                        //console.log("Scheduler: %o", scheduler);
                        var view = scheduler.view();
                        var tds = view.content.find("td");
                        tds.each(function () {
                            var slot = scheduler.slotByElement(jQuery(this));
                            //jldust 2-7-19
                            //Only process the data if slot is not null
                            //NOTE: Commented out the null check 2-11-19 until after update to 8.5.10
                            //The null check causes duplication on /calendar with regards to the date picker
                            //if(slot!=null){
                                //console.log("This is the slot: %o" , slot);
                                var ocurrences = scheduler.occurrencesInRange(slot.startDate, slot.endDate);
                                if (kendo.date.isToday(slot.startDate) && context.isMobile) {
                                    context._calendarService.setSelectedSlotEvents(ocurrences, slot.startDate);
                                }
                                if (!ocurrences.length && this.classList.contains('ncc-slot-has-events')) {
                                    this.classList.remove('ncc-slot-has-events');
                                }
                                if (ocurrences.length) {
                                    this.classList.add('ncc-slot-has-events');
                                }
                            //}
                        });
                    };
                    this._kendoSchedulerOptions.navigate = function (event) {
                        if (event.action == 'changeView') {
                            event.preventDefault();
                            return false;
                        }
                        else if (event.action == 'next' || event.action == 'previous' || event.action == 'changeDate') {
                            var kData = _this._kendoScheduler.data('kendoScheduler');
                            var view = kData.view();
                            var time = event.date.getTime();
                            if (!kendo.date.isInDateRange(event.date, view._firstDayOfMonth, view._lastDayOfMonth)) {
                                kData.date(event.date);
                                var currentView = event.sender.view();
                                _this._calendarService.getEvents(currentView._firstDayOfMonth, currentView._lastDayOfMonth);
                                setTimeout(function () {
                                    context.adjustSelection(kData, time);
                                });
                            }
                            else {
                                context.adjustSelection(kData, time);
                                event.preventDefault();
                                return false;
                            }
                        }
                    };
                    this._kendoSchedulerOptions.footer = false;
                    this._kendoSchedulerOptions.selectable = true;
                    var minDate = new Date().setFullYear(new Date().getFullYear() - 10);
                    var maxDate = new Date().setFullYear(new Date().getFullYear() + 10);
                    this._kendoSchedulerOptions.min = new Date(minDate);
                    this._kendoSchedulerOptions.max = new Date(maxDate);
                }
                NccCalendarComponent.prototype.initializeNccMonthView = function (context) {
                    var MORE_BUTTON_TEMPLATE = kendo.template("<div style=\"width:#=width#px;left:#=left#px;top:#=top#px\" class=\"k-more-events ncc-more-events ncc-label\">\n            <a href=\"\" style=\"font-size:8pt; margin-top: 0;\"> #=hiddenevents# More Event#if(hiddenevents > 1){#s#}#</a>\n            </div>");
                    return kendo.ui.MonthView.extend({
                        _createRow: function (startDate, startIdx, cellsPerRow, groupIndex) {
                            var that = this;
                            var min = that._firstDayOfMonth;
                            var max = that._lastDayOfMonth;
                            var content = that.dayTemplate;
                            var classes = '';
                            var html = '';
                            var resources = function () {
                                return that._resourceBySlot({ groupIndex: groupIndex });
                            };
                            for (var cellIdx = 0; cellIdx < cellsPerRow; cellIdx++) {
                                var time = kendo.date.getDate(startDate).getTime();
                                classes = '';
                                if (kendo.date.isToday(startDate)) {
                                    classes += 'k-today';
                                }
                                if (!kendo.date.isInDateRange(startDate, min, max)) {
                                    classes += ' k-other-month';
                                }
                                html += '<td ';
                                if (classes !== '') {
                                    html += 'class="' + classes + '"';
                                }
                                html += ' data-time="' + time + '"';
                                html += '>';
                                html += content({
                                    date: startDate,
                                    resources: resources
                                });
                                html += '</td>';
                                that._slotIndices[time] = startIdx + cellIdx;
                                startDate = kendo.date.nextDay(startDate);
                            }
                            return html;
                        },
                        _positionEvent: function (slotRange, element, group) {
                            var eventHeight = this.options.eventHeight;
                            var startSlot = slotRange.start;
                            if (slotRange.start.offsetLeft > slotRange.end.offsetLeft) {
                                startSlot = slotRange.end;
                            }
                            var startIndex = slotRange.start.index;
                            var endIndex = slotRange.end.index;
                            var eventCount = startSlot.eventCount;
                            var events = kendo.ui.SchedulerView.collidingEvents(slotRange.events(), startIndex, endIndex);
                            var rightOffset = startIndex !== endIndex ? 5 : 4;
                            if (startIndex != endIndex) {
                                element[0].classList.add('ncc-multiday-event');
                            }
                            events.push({
                                element: element,
                                start: startIndex,
                                end: endIndex
                            });
                            var scheduler = jQuery(slotRange.start.element).closest("[data-role=scheduler]").getKendoScheduler();
                            var rows = kendo.ui.SchedulerView.createRows(events);
                            for (var idx = 0, length = Math.min(rows.length, eventCount); idx < length; idx++) {
                                var rowEvents = rows[idx].events;
                                var eventTop = startSlot.offsetTop + startSlot.firstChildHeight + idx * eventHeight + 3 * idx + startSlot.firstChildTop + "px";
                                for (var j = 0, eventLength = rowEvents.length; j < eventLength; j++) {
                                    rowEvents[j].element[0].style.top = eventTop;
                                }
                            }
                            if (rows.length > eventCount) {
                                for (var slotIndex = startIndex; slotIndex <= endIndex; slotIndex++) {
                                    var collection = slotRange.collection;
                                    var slot = collection.at(slotIndex);
                                    if (slot.more) {
                                        return;
                                    }
                                    var eventsInRange = scheduler.occurrencesInRange(startSlot.startDate(), startSlot.endDate());
                                    var hiddenEvents = (eventsInRange.length - rows.length + 1);
                                    var options = {
                                        startSlot: slotRange.start,
                                        endSlot: slotRange.end,
                                        rowsCount: rows.length,
                                        ns: kendo.ns,
                                        start: slotIndex,
                                        end: slotIndex,
                                        width: slot.clientWidth - 2,
                                        left: slot.offsetLeft + 2,
                                        top: slot.offsetTop + slot.firstChildHeight + eventCount * eventHeight + 3 * eventCount + slot.firstChildTop,
                                        hiddenevents: hiddenEvents
                                    };
                                    if (hiddenEvents) {
                                        slot.more = jQuery(MORE_BUTTON_TEMPLATE(options));
                                        slot.more[0].addEventListener('click', function (event) {
                                            var slot = scheduler.slotByElement(event.target);
                                            var occurences = scheduler.occurrencesInRange(slot.startDate, slot.endDate);
                                            context._calendarService.setSelectedSlotEvents(occurences, slot.startDate);
                                        });
                                        this.content[0].appendChild(slot.more[0]);
                                    }
                                }
                            }
                            else {
                                slotRange.addEvent({
                                    element: element,
                                    start: startIndex,
                                    end: endIndex,
                                    groupIndex: startSlot.groupIndex
                                });
                                element[0].style.width = slotRange.innerWidth() - rightOffset + "px";
                                element[0].style.left = startSlot.offsetLeft + 2 + "px";
                                element[0].style.height = eventHeight + "px";
                                group._continuousEvents.push({
                                    element: element,
                                    uid: element.attr(kendo.attr("uid")),
                                    start: slotRange.start,
                                    end: slotRange.end
                                });
                                element.appendTo(this.content);
                            }
                        },
                        _layout: function () {
                            var calendarInfo = this.calendarInfo();
                            var weekDayNames = this._isMobile() ? calendarInfo.days.namesShort : calendarInfo.days.namesAbbr;
                            var names = weekDayNames.slice(calendarInfo.firstDay).concat(weekDayNames.slice(0, calendarInfo.firstDay));
                            var columns = jQuery.map(names, function (value) {
                                return { text: value };
                            });
                            var resources = this.groupedResources;
                            var rows;
                            if (resources.length) {
                                if (this._isVerticallyGrouped()) {
                                    var inner = [];
                                    for (var idx = 0; idx < 6; idx++) {
                                        inner.push({
                                            text: '<div>&nbsp;</div>',
                                            className: 'k-hidden k-slot-cell'
                                        });
                                    }
                                    rows = this._createRowsLayout(resources, inner, this.groupHeaderTemplate);
                                }
                                else {
                                    columns = this._createColumnsLayout(resources, columns, this.groupHeaderTemplate);
                                }
                            }
                            return {
                                columns: columns,
                                rows: rows
                            };
                        },
                        _selectSlots: function (selection) {
                            var isAllDay = selection.isAllDay;
                            var group = this.groups[selection.groupIndex];
                            if (!group.timeSlotCollectionCount()) {
                                isAllDay = true;
                            }
                            this._selectedSlots = [];
                            var ranges = group.ranges(selection.start, selection.end, isAllDay, false);
                            var element;
                            var slot;
                            for (var rangeIndex = 0; rangeIndex < ranges.length; rangeIndex++) {
                                var range = ranges[rangeIndex];
                                var collection = range.collection;
                                slot = collection.at(range.start.index);
                                element = slot.element;
                                element.setAttribute('aria-selected', 'true');
                                element.className = element.className.replace(/\s*k-state-selected/, '') + ' k-state-selected';
                                this._selectedSlots.push({
                                    start: slot.startDate(),
                                    end: slot.endDate(),
                                    element: element
                                });
                                break;
                            }
                            if (selection.backward) {
                                element = ranges[0].start.element;
                            }
                            this.current(element);
                        }
                    });
                };
                NccCalendarComponent.prototype.onResize = function (event) {
                    var target = event.target;
                    this.isMobile = (target.innerWidth <= 1024);
                };
                NccCalendarComponent.prototype.formatUSLocaleTime = function (date) {
                    return this._calendarService.formatUSLocaleTime(date);
                };
                NccCalendarComponent.prototype.createKendoScheduler = function (options) {
                    var _this = this;
                    this._kendoScheduler = jQuery(this.elscheduler.nativeElement).kendoScheduler(options);
                    var kData = this._kendoScheduler.data('kendoScheduler');
                    this.setCalendarMonthLabels(kData);
                    this.isLoading = false;
                    setTimeout(function () {
                        _this.repositionCalendarPicker(kData);
                    });
                };
                NccCalendarComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._calendarService.filters.subscribe(function (filters) {
                        _this.eventFilters = filters;
                    });
                    this._calendarService.selectedEvents.subscribe(function (_selectedEvents) {
                        _this.selectedEvents = _selectedEvents;
                    });
                };
                NccCalendarComponent.prototype.ngAfterViewInit = function () {
                    var _this = this;
                    this._calendarService.events.subscribe(function (events) {
                        _this.events = events;
                        if (_this.events) {
                            var scheduler = jQuery(_this.elscheduler.nativeElement).data("kendoScheduler");
                            if (typeof scheduler !== 'undefined') {
                                var dataSource = new kendo.data.SchedulerDataSource({
                                    data: _this.events
                                });
                                scheduler.setDataSource(dataSource);
                            }
                            else {
                                _this._kendoSchedulerOptions.dataSource = _this.events;
                                _this.createKendoScheduler(_this._kendoSchedulerOptions);
                            }
                        }
                    });
                };
                NccCalendarComponent.prototype.hasActiveFilters = function () {
                    var hasActive = false;
                    if (!this.eventFilters || !this.eventFilters.length) {
                        return hasActive;
                    }
                    for (var i = this.eventFilters.length - 1; i >= 0; i--) {
                        if (this.eventFilters[i]['active']) {
                            return true;
                        }
                    }
                    return hasActive;
                };
                NccCalendarComponent.prototype.toggleFilterById = function (event, filterId) {
                    this._calendarService.toggleFilterById(filterId);
                    var kData = this._kendoScheduler.data('kendoScheduler');
                    var view = kData.view();
                    if (view._selectedSlots.length && view._current) {
                        kData.trigger('change');
                    }
                    else {
                        this._calendarService.setSelectedSlotEvents([], new Date());
                    }
                };
                NccCalendarComponent.prototype.adjustSelection = function (that, time) {
                    var contentDiv = that.element.find("div.k-scheduler-content");
                    var slotEl = contentDiv.find('td[data-time="' + time + '"]');
                    if (slotEl.length) {
                        var slot = that.slotByElement(slotEl);
                        that.select({ start: slot.startDate, end: slot.endDate, isAllDay: slot.isDaySlot });
                        that._adjustSelectedDate();
                    }
                    return slotEl.length;
                };
                NccCalendarComponent.prototype.repositionCalendarPicker = function (kData) {
                    jQuery(kData.toolbar).clone(true).appendTo(".ncc-calendar-toolbar");
                    kData.toolbar.remove();
                    if (kData.calendar) {
                        kData.calendar.destroy();
                        kData.popup.destroy();
                    }
                    kData.toolbar = jQuery('.ncc-calendar-toolbar');
                    kData.element.kendoTooltip({
                        filter: ".k-event:not(.k-event-drag-hint) > div, .k-task",
                        position: "top",
                        width: 210,
                        animation: {
                            close: {
                                effects: "fade:out",
                                duration: 200
                            }
                        },
                        content: function (data) {
                            return data.target[0].getElementsByClassName('ncc-calendar-tooltip')[0].cloneNode(true);
                        }
                    });
                };
                NccCalendarComponent.prototype.setCalendarMonthLabels = function (kData) {
                    this.currentMonth = kData._model.formattedDate;
                    this.nextMonth = this.monthNames[kData.view().nextDate().getMonth()];
                    this.previousMonth = this.monthNames[kData.view().previousDate().getMonth()];
                    jQuery('.k-nav-current .k-sm-date-format', kData.toolbar).html(this.currentMonth);
                    jQuery('.k-nav-current .k-lg-date-format', kData.toolbar).html(this.currentMonth);
                };
                NccCalendarComponent.prototype.advanceToNextMonth = function () {
                    var kData = this._kendoScheduler.data('kendoScheduler');
                    var date = kData.view().nextDate();
                    this.goToMonth('next', date, kData);
                };
                NccCalendarComponent.prototype.backToPreviousMonth = function () {
                    var kData = this._kendoScheduler.data('kendoScheduler');
                    var date = kData.view().previousDate();
                    this.goToMonth('previous', date, kData);
                };
                NccCalendarComponent.prototype.goToMonth = function (action, date, kData) {
                    if (!kData.trigger('navigate', {
                        view: kData._selectedViewName,
                        action: action,
                        date: date
                    })) {
                    }
                };
                __decorate([
                    core_1.ViewChild('elscheduler'),
                    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
                ], NccCalendarComponent.prototype, "elscheduler", void 0);
                __decorate([
                    core_1.ViewChild('elselectedevents'),
                    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
                ], NccCalendarComponent.prototype, "elselectedevents", void 0);
                NccCalendarComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-calendar',
                        templateUrl: 'ncc-calendar.component.html',
                        styleUrls: ['ncc-calendar.component.css'],
                        host: {
                            '(window:resize)': 'onResize($event)'
                        }
                    }),
                    __metadata("design:paramtypes", [typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object, ncc_calendar_service_1.NccCalendarService])
                ], NccCalendarComponent);
                return NccCalendarComponent;
                var _a, _b, _c;
            }());
            exports_1("NccCalendarComponent", NccCalendarComponent);
        }
    };
});
//# sourceMappingURL=ncc-calendar.component.js.map