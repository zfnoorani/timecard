export interface EventFilter{
    id: string;
    name: string;
}

export interface Event {
    id:  string,
    title:  string,
    start:  Date,
    end:  Date,
    description: string,
    url:  string,
    type:  string,
    room?:  string,
    address?:  string,
    city?: string,
    zip_code?:  string,
    state?:  string,
    categories?:  string[]
}

export interface SelectedEvents {
    events: Event[],
    date: Date
}