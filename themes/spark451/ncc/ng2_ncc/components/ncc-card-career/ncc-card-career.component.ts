import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-career',
    templateUrl: 'ncc-card-career.component.html',
    styleUrls: ['ncc-card-career.component.css']
})

export class NccCardCareerComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    @Input() linkLabel: string;
    @Input() linkUrl: string;

    @ViewChild('elgraphic') elgraphic: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let graphicAnimation = this._ab.css();
        graphicAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.7', 'color': '#689f46' })
            .setToStyles({ 'opacity': '0.1', 'color': '#35393c' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.elgraphic && this.elcontent) {
            graphicAnimation.start(this.elgraphic.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elgraphic && this.elcontent) {
            this.elgraphic.nativeElement.style.opacity = '0.1';
            this.elgraphic.nativeElement.style.color = '#35393c';

            this.elcontent.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let graphicAnimation = this._ab.css();
        graphicAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.1', 'color': '#35393c' })
            .setToStyles({ 'opacity': '0.7', 'color': '#689f46' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        if (this.elgraphic && this.elcontent) {
            graphicAnimation.start(this.elgraphic.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);

        }*/

        if (this.elgraphic && this.elcontent) {
            this.elgraphic.nativeElement.style.opacity = '0.7';
            this.elgraphic.nativeElement.style.color = '#689f46';

            this.elcontent.nativeElement.style.opacity = '0';
        }
    }

}
