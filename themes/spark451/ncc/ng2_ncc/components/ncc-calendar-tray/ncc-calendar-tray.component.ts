import {Component, ElementRef, HostBinding, Input} from '@angular/core';

import {NccCalendarService} from '../ncc-calendar/ncc-calendar.service';
import {Event, EventFilter, SelectedEvents} from '../ncc-calendar/ncc-calendar.interface';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-calendar-tray',
    templateUrl: 'ncc-calendar-tray.component.html',
    styleUrls: ['ncc-calendar-tray.component.css']
})

export class NccCalendarTrayComponent {
    private selectedEvents: SelectedEvents;
    private monthNames: string[] = [];
    private currentDay: string;

    @HostBinding('class.tray-open') isTrayOpen: boolean = false;

    constructor(private _calendarService: NccCalendarService) {
        this.monthNames = this._calendarService.getMonthNames();
        // Subscribe to list of selected events
        this._calendarService.selectedEvents.subscribe((data: SelectedEvents) => {
            this.selectedEvents = data;
            this.isTrayOpen = (this.selectedEvents && this.selectedEvents.events && this.selectedEvents.events.length) ? true : false;
        });
    }

    // Close Tray
    public trayClose(): void {
        this.isTrayOpen = false;
    }

    // Format Event date for listing output
    private formatEventDateOutput(startDate: Date, endDate: Date): string {
        return this._calendarService.formatEventDateOutput(startDate, endDate, this.selectedEvents.date)
    }

    // Format Tray Heading
    private formatHeading(): string{
        var monthIndex = this.selectedEvents.date.getMonth();
        return this.monthNames[monthIndex] + ' '+ this.daySuffix(this.selectedEvents.date);
    }

    // Calculate date suffix for heading
    private daySuffix(date: Date): string {
        var day = date.getDate();
        this.currentDay = ( '0' + day ).slice(-2);
        if(day > 3 && day < 21){
             return day + 'TH';
        }
        switch (day % 10) {
            case 1:  return day + 'ST';
            case 2:  return day + 'ND';
            case 3:  return day + 'RD';
            default: return day + 'TH';
        }
    }
}
