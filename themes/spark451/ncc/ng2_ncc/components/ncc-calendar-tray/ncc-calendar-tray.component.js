System.register(["@angular/core", "../ncc-calendar/ncc-calendar.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_calendar_service_1, NccCalendarTrayComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_calendar_service_1_1) {
                ncc_calendar_service_1 = ncc_calendar_service_1_1;
            }
        ],
        execute: function () {
            NccCalendarTrayComponent = (function () {
                function NccCalendarTrayComponent(_calendarService) {
                    var _this = this;
                    this._calendarService = _calendarService;
                    this.monthNames = [];
                    this.isTrayOpen = false;
                    this.monthNames = this._calendarService.getMonthNames();
                    this._calendarService.selectedEvents.subscribe(function (data) {
                        _this.selectedEvents = data;
                        _this.isTrayOpen = (_this.selectedEvents && _this.selectedEvents.events && _this.selectedEvents.events.length) ? true : false;
                    });
                }
                NccCalendarTrayComponent.prototype.trayClose = function () {
                    this.isTrayOpen = false;
                };
                NccCalendarTrayComponent.prototype.formatEventDateOutput = function (startDate, endDate) {
                    return this._calendarService.formatEventDateOutput(startDate, endDate, this.selectedEvents.date);
                };
                NccCalendarTrayComponent.prototype.formatHeading = function () {
                    var monthIndex = this.selectedEvents.date.getMonth();
                    return this.monthNames[monthIndex] + ' ' + this.daySuffix(this.selectedEvents.date);
                };
                NccCalendarTrayComponent.prototype.daySuffix = function (date) {
                    var day = date.getDate();
                    this.currentDay = ('0' + day).slice(-2);
                    if (day > 3 && day < 21) {
                        return day + 'TH';
                    }
                    switch (day % 10) {
                        case 1: return day + 'ST';
                        case 2: return day + 'ND';
                        case 3: return day + 'RD';
                        default: return day + 'TH';
                    }
                };
                __decorate([
                    core_1.HostBinding('class.tray-open'),
                    __metadata("design:type", Boolean)
                ], NccCalendarTrayComponent.prototype, "isTrayOpen", void 0);
                NccCalendarTrayComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-calendar-tray',
                        templateUrl: 'ncc-calendar-tray.component.html',
                        styleUrls: ['ncc-calendar-tray.component.css']
                    }),
                    __metadata("design:paramtypes", [ncc_calendar_service_1.NccCalendarService])
                ], NccCalendarTrayComponent);
                return NccCalendarTrayComponent;
            }());
            exports_1("NccCalendarTrayComponent", NccCalendarTrayComponent);
        }
    };
});
//# sourceMappingURL=ncc-calendar-tray.component.js.map