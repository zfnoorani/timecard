System.register(["@angular/core", "../ncc-element-query/ncc-element-query.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_element_query_service_1, NccStackComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_element_query_service_1_1) {
                ncc_element_query_service_1 = ncc_element_query_service_1_1;
            }
        ],
        execute: function () {
            NccStackComponent = (function () {
                function NccStackComponent(_el, elementQueryService) {
                    this._el = _el;
                    this.elementQueryService = elementQueryService;
                    this.addClass = true;
                    this.cards = [];
                    this.elementQueryService.setElement(this._el);
                }
                NccStackComponent.prototype._activateCard = function (index) {
                    this.cards.forEach(function (card) {
                        card.isPeekAdjacent = false;
                        card.isPeek = false;
                        if (card.isActive) {
                            card.isActive = false;
                        }
                    });
                    for (var i = 5, j = 0; i > 0; i--, j++) {
                        if (this.cards[index] && !j) {
                            this.cards[index].inactiveStateCardAnimation(i);
                        }
                        if (this.cards[index - j] && j > 0) {
                            this.cards[index - j].inactiveStateCardAnimation(i);
                        }
                        if (this.cards[index + j] && j > 0) {
                            this.cards[index + j].inactiveStateCardAnimation(i);
                        }
                    }
                    for (var i = 5, j = 0; i > 0; i--, j++) {
                        if (this.cards[index] && !j) {
                            this.cards[index].activeStateCardAnimation();
                            this.cards[index].isActive = true;
                        }
                        if (this.cards[index - j] && j > 0) {
                            this.cards[index - j].activeStateCardAnimation();
                        }
                        if (this.cards[index + j] && j > 0) {
                            this.cards[index + j].activeStateCardAnimation();
                        }
                    }
                    if (this.cards[index + 1]) {
                        this.cards[index + 1].isPeekAdjacent = true;
                    }
                    else if (this.cards[index - 1]) {
                        this.cards[index - 1].isPeekAdjacent = true;
                    }
                };
                NccStackComponent.prototype.ngOnInit = function () {
                    this.elementQueryService.updateMinMaxWidthAttributes();
                };
                NccStackComponent.prototype.ngAfterViewInit = function () {
                    var index = 0;
                    this.cards.forEach(function (card, cardIndex) {
                        if (card.isActive) {
                            index = cardIndex;
                        }
                    });
                    for (var i = 5, j = 0; i > 0; i--, j++) {
                        if (this.cards[index] && !j) {
                            this.cards[index].updateZIndex(i);
                        }
                        if (this.cards[index - j] && j > 0) {
                            this.cards[index - j].updateZIndex(i);
                        }
                        if (this.cards[index + j] && j > 0) {
                            this.cards[index + j].updateZIndex(i);
                        }
                    }
                };
                NccStackComponent.prototype.onSwipeLeft = function (event) {
                    var index = this.cards.indexOf(this.getActiveCard());
                    if (this.cards[index + 1]) {
                        this._activateCard(index + 1);
                    }
                };
                NccStackComponent.prototype.onSwipeRight = function (event) {
                    var index = this.cards.indexOf(this.getActiveCard());
                    if (this.cards[index - 1]) {
                        this._activateCard(index - 1);
                    }
                };
                NccStackComponent.prototype.onResize = function (event) {
                    this.elementQueryService.updateMinMaxWidthAttributes();
                };
                NccStackComponent.prototype.deactivateCards = function () {
                    this.cards.forEach(function (card) {
                        if (card.isActive) {
                            card.isActive = false;
                        }
                    });
                };
                NccStackComponent.prototype.activateCard = function (card) {
                    var index = this.cards.indexOf(card);
                    this._activateCard(index);
                };
                NccStackComponent.prototype.addCard = function (card) {
                    this.cards.push(card);
                };
                NccStackComponent.prototype.removeCard = function (card) {
                    var index = this.cards.indexOf(card);
                    if (index !== -1) {
                        this.cards.splice(index, 1);
                    }
                };
                NccStackComponent.prototype.getCards = function () {
                    return this.cards;
                };
                NccStackComponent.prototype.getActiveCard = function () {
                    var activeCard;
                    this.cards.forEach(function (card) {
                        if (card.isActive) {
                            activeCard = card;
                        }
                    });
                    return activeCard;
                };
                NccStackComponent.prototype.peekCard = function (card) {
                    var index = this.cards.indexOf(card);
                    if (index !== -1 && !card.isActive) {
                        this.cards.forEach(function (card) {
                            if (!card.isActive) {
                                card.isPeekAdjacent = false;
                                card.isPeek = false;
                            }
                        });
                        card.isPeek = true;
                    }
                };
                NccStackComponent.prototype.unpeekCard = function (card) {
                    var _this = this;
                    var index = this.cards.indexOf(card);
                    if (index !== -1) {
                        this.cards.forEach(function (card, index) {
                            card.isPeek = false;
                            if (card.isActive) {
                                if (_this.cards[index + 1]) {
                                    _this.cards[index + 1].isPeekAdjacent = true;
                                }
                                else if (_this.cards[index - 1]) {
                                    _this.cards[index - 1].isPeekAdjacent = true;
                                }
                            }
                        });
                    }
                };
                NccStackComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-stack',
                        template: "\n    <div (window:resize)=\"onResize($event)\">\n      <ng-content></ng-content>\n    </div>\n    ",
                        styleUrls: ['ncc-stack.component.css'],
                        providers: [
                            ncc_element_query_service_1.NccElementQueryService
                        ]
                    }),
                    __metadata("design:paramtypes", [typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object, ncc_element_query_service_1.NccElementQueryService])
                ], NccStackComponent);
                return NccStackComponent;
                var _a;
            }());
            exports_1("NccStackComponent", NccStackComponent);
        }
    };
});
//# sourceMappingURL=ncc-stack.component.js.map