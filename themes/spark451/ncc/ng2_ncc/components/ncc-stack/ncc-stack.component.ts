import {Component, HostBinding, ElementRef, OnInit, AfterViewInit} from '@angular/core';

import {NccElementQueryService} from '../ncc-element-query/ncc-element-query.service';
import {NccCardComponent} from '../ncc-card/ncc-card.component';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-stack',
    template: `
    <div (window:resize)="onResize($event)">
      <ng-content></ng-content>
    </div>
    `,
    styleUrls: ['ncc-stack.component.css'],
    providers: [
        NccElementQueryService
    ]
})

export class NccStackComponent implements OnInit, AfterViewInit {
    // @HostBinding('class.card-group');
    private addClass: boolean = true;

    private cards: Array<NccCardComponent> = [];

    public constructor(private _el: ElementRef, private elementQueryService: NccElementQueryService) {
        this.elementQueryService.setElement(this._el);
    }

    private _activateCard(index: number): void {
        this.cards.forEach((card: NccCardComponent) => {
            card.isPeekAdjacent = false;
            card.isPeek = false;

            if (card.isActive) {
                card.isActive = false;
            }
        });

        for (let i = 5, j = 0; i > 0; i-- , j++) {
            if (this.cards[index] && !j) {
                this.cards[index].inactiveStateCardAnimation(i);
            }
            if (this.cards[index - j] && j > 0) {
                this.cards[index - j].inactiveStateCardAnimation(i);
            }
            if (this.cards[index + j] && j > 0) {
                this.cards[index + j].inactiveStateCardAnimation(i);
            }
        }

        for (let i = 5, j = 0; i > 0; i-- , j++) {
            if (this.cards[index] && !j) {
                this.cards[index].activeStateCardAnimation();
                this.cards[index].isActive = true;
            }
            if (this.cards[index - j] && j > 0) {
                this.cards[index - j].activeStateCardAnimation();
            }
            if (this.cards[index + j] && j > 0) {
                this.cards[index + j].activeStateCardAnimation();
            }
        }

        if (this.cards[index + 1]) {
            this.cards[index + 1].isPeekAdjacent = true
        } else if (this.cards[index - 1]) {
            this.cards[index - 1].isPeekAdjacent = true;
        }
    }

    public ngOnInit(): void {
        this.elementQueryService.updateMinMaxWidthAttributes();
    }

    public ngAfterViewInit(): void {
        // console.time('ngAfterViewInit()');

        let index = 0;

        this.cards.forEach((card: NccCardComponent, cardIndex: number) => {
            if (card.isActive) {
                index = cardIndex;
            }
        });

        for (let i = 5, j = 0; i > 0; i-- , j++) {
            if (this.cards[index] && !j) {
                this.cards[index].updateZIndex(i);
            }
            if (this.cards[index - j] && j > 0) {
                this.cards[index - j].updateZIndex(i);
            }
            if (this.cards[index + j] && j > 0) {
                this.cards[index + j].updateZIndex(i);
            }
        }

        // console.timeEnd('ngAfterViewInit()');
    }

    public onSwipeLeft(event: MouseEvent) {
        let index = this.cards.indexOf(this.getActiveCard());

        if (this.cards[index + 1]) {
            this._activateCard(index + 1);
        }
    }

    public onSwipeRight(event: MouseEvent) {
        let index = this.cards.indexOf(this.getActiveCard());

        if (this.cards[index - 1]) {
            this._activateCard(index - 1);
        }
    }

    public onResize(event: MouseEvent) {
        this.elementQueryService.updateMinMaxWidthAttributes();
    }

    public deactivateCards(): void {
        // console.time('deactivateCards()');

        this.cards.forEach((card: NccCardComponent) => {
            if (card.isActive) {
                card.isActive = false;
            }
        });

        // console.timeEnd('deactivateCards()');
    }

    public activateCard(card: NccCardComponent): void {
        // console.time('activateCard()');
        let index = this.cards.indexOf(card);

        this._activateCard(index);

        // console.timeEnd('activateCard()');
    }

    public addCard(card: NccCardComponent): void {
        // console.time('addCard()');

        this.cards.push(card);

        // console.timeEnd('addCard()');
    }

    public removeCard(card: NccCardComponent): void {
        // console.time('removeCard()');

        let index = this.cards.indexOf(card);

        if (index !== -1) {
            this.cards.splice(index, 1);
        }

        // console.timeEnd('removeCard()');
    }

    public getCards(): Array<NccCardComponent> {
        return this.cards;
    }

    public getActiveCard(): NccCardComponent {
        let activeCard: NccCardComponent;

        this.cards.forEach((card: NccCardComponent) => {
            if (card.isActive) {
                activeCard = card;
            }
        })

        return activeCard;
    }

    public peekCard(card: NccCardComponent): void {
        // console.time('peekCard()');

        let index = this.cards.indexOf(card);

        // if (index !== -1 && !card.isActive && !card.isPeekAdjacent) {
        if (index !== -1 && !card.isActive) {
            this.cards.forEach((card: NccCardComponent) => {
                if (!card.isActive) {
                    card.isPeekAdjacent = false;
                    card.isPeek = false;
                }
            });

            card.isPeek = true;
        }

        // console.timeEnd('peekCard()');
    }

    public unpeekCard(card: NccCardComponent): void {
        // console.time('unpeekCard()');

        let index = this.cards.indexOf(card);

        if (index !== -1) {
            this.cards.forEach((card: NccCardComponent, index: number) => {
                card.isPeek = false;

                if (card.isActive) {
                    if (this.cards[index + 1]) {
                        // first right card
                        this.cards[index + 1].isPeekAdjacent = true;
                    } else if (this.cards[index - 1]) {
                        // first left card (only if last card is active)
                        this.cards[index - 1].isPeekAdjacent = true;
                    }
                }
            })
        }

        // console.timeEnd('unpeekCard()');
    }

}
