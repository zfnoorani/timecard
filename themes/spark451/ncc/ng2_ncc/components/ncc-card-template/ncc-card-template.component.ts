import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-template', // element, [attribute], .class, and :not()
    // template: `Hello {{name}}`,
    templateUrl: 'ncc-card-template.component.html',
    //     styles: [`
    // :host {
    //     display: block;

    //     .primary {
    //         color: red;
    //     }
    // }
    //     `],
    styleUrls: ['ncc-card-template.component.css'],
    // directives: [MyDirective, MyComponent],
    // pipes: [MyPipe, OtherPipe],
    // providers: [MyService, provide(...)],
})

export class NccCardTemplateComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() description: string;
    @Input() linkLabel: string;
    @Input() linkUrl: string;
    @Input() tagLabel: string;
    @Input() tagUrl: string;
    @Input() image: string;
    @Input() graphic: string;
    @Input() icon: string;

    @ViewChild('eltitle') eltitle: ElementRef;
    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('elgraphic') elgraphic: ElementRef;
    @ViewChild('elicon') elicon: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.elcontent && this.eltitle) {
            headingAnimation.start(this.eltitle.nativeElement)
                .onComplete(() => {
                    contentAnimation.start(this.elcontent.nativeElement);
                });
        }*/

        if (this.eltitle && this.elcontent) {
            this.eltitle.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.opacity = '1';
        }

        // .onComplete(() => { })

        // 'pointer-events': 'none',
        // 'pointer-events': 'auto',
    }

    public inactiveStateContentAnimation(): void {
        /*let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({ 'top': '0' })
            .setToStyles({ 'top': '-500px' })

        let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(500)
            .setFromStyles({ 'bottom': '0' })
            .setToStyles({ 'bottom': '-55px' })

        if (this.elcontent && this.eltitle) {
            contentAnimation.start(this.elcontent.nativeElement)
                .onComplete(() => {
                    headingAnimation.start(this.eltitle.nativeElement);
                });
        }*/

        if (this.eltitle && this.elcontent) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.eltitle.nativeElement.style.opacity = '1';
        }

        // .onComplete(() => { })

        // 'pointer-events': 'none',
        // 'pointer-events': 'auto',
    }

}
