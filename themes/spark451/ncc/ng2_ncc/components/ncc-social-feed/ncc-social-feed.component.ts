import {Component, Input, ViewChild, ElementRef, Host, OnInit, AfterViewInit} from '@angular/core';

declare var Swiper: any;

@Component({
    moduleId: __moduleName,
    selector: 'ncc-social-feed',
    templateUrl: 'ncc-social-feed.component.html',
    styleUrls: ['ncc-social-feed.component.css']
})

export class NccSocialFeedComponent implements OnInit, AfterViewInit {
    @Input() hidePager: boolean;
    @Input() hideNavigation: boolean;
    @Input() moveToClicked: boolean;
    @Input() options: any;

    @ViewChild('elswipercontainer') elswipercontainer: ElementRef;
    @ViewChild('elswiperpagination') elswiperpagination: ElementRef;
    @ViewChild('elpaginationwrapper') elpaginationwrapper: ElementRef;

    private swiper: any;

    constructor(private elementRef: ElementRef) { }

    private defaults(dest: any, ...args: any[]) {
        for (let i = arguments.length - 1; i >= 1; i--) {
            let source = arguments[i] || {};
            for (let key in source) {
                if (source.hasOwnProperty(key) && !dest.hasOwnProperty(key)) {
                    dest[key] = source[key];
                }
            }
        }

        return dest;
    }

    public ngOnInit(): void {
        if (typeof Swiper !== 'function') {
            console.error("ncc-social-feed: Swiper is not defined");
        }
    }

    public ngAfterViewInit(): void {
        if (!(this.options instanceof Object)) {
            this.options = JSON.parse(this.options);
        }

        var navContainer = this.elpaginationwrapper.nativeElement;
        var containerOffset = navContainer.getBoundingClientRect().left;

        let options = this.defaults({
            pagination: this.elswiperpagination.nativeElement,
            slidesOffsetBefore: containerOffset
        }, this.options);

        this.swiper = new Swiper(this.elswipercontainer.nativeElement, options);
    }

    public update(): void {
        this.swiper.update();
    }

    public moveNext(event: MouseEvent): void {
        event.preventDefault();
        this.swiper.slideNext();
    }

    public movePrev(event: MouseEvent): void {
        event.preventDefault();

        this.swiper.slidePrev();
    }

    public moveTo(index: number, event: MouseEvent): void {
        if (this.moveToClicked) {
            // event.preventDefault();
            this.swiper.slideTo(index);
        }
    }

    public attachEvents(): void {
        this.swiper.attachEvents();
    }

    public startAutoplay(): void {
        this.swiper.startAutoplay();
    }

    public destroy(deleteInstance: boolean = true, cleanupStyles: boolean = false): void {
        this.swiper.stopAutoplay(deleteInstance, cleanupStyles);
    }

    public appendSlide(slides: any): void {
        this.swiper.appendSlide(slides);
    }

    public prependSlide(slides: any): void {
        this.swiper.prependSlide(slides);
    }

    public removeSlide(slideIndex: number): void {
        this.swiper.removeSlide(slideIndex);
    }

    public removeAllSlides(): void {
        this.swiper.removeAllSlides();
    }

    public lockSwipes(): void {
        this.swiper.lockSwipes();
    }

    public unlockSwipes(): void {
        this.swiper.unlockSwipes();
    }
}
