System.register(["@angular/core"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, NccSocialFeedComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            }
        ],
        execute: function () {
            NccSocialFeedComponent = (function () {
                function NccSocialFeedComponent(elementRef) {
                    this.elementRef = elementRef;
                }
                NccSocialFeedComponent.prototype.defaults = function (dest) {
                    var args = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        args[_i - 1] = arguments[_i];
                    }
                    for (var i = arguments.length - 1; i >= 1; i--) {
                        var source = arguments[i] || {};
                        for (var key in source) {
                            if (source.hasOwnProperty(key) && !dest.hasOwnProperty(key)) {
                                dest[key] = source[key];
                            }
                        }
                    }
                    return dest;
                };
                NccSocialFeedComponent.prototype.ngOnInit = function () {
                    if (typeof Swiper !== 'function') {
                        console.error("ncc-social-feed: Swiper is not defined");
                    }
                };
                NccSocialFeedComponent.prototype.ngAfterViewInit = function () {
                    if (!(this.options instanceof Object)) {
                        this.options = JSON.parse(this.options);
                    }
                    var navContainer = this.elpaginationwrapper.nativeElement;
                    var containerOffset = navContainer.getBoundingClientRect().left;
                    var options = this.defaults({
                        pagination: this.elswiperpagination.nativeElement,
                        slidesOffsetBefore: containerOffset
                    }, this.options);
                    this.swiper = new Swiper(this.elswipercontainer.nativeElement, options);
                };
                NccSocialFeedComponent.prototype.update = function () {
                    this.swiper.update();
                };
                NccSocialFeedComponent.prototype.moveNext = function (event) {
                    event.preventDefault();
                    this.swiper.slideNext();
                };
                NccSocialFeedComponent.prototype.movePrev = function (event) {
                    event.preventDefault();
                    this.swiper.slidePrev();
                };
                NccSocialFeedComponent.prototype.moveTo = function (index, event) {
                    if (this.moveToClicked) {
                        this.swiper.slideTo(index);
                    }
                };
                NccSocialFeedComponent.prototype.attachEvents = function () {
                    this.swiper.attachEvents();
                };
                NccSocialFeedComponent.prototype.startAutoplay = function () {
                    this.swiper.startAutoplay();
                };
                NccSocialFeedComponent.prototype.destroy = function (deleteInstance, cleanupStyles) {
                    if (deleteInstance === void 0) { deleteInstance = true; }
                    if (cleanupStyles === void 0) { cleanupStyles = false; }
                    this.swiper.stopAutoplay(deleteInstance, cleanupStyles);
                };
                NccSocialFeedComponent.prototype.appendSlide = function (slides) {
                    this.swiper.appendSlide(slides);
                };
                NccSocialFeedComponent.prototype.prependSlide = function (slides) {
                    this.swiper.prependSlide(slides);
                };
                NccSocialFeedComponent.prototype.removeSlide = function (slideIndex) {
                    this.swiper.removeSlide(slideIndex);
                };
                NccSocialFeedComponent.prototype.removeAllSlides = function () {
                    this.swiper.removeAllSlides();
                };
                NccSocialFeedComponent.prototype.lockSwipes = function () {
                    this.swiper.lockSwipes();
                };
                NccSocialFeedComponent.prototype.unlockSwipes = function () {
                    this.swiper.unlockSwipes();
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Boolean)
                ], NccSocialFeedComponent.prototype, "hidePager", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Boolean)
                ], NccSocialFeedComponent.prototype, "hideNavigation", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Boolean)
                ], NccSocialFeedComponent.prototype, "moveToClicked", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Object)
                ], NccSocialFeedComponent.prototype, "options", void 0);
                __decorate([
                    core_1.ViewChild('elswipercontainer'),
                    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
                ], NccSocialFeedComponent.prototype, "elswipercontainer", void 0);
                __decorate([
                    core_1.ViewChild('elswiperpagination'),
                    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
                ], NccSocialFeedComponent.prototype, "elswiperpagination", void 0);
                __decorate([
                    core_1.ViewChild('elpaginationwrapper'),
                    __metadata("design:type", typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object)
                ], NccSocialFeedComponent.prototype, "elpaginationwrapper", void 0);
                NccSocialFeedComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-social-feed',
                        templateUrl: 'ncc-social-feed.component.html',
                        styleUrls: ['ncc-social-feed.component.css']
                    }),
                    __metadata("design:paramtypes", [typeof (_d = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _d || Object])
                ], NccSocialFeedComponent);
                return NccSocialFeedComponent;
                var _a, _b, _c, _d;
            }());
            exports_1("NccSocialFeedComponent", NccSocialFeedComponent);
        }
    };
});
//# sourceMappingURL=ncc-social-feed.component.js.map