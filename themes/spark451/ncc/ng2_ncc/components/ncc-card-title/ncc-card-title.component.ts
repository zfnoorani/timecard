import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-title',
    templateUrl: 'ncc-card-title.component.html',
    styleUrls: ['ncc-card-title.component.css']
})

export class NccCardTitleComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() icon: string;

    @ViewChild('eltitle1') eltitle1: ElementRef;
    @ViewChild('eltitle2') eltitle2: ElementRef;
    @ViewChild('elicon') elicon: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.15' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        if (this.eltitle1 && this.eltitle2 && this.elicon) {
            headingAnimation.start(this.eltitle1.nativeElement);
            contentAnimation.start(this.eltitle2.nativeElement);
            contentAnimation.start(this.elicon.nativeElement);
        }*/

        if (this.eltitle1 && this.eltitle2) {
            this.eltitle1.nativeElement.style.opacity = '0';
            this.eltitle2.nativeElement.style.opacity = '1';
        }

        if (this.elicon) {
            this.elicon.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '0.15' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        if (this.eltitle1 && this.eltitle2 && this.elicon) {
            contentAnimation.start(this.eltitle2.nativeElement);
            contentAnimation.start(this.elicon.nativeElement);
            headingAnimation.start(this.eltitle1.nativeElement);
        }*/

        if (this.eltitle1 && this.eltitle2) {
            this.eltitle2.nativeElement.style.opacity = '0';
            this.eltitle1.nativeElement.style.opacity = '0.15';
        }

        if (this.elicon) {
            this.elicon.nativeElement.style.opacity = '0';
        }
    }

}
