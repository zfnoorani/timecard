System.register(["@angular/core", "../ncc-card/ncc-card.base"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_card_base_1, NccCardTitleComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_card_base_1_1) {
                ncc_card_base_1 = ncc_card_base_1_1;
            }
        ],
        execute: function () {
            NccCardTitleComponent = (function (_super) {
                __extends(NccCardTitleComponent, _super);
                function NccCardTitleComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardTitleComponent.prototype.activeStateContentAnimation = function () {
                    if (this.eltitle1 && this.eltitle2) {
                        this.eltitle1.nativeElement.style.opacity = '0';
                        this.eltitle2.nativeElement.style.opacity = '1';
                    }
                    if (this.elicon) {
                        this.elicon.nativeElement.style.opacity = '1';
                    }
                };
                NccCardTitleComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.eltitle1 && this.eltitle2) {
                        this.eltitle2.nativeElement.style.opacity = '0';
                        this.eltitle1.nativeElement.style.opacity = '0.15';
                    }
                    if (this.elicon) {
                        this.elicon.nativeElement.style.opacity = '0';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTitleComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTitleComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardTitleComponent.prototype, "icon", void 0);
                __decorate([
                    core_1.ViewChild('eltitle1'),
                    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
                ], NccCardTitleComponent.prototype, "eltitle1", void 0);
                __decorate([
                    core_1.ViewChild('eltitle2'),
                    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
                ], NccCardTitleComponent.prototype, "eltitle2", void 0);
                __decorate([
                    core_1.ViewChild('elicon'),
                    __metadata("design:type", typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object)
                ], NccCardTitleComponent.prototype, "elicon", void 0);
                NccCardTitleComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-title',
                        templateUrl: 'ncc-card-title.component.html',
                        styleUrls: ['ncc-card-title.component.css']
                    }),
                    __metadata("design:paramtypes", [typeof (_d = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _d || Object])
                ], NccCardTitleComponent);
                return NccCardTitleComponent;
                var _a, _b, _c, _d;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardTitleComponent", NccCardTitleComponent);
        }
    };
});
//# sourceMappingURL=ncc-card-title.component.js.map