System.register(["@angular/core", "../ncc-card/ncc-card.base"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_card_base_1, NccCardCtaPhotoComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_card_base_1_1) {
                ncc_card_base_1 = ncc_card_base_1_1;
            }
        ],
        execute: function () {
            NccCardCtaPhotoComponent = (function (_super) {
                __extends(NccCardCtaPhotoComponent, _super);
                function NccCardCtaPhotoComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardCtaPhotoComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elimage && this.elbutton && this.eltitle) {
                        this.elimage.nativeElement.style.transform = 'scale(1)';
                        this.elbutton.nativeElement.style.top = '0';
                        this.elbutton.nativeElement.style.right = '0';
                        this.elbutton.nativeElement.style.width = '50px';
                        this.elbutton.nativeElement.style.height = '100%';
                        this.elbutton.nativeElement.style.border = 'none';
                        this.elbutton.nativeElement.style.transform = 'none';
                        this.elbutton.nativeElement.style.borderRadius = '0';
                        this.elbutton.nativeElement.style.backgroundColor = 'rgba(53,57,60,0.8)';
                        this.elbutton.nativeElement.style.pointerEvents = 'auto';
                        this.eltitle.nativeElement.style.opacity = '1';
                    }
                };
                NccCardCtaPhotoComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elimage && this.elbutton && this.eltitle) {
                        this.eltitle.nativeElement.style.opacity = '0';
                        this.elimage.nativeElement.style.transform = 'scale(1.15)';
                        this.elbutton.nativeElement.style.top = '50%';
                        this.elbutton.nativeElement.style.right = '50%';
                        this.elbutton.nativeElement.style.width = '55px';
                        this.elbutton.nativeElement.style.height = '55px';
                        this.elbutton.nativeElement.style.border = '3px solid rgba(255,192,59,0.7)';
                        this.elbutton.nativeElement.style.transform = 'translate(50%, -50%)';
                        this.elbutton.nativeElement.style.borderRadius = '50%';
                        this.elbutton.nativeElement.style.backgroundColor = 'transparent';
                        this.elbutton.nativeElement.style.pointerEvents = 'none';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaPhotoComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaPhotoComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaPhotoComponent.prototype, "image", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaPhotoComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_1.ViewChild('elimage'),
                    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
                ], NccCardCtaPhotoComponent.prototype, "elimage", void 0);
                __decorate([
                    core_1.ViewChild('eltitle'),
                    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
                ], NccCardCtaPhotoComponent.prototype, "eltitle", void 0);
                __decorate([
                    core_1.ViewChild('elbutton'),
                    __metadata("design:type", typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object)
                ], NccCardCtaPhotoComponent.prototype, "elbutton", void 0);
                NccCardCtaPhotoComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-cta-photo',
                        templateUrl: 'ncc-card-cta-photo.component.html',
                        styleUrls: ['ncc-card-cta-photo.component.css']
                    }),
                    __metadata("design:paramtypes", [typeof (_d = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _d || Object])
                ], NccCardCtaPhotoComponent);
                return NccCardCtaPhotoComponent;
                var _a, _b, _c, _d;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardCtaPhotoComponent", NccCardCtaPhotoComponent);
        }
    };
});
//# sourceMappingURL=ncc-card-cta-photo.component.js.map