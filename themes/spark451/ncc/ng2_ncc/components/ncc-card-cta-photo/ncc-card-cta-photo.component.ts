import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-cta-photo',
    templateUrl: 'ncc-card-cta-photo.component.html',
    styleUrls: ['ncc-card-cta-photo.component.css']
})

export class NccCardCtaPhotoComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() image: string;
    @Input() linkUrl: string;

    @ViewChild('elimage') elimage: ElementRef;
    @ViewChild('eltitle') eltitle: ElementRef;
    @ViewChild('elbutton') elbutton: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(100)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let buttonAnimation = this._ab.css();
        buttonAnimation
            .setDuration(400)
            .setFromStyles({
                'top': '50%',
                'right': '50%',
                'width': '55px',
                'height': '55px',
                'border': '3px solid rgba(255,192,59,0.7)',
                'transform': 'translate(50%, -50%)',
                'border-radius': '50%',
                'background-color': 'none',
                'pointer-events': 'none'
            })
            .setToStyles({
                'top': '0',
                'right': '0',
                'width': '50px',
                'height': '100%',
                'border': 'none',
                'transform': 'none',
                'border-radius': '0',
                'background-color': 'rgba(53,57,60,0.8)',
                'pointer-events': 'auto'
            });

        if (this.elimage && this.elbutton && this.eltitle) {
            imageAnimation.start(this.elimage.nativeElement)
            buttonAnimation.start(this.elbutton.nativeElement)
                .onComplete(() => {
                    headingAnimation.start(this.eltitle.nativeElement);
                })
        }*/

        if (this.elimage && this.elbutton && this.eltitle) {
            this.elimage.nativeElement.style.transform = 'scale(1)';

            this.elbutton.nativeElement.style.top = '0';
            this.elbutton.nativeElement.style.right = '0';
            this.elbutton.nativeElement.style.width = '50px';
            this.elbutton.nativeElement.style.height = '100%';
            this.elbutton.nativeElement.style.border = 'none';
            this.elbutton.nativeElement.style.transform = 'none';
            this.elbutton.nativeElement.style.borderRadius = '0';
            this.elbutton.nativeElement.style.backgroundColor = 'rgba(53,57,60,0.8)';
            this.elbutton.nativeElement.style.pointerEvents = 'auto';

            this.eltitle.nativeElement.style.opacity = '1';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.15)' })

        let headingAnimation = this._ab.css();
        headingAnimation
            .setDuration(100)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let buttonAnimation = this._ab.css();
        buttonAnimation
            .setDuration(200)
            .setFromStyles({
                'top': '0',
                'right': '0',
                'width': '50px',
                'height': '100%',
                'border': 'none',
                'transform': 'none',
                'border-radius': 'none',
                'background-color': 'rgba(53,57,60,0.8)',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'top': '50%',
                'right': '50%',
                'width': '55px',
                'height': '55px',
                'border': '3px solid rgba(255,192,59,0.7)',
                'transform': 'translate(50%, -50%)',
                'border-radius': '50%',
                'background-color': 'transparent',
                'pointer-events': 'none'
            });


        if (this.elimage && this.elbutton && this.eltitle) {
            imageAnimation.start(this.elimage.nativeElement)
            headingAnimation.start(this.eltitle.nativeElement)
                .onComplete(() => {
                    buttonAnimation.start(this.elbutton.nativeElement);
                })
        }*/

        if (this.elimage && this.elbutton && this.eltitle) {
            this.eltitle.nativeElement.style.opacity = '0';

            this.elimage.nativeElement.style.transform = 'scale(1.15)';

            this.elbutton.nativeElement.style.top = '50%';
            this.elbutton.nativeElement.style.right = '50%';
            this.elbutton.nativeElement.style.width = '55px';
            this.elbutton.nativeElement.style.height = '55px';
            this.elbutton.nativeElement.style.border = '3px solid rgba(255,192,59,0.7)';
            this.elbutton.nativeElement.style.transform = 'translate(50%, -50%)';
            this.elbutton.nativeElement.style.borderRadius = '50%';
            this.elbutton.nativeElement.style.backgroundColor = 'transparent';
            this.elbutton.nativeElement.style.pointerEvents = 'none';
        }
    }

}
