import {Directive, ElementRef, Input, Output, HostBinding, Renderer, EventEmitter, OnInit} from '@angular/core';

@Directive({
    selector: '[nccCollapse]'
})

export class NccCollapseDirective implements OnInit {
    @Output() public collapsed: EventEmitter<any> = new EventEmitter<any>(false);
    @Output() public expanded: EventEmitter<any> = new EventEmitter<any>(false);

    @HostBinding('style.display')
    private display: string;

    // shown
    @HostBinding('class.in')
    @HostBinding('attr.aria-expanded')
    private isExpanded: boolean = true;

    // hidden
    @HostBinding('attr.aria-hidden')
    private isCollapsed: boolean = false;

    // stale state
    @HostBinding('class.collapse')
    private isCollapse: boolean = true;

    // animation state
    @HostBinding('class.collapsing')
    private isCollapsing: boolean = false;

    @Input() private transitionDuration: number = 450; // Duration in ms

    @Input()
    private set nccCollapse(value: boolean) {
        this.isExpanded = value;
        this.toggle();
    }

    private get nccCollapse(): boolean {
        return this.isExpanded;
    }

    private _el: ElementRef;
    private _renderer: Renderer;

    public constructor(_el: ElementRef, _renderer: Renderer) {
        this._el = _el;
        this._renderer = _renderer;
    }

    public ngOnInit(): void {
        this._el.nativeElement.style.transition = 'height ' + this.transitionDuration + 'ms ease, visibility ' + this.transitionDuration + 'ms ease';
    }

    public toggle(): void {
        if (this.isExpanded) {
            this.hide();
        } else {
            this.show();
        }
    }

    public hide(): void {
        this.isCollapse = false;
        this.isCollapsing = true;

        this.isExpanded = false;
        this.isCollapsed = true;

        // // Old:
        // this.isCollapse = true;
        // this.isCollapsing = false;

        // this.display = 'none';
        // this.collapsed.emit(event);

        // Transition:
        this._el.nativeElement.style.height = '0px';
        this._el.nativeElement.style.overflow = 'hidden';

        setTimeout(() => {
            this.isCollapse = true;
            this.isCollapsing = false;

            this.collapsed.emit(event);
        }, this.transitionDuration);
    }

    public show(): void {
        this.isCollapse = false;
        this.isCollapsing = true;

        this.isExpanded = true;
        this.isCollapsed = false;

        // // Old:
        // this.display = 'block';
        // this.isCollapse = true;
        // this.isCollapsing = false;
        // this._renderer.setElementStyle(this._el.nativeElement, 'overflow', 'visible');
        // this._renderer.setElementStyle(this._el.nativeElement, 'height', 'auto');
        // this.expanded.emit(event);

        // Transition:
        this._el.nativeElement.style.height = this._el.nativeElement.scrollHeight + 'px';

        setTimeout(() => {
            this.isCollapse = true;
            this.isCollapsing = false;

            this._el.nativeElement.style.overflow = 'visible';

            this.expanded.emit(event);
        }, this.transitionDuration);
    }
}
