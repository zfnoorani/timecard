System.register(["@angular/core", "../ncc-card/ncc-card.base"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ncc_card_base_1, NccCardCtaComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ncc_card_base_1_1) {
                ncc_card_base_1 = ncc_card_base_1_1;
            }
        ],
        execute: function () {
            NccCardCtaComponent = (function (_super) {
                __extends(NccCardCtaComponent, _super);
                function NccCardCtaComponent(_el) {
                    var _this = _super.call(this) || this;
                    _this._el = _el;
                    return _this;
                }
                NccCardCtaComponent.prototype.activeStateContentAnimation = function () {
                    if (this.elcontent && this.elbutton) {
                        this.elcontent.nativeElement.style.opacity = '1';
                        this.elcontent.nativeElement.style.pointerEvents = 'auto';
                        this.elbutton.nativeElement.style.opacity = '1';
                        this.elbutton.nativeElement.style.pointerEvents = 'auto';
                    }
                    if (this.elicon) {
                        this.elicon.nativeElement.style.opacity = '0';
                    }
                };
                NccCardCtaComponent.prototype.inactiveStateContentAnimation = function () {
                    if (this.elcontent && this.elbutton) {
                        this.elcontent.nativeElement.style.opacity = '0';
                        this.elcontent.nativeElement.style.pointerEvents = 'none';
                        this.elbutton.nativeElement.style.opacity = '0';
                        this.elbutton.nativeElement.style.pointerEvents = 'none';
                    }
                    if (this.elicon) {
                        this.elicon.nativeElement.style.opacity = '0.7';
                    }
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "cardState", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "title", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "icon", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "linkLabel", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], NccCardCtaComponent.prototype, "linkUrl", void 0);
                __decorate([
                    core_1.ViewChild('elicon'),
                    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
                ], NccCardCtaComponent.prototype, "elicon", void 0);
                __decorate([
                    core_1.ViewChild('elcontent'),
                    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
                ], NccCardCtaComponent.prototype, "elcontent", void 0);
                __decorate([
                    core_1.ViewChild('elbutton'),
                    __metadata("design:type", typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object)
                ], NccCardCtaComponent.prototype, "elbutton", void 0);
                NccCardCtaComponent = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: 'ncc-card-cta',
                        templateUrl: 'ncc-card-cta.component.html',
                        styleUrls: ['ncc-card-cta.component.css']
                    }),
                    __metadata("design:paramtypes", [typeof (_d = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _d || Object])
                ], NccCardCtaComponent);
                return NccCardCtaComponent;
                var _a, _b, _c, _d;
            }(ncc_card_base_1.NccCardBase));
            exports_1("NccCardCtaComponent", NccCardCtaComponent);
        }
    };
});
//# sourceMappingURL=ncc-card-cta.component.js.map