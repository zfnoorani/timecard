import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-cta',
    templateUrl: 'ncc-card-cta.component.html',
    styleUrls: ['ncc-card-cta.component.css']
})

export class NccCardCtaComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() title: string;
    @Input() icon: string;
    @Input() linkLabel: string;
    @Input() linkUrl: string;

    @ViewChild('elicon') elicon: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;
    @ViewChild('elbutton') elbutton: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0.7' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elicon && this.elcontent && this.elbutton) {
            iconAnimation.start(this.elicon.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
            contentAnimation.start(this.elbutton.nativeElement);
        }*/

        if (this.elcontent && this.elbutton) {
            this.elcontent.nativeElement.style.opacity = '1';
            this.elcontent.nativeElement.style.pointerEvents = 'auto';

            this.elbutton.nativeElement.style.opacity = '1';
            this.elbutton.nativeElement.style.pointerEvents = 'auto';
        }

        if (this.elicon) {
            this.elicon.nativeElement.style.opacity = '0';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let iconAnimation = this._ab.css();
        iconAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '0.7' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(200)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elicon && this.elcontent && this.elbutton) {
            iconAnimation.start(this.elicon.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
            contentAnimation.start(this.elbutton.nativeElement);
        }*/

        if (this.elcontent && this.elbutton) {
            this.elcontent.nativeElement.style.opacity = '0';
            this.elcontent.nativeElement.style.pointerEvents = 'none';

            this.elbutton.nativeElement.style.opacity = '0';
            this.elbutton.nativeElement.style.pointerEvents = 'none';
        }

        if (this.elicon) {
            this.elicon.nativeElement.style.opacity = '0.7';
        }
    }

}
