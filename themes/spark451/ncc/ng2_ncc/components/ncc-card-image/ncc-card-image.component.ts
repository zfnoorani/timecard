import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-image',
    templateUrl: 'ncc-card-image.component.html',
    styleUrls: ['ncc-card-image.component.css']
})

export class NccCardImageComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() image: string;
    @Input() alt: string;

    @ViewChild('elimage') elimage: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1.25)' })
            .setToStyles({ 'transform': 'scale(1)' })

        if (this.elimage) {
            imageAnimation.start(this.elimage.nativeElement)
        }*/

        if (this.elimage) {
            this.elimage.nativeElement.style.transform = 'scale(1)';
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let imageAnimation = this._ab.css();
        imageAnimation
            .setDuration(500)
            .setFromStyles({ 'transform': 'scale(1)' })
            .setToStyles({ 'transform': 'scale(1.25)' })

        if (this.elimage) {
            imageAnimation.start(this.elimage.nativeElement)
        }*/

        if (this.elimage) {
            this.elimage.nativeElement.style.transform = 'scale(1.25)';
        }
    }

}
