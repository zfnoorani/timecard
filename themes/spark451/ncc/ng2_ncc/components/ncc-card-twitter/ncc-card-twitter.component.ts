import {Component, Input, ViewChild, ElementRef} from '@angular/core';
//import {AnimationBuilder} from '@angular/platform-browser/src/animate/animation_builder';

import {NccCardBase} from '../ncc-card/ncc-card.base';

@Component({
    moduleId: __moduleName,
    selector: 'ncc-card-twitter',
    templateUrl: 'ncc-card-twitter.component.html',
    styleUrls: ['ncc-card-twitter.component.css']
})

export class NccCardTwitterComponent extends NccCardBase {
    @Input() cardState: string;

    @Input() icon: string;
    @Input() name: string;
    @Input() account: string;
    @Input() date: string;
    @Input() content: string;
    @Input() profileUrl: string;

    @ViewChild('elwrapper') elwrapper: ElementRef;
    @ViewChild('elpreview') elpreview: ElementRef;
    @ViewChild('elcontent') elcontent: ElementRef;

    // public constructor(private _el: ElementRef, private _ab: AnimationBuilder) {
    public constructor(private _el: ElementRef) {
        super();
    }

    public activeStateContentAnimation(): void {
        /*let wrapperAnimation = this._ab.css();
        wrapperAnimation
            .setDuration(500)
            .setFromStyles({ 'background-color': '#00aced' })
            .setToStyles({ 'background-color': '#fff' })

        let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '1' })
            .setToStyles({ 'opacity': '0' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })
            .setToStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })

        if (this.elwrapper && this.elpreview && this.elcontent) {
            wrapperAnimation.start(this.elwrapper.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elwrapper && this.elpreview && this.elcontent) {
            this.elwrapper.nativeElement.style.backgroundColor = '#fff';

            this.elpreview.nativeElement.style.opacity = '0';

            this.elcontent.nativeElement.style.opacity = '1'
            this.elcontent.nativeElement.style.pointerEvents = 'auto'
        }
    }

    public inactiveStateContentAnimation(): void {
        /*let wrapperAnimation = this._ab.css();
        wrapperAnimation
            .setDuration(500)
            .setFromStyles({ 'background-color': '#fff' })
            .setToStyles({ 'background-color': '#00aced' })

        let previewAnimation = this._ab.css();
        previewAnimation
            .setDuration(500)
            .setFromStyles({ 'opacity': '0' })
            .setToStyles({ 'opacity': '1' })

        let contentAnimation = this._ab.css();
        contentAnimation
            .setDuration(500)
            .setFromStyles({
                'opacity': '1',
                'pointer-events': 'auto'
            })
            .setToStyles({
                'opacity': '0',
                'pointer-events': 'none'
            })

        if (this.elwrapper && this.elpreview && this.elcontent) {
            wrapperAnimation.start(this.elwrapper.nativeElement);
            previewAnimation.start(this.elpreview.nativeElement);
            contentAnimation.start(this.elcontent.nativeElement);
        }*/

        if (this.elwrapper && this.elpreview && this.elcontent) {
            this.elwrapper.nativeElement.style.backgroundColor = '#00aced';

            this.elpreview.nativeElement.style.opacity = '1';

            this.elcontent.nativeElement.style.opacity = '0'
            this.elcontent.nativeElement.style.pointerEvents = 'none'
        }
    }

}
