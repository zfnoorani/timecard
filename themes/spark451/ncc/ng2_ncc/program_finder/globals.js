System.register(["helpers/globals"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var globals_1, Ng2NccProgramFinderGlobals;
    return {
        setters: [
            function (globals_1_1) {
                globals_1 = globals_1_1;
            }
        ],
        execute: function () {
            exports_1("Ng2NccProgramFinderGlobals", Ng2NccProgramFinderGlobals = globals_1.mergeGlobals({}, []));
        }
    };
});
//# sourceMappingURL=globals.js.map