System.register(["@angular/core", "@angular/http", "@angular/common", "helpers/lazy-load-component", "./ng2-ncc-program_finder.component", "../components/ncc-program-finder/ncc-program-finder.component", "../components/ncc-program-finder/ncc-program-finder.service", "../components/ncc-collapse/ncc-collapse.directive", "../components/ncc-accordion/ncc-accordion.component", "../components/ncc-accordion/ncc-accordion-group.component", "../components/ncc-program-card/ncc-program-card.component", "../components/ncc-program-tray/ncc-program-tray.component", "../components/ncc-program-finder/ncc-program-finder-collapse.directive", "../ncc-dynamic-html.module", "./globals"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, common_1, lazy_load_component_1, ng2_ncc_program_finder_component_1, ncc_program_finder_component_1, ncc_program_finder_service_1, ncc_collapse_directive_1, ncc_accordion_component_1, ncc_accordion_group_component_1, ncc_program_card_component_1, ncc_program_tray_component_1, ncc_program_finder_collapse_directive_1, ncc_dynamic_html_module_1, Ng2NccProgramFinderModule;
    var exportedNames_1 = {
        "Ng2NccProgramFinderModule": true
    };
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default" && !exportedNames_1.hasOwnProperty(n)) exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (lazy_load_component_1_1) {
                lazy_load_component_1 = lazy_load_component_1_1;
            },
            function (ng2_ncc_program_finder_component_1_1) {
                ng2_ncc_program_finder_component_1 = ng2_ncc_program_finder_component_1_1;
            },
            function (ncc_program_finder_component_1_1) {
                ncc_program_finder_component_1 = ncc_program_finder_component_1_1;
            },
            function (ncc_program_finder_service_1_1) {
                ncc_program_finder_service_1 = ncc_program_finder_service_1_1;
            },
            function (ncc_collapse_directive_1_1) {
                ncc_collapse_directive_1 = ncc_collapse_directive_1_1;
            },
            function (ncc_accordion_component_1_1) {
                ncc_accordion_component_1 = ncc_accordion_component_1_1;
            },
            function (ncc_accordion_group_component_1_1) {
                ncc_accordion_group_component_1 = ncc_accordion_group_component_1_1;
            },
            function (ncc_program_card_component_1_1) {
                ncc_program_card_component_1 = ncc_program_card_component_1_1;
            },
            function (ncc_program_tray_component_1_1) {
                ncc_program_tray_component_1 = ncc_program_tray_component_1_1;
            },
            function (ncc_program_finder_collapse_directive_1_1) {
                ncc_program_finder_collapse_directive_1 = ncc_program_finder_collapse_directive_1_1;
            },
            function (ncc_dynamic_html_module_1_1) {
                ncc_dynamic_html_module_1 = ncc_dynamic_html_module_1_1;
            },
            function (globals_1_1) {
                exportStar_1(globals_1_1);
            }
        ],
        execute: function () {
            Ng2NccProgramFinderModule = (function () {
                function Ng2NccProgramFinderModule() {
                }
                Ng2NccProgramFinderModule = __decorate([
                    core_1.NgModule({
                        imports: [
                            http_1.HttpModule,
                            common_1.CommonModule,
                            ncc_dynamic_html_module_1.NccDynamicHtmlModule
                        ],
                        declarations: [
                            ng2_ncc_program_finder_component_1.NccProgramFinderApp,
                            ncc_program_card_component_1.NccProgramCardComponent,
                            ncc_program_finder_collapse_directive_1.NccProgramFinderCollapseDirective,
                            ncc_collapse_directive_1.NccCollapseDirective,
                            ncc_program_tray_component_1.NccProgramTrayComponent,
                            ncc_accordion_group_component_1.NccAccordionGroupComponent,
                            ncc_accordion_component_1.NccAccordionComponent,
                            ncc_program_finder_component_1.NccProgramFinderComponent
                        ],
                        entryComponents: [
                            ng2_ncc_program_finder_component_1.NccProgramFinderApp
                        ],
                        providers: [
                            ncc_program_finder_service_1.NccProgramFinderService,
                            { provide: ncc_program_finder_service_1.PROGRAM_FINDER_API_URL, useValue: '/sites/default/files/programs/programs.json' },
                            { provide: lazy_load_component_1.LazyLoadComponent, useValue: ng2_ncc_program_finder_component_1.NccProgramFinderApp }
                        ]
                    })
                ], Ng2NccProgramFinderModule);
                return Ng2NccProgramFinderModule;
            }());
            exports_1("Ng2NccProgramFinderModule", Ng2NccProgramFinderModule);
        }
    };
});
//# sourceMappingURL=index.js.map