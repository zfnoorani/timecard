/**
 * @module Ng2NccProgramFinder
 */ /** */

// external imports
import {mergeGlobals} from 'helpers/globals';

export const Ng2NccProgramFinderGlobals = mergeGlobals({}, []);