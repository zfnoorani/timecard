/// <reference path="../../typings/index.d.ts" />
/// <reference path="../../app/core/typings.d.ts" />

// Modules
import { NgModule }      from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';

// external imports
import {LazyLoadComponent} from 'helpers/lazy-load-component';

// Components
import { NccProgramFinderApp }  from './ng2-ncc-program_finder.component';
import { NccProgramFinderComponent } from '../components/ncc-program-finder/ncc-program-finder.component';
import { PROGRAM_FINDER_API_URL, NccProgramFinderService } from '../components/ncc-program-finder/ncc-program-finder.service';

import { NccCollapseDirective } from '../components/ncc-collapse/ncc-collapse.directive';
import { NccAccordionComponent } from '../components/ncc-accordion/ncc-accordion.component';
import { NccAccordionGroupComponent } from '../components/ncc-accordion/ncc-accordion-group.component';
import { NccProgramCardComponent } from '../components/ncc-program-card/ncc-program-card.component';
import { NccProgramTrayComponent } from '../components/ncc-program-tray/ncc-program-tray.component';
import { NccProgramFinderCollapseDirective } from '../components/ncc-program-finder/ncc-program-finder-collapse.directive'; // TODO: temp solution for transition
import { NccDynamicHtmlModule } from '../ncc-dynamic-html.module';

// exports
export * from './globals';

@NgModule({
    imports: [                        // module dependencies
        HttpModule,
        CommonModule,
        NccDynamicHtmlModule
    ],
    declarations: [                      // components, pipes and directives
        NccProgramFinderApp,
        NccProgramCardComponent,
        NccProgramFinderCollapseDirective,
        NccCollapseDirective,
        NccProgramTrayComponent,
        NccAccordionGroupComponent,
        NccAccordionComponent,
        NccProgramFinderComponent
    ],                
    entryComponents: [                     // root component
        NccProgramFinderApp 
    ],
    providers: [                      // providers/services
        NccProgramFinderService,
        { provide: PROGRAM_FINDER_API_URL, useValue: '/api/programs?_format=json' }, // Local JSON Data (dev data)
        { provide: LazyLoadComponent, useValue: NccProgramFinderApp }
    ] 
})

export class Ng2NccProgramFinderModule { }


