import {Component} from '@angular/core';

@Component({
    selector: 'ncc-app-3',
    template: `
        <ncc-program-finder></ncc-program-finder>
    `,
    styles: [`:host { display: block; }`]
})

export class NccProgramFinderApp {
    constructor() { }
}
