<?php

/**
 * Class redirectToExternalUrls
 */
class redirectToExternalUrls {

  /**
   * Domain of live site
   */
  const LIVE_HOST = 'northcentralcollege.edu';

  /**
   * @var
   */
  private static $instance;

  /**
   * @var String
   */
  private $origin;

  /**
   * @var Object
   */
  private $parseUrl;

  /**
   * @var array
   */
  private $messages = [];

  const IMAGE_STAFF_REGEX = 'Images\/staff\/';

  public static function getInstance() {
    if (null === static::$instance) {
      static::$instance = new static();
    }

    return static::$instance;
  }

  /**
   * Private constructor to prevent creating a new instance via the `new` operator from outside of this class
   *
   * redirectToExternalUrls constructor.
   * @param null $url
   */
  private function __construct($url = null) {

    if(is_null($url)) {
      $url = $this->setOriginFromCurrentUrl();
    }

    $this->setOrigin($url);
  }

  /**
   * @return String
   */
  public function getOrigin()
  {
    return $this->origin;
  }

  /**
   * @param String $origin
   */
  public function setOrigin($origin)
  {
    $this->origin = $origin;
  }

  /**
   * Private clone method to prevent cloning of the instance
   */
  private function __clone() {}

  /**
   * Private unserialize method to prevent unserializing
   */
  private function __wakeup() {}

  /**
   * @return string
   */
  public function __toString() {
    return 'The origin is ' . $this->getOrigin() . ', and destination is ' . $this->getDestination();
  }

  /**
   * @return string
   */
  private function setOriginFromCurrentUrl() {
    return self::getBaseHost() . $_SERVER['REQUEST_URI'];
  }

  private static function getBaseHost() {
    if (!isset($_SERVER['REQUEST_SCHEME']) && isset($_SERVER['HTTP_X_PROTO'])) {
      $scheme = $_SERVER['HTTP_X_PROTO'];
    } else {
      $scheme = $_SERVER['REQUEST_SCHEME'] . '://';
    }
    return $scheme . $_SERVER['HTTP_HOST'];
  }

  /**
   * @param bool $toString
   * @return array|string
   */
  public function getMessages($toString = false) {

    if ($toString) {
      $this->messages = implode("\n", $this->messages);
    }

    return $this->messages;
  }

  /**
   * Check current URI
   *
   * @return bool
   */
  public static function isValidOrigin() {
    $validSubDomainsPattern = '/^(\/)(magazine|brilliantfuture|orientation|webcam|'. self::IMAGE_STAFF_REGEX .')/';
    return preg_match($validSubDomainsPattern, $_SERVER['REQUEST_URI']);
  }

  /**
   * @return $this
   */
  public function forceLiveHost() {
    $this->parseUrl->host = self::LIVE_HOST;

    return $this;
  }

  /**
   * @return $this
   */
  public function forceHttpsScheme() {
    $this->parseUrl->scheme = 'https';

    return $this;
  }

  /**
   * @param bool $removeParams
   * @return $this
   */
  public function setParseUrl($removeParams = FALSE) {
    $url = (object) parse_url($this->getOrigin());

    $url->params = explode('/', trim($url->path, '/'));

    $url->subDomain = reset($url->params);
    if (!empty($url->subDomain)) {
      array_shift($url->params);
    }

    if ($removeParams) {
      $url->params = [];
    }

    $this->parseUrl = $url;

    return $this;
  }

  /**
   * @return string
   */
  private function getDestination() {
    $destination = sprintf('%s://%s.%s/%s',
      $this->parseUrl->scheme,
      $this->parseUrl->subDomain,
      $this->parseUrl->host,
      implode('/', $this->parseUrl->params)
    );

    if (isset($this->parseUrl->query)) {
      $destination .= '?' . $this->parseUrl->query;
    }

    return $destination;
  }

  /**
   * Validate URL
   *
   * @return bool
   */
  public function validateDestination() {
    $pattern = '/^https?(\:\/\/)([a-z]+)?\.?([\w+]+)\.([\w+]{3})((\/[\w\W+]+)+)?\/?$/';

    if (preg_match($pattern, $this->getDestination())) {
      return true;
    }

    $this->messages[] = 'Destination '. $this->getDestination() .' isn\'t validate';
    return false;
  }

  /**
   * @return bool
   */
  public function checkDestination() {
    $url = $this->getDestination();
    $headers = @get_headers($url);

    if($headers) {
      $codes = [];
      preg_match('/\d{3}/', reset($headers), $codes);
      $code = reset($codes);

      switch ($code) {
        case 200:
          return true;
          break;
        default:
          $this->messages[] = 'Header isn\'t valid, it returns the code: ' . $code;
          return false;
      }
    }

    $this->messages[] = 'Impossible get headers in ' . __METHOD__;
    return false;
  }

  /**
   * @param null $http_response_code
   */
  public function redirectToDestination($http_response_code = null) {
    $url = sprintf('Location: %s', $this->getDestination());
    header($url, true, $http_response_code);
    exit;
  }

  public function redirectToStaffResource() {
    //
    array_shift($this->parseUrl->params);
    $destination = 'https://directory.northcentralcollege.edu/employeephotos/%s';
    $location = sprintf($destination, implode('/', $this->parseUrl->params));
    if (isset($this->parseUrl->query)) {
      $location .= '?' . $this->parseUrl->query;
    }
    $url = sprintf('Location: %s', $location);
    header($url, TRUE, 301);
    exit;
  }

  public function isStaffResource() {
    return preg_match('/' . self::IMAGE_STAFF_REGEX . '/', $_SERVER['REQUEST_URI']);
  }
}
