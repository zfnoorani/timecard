<?php
/**
 * Redirect some paths to external domains
 */
if (isset($_SERVER['PANTHEON_ENVIRONMENT']) && (php_sapi_name() != "cli")) {
  require_once 'private/ncc/redirectToExternalUrls.php';
  if (redirectToExternalUrls::isValidOrigin()) {
    /** @var redirectToExternalUrls $redirect */
    $redirect = redirectToExternalUrls::getInstance();
    $redirect->setParseUrl()->forceLiveHost()->forceHttpsScheme();
    // Check staff resource.
    if ($redirect->isStaffResource()) {
      $redirect->redirectToStaffResource();
    }
    // Check dynamic destination.
    elseif ($redirect->validateDestination()) {
      $redirect->redirectToDestination(301);
    }
    // Shows error messages.
    else {
      $erros = $redirect->getMessages(true);
      error_log($erros);
    }
  }
}

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all envrionments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to insure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}
$settings['install_profile'] = 'standard';

// Grab pressflow settings.
$pressflow_settings = (!empty($_SERVER['PRESSFLOW_SETTINGS'])) ?
  $_SERVER['PRESSFLOW_SETTINGS'] : '';
# Decode Pantheon Settings
$ps = json_decode($pressflow_settings, TRUE);
$simplesamlphp_auth_installdir = (!empty($ps['conf']['pantheon_binding'])) ?
  '/srv/bindings/' . $ps['conf']['pantheon_binding'] . '/code' : $_SERVER['DOCUMENT_ROOT'];
# Provide universal absolute path to the installation.
$conf['simplesamlphp_auth_installdir'] = $simplesamlphp_auth_installdir . '/private/simplesamlphp-1.14.7';
// Define "simplesamlphp_dir" absolute path to the installation.
$settings['simplesamlphp_dir'] = $simplesamlphp_auth_installdir . '/private/simplesamlphp-1.14.7';

// Check Pantheon constant.
if (defined('PANTHEON_ENVIRONMENT')) {
  // Add Pantheon hosts.
  $settings['trusted_host_patterns'][] = "{$_ENV['PANTHEON_ENVIRONMENT']}-{$_ENV['PANTHEON_SITE_NAME']}.getpantheon.io";
  $settings['trusted_host_patterns'][] = "{$_ENV['PANTHEON_ENVIRONMENT']}-{$_ENV['PANTHEON_SITE_NAME']}.pantheon.io";
  $settings['trusted_host_patterns'][] = "{$_ENV['PANTHEON_ENVIRONMENT']}-{$_ENV['PANTHEON_SITE_NAME']}.pantheonsite.io";
  $settings['trusted_host_patterns'][] = "{$_ENV['PANTHEON_ENVIRONMENT']}-{$_ENV['PANTHEON_SITE_NAME']}.panth.io";
  // Add beta host URL as trusted host.
  $settings['trusted_host_patterns'][] = '^beta\.northcentralcollege\.edu$';
  // include $migrate_settings;
  $databases['migrate']['default'] = array(
    'database' => 'ncc_web',
    'username' => 'ncc_web',
    'password' => 'lJHYc3fd4MBrutiX',
    'prefix' => '',
    'host' => 'appserve-a.spark451.com',
    'port' => '10000',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
  );
}
// Overwrite system.file "allow_insecure_uploads" settings.
$config['system.file']['allow_insecure_uploads'] = TRUE;

// Require www.
if (isset($_SERVER['PANTHEON_ENVIRONMENT']) &&
  ($_SERVER['PANTHEON_ENVIRONMENT'] === 'live') &&
  (php_sapi_name() != "cli")) {
  if ($_SERVER['HTTP_HOST'] == 'northcentralcollege.edu'
    // || $_SERVER['HTTP_HOST'] == 'thatothersiteyouhad.com'
  )
  {
    header('HTTP/1.0 301 Moved Permanently');
    header('Location: http://www.northcentralcollege.edu'. $_SERVER['REQUEST_URI']);
    exit();
  }
}
// Require HTTPS.
// Check if Drupal is running via command line
if (isset($_SERVER['PANTHEON_ENVIRONMENT']) &&
  ($_SERVER['HTTPS'] === 'OFF') &&
  (php_sapi_name() != "cli")) {
  if (!isset($_SERVER['HTTP_X_SSL']) ||
    (isset($_SERVER['HTTP_X_SSL']) && $_SERVER['HTTP_X_SSL'] != 'ON')) {
    header('HTTP/1.0 301 Moved Permanently');
    header('Location: https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    exit();
  }
}

if (isset($_ENV['PANTHEON_ROLLING_TMP']) && isset($_ENV['PANTHEON_DEPLOYMENT_IDENTIFIER'])) {
  // Relocate the compiled twig files to <binding-dir>/tmp/ROLLING/twig.
  // The location of ROLLING will change with every deploy.
  $settings['php_storage']['twig']['directory'] = $_ENV['PANTHEON_ROLLING_TMP'];
  // Ensure that the compiled twig templates will be rebuilt whenever the
  // deployment identifier changes.  Note that a cache rebuild is also necessary.
  $settings['deployment_identifier'] = $_ENV['PANTHEON_DEPLOYMENT_IDENTIFIER'];
  $settings['php_storage']['twig']['secret'] = $_ENV['DRUPAL_HASH_SALT'] . $settings['deployment_identifier'];
}
/**
 * Install the Pantheon Service Provider to hook Pantheon services into
 * Drupal 8. This service provider handles operations such as clearing the
 * Pantheon edge cache whenever the Drupal cache is rebuilt.
 */
if (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
  $GLOBALS['conf']['container_service_providers']['PantheonServiceProvider'] = '\Pantheon\Internal\PantheonServiceProvider';
}