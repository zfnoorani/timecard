<?php
namespace Drupal\static_programs_cron\Api;
 //6/10/2019 coded by Evan Awesome
class staticprogramsapi
{
    public function setup()
    {
        
        if (!is_dir(getcwd() . '/sites/default/files/programs/')) {
            mkdir(getcwd() . '/sites/default/files/programs/');    
        }
        
        $json = file_get_contents("https://www.northcentralcollege.edu/api/programs"); //Gets data from the programs feed
        if (!$json) //if it was empty, log the error and return so that the json file is not written with null data
            {
            \Drupal::logger('Static_Programs_Cron')->error("https://www.northcentralcollege.edu/api/programs could not be reached/found");
            return;
        }
        $file = fopen(getcwd() . '/sites/default/files/programs/programs.json', "w"); //open our local file and do not keep old data
        
        if ($file) { // if we are allowed to create a file write the json feed and log. If file permission doesn't allow a file to be created log the error. 
            fwrite($file, $json);
            \Drupal::logger('Static_Programs_Cron')->notice("Finished writing to programs.json");
            fclose($file);
        } 
        else {
            \Drupal::logger('Static_Programs_Cron')->notice("The file didn't write");
            fclose($file);
        }
    }
}
      