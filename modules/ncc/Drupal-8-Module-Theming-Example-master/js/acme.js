(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the JS test behavior of first example.
   */
  Drupal.behaviors.exampleOne = {
    attach: function (context, settings) {
      console.log('hola 1');
      $('.do-event', context).click(function () {
        $('.container .container-wrapper > h2').toggle('slow');
      });
    }
  };

  })(jQuery, Drupal, drupalSettings);