(function($){
  Drupal.behaviors.exportSingleConfig = {
    attach: function(context) {
      $('body', context).once('exportSingleConfig').each(function() {
        // Document ready
        $(function() {
          $(document).ajaxSuccess(function() {
            if ($('.description').length) {
              $('#downloadWrapper').show();
            }
          });
          // Create wrapper and button
          var btnWrapper = $('<div>', {class: 'form-actions form-wrapper', id: 'downloadWrapper'}),
            btnDownload = $('<a>', {id: 'download-config', class: 'button', text: 'Download configuration'});
          // Append button and wrapper
          btnWrapper.append(btnDownload);
          btnWrapper.appendTo('.page-content').hide();
          // On click
          $('#download-config').on('click', function() {
            var config = $('textarea[name="export"]').val(),
              filename = $('.description').find('em.placeholder').text(),
              content = 'data:text/plain;charset=utf-8,' + encodeURIComponent(config);
            // Check if content or filename is empty
            if (filename !== '' && config !== '') {
              // Create file and force download
              var element = document.createElement('a');
              element.setAttribute('href', content);
              element.setAttribute('download', filename);
              element.style.display = 'none';
              document.body.appendChild(element);
              element.click();
              document.body.removeChild(element);
            }
          });
        });
      });
    }
  }
})(jQuery);
