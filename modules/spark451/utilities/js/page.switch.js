(function($) {
  Drupal.behaviors.switchTypePage = {
    attach: function(context) {
      var self = this;
      $('#edit-field-page-type', context).once('switchTypePage').each(function() {
        // Get form the radio selected by default
        var checked = $(this).find('input[type="radio"]:checked').val();

        if(typeof checked !== 'undefined') {
          self.togglePageType(checked);
        }

        $(this).find('input[type="radio"]').change(function() {
          var checked = $(this).val();
          self.togglePageType(checked);
        });
       });
    },

    //Which page type was selected, hide the correct tabs
    togglePageType: function(checked) {

      //Use for hidden functions
      var normal_hidden_array = ["edit-group-home-page"];
      var home_hidden_array = ["edit-group-header"];

      //Use for showing functions
      var normal_show_array = ["edit-group-header"];
      var home_show_array = ["edit-group-home-page"];

        switch (checked){
          //Normal Page
          case "normal_page" :
            normal_hidden_array.forEach(toggleHidden);
            //------
            normal_show_array.forEach(toggleShow);
            break;

          //Home Page
          case "home_page":
            home_hidden_array.forEach(toggleHidden);
            //----
            home_show_array.forEach(toggleShow);
            break;

          default:
            //Should not be used
            break;
        }
  
        //Passed an array, turn on and off the hidden for tabs
        function toggleHidden(hiddenElement) {
            var link = 'a[href="#%"]'.replace('%', hiddenElement), parent = $('.horizontal-tabs-list').find(link).parent();
            parent.addClass('hidden');
            //Set as selected 
            if (parent.hasClass('selected')) {
              parent.removeClass('selected');
            }
        };
        
        //Passed an arary, turn off the hidden for tabs
        function toggleShow(shownElement) {
            var link = 'a[href="#%"]'.replace('%', shownElement), parent = $('.horizontal-tabs-list').find(link).parent();
            //Unhide 
            if (parent.hasClass('hidden')) {
              parent.removeClass('hidden');
            }
        };
    }
  }
})(jQuery);
