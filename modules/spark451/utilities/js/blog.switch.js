(function($) {
  Drupal.behaviors.switchTypeBlog = {
    attach: function(context) {
      var self = this;

      $('#edit-field-blog-type', context).once('switchTypeBlog').each(function() {
        // Get form the selected dropdown by default
        var checked = $(this).find('option:selected').val();

        if(typeof checked !== 'undefined') {
          self.toggleBlogType(checked);
        }

        //If the user changed the drop down update the displayed catagories
        $("#edit-field-blog-type").change(function() {
          var checked = $(this).val();
          self.toggleBlogType(checked);
        });
       });
    },

    //Which blog type was selected, hide the other categories
    toggleBlogType: function(checked) {

      //Use for hidden functions
      var uniquelyNC_hidden_array = ["edit-group-shimer-categories", "edit-group-engineering-categories"];
      var shimer_hidden_array = ["edit-group-uniquelync-categories", "edit-group-engineering-categories"];
      var engineering_hidden_array = ["edit-group-uniquelync-categories", "edit-group-shimer-categories"];

      //Use for showing functions
      var uniquelyNC_show_array = ["edit-group-uniquelync-categories"];
      var shimer_show_array = ["edit-group-shimer-categories"];
      var engineering_show_array = ["edit-group-engineering-categories"];

        switch (checked){
          //UniquelyNC
          case "blog" :
            uniquelyNC_hidden_array.forEach(toggleHidden);
            //------
            uniquelyNC_show_array.forEach(toggleShow);
            break;

          //Shimer Blog
          case "shimer_blog":
            shimer_hidden_array.forEach(toggleHidden);
            //----
            shimer_show_array.forEach(toggleShow);
            break;

          //Engineering  Blog
          case "engineering_blog":
            engineering_hidden_array.forEach(toggleHidden);
            //----
            engineering_show_array.forEach(toggleShow);
            break;

          default:
            //Should not be used
            break;
        }
  
        //Passed an array, turn on and off the hidden for tabs
        function toggleHidden(hiddenElement) {
            var link = 'a[href="#%"]'.replace('%', hiddenElement), parent = $('.horizontal-tabs-list').find(link).parent();
            parent.addClass('hidden');
            //Set as selected 
            if (parent.hasClass('selected')) {
              parent.removeClass('selected');
            } 
        };
        
        //Passed an arary, turn off the hidden for tabs
        function toggleShow(shownElement) {
            var link = 'a[href="#%"]'.replace('%', shownElement), parent = $('.horizontal-tabs-list').find(link).parent();
            //Unhide 
            if (parent.hasClass('hidden')) {
              parent.removeClass('hidden');
            }
            parent.find('a').click();
        };
    }
  }
})(jQuery);
