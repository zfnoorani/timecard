(function($) {
  Drupal.behaviors.iconDropDown = {
    attach: function(context) {
      var self = this;
      $('select[name*="[field_ncc_icon]"]', context).once('iconDropDown').each(function() {
        var select = this;
        $(function() {
          var name = $(select).attr('name');
          self.createDropDown(name);
          self.eventOnClick(select);
          $(select).hide();
        });
      });
    },

    createDropDown: function(name) {
      var self = this,
        // Field element
        select = 'select[name="%"]'.replace('%', name),
        // List element to append items
        ul = $('<ul>', {
          class: 'ncc-icon-drop-down',
          'data-select-parent': name
        }),
        // Preview element for show selected item
        selectedWrapper = $('<div>', {
          class: 'selected-wrapper',
          id: 'selected_for_' + name
        });

      // Get selected item for current select field
      var selected = $(select + ' option:selected'),
        // Create icon container to append
        element = self.appendItemToList(selected.val(), selected.text(), 'div');
      // Append icon container into preview element
      selectedWrapper.html(element);

      // Append preview element to DOM
      $(select).parent().append(selectedWrapper);
      // Append list of items to DOM
      $(select).parent().append(ul);

      // Get each option into select
      $(select + ' option').each(function() {
        // Create item container
        var item = self.appendItemToList(this.value, this.text);
        // Append it to list of items
        item.appendTo(ul);
      });

      var markSelected = 'li[data-icon-value="%"]'.replace('%', selected.val());
      ul.find(markSelected).addClass('selected');
    },

    appendItemToList: function(value, text, element) {
      // Use LI by default
      var item = $('<li>', {class: 'ncc-icon-drop-down-li', 'data-icon-value': value});

      // But if element isn't undefined use the element passed like parameter
      if ('undefined' !== typeof element) {
        var elem = '<%>'.replace('%', element);
        item = $(elem, {class: 'ncc-icon-drop-down-' + element, 'data-icon-value': value})
      }

      // Create li, wrapper, icon text and href
      var wrapper = $('<span>',  {class: 'icon-wrapper'}),
        icon = $('<span>', {text: text}),
        href = '/themes/spark451/ncc/icons/core-icons.svg#' + value;

      // Do not create SVG if current is _none
      if ('_none' !== value) {
        // Create svg and use elements, it doesn't works with jquery, only with JS pure
        var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg'),
          use = document.createElementNS('http://www.w3.org/2000/svg', 'use');
        // Set href of SVG
        use.setAttributeNS('http://www.w3.org/1999/xlink', 'href', href);
        // Append items to wrapper
        svg.appendChild(use);
        wrapper.append(svg);
      }

      // Append icon to wrapper and wrapper to li
      icon.appendTo(wrapper);
      wrapper.appendTo(item);

      return item;
    },

    eventOnClick: function(field) {
      var self = this;

      $(field).parent().find('.selected-wrapper').on('click', function() {
        // var iconValue = $(this).find('.ncc-icon-drop-down-div').data('icon-value');

        $(this).parent().find('.ncc-icon-drop-down').slideToggle('fast', function() {
          if ($(this).hasClass('expanded')) {
            $(this).removeClass('expanded')
          } else {
            $(this).addClass('expanded')
          }

          // var element = 'li[data-icon-value="%"]'.replace('%', iconValue);
          // $(this).scrollTop($(element).position().top);
        });
      });

      $(field).parent().find('.ncc-icon-drop-down').find('.ncc-icon-drop-down-li').on('click', function() {
        var selected = $(this).data('icon-value'),
          text = $(this).text();
        $(field).val(selected);

        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');

        $(this).parent().slideUp('fast');

        var element = self.appendItemToList(selected, text, 'div');
        $(this).parent().parent().find('.selected-wrapper').html(element);
      });
    }
  }
})(jQuery);
