<?php

namespace Drupal\utilities\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\user\UserInterface;
use Drupal\utilities\Entity\UserProfile;

/**
 * Class UserProfileController
 * @package Drupal\utilities\Controller
 */
class UserProfileController extends ControllerBase {

  /**
   * Redirect to external URL where user can reset its password.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   Trusted redirect response object.
   */
  public function userRedirectToResetPassword() {
    // Redirect to SSO reset password page.
    $url = \Drupal::config('simplesamlphp_auth.settings')
      ->get('reset_password_url');
    return new TrustedRedirectResponse($url);
  }

  /**
   * Redirects users to their profile page.
   *
   * This controller assumes that it is only invoked for authenticated users.
   * This is enforced for the 'user.page' route with the '_user_is_logged_in'
   * requirement.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a redirect to the profile of the currently logged in user.
   */
  public function userProfilePage() {
    return $this->redirect('entity.user.canonical', array(
      'user' => $this->currentUser()
        ->id()
    ));
  }

  /**
   * Redirect profile page to custom user page.
   *
   * @param \Drupal\Core\Entity\EntityInterface $_entity
   *   The Entity to be rendered. Note this variable is named $_entity rather
   *   than $entity to prevent collisions with other named placeholders in the
   *   route.
   * @param string $view_mode
   *   (optional) The view mode that should be used to display the entity.
   *   Defaults to 'full'.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function profileTypeRedirect(EntityInterface $profile, $view_mode = 'full', $langcode = NULL) {
    // Check full view mode.
    if ('full' == $view_mode) {
      return $this->redirect('entity.user.canonical', ['user' => $profile->getOwnerId()]);
    }
  }
}
