<?php

/**
 * Created by PhpStorm.
 * User: gustavorf
 * Date: 1/3/17
 */

namespace Drupal\utilities\Controller;

use Drupal\Core\Queue\SuspendQueueException;

/**
 * Class for import all users from csv file.
 * Class Spark451ProfileImportBatch
 * @package Drupal\utilities\Controller
 */
class Spark451ProfileImportBatch {

  /**
   * Operations function.
   * @return array
   */
  public static function operations() {
    $operations = array();
    $queue = \Drupal::queue('cron_profile_import');
    while ($queue->numberOfItems() > 0) {
      try {
        $item = $queue->claimItem();
        if ($item) {
          $operations[] = array(
            '\Drupal\utilities\Controller\Spark451ProfileImportBatch::execute',
            array($item->data),
          );
          $queue->deleteItem($item);
        }
        else {
          continue;
        }
      }
      catch (SuspendQueueException $exception) {
        $queue->releaseItem($item);
        break;
      }
      catch (\Exception $exception) {
        watchdog_exception(__METHOD__, $exception);
      }
    }

    return $operations;
  }

  /**
   * Execute function of batch.
   * @param $event
   * @param $context
   */
  public static function execute($profile, &$context) {

    $queueWorker = \Drupal::service('plugin.manager.queue_worker')
      ->createInstance('cron_profile_import');

    $context['message'] = 'Processing profile...';
    $queueWorker->processItem($profile);
    $context['results'][] = $profile['main']['username'];
    $context['message'] = sprintf('Importing Profiles', $profile['main']['username']);
  }

  /**
   * Finish callback functionn for batch execution.
   * @param $success
   * @param $results
   * @param $operations
   */
  public static function finishedCallBack($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results), 'One Profile imported', '@count profiles imported');
    }
    else {
      $message = t('Finished with an error');
    }
    drupal_set_message($message);
  }
}