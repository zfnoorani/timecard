<?php

namespace Drupal\utilities\Controller;


use Drupal\Core\Queue\SuspendQueueException;

class Spark451EventsCancelledBatch {

  public static function operations() {
    $operations = array();
    $queue = \Drupal::queue('cron_cancelled_event_feeds');

    while($queue->numberOfItems() > 0) {
      try {
        $item = $queue->claimItem();
        if ($item) {
          $operations[] = array(
            '\Drupal\utilities\Controller\Spark451EventsCancelledBatch::execute',
            array($item->data)
          );
          $queue->deleteItem($item);
        } else {
          continue;
        }
      } catch (SuspendQueueException $exception) {
        $queue->releaseItem($item);
        break;
      } catch (\Exception $exception) {
        watchdog_exception(__METHOD__, $exception);
      }
    }

    return $operations;
  }

  public static function execute($event, &$context) {

    $queueWorker = \Drupal::service('plugin.manager.queue_worker')
      ->createInstance('cron_cancelled_event_feeds');

    $context['message'] = 'Processing cancelled event...';
    $queueWorker->processItem($event);
    $context['results'][] = $event['UID'];
    $context['message'] = sprintf('The event "%s" was deported successfully.', $event['Title']);
  }

  public static function finishedCallBack($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results), 'One event processed', '@count events processed');
    } else {
      $message = t('Finished with an error');
    }
    drupal_set_message($message);
  }
}
