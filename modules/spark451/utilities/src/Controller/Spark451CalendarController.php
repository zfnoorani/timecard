<?php

namespace Drupal\utilities\Controller;

use Drupal\Component\Uuid\Php;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Spark451Ng2Calendar.
 *
 * @package Drupal\utilities
 */
class Spark451CalendarController extends ControllerBase {

  /**
   * @var \Drupal\Component\Uuid\Php
   */
  protected $uuid;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Spark451Ng2Stack constructor.
   * @param \Drupal\Component\Uuid\Php $uuid
   */
  public function __construct(Php $uuid) {
    $this->uuid = $uuid;
    $this->config = $this->config('pdb_ng2.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('uuid'));
  }

  /**
   * buildNg2Calendar.
   *
   * @return string
   *   Return an Angular 2 Calendar component.
   */
  public function buildNg2Calendar() {
    //
    $uuid = $this->uuid->generate();
    //
    $component = \Drupal::service('ng2_entity.ng2_view_display')
      ->getComponentByMachineName('ng2-ncc-calendar');
    // Attach Libraries
    $build['#attached']['library'] = [
      'ncc/lodash',
      'ncc/kendo-ui',
      'pdb_ng2/pdb.ng2.config'
    ];
    //
    $config_data_default_timezone = \Drupal::config('system.date')
      ->get('timezone.default');
    //
    $build['#attached']['drupalSettings']['timezone'] = !empty($config_data_default_timezone) ?
      $config_data_default_timezone : @date_default_timezone_get();
    // Attach Settings
    $build['#attached']['drupalSettings']['pdb']['ng2'] = [
      'module_path' => drupal_get_path('module', 'pdb_ng2'),
      'development_mode' => $this->config->get('development_mode'),
      'global_injectables' => [],
      'components' => [
        "instance-id-{$uuid}" => [
          'uri' => $component['path'],
          'element' => $component['machine_name'],
        ]
      ]
    ];
    $build['#allowed_tags'] = array($component['machine_name']);
    $build['#prefix'] = '<div class="ncc-calendar-wrapper">';
    $build['#markup'] = "<{$component['machine_name']} id='instance-id-{$uuid}'></{$component['machine_name']}>";
    $build['#suffix'] = '</div>';
    return $build;
  }

}
