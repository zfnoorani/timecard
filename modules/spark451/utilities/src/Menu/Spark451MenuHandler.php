<?php

namespace Drupal\utilities\Menu;

use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Menu\MenuActiveTrail;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Menu\MenuTreeParameters;

/**
 * Class Spark451MenuHandler.
 *
 * @package Drupal\utilities
 */
class Spark451MenuHandler implements Spark451MenuHandlerInterface {


  /**
   * Drupal\Core\Menu\MenuLinkTree definition.
   *
   * @var \Drupal\Core\Menu\MenuLinkTree
   */
  protected $menuLinkTree;

  /**
   * Drupal\Core\Menu\MenuActiveTrail definition.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrail
   */
  protected $menuActiveTrail;

  /**
   * Spark451MenuHandler constructor.
   * @param \Drupal\Core\Menu\MenuLinkTree $menu_link_tree
   * @param \Drupal\Core\Menu\MenuActiveTrail $menu_active_trail
   */
  public function __construct(MenuLinkTree $menu_link_tree, MenuActiveTrail $menu_active_trail) {
    $this->menuLinkTree = $menu_link_tree;
    $this->menuActiveTrail = $menu_active_trail;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMenuItems(array $vars) {
    // Retrieve menu items from content.
    $items = NestedArray::getValue($vars, ['content', '#items']);
    return (!empty($items)) ? $items : [];
  }

  /**
   * @param $menu_name
   * @return array
   */
  public function getMenuItems($menu_name) {
    // Retrieve menu active trail.
    $menuActiveTrail = $this->menuActiveTrail->getActiveTrailIds($menu_name);
    // Create MenuTreeParameters instance.
    $parameters = new MenuTreeParameters();
    // Exclude root, enable only active links and define active trail.
    $parameters->excludeRoot()->onlyEnabledLinks()->setActiveTrail($menuActiveTrail);
    // Get active link.
    $activeMenuId = NULL;
    if($active = $this->menuActiveTrail->getActiveLink($menu_name)) {
      $activeMenuId = $active->getPluginId();
    }
    // Load link menu tree.
    $tree = $this->menuLinkTree->load($menu_name, $parameters);
    // START: Issue: https://www.drupal.org/node/2226481
    // Map callback for sorting menu items
    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    // Properly reorder menu items
    $tree = $this->menuLinkTree->transform($tree, $manipulators);
    // END: Issue: https://www.drupal.org/node/2226481
    // Build hierarchical menu.
    return $this->buildHierarchicalMenu($tree, $activeMenuId);
  }

  /**
   * {@inheritdoc}
   */
  public function buildMainMenu(array $vars) {
    // Load menu name.
    $menuName = NestedArray::getValue($vars, ['content', '#menu_name']);
    return $this->getMenuItems($menuName);
  }

  /**
   * Walk through given menu links to build hierarchy menu.
   *
   * @param $items array Menu link objects to walk through.
   * @param array $build Given array to store link data.
   * @return array Menu hierarchy.
   */
  protected function buildHierarchicalMenu($items, $active, $build = []) {
    // Walk through all menu links.
    foreach ($items as $key => $item) {
      // Setup link item data.
      $build[$key] = [
        'href' => $item->link->getUrlObject()->toString(),
        'caption' => $item->link->getTitle(),
      ];
      // If current item is inside active trail.
      if($item->inActiveTrail) {
        // Define item classes (Active and current based on active link).
        $build[$key]['class'] = ($item->link->getPluginId() === $active) ? 'is-active current' : 'is-active';
      }
      // Check children related.
      if($item->hasChildren) {
        // Recursive call to build children.
        $build[$key]['children'] = $this->buildHierarchicalMenu($item->subtree, $active);
      }
    }
    // Return current build.
    return $build;
  }

}
