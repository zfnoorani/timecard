<?php

namespace Drupal\utilities\Menu;

/**
 * Interface Spark451MenuHandlerInterface.
 *
 * @package Drupal\utilities
 */
interface Spark451MenuHandlerInterface {

  /**
   * Load menu items from content variable inside given parent array.
   *
   * @param array $vars Given parent array to retrieve data.
   * @return array An array with menu items, otherwise empty array.
   */
  public function loadMenuItems(array $vars);

  /**
   * Build main menu structure.
   *
   * @param array $vars Given parent variables.
   * @return array Main menu structure.
   */
  public function buildMainMenu(array $vars);

}
