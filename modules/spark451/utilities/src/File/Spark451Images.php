<?php

namespace Drupal\utilities\File;


trait Spark451Images
{
  /**
   * @var string
   */
  private $url;

  /**
   * @var string
   */
  private $destination;

  /**
   * @var binary
   */
  private $file;

  /**
   * Process image
   *
   * @param $url
   * @param $destination
   * @return \Drupal\file\FileInterface|false
   */
  protected function process($url, $destination) {
    $this->url = $url;
    $this->destination = $destination;

    $this->setDestination();
    $this->setFile();

    return $this->saveFile();
  }

  /**
   * Set destination for drupal
   * Destination should be public://
   */
  private function setDestination() {
    if (!file_prepare_directory($this->destination, FILE_CREATE_DIRECTORY)) {
      \Drupal::logger(__METHOD__)->error('The destination is wrong.');
    }

    $this->destination .= '/' .basename($this->url);
  }

  /**
   * Get file content and set it
   */
  private function setFile() {
    $this->file = file_get_contents($this->url);
  }

  /**
   * Create new file
   *
   * @return \Drupal\file\FileInterface|false
   */
  private function saveFile() {
    return file_save_data($this->file, $this->destination, FILE_EXISTS_REPLACE);
  }
}
