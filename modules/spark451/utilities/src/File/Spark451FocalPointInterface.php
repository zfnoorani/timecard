<?php

namespace Drupal\utilities\File;

use \Drupal\Core\Form\FormStateInterface;

/**
 * Interface Spark451FocalPointInterface.
 *
 * @package Drupal\utilities
 */
interface Spark451FocalPointInterface {

  //
  public function hookFormFileImageEditFormAlter(&$form, FormStateInterface $form_state);
}
