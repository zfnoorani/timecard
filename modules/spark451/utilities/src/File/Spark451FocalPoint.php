<?php

namespace Drupal\utilities\File;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\focal_point\FocalPointManager;
use Drupal\Core\Image\ImageFactory;

/**
 * Class Spark451FocalPoint.
 *
 * @package Drupal\utilities
 */
class Spark451FocalPoint implements Spark451FocalPointInterface {

  /**
   * Drupal\focal_point\FocalPointManager definition.
   *
   * @var \Drupal\focal_point\FocalPointManager
   */
  protected $focalPointManager;
  /**
   * Drupal\Core\Image\ImageFactory definition.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * @var array
   */
  protected $config = [];

  /**
   * Spark451FocalPoint constructor.
   * @param \Drupal\focal_point\FocalPointManager $focal_point_manager
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   */
  public function __construct(FocalPointManager $focal_point_manager, ImageFactory $image_factory) {
    $this->focalPointManager = $focal_point_manager;
    $this->imageFactory = $image_factory;
    $this->config = [
      'crop_type' => \Drupal::config('focal_point.settings')->get('crop_type'),
      'default_value' => \Drupal::config('focal_point.settings')->get('default_value'),
    ];
  }

  /**
   * {@inheritDocs}
   */
  public function hookFormFileImageEditFormAlter(&$form, FormStateInterface $form_state) {
    $file = $form_state->getBuildInfo()['callback_object']->getEntity();
    $element_selector = 'focal-point-' . $file->id();
    $preview_image_render = [
      'style_name' => 'medium',
      'uri' => $file->getFileUri(),
    ];

    $image = $this->imageFactory->get($file->getFileUri());
    if ($image->isValid()) {
      $preview_image_render['width'] = $image->getWidth();
      $preview_image_render['height'] = $image->getHeight();
    }
    else {
      $preview_image_render['width'] = $preview_image_render['height'] = NULL;
    }

    $focal_point = NULL;
    if($crop = $this->focalPointManager->getCropEntity($file, $this->config['crop_type'])) {
      $anchor = $this->focalPointManager
        ->absoluteToRelative($crop->x->value, $crop->y->value, $image->getWidth(), $image->getHeight());
      $focal_point = "{$anchor['x']},{$anchor['y']}";
    }

    $form['preview'] = [
      '#weight' => -1,
      'indicator' => [
        '#theme_wrappers' => ['container'],
        '#attributes' => [
          'class' => ['focal-point-indicator'],
          'data-selector' => $element_selector,
          'data-field-name' => 'file-entity',
        ],
        '#markup' => '',
        '#prefix' => '<p>' . t('Choose a Focal Point') . '</p>',
      ],
      'thumbnail' => [
        '#theme' => 'image_style',
        '#width' => $preview_image_render['width'],
        '#height' => $preview_image_render['height'],
        '#style_name' => $preview_image_render['style_name'],
        '#uri' => $preview_image_render['uri'],
        '#suffix' => '<p>' . t('Move the plus sign to the most important part of the image. If the image needs to be cropped for smaller screens, the focal point won’t be cut off.') . '</p>',
      ],
    ];

    $form['focal_point'] = [
      '#type' => 'textfield',
      '#title' => 'Focal point',
      '#description' => t('Specify the focus of this image in the form "leftoffset,topoffset" where offsets are in percents. Ex: 25,75'),
      '#default_value' => $focal_point ?: $this->config['default_value'],
      '#element_validate' => [self::class . '::callbackFileImageEditFormValidateAlter'],
      '#attributes' => [
        'class' => ['focal-point', $element_selector],
        'data-selector' => $element_selector,
        'data-field-name' => 'file-entity',
      ],
      '#attached' => [
        'library' => [
          'focal_point/drupal.focal_point',
        ],
      ],
    ];
    $form['replace_upload']['#description'] = t('This file will replace the existing file. This action cannot be undone.');

    $form['actions']['submit']['#submit'][] = self::class . '::callbackFileImageEditFormSubmitAlter' ;
    //TODO: Check behavior from metadata and entity file fields.
    $form['field_image_alt_text']['#access'] = FALSE;
    $form['field_image_title_text']['#access'] = FALSE;
  }

  //
  public static function callbackFileImageEditFormValidateAlter($element, FormStateInterface $form_state) {
    $focal_point_value = $form_state->getValue('focal_point');
    if(!\Drupal::service('focal_point.manager')->validateFocalPoint($focal_point_value)) {
      $form_state->setError($element, new TranslatableMarkup('The !title field should be in the form "leftoffset,topoffset" where offsets are in percents. Ex: 25,75.', ['!title' => $element['#title']]));
    }
  }

  //
  public static function callbackFileImageEditFormSubmitAlter(&$form, FormStateInterface $form_state) {
    //
    $file = $form_state->getBuildInfo()['callback_object']->getEntity();
    $focal_point = $form_state->getValue('focal_point');
    list($x, $y) = explode(',', $focal_point);

    $crop_type = \Drupal::config('focal_point.settings')->get('crop_type');
    $crop = \Drupal::service('focal_point.manager')->getCropEntity($file, $crop_type);

    $width = NULL;
    $height = NULL;
    $image = \Drupal::service('image.factory')->get($file->getFileUri());
    if ($image->isValid()) {
      $width = $image->getWidth();
      $height = $image->getHeight();
    }
    \Drupal::service('focal_point.manager')->saveCropEntity($x, $y, $width, $height, $crop);
  }

}
