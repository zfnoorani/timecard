<?php

namespace Drupal\utilities\Ng2;

use Drupal\Component\Uuid\Php;
use Drupal\Core\Config\ConfigFactory;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class Spark451Ng2Stack.
 *
 * @package Drupal\utilities
 */
class Spark451Ng2Stack implements Spark451Ng2StackInterface {

  /**
   * @var \Drupal\Component\Uuid\Php
   */
  protected $uuid;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Spark451Ng2Stack constructor.
   * @param \Drupal\Component\Uuid\Php $uuid
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   */
  public function __construct(Php $uuid, ConfigFactory $config_factory) {
    $this->uuid = $uuid;
    $this->config = $config_factory->get('pdb_ng2.settings');
  }

  public function hookPreprocessAngular2Component(&$variables) {
    if (empty($variables['entity']) || empty($variables['component'])) {
      return;
    }
    // TODO: Check this condition.
    if(!empty($variables['component']['component_wrapper'])) {
      //
      unset($variables['#attached']);
    }
    //
    $entity = $variables['entity'];
    if(!$entity instanceof ParagraphInterface || empty($entity->_referringItem)){
      return;
    }
    //
    $component = $variables['component'];
    if (empty($component['component_wrapper'])) {
      return;
    }
    //
    $isActive = FALSE;
    $isPeekAdjacent = FALSE;
    //
    array_map(function($field) use ($entity, &$isActive, &$isPeekAdjacent) {
      //
      $parent = $entity->getParentEntity();
      //

      if (!empty($parent)) {
        if (!$parent->hasField($field)) {
          return;
        }
      } else {
        return;
      }

      //
      $result = array_filter(['field_stack_featured', 'field_highlight_featured'], function($fieldName) use ($parent) {
        return $parent->hasField($fieldName);
      });
      if(empty($result)) {
        return;
      }
      //
      $featuredField = reset($result);
      //
      foreach ($parent->{$field} as $key => $card) {
        // Peek Adjacent card special case
        // if last card (5th card) is Active (Featured) card
        // than second to last card (4th) is Peek Adjacent card
        if ($parent->{$featuredField}->value == 5 &&
            $key == 3 &&
            $card->getValue()['target_id'] == $entity->id()
        ) {
          $isPeekAdjacent = TRUE;
        }

        // Peek Adjacent card value is equals to current key
        if ($parent->{$featuredField}->value == $key &&
          $card->getValue()['target_id'] == $entity->id()
        ) {
          $isPeekAdjacent = TRUE;
        }
        // Active (Featured) card value is equals to current key + 1
        if ($parent->{$featuredField}->value == ++$key &&
          $card->getValue()['target_id'] == $entity->id()
        ) {
          $isActive = TRUE;
        }
      }
    }, ['field_stack_card', 'field_stack_highlight_card', 'field_stack_event_card', 'field_department_stack', 'field_blog_stack']);
    //
    $cardState = array_reduce(explode('-', $component['machine_name']), function ($carry, $value) {
      $carry[] = ucfirst($value);
      return $carry;
    });
    //
    $cardState = implode($cardState) . $entity->id();
    // TODO: Refactor to use only once. @see Spark451Themer L678.
    $attributes = [
      '[isActive]' => (!empty($isActive)) ? '"true"' : '"false"',
      '[isPeekAdjacent]' => (!empty($isPeekAdjacent)) ? '"true"' : '"false"',
    ];
    $attributes = array_reduce(array_keys($attributes), function ($carry, $key) use ($attributes) {
      $carry[] = "{$key}={$attributes[$key]}";
      return $carry;
    }, []);
    $attributes = implode(' ', $attributes);
    //
    $componentWrapper = "<{$component['component_wrapper']} {$attributes} #{$cardState}>";
    $searchTag = "<{$component['machine_name']}";
    $ng2Tag = str_replace($searchTag, "{$searchTag} [cardState]='{$cardState}.cardState'", $variables['ng2_tag']);
    $variables['ng2_tag'] = $componentWrapper . $ng2Tag . "</{$component['component_wrapper']}>";
  }

  public function hookPreprocessAngular2Stack(&$variables) {

    if (empty($variables['component']) || empty($variables['ng_content'])) {
      return;
    }
    //
    $component = $variables['component'];
    //
    $uuid = $this->uuid->generate();
    $n2Tag = "<{$component['machine_name']} id='instance-id-{$uuid}'><div class='ncc-stack-placeholder'></div></{$component['machine_name']}>";
    // Get the attributes of the request
    $attributes = \Drupal::request()->attributes;
    if (!empty($attributes)) {
      // Get the route name
      $route = $attributes->get('_route');
      // If the page is profile.
      if ($route === 'entity.user.canonical') {
        // Get the user entity
        $user = $attributes->get('user');
        if (!empty($user)) {
          //Get the user image url
          $content = $variables['ng_content'];
          // If type image
          if (strpos($content, 'ncc-card-image')) {
            if (!empty($user->user_picture->entity)) {
              $imageUrl = \Drupal::service('spark451.themer')
                ->getImageByStyle('card_image_2x', $user->user_picture->entity->getFileUri());
              if ($imageUrl) {
                // Set the new placeholder with the image
                $n2Tag = "<{$component['machine_name']} id='instance-id-{$uuid}'>
                <div class='ncc-stack-placeholder' >
                    <img src='{$imageUrl}' style='width: 100%; border-radius: 6px; transform: scale(1); background-position: center;'></div></{$component['machine_name']}>";
              }
            }
          }
        }
      }
    }

    $variables['ng2_tag'] = $n2Tag;

    $variables['#attached']['library'] = [
      'ncc/hammerjs',
      'pdb_ng2/pdb.ng2.config',
    ];
    $variables['#attached']['drupalSettings']['pdb']['ng2'] = [
      'module_path' => drupal_get_path('module', 'pdb_ng2'),
      'development_mode' => $this->config->get('development_mode'),
      'global_injectables' => [],
      'components' => [
        "instance-id-{$uuid}" => [
          'uri' => $component['path'],
          'ngClassName' => $component['ng_class_name'],
          'template' => "<{$component['component_wrapper']}>{$variables['ng_content']}</{$component['component_wrapper']}>",
        ],
      ],
    ];
  }
}
