<?php

namespace Drupal\utilities\Ng2;

use Drupal\Core\Config\ImmutableConfig;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Facebook;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\paragraphs\Entity\Paragraph;
use DateTime;
use Drupal\Component\Uuid\Php;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class Ng2SocialFeeds
 * @package Drupal\utilities\Ng2
 */
class Ng2SocialFeeds {

  /**
   * HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Facebook Client.
   *
   * @var \Facebook\Facebook
   */
  protected $fb;

  /**
   * Instagram Token.
   *
   * @var string
   */
  protected $instagramToken;

  /**
   * Twitter API ID.
   *
   * @var string
   */
  protected $twitterId;

  /**
   * Twitter API Key.
   *
   * @var string
   */
  protected $twitterAPIKey;

  /**
   * Twitter API Secret.
   *
   * @var string
   */
  protected $twitterAPISecret;

  /**
   * Twitter Username.
   *
   * @var string
   */
  protected $twitterUsername;

  /**
   * Twitter Access Token.
   *
   * @var string
   */
  protected $twitterAccessToken;

  /**
   * Twitter Access Token Secret.
   *
   * @var string
   */
  protected $twitterAccessTokenSecret;

  /**
   * Facebook Access token.
   *
   * @var string
   */
  protected $facebookAccessToken ;

  /**
   * Feed limit.
   *
   * @var int
   */
  protected $feedLimit = 9;

  /**
   * Ng2SocialFeeds constructor.
   * @param \GuzzleHttp\Client $http_client
   * @param \Drupal\Component\Uuid\Php $uuid
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   */
  public function __construct(Client $http_client, Php $uuid, ConfigFactory $config_factory) {
    $this->httpClient = $http_client;
    $this->uuid =  $uuid;
    $this->config =  $config_factory->get('pdb_ng2.settings');
    // Init social networks credentials.
    $this->init($config_factory->get('utilities.SocialFeedSettings'));
  }

  /**
   * Setup all credentials based on configuration object.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Configuration object.
   */
  protected function init(ImmutableConfig $config) {
    // Setup Facebook attribute by create new Facebook object.
    $this->fb = new Facebook([
      'app_id' => $config->get('facebook.app_id'),
      'app_secret' => $config->get('facebook.app_secret'),
      'default_graph_version' => $config->get('facebook.graph_version'),
      'default_access_token' => $config->get('facebook.access_token'),
    ]);
    // Define "Facebook Access Token" attribute.
    $this->facebookAccessToken = $config->get('facebook.access_token');
    // Setup Twitter attributes.
    $this->twitterId = $config->get('twitter.app_id');
    $this->twitterAPIKey = $config->get('twitter.app_key');
    $this->twitterAPISecret = $config->get('twitter.app_secret');
    $this->twitterUsername = $config->get('twitter.app_username');
    $this->twitterAccessToken = $config->get('twitter.access_token');
    $this->twitterAccessTokenSecret = $config->get('twitter.access_token_secret');
    // Setup Instragram attribute.
    $this->instagramToken = $config->get('instagram.access_token');
  }

  //
  protected function buildHtmlContent(Array $allCards) {
    $nccSocialFeedOptions = [
      "simulateTouch" => false,
      "slidesPerView" => 'auto',
      "breakpoints" => [
        '1024' => [
          "slidesOffsetBefore" => 0
        ]
      ],
      "paginationClickable" => true,
      "spaceBetween" => 10,
      "keyboardControl" => true,
      "preventClicksPropagation" => false,
      "preventClicks" => false
    ];
    $slides = $this->buildSlides($allCards);

    $encodedOptions = json_encode($nccSocialFeedOptions);

    return <<<HTML
      <ncc-social-feed
        [options]="{ preventClicksPropagation: false, preventClicks: false, simulateTouch: false, slidesPerView: 'auto', breakpoints: { '1024': { slidesOffsetBefore: 0 } }, paginationClickable: true, spaceBetween: 10, keyboardControl: true }"
        [moveToClicked]="false"
        [hideNavigation]="false"
        [hidePager]="false"
        #socialfeed>
        {$slides}
      </ncc-social-feed>
HTML;
  }

  // builds HTML for ncc-stack items from an array of cards from all sources
  protected function buildSlides(Array $allCards) {
    $result = '';
    // create chunks of 3 items each for the social feed component
    $chunks = array_chunk($allCards, 3);

    foreach ($chunks as $slideIndex => $chunk) {
      // add the slider wrapper for each chunk
      $result .= '<div class="swiper-slide" (click)="socialfeed.moveTo(' . $slideIndex . ', $event)"><ncc-stack>';
      foreach ($chunk as $cardIndex => $card) {
        // create different ncc-card wrappers based on card index
        $prefix = '<ncc-card [isActive]="true" [isPeekAdjacent]="false" #' . $card->id . '>';
        if ($cardIndex === 1) {
          $prefix = '<ncc-card [isActive]="false" [isPeekAdjacent]="true" #' . $card->id . '>';
        }
        if ($cardIndex === 2) {
          $prefix = '<ncc-card [isActive]="false" [isPeekAdjacent]="false" #' . $card->id . '>';
        }
        $suffix = '</ncc-card>';
        // build the card with appropriate wrapper
        $result .= $prefix . $card->build() . $suffix;
      }
      // close the slider wrapper
      $result .= '</ncc-stack></div>';
    }

    return $result;
  }

  //
  protected function getAllCards(Paragraph $paragraph) {
    $result = [];

    $facebookCards = $this->getFacebookCards($paragraph, $this->feedLimit);
    $instagramCards = $this->getInstagramCards($paragraph, $this->feedLimit);
    $twitterCards = $this->getTwitterCards($paragraph, $this->feedLimit);

    // get the max number of items from all card arrays
    $max = max(count($instagramCards), count($twitterCards), count($facebookCards));

    // alternately add cards to the results
    for ($i = 0; $i < $max; $i++) {
      if (isset($facebookCards[$i])) {
        $result[] = $facebookCards[$i];
      }
      if (isset($instagramCards[$i])) {
        $result[] = $instagramCards[$i];
      }
      if (isset($twitterCards[$i])) {
        $result[] = $twitterCards[$i];
      }
    }

    return $result;
  }

  //
  protected function linkifyFacebook($post) {
    return $this->linkify($post, 'facebook');
  }

  //
  protected function linkifyTweet($tweet) {
    return $this->linkify($tweet, 'twitter');
  }

  /**
   * Takes a simple text value and creates html links
   * if it finds http(s) or www also converts hashtags
   * on twitter and facebook to links.
   *
   * @param $input string
   *   Text to parse.
   * @param $type string
   *   Social Network name to parse.
   * @return mixed
   *   HTML link parsed.
   */
  protected function linkify($input, $type) {
    //Convert urls to <a> links
    //jldust 5-16-19
    //Updated for PHP7.3
    $input = preg_replace("/([\w]+\:\/\/[\w\-?&;#~=\.\/\@]+[\w\/])/", "<a rel=\"nofollow\" target=\"_blank\" href=\"$1\">$1</a>", $input);
    if ($type === 'twitter') {
      //Convert hashtags to twitter searches in <a> links
      $input = preg_replace("/#([A-Za-z0-9\/\.]*)/", "<a target=\"_new\" href=\"http://twitter.com/search?q=$1\">#$1</a>", $input);
      //Convert attags to twitter profiles in <a> links
      $input = preg_replace("/#([A-Za-z0-9\/\.]*)/", "<a target=\"_new\" href=\"http://twitter.com/search?q=$1\">#$1</a>", $input);
    } else if ($type === 'facebook') {
      //Convert hashtags to facebook searches in <a> links
      $input = preg_replace("/@([A-Za-z0-9\/\.]*)/", "<a href=\"https://www.facebook.com/hashtag/$1\">@$1</a>", $input);
    }

    return htmlentities($input);
  }

  /**
   * @see http://stackoverflow.com/a/12939923/790920
   */
  protected function getTwitterCards(Paragraph $paragraph, $count) {
    $result = [];

    // check if we have a twitter account defined in the paragraph
    if ($paragraph->hasField('field_cb_twitter_account') && !$paragraph->field_cb_twitter_account->isEmpty()) {
      // grab the username from the field
      $twitterUsername = $paragraph->field_cb_twitter_account->value;

      $twitterData = $this->getTwitterFeed($twitterUsername, $count);

      if (is_object($twitterData) && isset($twitterData->errors)) {
        // TODO, enable it if you need debug this method
        // $error = reset($twitterData->errors);
        // watchdog_exception(__METHOD__, $error->message);
        return $result;
      }

      foreach ($twitterData as $tweet) {
        // loop through all tweets and add to array
        $card = new Ng2SocialCard();
        $card->id = 'card' . uniqid();
        $card->type = 'twitter';
        $card->data = [
          'thumbnail' => $tweet->user->profile_image_url_https,
          'caption' => $tweet->user->name,
          'account' => $tweet->user->screen_name,
          'profileUrl' => 'https://twitter.com/' . $tweet->user->screen_name,
          'content' => $this->linkifyTweet($tweet->text),
          'timeAgo' => $this->timeAgo($tweet->created_at)
        ];

        $result[] = $card;
      }
    }

    return $result;
  }

  //
  public function getTwitterFeed($twitterUsername, $count) {
    $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";

    $oauth = array(
      'count' => $count, // limit of tweets
      'screen_name' => $twitterUsername, // username used for feed
      'exclude_replies' => 'true', // don't get replies
      'include_rts' => 'false', // don't get retweets
      'oauth_consumer_key' => $this->twitterAPIKey,
      'oauth_nonce' => time(),
      'oauth_signature_method' => 'HMAC-SHA1',
      'oauth_token' => $this->twitterAccessToken,
      'oauth_timestamp' => time(),
      'oauth_version' => '1.0'
    );

    $baseInfo = $this->buildTwitterBaseString($url, 'GET', $oauth);
    $compositeKey = rawurlencode($this->twitterAPISecret) . '&' . rawurlencode($this->twitterAccessTokenSecret);
    $oauthSignature = base64_encode(hash_hmac('sha1', $baseInfo, $compositeKey, true));
    $oauth['oauth_signature'] = $oauthSignature;

    // Make requests
    $header = array($this->buildTwitterAuthorizationHeader($oauth), 'Expect:');
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_HEADER => false,
      CURLOPT_URL => $url . '?screen_name=' . $twitterUsername . '&count=' . $count . '&exclude_replies=true&include_rts=false',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_SSL_VERIFYPEER => false
    );

    $feed = curl_init();
    curl_setopt_array($feed, $options);
    $json = curl_exec($feed);
    $info = curl_getinfo($feed);
    curl_close($feed);

    $twitterData = json_decode($json);
    return $info['http_code'] == 200 ? $twitterData : null;
  }

  //
  public function getTwitterCard($twitterUrl) {
    $card = null;
    // http://stackoverflow.com/questions/5948051/regex-extract-twitterusername-from-url
    $result = preg_match("|https?://(www\.)?twitter\.com/(#!/)?@?([^/]*)|", $twitterUrl, $matches);

    if($result === 1 && $matches[3]) {
      $twitterData = $this->getTwitterFeed($matches[3], 1);
      if($twitterData) {
        if (empty($twitterData->errors)) {
          $tweet = $twitterData[0];
          $card = new Ng2SocialCard();
          $card->id = 'card' . uniqid();
          $card->type = 'twitter';
          $card->data = [
            'thumbnail' => $tweet->user->profile_image_url_https,
            'caption' => $tweet->user->name,
            'account' => $tweet->user->screen_name,
            'profileUrl' => 'http://twitter.com/' . $tweet->user->screen_name,
            'content' => $this->linkifyTweet($tweet->text),
            'timeAgo' => $this->timeAgo($tweet->created_at)
          ];
        }
      }
    }
    return $card;
  }

  //
  protected function getFacebookPosts($facebookId, $count) {
    $result = false;
    if (ctype_digit($facebookId)) {
      try {
        $result = $this->fb->get('/' . $facebookId . '/feed?limit=' . $count, $this->facebookAccessToken);
      } catch (FacebookResponseException $e) {
        drupal_set_message($e->getMessage(), 'error');
        watchdog_exception(__METHOD__, $e);
      }
    }
    return $result;
  }

  //
  protected function getFacebookInfo($facebookId) {
    return $this->fb->get('/' . $facebookId, $this->facebookAccessToken);
  }

  //
  protected function getFacebookImage($facebookId) {
    return $this->fb->get('/' . $facebookId . '/picture?redirect=0', $this->facebookAccessToken);
  }

  //
  protected function generateFacebookCardsFromFeed($facebookId, $pagePostsResponse) {
    $result = [];

    $pageInfoResponse = $this->getFacebookInfo($facebookId);
    $pageInfoNode = $pageInfoResponse->getGraphNode();
    $pageImageResponse = $this->getFacebookImage($facebookId);
    $pageImageNode = $pageImageResponse->getGraphNode();
    $pagePostsEdge = $pagePostsResponse->getGraphEdge();
    $pageName = $pageInfoNode->getField('name');
    $pageImage = $pageImageNode->getField('url');
    foreach($pagePostsEdge as $nodeObject) {
      $maxLen = 250;
      $message =  $nodeObject->getField('message');
      //Added this to remove blank Facebook cards from shared posts on Facebook, regards ticket number: 433877
      if($message == ''){
	continue;
      }
      if (strlen($message) > $maxLen) {
        $message = substr($message, 0, strrpos(substr($message, 0, $maxLen), ' '));
        $message .= '...';
      }
      $createdTime =  $nodeObject->getField('created_time');
      $formatedTime = $createdTime->format(DateTime::ISO8601);
      $card = new Ng2SocialCard();
      $card->id = 'card' . uniqid();
      $card->type = 'facebook';
      $card->data = [
        'thumbnail' => $pageImage ? $pageImage : '',
        'caption' => $pageName ? $pageName : '',
        'profileUrl' => 'https://facebook.com/' . $facebookId,
        'content' => $message ? $this->linkifyFacebook(str_replace('"', "'", $message)) : '',
        'timeAgo' => $this->timeAgo(date($formatedTime))
      ];
      // add card to array
      $result[] = $card;
    }

    return $result;
  }

  //
  protected function getFacebookCards(Paragraph $paragraph, $count) {
    $result = [];
    // check if we have an facebook account defined in the paragraph
    if ($paragraph->hasField('field_cb_facebook_account') && !$paragraph->field_cb_facebook_account->isEmpty()) {
      $facebookId = $paragraph->field_cb_facebook_account->value;
      $pagePostsResponse = $this->getFacebookPosts($facebookId, $count);
      if($pagePostsResponse && $pagePostsResponse->getHttpStatusCode() === 200) {
        $result = $this->generateFacebookCardsFromFeed($facebookId, $pagePostsResponse);
      }
    }
    return $result;
  }

  //
  public function getFacebookCard($facebookId) {
    $result = false;

    $pagePostsResponse = $this->getFacebookPosts($facebookId, 1);
    if($pagePostsResponse && $pagePostsResponse->getHttpStatusCode() === 200) {
      $cards = $this->generateFacebookCardsFromFeed($facebookId, $pagePostsResponse);
      if (count($cards)) {
        $result = $cards[0];
      }
    }

    return $result;
  }

  //
  protected function getInstagramCards(Paragraph $paragraph, $count) {
    $result = [];

    // check if we have an instagram account defined in the paragraph
    if ($paragraph->hasField('field_cb_instagram_account') && !$paragraph->field_cb_instagram_account->isEmpty()) {
      $instagramUserId = $paragraph->field_cb_instagram_account->value;
      // get user's images based on the user id
      $feedResponse = $this->getInstagramFeed($instagramUserId, $count);
      // if the request was successful
      if ($feedResponse && $feedResponse['meta']['code'] === 200) {
        // loop through all images and create cards
        foreach($feedResponse['data'] as $instagramItem) {
          $card = new Ng2SocialCard();
          $card->id = 'card' . uniqid();
          $card->type = 'instagram';
          $card->data = [
            'thumbnail' => $instagramItem['images']['thumbnail']['url'],
            'caption' => $instagramItem['user']['full_name'],
            'profileUrl' => 'https://instagram.com/' . $instagramItem['user']['username'],
            'image' => $instagramItem['images']['standard_resolution']['url'],
            'timeAgo' => $this->timeAgo(date(DateTime::ISO8601, $instagramItem['created_time']))
          ];
          // add card to array
          $result[] = $card;
        }
      }
    }

    return $result;
  }

  /**
   * buildNg2SocialFeed.
   *
   * @param Paragraph $paragraph
   *   Paragraph that contains the fields to social media usernames
   *
   * @return string
   *   Return an Angular 2 Social Feed component.
   */
  public function buildNg2SocialFeed(Paragraph $paragraph) {
    $uuid = $this->uuid->generate();
    $allCards = $this->getAllCards($paragraph);
    $ngContent = $this->buildHtmlContent($allCards);

    $component = \Drupal::service('ng2_entity.ng2_view_display')
      ->getComponentByMachineName('ng2-ncc-social-feed');

    $build = array();

    // Attach Libraries
    $build['#attached']['library'] = [
      'ncc/hammerjs',
      'ncc/swiper',
      'pdb_ng2/pdb.ng2.config'
    ];

    // Attach Settings
    $build['#attached']['drupalSettings']['pdb']['ng2'] = [
      'module_path' => drupal_get_path('module', 'pdb_ng2'),
      'development_mode' => $this->config->get('development_mode'),
      'global_injectables' => [],
      'components' => [
        "instance-id-{$uuid}" => [
          'uri' => $component['path'],
          'element' => $component['machine_name'],
          'ngClassName' => $component['ng_class_name'],
          'template' => $ngContent
        ]
      ]
    ];

    $build['#allowed_tags'] = array($component['machine_name'], 'div');
    $build['#prefix'] = '<div class="ncc-social-feed-wrapper clearfix">';
    $build['#markup'] = "<{$component['machine_name']} id='instance-id-{$uuid}'>
      <div class='ncc-social-feed-placeholder'></div>
    </{$component['machine_name']}>";
    $build['#suffix'] = '</div>';

    return $build;
  }

  //
  protected function getInstagramFeed($userId, $count) {
    /**
     * @TODO: Lazar why we're not using $userId parameter?
     */
    $uri = "https://api.instagram.com/v1/users/$userId/media/recent/";
    $options = [
      'query' => [
        'type' => 'image', // limit to images
        'scope' => 'public_content', // needed to get images from accounts other than the user's
        'access_token' => $this->instagramToken,
        'count' => $count,
      ],
    ];
    $url = Url::fromUri($uri, $options)->toString();

    return $this->fetchDataFromUrl($url);
  }

  /**
   * @see http://stackoverflow.com/a/12939923/790920
   */
  protected function buildTwitterBaseString($baseURI, $method, $params) {
    $r = array();
    ksort($params);
    foreach($params as $key=>$value){
      $r[] = "$key=" . rawurlencode($value);
    }
    return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
  }

  /**
   * @see http://stackoverflow.com/a/12939923/790920
   */
  protected function buildTwitterAuthorizationHeader($oauth) {
    $r = 'Authorization: OAuth ';
    $values = array();
    foreach($oauth as $key=>$value) {
      $values[] = "$key=\"" . rawurlencode($value) . "\"";
    }
    $r .= implode(', ', $values);
    return $r;
  }

  /**
   * Sends an http request to provided url
   *
   * @param string $url
   *   URL for http request
   *
   * @return bool|mixed
   *   The encoded response containing the response or FALSE
   */
  protected function fetchDataFromUrl($url, $headers = NULL) {
    // set default headers if none were passed
    if (!$headers) {
      $headers = array(
        'headers' => array(
          'Accept' => 'application/json'
        )
      );
    }
    try {
      $response = $this->httpClient->get($url, $headers);
      $data = Json::decode($response->getBody());
      if (empty($data)) {
        return FALSE;
      }

      return $data;
    }
    catch (RequestException $e) {
      return FALSE;
    }
  }

  /**
   * @see http://stackoverflow.com/a/37506967/790920
   */
  protected function timeAgo($time, $tense='ago') {
    // declaring periods as static function var for future use
    static $periods = array('year', 'month', 'day', 'hour', 'minute', 'second');

    // checking time format
    if(!(strtotime($time) > 0)) {
      return;
    }

    // getting diff between now and time
    $now  = new DateTime('now');
    $time = new DateTime($time);
    $diff = $now->diff($time)->format('%y %m %d %h %i %s');
    // combining diff with periods
    $diff = explode(' ', $diff);
    $diff = array_combine($periods, $diff);
    // filtering zero periods from diff
    $diff = array_filter($diff);
    // getting first period and value
    $period = key($diff);
    $value  = current($diff);

    // if input time was equal now, value will be 0, so checking it
    if(!$value) {
      $period = 'seconds';
      $value  = 0;
    } else {
      // converting days to weeks
      if($period=='day' && $value>=7) {
        $period = 'week';
        $value  = floor($value/7);
      }
      // adding 's' to period for human readability
      if($value>1) {
        $period .= 's';
      }
    }

    // returning timeago
    return "$value $period $tense";
  }

}
