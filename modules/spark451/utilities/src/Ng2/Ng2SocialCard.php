<?php

namespace Drupal\utilities\Ng2;

/**
 * Class Ng2SocialCard
 * @package Drupal\utilities\Ng2
 */
class Ng2SocialCard {

  /**
   * Unique Card ID.
   *
   * @var string
   */
  public $id;

  /**
   * Card type (twitter, facebook, instagram or image).
   *
   * @var string
   */
  public $type;

  /**
   * Content of specific card (differs from type to type).
   *
   * @var
   */
  public $data;

  public function build() {
    $result = '';
    switch ($this->type) {
      case 'instagram':
        $result = $this->buildInstagramCard();
        break;
      case 'twitter':
        $result = $this->buildTwitterCard();
        break;
      case 'facebook':
        $result = $this->buildFacebookCard();
        break;
      case 'image':
        $result = $this->buildImageCard();
        break;
    }
    return $result;
  }

  protected function buildInstagramCard() {
    $result = <<<HTML
              <ncc-card-instagram
                [cardState]="{$this->id}.cardState"
                profileUrl="{$this->data['profileUrl']}"
                icon="{$this->data['thumbnail']}"
                name="{$this->data['caption']}"
                date="{$this->data['timeAgo']}"
                image="{$this->data['image']}">
              </ncc-card-instagram>
HTML;
    return $result;
  }

  protected function buildTwitterCard() {
    $result = <<<HTML
              <ncc-card-twitter
                [cardState]="{$this->id}.cardState"
                profileUrl="{$this->data['profileUrl']}"
                icon="{$this->data['thumbnail']}"
                name="{$this->data['caption']}"
                account="{$this->data['account']}"
                date="{$this->data['timeAgo']}"
                content="{$this->data['content']}">
              </ncc-card-twitter>
HTML;
    return $result;
  }

  protected function buildFacebookCard() {
    $result = <<<HTML
              <ncc-card-facebook
                [cardState]="{$this->id}.cardState"
                profileUrl="{$this->data['profileUrl']}"
                icon="{$this->data['thumbnail']}"
                name="{$this->data['caption']}"
                date="{$this->data['timeAgo']}"
                content="{$this->data['content']}">
              </ncc-card-facebook>
HTML;
    return $result;
  }

  protected function buildImageCard() {
    $result = <<<HTML
              <ncc-card-image
                [cardState]="{$this->id}.cardState"
                image="{$this->data['image']}">
              </ncc-card-image>
HTML;
    return $result;
  }
}
