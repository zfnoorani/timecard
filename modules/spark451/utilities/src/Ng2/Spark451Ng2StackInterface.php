<?php

namespace Drupal\utilities\Ng2;

/**
 * Interface Spark451Ng2StackInterface.
 *
 * @package Drupal\utilities
 */
interface Spark451Ng2StackInterface {

  public function hookPreprocessAngular2Component(&$variables);

  public function hookPreprocessAngular2Stack(&$variables);

}
