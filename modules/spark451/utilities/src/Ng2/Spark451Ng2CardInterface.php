<?php

namespace Drupal\utilities\Ng2;

use Drupal\Core\Entity\ContentEntityBase;


/**
 * Class Spark451Ng2EventDateConverter.
 *
 * @package Drupal\utilities
 */
interface Spark451Ng2CardInterface {

  /**
   * Implements callback alter to modify field mapping provided by entity_ng2.
   *
   * @param $value
   *   Raw value retrieve from simple execution.
   * @param $entity ContentEntityBase
   *   Entity which is being parsed.
   * @param $field string
   *   Field name which is being parsed.
   * @param $metadata array
   *   Extra data from YAML file definition.
   * @return mixed
   *   Final value parsed.
   */
  public function dateFormatter($value, $entity, $field, $metadata);

  /**
   * Implements hook_preprocess_HOOK() for angular2-component.html.twig.
   * @param $variables
   */
  public function hookPreprocessAngular2Component(&$variables);
}