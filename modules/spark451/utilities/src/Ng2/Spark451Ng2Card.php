<?php

namespace Drupal\utilities\Ng2;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\node\Entity\Node;
use Drupal\utilities\Theme\Spark451Themer;

/**
 * Class Spark451Ng2EventDateConverter.
 *
 * @package Drupal\utilities
 */
class Spark451Ng2Card implements Spark451Ng2CardInterface {

  /**
   * @var \Drupal\utilities\Theme\Spark451Themer
   */
  protected $themer;

  /**
   * Spark451Ng2Card constructor.
   * @param \Drupal\utilities\Theme\Spark451Themer $themer
   */
  public function __construct(Spark451Themer $themer) {
    $this->themer = $themer;
  }

  /**
   * {@inheritdoc}
   */
  public function dateFormatter($value, $entity, $field, $metadata) {
    //
    $format = implode(':', $metadata);
    //
    $dateTime = new \DateTime($value, New \DateTimeZone(DATETIME_STORAGE_TIMEZONE));
    $dateTime->setTimezone(New \DateTimeZone(drupal_get_user_timezone()));
    //
    return $dateTime->format($format);
  }

  /**
   * {@inheritdoc}
   */
  public function hookPreprocessAngular2Component(&$variables) {
    // Check entity and component variables.
    if (empty($variables['entity']) || empty($variables['component'])) {
      return;
    }
    //
    $this->prepareNccCardNews($variables);
    //
    $this->prepareNccCardImage($variables);
    //
    $this->prepareNccCardVideo($variables);
    //
    $this->prepareNccCardGallery($variables);
    //
    $this->prepareNccCardProfile($variables);
  }

  //
  protected function prepareNccCardNews(&$variables) {
    //
    if ('ncc-card-news' != $variables['component']['machine_name'] ||
      'card_news' != $variables['entity']->bundle()) {
      return;
    }
    // Retrieve entity and component.
    $entity = $variables['entity']->field_card_news->entity;
    //

    if ($entity) {
      if ('article' == $entity->bundle() && 'story' == $entity->field_article_type->value) {
        //
        $variables['ng2_tag'] = str_replace($variables['component']['machine_name'], 'ncc-card-story', $variables['ng2_tag']);
        //
        if (!$entity->field_image->isEmpty()) {
          //
          $imgUrl = $this->themer->getImageByStyle('card_image_2x', $entity->field_image->first()->entity->getFileUri());
          $variables['ng2_tag'] = str_replace('image=""', "image='{$imgUrl}'", $variables['ng2_tag']);
        }
      }

      if ($entity->hasField('field_publication_date') && !$entity->field_publication_date->isEmpty()) {

        $timestamp = strtotime($entity->field_publication_date->value);
        $date = \Drupal::service('date.formatter')
          ->format($timestamp, 'custom', 'M j, Y');

        $variables['ng2_tag'] = str_replace('date=""', "date='{$date}'", $variables['ng2_tag']);
      }
    }
  }

  //
  protected function prepareNccCardImage(&$variables) {
    // Cards that should be affected with preprocess.
    $cards = [
      'ncc-card-image',
      'ncc-card-profile',
      'ncc-card-cta-photo',
      'ncc-card-story',
      'ncc-card-feature',
    ];
    // Replace the original image with image_card_2x image style for cards above
    if (!in_array($variables['component']['machine_name'], $cards)) {
      return;
    }
    // Retrieve entity.
    $entity = $variables['entity'];
    // Check for image card field.
    if ($entity->hasField('field_card_image') && $entity->field_card_image->count()) {
      // Retrieve image entity.
      if ($image = $entity->field_card_image->entity) {
        // Retrieve image URL based on image style.
        $imgUrl = $this->themer->getImageByStyle('card_image_2x', $image->getFileUri());
        $variables['ng2_tag'] = preg_replace('/image\=\"(.*)\"/U', "image='{$imgUrl}'", $variables['ng2_tag']);
      }
    }
  }

  //
  protected function prepareNccCardVideo(&$variables) {
    //
    if ('ncc-card-video' != $variables['component']['machine_name']) {
      return;
    }
    // Retrieve entity and component.
    $video = FALSE;
    $mediaModal = [];
    $entity = $variables['entity'];
    //
    if($variables['entity'] instanceof Node) {
      //
      $video = $entity;
      //
      $mediaModal = $this->themer->buildVideoContent($video);
    }
    elseif ($entity->hasField('field_card_video') && !$entity->field_card_video->isEmpty()) {
      //
      if ($video = $entity->field_card_video->first()->entity) {
        // Setup media modal content.
        $mediaModal = $this->themer->setUpMediaVideoContent($entity, 'field_card_video');
      }
    }
    // Retrieve File URI.
    $file_uri = NULL;
    if ($video && !$video->field_image->isEmpty()) {
      $file_uri = $video->field_image->first()->entity->getFileUri();

    } elseif (!empty($mediaModal['media']['#uri'])) {
      $file_uri = $mediaModal['media']['#uri'];
    }
    // Setup image URL based on image style.
    $imgUrl = $this->themer->getImageByStyle('card_image_2x', $file_uri);
    // Define media modal.
    if ($mediaModal && !empty($mediaModal['modal'])) {
      $output = [
        '#type' => 'inline_template',
        '#template' => "{% include '@ncc/partials/modal.html.twig' %}",
        '#attached' => [
          'library' => [
            'ncc/ncc-modal',
          ]
        ],
        '#context' => [
          'base_path' => base_path(),
          'directory' => $this->themer->getActiveTheme()->getPath(),
          'modal' => $mediaModal['modal']
        ],
      ];

      $id = $mediaModal['modal']['id'];
      $modalContent = $this->themer->getRenderer()->render($output);
      $variables['ng2_tag'] = str_replace('image=""', "image='{$imgUrl}' modalContentId='{$id}'", $variables['ng2_tag']) . $modalContent;
    }
  }

  //
  protected function prepareNccCardGallery(&$variables) {
    //
    if ('ncc-card-gallery' != $variables['component']['machine_name']) {
      return;
    }

    // Retrieve entity and component.
    $entity = $variables['entity'];
    //
    if ($entity->hasField('field_card_gallery') && $entity->field_card_gallery->count()) {
      //
      $galleryModal = $this->themer->setUpMediaGalleryContent($entity, 'field_card_gallery');

      if ($galleryModal && !empty($galleryModal['modal'])) {

        $imgUrl = $galleryModal['fullImageUrl'];

        $output = [
          '#type' => 'inline_template',
          '#template' => "{% include '@ncc/partials/modal.html.twig'  %}",
          '#attached' => [
            'library' => [
              'ncc/swiper',
              'ncc/object-fit-images'
            ]
          ],
          '#context' => [
            'base_path' => base_path(),
            'directory' =>  $this->themer->getActiveTheme()->getPath(),
            'modal' => $galleryModal['modal']
          ],
        ];

        $id = $galleryModal['modal']['id'];
        $modalContent = $this->themer->getRenderer()->render($output);

        $variables['ng2_tag'] = str_replace('image=""', "image='{$imgUrl}' modalContentId='{$id}'", $variables['ng2_tag']) . $modalContent;
      }
    }
    else {
      if ($entity->hasField('field_gallery_image') && $entity->field_gallery_image->count()) {
        $file = $entity->field_gallery_image->first()->entity;
        $imgUrl = $this->themer->getImageByStyle('card_image_2x', $file->getFileUri());
        $variables['ng2_tag'] = str_replace('image=""', "image='{$imgUrl}'", $variables['ng2_tag']);
      }
    }
  }

  //
  protected function prepareNccCardProfile(&$variables) {
    //
    if ('ncc-card-profile' != $variables['component']['machine_name']) {
      return;
    }
    // Retrieve entity and component.
    $entity = $variables['entity'];
    $component = $variables['component'];
    //
    $trayContent = '';
    $attributes = [];
    //
    if ($entity->hasField('field_card_profile') && $entity->field_card_profile->count()) {
      //
      if ($profile = $entity->field_card_profile->first()->entity) {
        //
        $attributes += [
          'linkUrl' => '"' . $profile->toUrl()->toString() . '"',
          'name' => '"' . implode(' ', [
              $profile->field_profile_first_name->value,
              $profile->field_profile_middle_name->value,
              $profile->field_profile_last_name->value,
            ]) . '"',
        ];
        //
        $account = $profile->uid->first()->entity;
        //
        if (!$account->user_picture->isEmpty() && $file = $account->get('user_picture')->entity) {
          $imgUrl = $this->themer->getImageByStyle('card_image_2x', $file->getFileUri());
          $variables['ng2_tag'] = str_replace('image=""', "image='{$imgUrl}'", $variables['ng2_tag']);
        }
      }
    }
    else {
      //
      $first_name = (!empty($entity->field_card_first_name->value)) ? $entity->field_card_first_name->value : '';
      $last_name = (!empty($entity->field_card_last_name->value)) ? $entity->field_card_last_name->value : '';

      //  Merge the name
      $name = implode(' ', [$first_name, $last_name]);
      // Search if the name has special quote.
      if (strpos($name, '"')) {
        //Convert special characters to HTML entities
        $name =  htmlspecialchars($name);
      }

      $attributes += [
        'linkUrl' => '"#view-profile-link"',
        'trayContentId' => '"' . $entity->uuid() . '"',
        'name' => '"' . $name . '"',
      ];
      //
      $quote = (!empty($entity->field_card_quote->value)) ? $entity->field_card_quote->value : '';
      $department = (!empty($entity->field_card_department->value)) ? $entity->field_card_department->value : '';
      $job_title = !empty($entity->field_card_job_title->value) ? $entity->field_card_job_title->value : '';
      $description = !empty($entity->field_description->value) ? $entity->field_description->value : '';
      $trayContentTheme = [
        '#theme' => 'tray_content',
        '#trayContent' => [
          'id' => $entity->uuid(),
          'type' => 'tray-profile',
          'quote' => ['text' => $quote],
          'name' => $first_name,
          'department' => $department,
          'title' => $job_title,
          'description' => $description,
        ],
      ];
      if ($entity->field_card_image->count() && $entity->field_card_image->entity) {
        $trayContentTheme['#trayContent']['image'] = $this->themer->getImageByStyle('card_image_2x', $entity->field_card_image->entity->getFileUri());
      }
      $trayContent = $this->themer->getRenderer()->render($trayContentTheme);
    }
    //
    $attributes = array_reduce(array_keys($attributes), function ($carry, $key) use ($attributes) {
      $carry[] = "{$key}={$attributes[$key]}";
      return $carry;
    }, []);
    //
    $searchTag = "></{$component['machine_name']}>";
    $variables['ng2_tag'] = str_replace($searchTag, ' ' . implode(' ', $attributes) . $searchTag, $variables['ng2_tag']) . $trayContent;
  }
}
