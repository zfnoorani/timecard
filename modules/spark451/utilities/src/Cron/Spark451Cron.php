<?php

namespace Drupal\utilities\Cron;

use Drupal\Component\Utility\Timer;
use Drupal\Core\Cron;

/**
 * Class Spark451Cron
 */
class Spark451Cron extends Cron {

  /**
   * Invokes any cron handlers implementing hook_cron.
   */
  protected function invokeCronHandlers() {
    $cron_debug_invokes = $this->state->get('cron_debug_invokes', FALSE);
    $module_previous = '';

    // Iterate through the modules calling their cron handlers (if any):
    foreach ($this->moduleHandler->getImplementations('cron') as $module) {

      if ($cron_debug_invokes) {

        if (!$module_previous) {
          $this->logger->notice('Starting execution of @module_cron().', [
            '@module' => $module,
          ]);
        }
        else {
          $this->logger->notice('Starting execution of @module_cron(), execution of @module_previous_cron() took @time.', [
            '@module' => $module,
            '@module_previous' => $module_previous,
            '@time' => Timer::read('cron_' . $module_previous) . 'ms',
          ]);
        }
      }
      Timer::start('cron_' . $module);

      // Do not let an exception thrown by one module disturb another.
      try {
        $this->moduleHandler->invoke($module, 'cron');
      } catch (\Exception $e) {
        watchdog_exception('cron', $e);
      }

      Timer::stop('cron_' . $module);
      $module_previous = $module;
    }
    if ($module_previous && $cron_debug_invokes) {
      $this->logger->notice('Execution of @module_previous_cron() took @time.', [
        '@module_previous' => $module_previous,
        '@time' => Timer::read('cron_' . $module_previous) . 'ms',
      ]);
    }
  }
}