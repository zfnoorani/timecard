<?php

namespace Drupal\utilities\Entity;


use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\workbench_moderation\EntityOperations;
use Drupal\workbench_moderation\Form\EntityModerationForm;

/**
 * Class Spark451EntityOperations
 * @package Drupal\utilities\Entity
 */
class Spark451EntityOperations extends EntityOperations {

  /**
   * {@inheritdoc}
   */
  public function entityView(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    // Check if current entity has moderation enabled.
    if (!$this->moderationInfo->isModeratableEntity($entity)) {
      return;
    }
    // Check component.
    $component = $display->getComponent('workbench_moderation_control');
    if ($component) {
      // Load moderation form.
      $moderationForm = $this->formBuilder->getForm(EntityModerationForm::class, $entity);
      if(!isset($moderationForm['submit'])) {
        return;
      }
      // Check if entity is published and draft option exists.
      if(NODE_PUBLISHED == $entity->status->value && !empty($moderationForm['new_state']['#options']['draft'])) {
        // Remove draft option.
        unset($moderationForm['new_state']['#options']['draft']);
      }
      // check if the form has values to change.
      if (!empty($moderationForm['new_state']['#options'])){
        // Setup moderation form.
        $build['workbench_moderation_control'] = $moderationForm;
        $build['workbench_moderation_control']['#weight'] = $component['weight'];
      }

    }
  }

}