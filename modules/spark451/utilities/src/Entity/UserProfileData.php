<?php

namespace Drupal\utilities\Entity;

use Drupal\User\Entity\User;
use Drupal\profile\Entity\Profile;

/**
 * User profile data processor.
 *
 * Class UserProfileData
 * @package Drupal\utilities\Entity
 */
class UserProfileData {

  /**
   * Get User entity
   *
   * @param null $uid
   * @param null $username
   * @param null $role
   * @param null $email
   * @return bool|\Drupal\Core\Entity\EntityInterface|null|object|static
   */
  private function getUser($uid = NULL, $username = NULL, $mail, $role) {
    $user = NULL;

    if (!empty($uid)) {
      $user = User::load($uid);
      if (empty($user)) {
        $user = User::create();
        $user->setUsername($username);
        $user->enforceIsNew();
        $user->setEmail($mail);
        $user->status->value = 1;
        $user->addRole($role);
        $user->save();
      }
    }
    elseif (!empty($username)) {
      $user = user_load_by_name($username);
      if (empty($user)) {
        $user = User::create();
        $user->setUsername($username);
        $user->enforceIsNew();
        $user->setEmail($mail);
        $user->status->value = 1;
        $user->addRole($role);
        $user->save();
      }
    }

    return $user;
  }

  /**
   * Get Profile Entity
   *
   * @param $user
   * @param $type
   * @return mixed
   */
  private function getProfile($user, $type) {

    $profile = \Drupal::entityTypeManager()
      ->getStorage('profile')
      ->loadByUser($user, $type);

    if (!$profile) {
      $profile = Profile::create([
        'type' => $type,
        'uid' => $user->id(),
      ]);
    }
    return $profile;
  }

  /**
   * Save all data in to the profile
   *
   * @param array $profile
   */
  public function save(array $profiles) {
    $main = (isset($profiles['main']) ? $profiles['main'] : NULL);
    $faculty = (isset($profiles['faculty']) ? $profiles['faculty'] : NULL);

    if (isset($main['username']) && isset($main['mail'])) {
      $user = $this->getUser(NULL, $main['username'], $main['mail'], 'faculty');
      $main_profile = $this->getProfile($user, 'main');
      $faculty_profile = $this->getProfile($user, 'faculty');
      if (!empty($main)) {
        foreach ($main as $field => $value) {
          if ($field !== 'username' && $field !== 'mail') {
            if ('field_profile_links' === $field) {
              $main_profile->$field->setValue([
                array(
                  'uri' => $value,
                  'title' => 'Website',
                  'attributes' => array()
                )
              ]);
            }
            else {
              $main_profile->set($field, $value);
            }
          }
        }
        $main_profile->save();
      }
      if (!empty($faculty)) {
        foreach ($faculty as $field => $value) {
          if ($field !== 'username' && $field !== 'mail') {
            $faculty_profile->$field->setValue($value);
          }
        }
        $faculty_profile->save();
      }
    }
  }
}