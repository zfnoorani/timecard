<?php

namespace Drupal\utilities\Entity;

use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileType;

/**
 * Class UserProfile
 * @package Drupal\utilities\Entity
 */
class UserProfile extends Profile {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Check main profile type.
    if('main' != $this->bundle()) {
      return parent::label();
    }
    // Setup label.
    return
      t('@full_name (@username - uid: @uid)',
        [
          '@full_name' => self::buildProfileFullName($this),
          '@username' => $this->getOwner()->getDisplayName(),
          '@uid' => $this->getOwnerId(),
        ]);
  }

  public static function buildProfileFullName(Profile $profile) {
    // Define field to check.
    $fields = [
      'field_profile_first_name',
      'field_profile_middle_name',
      'field_profile_last_name',
    ];
    // Get raw values.
    $values = array_map(function($field) use ($profile) {
      // Check field values.
      if($profile->hasField($field)) {
        return $profile->{$field}->value;
      }
      return NULL;
    }, $fields);
    // Put together all field values.
    return implode(' ', $values);
  }
}
