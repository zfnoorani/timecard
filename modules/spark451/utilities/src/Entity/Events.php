<?php

namespace Drupal\utilities\Entity;


use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\file_entity\Entity\FileEntity;
use Drupal\utilities\File\Spark451Images;

/**
 * Class Events
 * @package Drupal\utilities\Entity
 */
class Events {

  /**
   * VID of the taxonomy type
   */
  const VID_TYPE = 'event_type';

  /**
   * VID of taxonomy categories
   */
  const VID_CATEGORY = 'event_categories';

  /**
   * Destination to save images
   */
  const FILE_DESTINATION = 'public://events';

  /**
   * Id of the node
   *
   * @var integer
   */
  protected $id;

  /**
   * Unique ID for event
   *
   * @var integer
   */
  protected $uid;

  /**
   * Name of taxonomy category
   *
   * @var string
   */
  protected $category;

  /**
   * Name of taxonomy type
   *
   * @var string
   */
  protected $type;

  /**
   * Name of event
   *
   * @var string
   */
  protected $title;

  /**
   * Event body
   *
   * @var string
   */
  protected $body;

  /**
   * Location of event
   *
   * @var string
   */
  protected $address;

  /**
   * Email of contact
   *
   * @var string
   */
  protected $contactEmail;

  /**
   * Name of contact
   * @var string
   */
  protected $contactName;

  /**
   * Phone number of contact
   * @var string
   */
  protected $contactPhone;

  //Added tickets info 10/20/2017 - Alexander Spaulding, Peter Olache - RT #400314
  //All three @var string
  protected $ticketInformation;
  protected $ticketUrl;
  protected $priceInformation;

  /**
   * Ticket price
   * @var double
   */
  protected $ticketPrice;

  /**
   * Ticket price
   * @var string
   */
  protected $ticketLink;

  /**
   * Ticket price
   * @var string
   */
  protected $ticketDescription;

  /**
   * Image of the event
   *
   * @var FileEntity
   */
  protected $image;

  /**
   * Room of event
   *
   * @var string
   */
  protected $room;

  /**
   * Start Date and time
   *
   * @var \DateTime
   */
  protected $startDate;

  /**
   * End Date and time
   *
   * @var \DateTime
   */
  protected $endDate;

  /**
   * @var string
   */
  protected $langCode;

  /**
   * Implement image trait
   */
  use Spark451Images;

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * @param int $uid
   */
  public function setUid($uid) {
    $this->uid = $uid;
  }

  /**
   * @return string
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * @param mixed $category
   */
  public function setCategory($category) {
    // Get the category id filtering by name
    $this->category = array_map(function($term) {
      return $this->taxonomyTerm(trim($term), self::VID_CATEGORY);
    }, explode(',', $category));
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param $type
   */
  public function setType($type) {
    $this->type = $this->taxonomyTerm($type, self::VID_TYPE);
  }

  /**
   * @return mixed
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param mixed $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * @return mixed
   */
  public function getBody() {
    return $this->body;
  }

  /**
   * @param mixed $body
   */
  public function setBody($body) {
    $this->body = $body;
  }

  /**
   * @return mixed
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * @param mixed $address
   */
  public function setAddress($address, $city, $state, $zipCode) {
    $this->address = $address;

    if (NULL != $address) {
      $this->address = [
        'country_code' => 'US',
        'address_line1' => $address,
        'locality' => $city,
        'administrative_area' => $state,
        'postal_code' => $zipCode
      ];
    }
  }

  /**
   * @return mixed
   */
  public function getContactEmail() {
    return $this->contactEmail;
  }

  /**
   * @param mixed $contactEmail
   */
  public function setContactEmail($contactEmail) {
    $this->contactEmail = $contactEmail;
  }

  /**
   * @return mixed
   */
  public function getContactName() {
    return $this->contactName;
  }

  /**
   * @param mixed $contactName
   */
  public function setContactName($contactName) {
    $this->contactName = $contactName;
  }

  /**
   * @return mixed
   */
  public function getContactPhone() {
    return $this->contactPhone;
  }

  /**
   * @param mixed $contactPhone
   */
  public function setContactPhone($contactPhone) {
    $this->contactPhone = $contactPhone;
  }

  //Added tickets getters and setters 10/20/2017 - Alexander Spaulding, Peter Olache - RT #400314
  public function getTicketInformation(){
    return $this->ticketInformation;
  }

   public function setTicketInformation($ticketInformation){
    $this->ticketInformation = $ticketInformation;
  }

  public function getPriceInformation(){
    return $this->priceInformation;
  }

   public function setPriceInformation($priceInformation){
    $this->priceInformation = $priceInformation;
  }

  public function getTicketUrl(){
    return $this->ticketUrl;
  }

   public function setTicketUrl($ticketUrl){
    $this->ticketUrl = $ticketUrl;
  }
  //End tickets getters and setters

  /**
   * @return mixed
   */
  public function getImage() {
    return $this->image;
  }

  /**
   * @param mixed $image
   */
  public function setImage($image) {
    if (NULL != $image) {
      if ($saved = $this->process($image, self::FILE_DESTINATION)) {
        $image = [
          'target_id' => $saved->id()
        ];
      }
    }

    $this->image = $image;
  }

  /**
   * @return mixed
   */
  public function getRoom() {
    return $this->room;
  }

  /**
   * @param mixed $room
   */
  public function setRoom($room) {
    $this->room = $room;
  }

  /**
   * @return mixed
   */
  public function getStartDate() {
    return $this->startDate;
  }

  /**
   * @param mixed $startDate
   */
  public function setStartDate($startDate) {
    $this->formattedDate($startDate);
    $this->startDate = $startDate;
  }

  /**
   * @return mixed
   */
  public function getEndDate() {
    return $this->endDate;
  }

  /**
   * @param mixed $endDate
   */
  public function setEndDate($endDate) {
    $this->formattedDate($endDate);
    $this->endDate = $endDate;
  }

  /**
   * Create or update events
   *
   * @param array $event
   * @return mixed
   */
  public function save(array $event) {

    // Create event object
    $this->setup($event);

    // Get entity by event uid
    $entity = $this->getEventByUid();

    // Set the event unique id
    $entity->set('field_event_uid', $this->getUid());

    // Set the new values
    $entity->setTitle($this->getTitle());
    $entity->set('uid', 1);
    $entity->set('body', [
      'value' => $this->getBody(),
      'format' => 'full_html'
    ]);
    $entity->set('field_location_room', $this->getRoom());

    // Contact information
    $entity->set('field_event_contact_email', $this->getContactEmail());
    $entity->set('field_event_contact_name', $this->getContactName());
    $entity->set('field_event_contact_phone', $this->getContactPhone());

    // Set ticket information - Added 10/20/2017 - Alexander Spaulding - RT #400314
    $entity->set('field_event_ticket_information', $this->getTicketInformation());
    $entity->set('field_event_price_information', $this->getPriceInformation());
    $entity->set('field_event_ticket_url', $this->getTicketUrl());


    // Category and type taxonomies
    $entity->set('field_event_categories', $this->getCategory());
    $entity->set('field_event_type', $this->getType());

    // Set address
    $entity->set('field_location_building', $this->getAddress());

    // Download image, save and set it
    $entity->set('field_image', $this->getImage());

    // Date range
    $entity->set('field_event_date_time', [
      'value' => $this->getStartDate(),
      'end_value' => $this->getEndDate()
    ]);

    return $entity->save();
  }


  /**
   * Deletes events
   *
   * @param array $event
   * Creates temporary data to locate and then delete
   * RT: #411249
   * maholmes, jdust 31 May 2017
   */
  public function deleteEvent(array $event) {
     
    // Create event object
    $this->setupUid($event);

    // Get entity by event uid
    $entity = $this->getEventByUid();

    // Set the event unique id
    $entity->set('field_event_uid', $this->getUid());

    // Delete Entity
    $entity->delete();
	
  }

  /**
   * Get the TIV by name
   *
   * @param $name
   * @param $vid
   * @return int|null
   */
  private function taxonomyTerm($name, $vid) {
    $tid = NULL;

    $term = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => $vid,
        'name' => $name
      ]);

    if ($term) {
      $entity = reset($term);
      $tid = $entity->id();
    }

    return $tid;
  }

  /**
   * Create event object
   *
   * @param array $event
   * @return $this
   */
  private function setup(array &$event) {
    $this->langCode = \Drupal::languageManager()
      ->getCurrentLanguage()
      ->getId();

    $this->setUid($event['UID']);
    $this->setCategory($event['Event Category']);
    $this->setType($event['Event Type']);
    $this->setTitle($event['Title']);
    $this->setBody($event['Body']);
    $this->setAddress($event['Address'], $event['City'], $event['State'], $event['Zip Code']);
    $this->setContactEmail($event['Contact Email']);
    $this->setContactName($event['Contact Name']);
    $this->setContactPhone($event['Contact Phone']);

    //Added tickets info to setup 10/20/2017 - Alexander Spaulding, Peter Olache - RT #400314
    $this->setTicketInformation($event['ticket_information']);
    $this->setPriceInformation($event['price_information']);
    $this->setTicketUrl($event['tickets_url']);

    $this->setImage($event['Image']);
    $this->setRoom($event['Room']);
    $this->setStartDate($event['Start Date']);
    $this->setEndDate($event['End Date']);

    $event = $this;
  }

  /**
   * Create event object uid only for removing cancelled events
   *
   * @param array $event
   * @return $this
   */
  private function setupUid(array &$event) {
    $this->setUid($event['UID']);

    $event = $this;
  }

  /**
   * Get the event entity or create new
   *
   * @param $uid
   * @return bool|mixed
   */
  private function getEventByUid($uid = NULL) {
    if (NULL === $uid) {
      $uid = $this->getUid();
    }

    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $query = $storage->getQuery();
    $query->condition('type', 'event')
      ->condition('status', 1)
      ->condition('field_event_uid', $uid);

    if ($result = $query->execute()) {
      $id = reset($result);
      $entity = $storage->load($id);
    }
    else {
      $entity = $storage->create([
        'langcode' => $this->langCode,
        'type' => 'event'
      ]);
    }

    return $entity;
  }

  /**
   * Get a string like this 2016:10:19:12:00:00 and return a formatted date
   * using DrupalDateTime::fromArrayToISO()
   *
   * @param $date string
   */
  private function formattedDate(&$date) {

    $stringToArray = array();

    list(
      $stringToArray['year'],
      $stringToArray['month'],
      $stringToArray['day'],
      $stringToArray['hour'],
      $stringToArray['minute'],
      $stringToArray['second']
      ) = explode(':', $date);

    $date = DrupalDateTime::arrayToISO($stringToArray);
  }
}
