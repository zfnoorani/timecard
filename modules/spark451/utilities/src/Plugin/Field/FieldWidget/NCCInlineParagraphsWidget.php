<?php

/**
 * @file
 * Paragraphs widget NCC implementation for entity reference.
 */

namespace Drupal\utilities\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\paragraphs\Plugin\Field\FieldWidget\InlineParagraphsWidget;
use Drupal\Component\Utility\Html;

/**
 * Plugin NCC implementation of the 'entity_reference paragraphs' widget.
 *
 * We hide add / remove buttons when translating to avoid accidental loss of
 * data because these actions effect all languages.
 *
 * @FieldWidget(
 *   id = "ncc_entity_reference_paragraphs",
 *   label = @Translation("NCC Paragraphs"),
 *   description = @Translation("An paragraphs inline form widget."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class NCCInlineParagraphsWidget extends InlineParagraphsWidget {

  protected $isTranslating;

  private $iconsRoot = '/modules/spark451/utilities/assets/icons';

  //Controls the icons displayed on paragraph types
  private $icons = array(
    'border-none.svg' => array('cb_table'),
    'calendar-1.svg' => array('event_feed_stack'),
    'camera-2.svg' => array('cb_image'),
    'cursor-finger.svg' => array('cb_cta_button', 'card_cta', 'horizontal_buttons_holder'),
    'cursor-touch-1.svg' => array('card_cta_photo'),
    'factoid.svg' => array('card_factoid'),
    'graduate.svg' => array('card_program'),
    'highlight_stack.svg' => array('stack_highlight'),
    'id-card-4.svg' => array('card_profile_full'),
    'id-card-double.svg' => array('card_profile_story'),
    'intro_teaser.svg' => array('cb_teaser_intro'),
    'logo-twitter-bird.svg' => array('cb_social_feed'),
    'move-up-1.svg' => array('cb_rule_horizontal'),
    'news-article.svg' => array('card_news'),
    'newspaper.svg' => array('article_feed_stack', 'external_links'),
    'picture-1.svg' => array('card_image'),
    'picture-2.svg' => array('card_gallery'),
    'profile-picture-1.svg' => array('profile_listing'),
    'quote-1.svg' => array('card_quote', 'cb_quote'),
    'rank-army-star-badge-4.svg' => array('card_feature'),
    'small-caps.svg' => array('cb_wysiwyg', 'cb_content_text'),
    'stack.svg' => array('stack_group', 'stack_blog', 'stack_department', 'stack_event', 'stack_list', 'stack_program' ),
    'star-half.svg' => array('cb_teaser_feature'),
    'teaser.svg' => array('cb_teasers'),
    'title.svg' => array('card_title'),
    'video-clip-1.svg' => array('card_video', 'cb_media_video'),
    'account-files.svg' => array('cb_program_acalog'),
    'add-tab.svg' => array('tab_section', 'field_department_section', 'field_tab_sections', 'cb_tab_section'),
    'book-open-2.svg' => array('card_story'),
    'briefcase.svg' => array('card_career'),
    'business-increase.svg' => array('event_presenter'),
    'business-strategy.svg' => array('event_organizer'),
    'expand-vertical-5.svg' => array('margins_settings', 'field_cb_bottom_margin', 'field_cb_top_margin', 'cb_spacer'),
    'id-card-3.svg' => array('program_contact'),
    'view-array.svg' => array('cb_slide'),
    'view-column.svg' => array('cb_slider')
  );

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\content_translation\Controller\ContentTranslationController::prepareTranslation()
   *   Uses a similar approach to populate a new translation.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    //
    if(!empty($element['info']['must_be_saved_info'])) {
      $element['info']['must_be_saved_info']['#markup'] = '<em>' . t('Warning: Make sure to save to keep your changes.') . '</em>';
    }
    return $element;
  }

  public function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    //
    // $elements = parent::formMultipleElements($items, $form, $form_state);

    $field_name = $this->fieldDefinition->getName();
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $parents = $form['#parents'];
    $field_state = static::getWidgetState($parents, $field_name, $form_state);

    $max = $field_state['items_count'];
    $real_item_count = $max;
    $is_multiple = $this->fieldDefinition->getFieldStorageDefinition()->isMultiple();

    $title = $this->fieldDefinition->getLabel();
    $description = FieldFilteredMarkup::create(\Drupal::token()->replace($this->fieldDefinition->getDescription()));

    $elements = array();
    $id_prefix = implode('-', array_merge($parents, array($field_name)));
    $wrapper_id = Html::getUniqueId($id_prefix . '-add-more-wrapper');
    $elements['#prefix'] = '<div id="' . $wrapper_id . '">';
    $elements['#suffix'] = '</div>';

    $field_state['ajax_wrapper_id'] = $wrapper_id;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    if ($max > 0) {
      for ($delta = 0; $delta < $max; $delta++) {

        // Add a new empty item if it doesn't exist yet at this delta.
        if (!isset($items[$delta])) {
          $items->appendItem();
        }

        // For multiple fields, title and description are handled by the wrapping
        // table.
        $element = array(
          '#title' => $is_multiple ? '' : $title,
          '#description' => $is_multiple ? '' : $description,
        );
        $element = $this->formSingleElement($items, $delta, $element, $form, $form_state);

        if ($element) {
          // Input field for the delta (drag-n-drop reordering).
          if ($is_multiple) {
            // We name the element '_weight' to avoid clashing with elements
            // defined by widget.
            $element['_weight'] = array(
              '#type' => 'weight',
              '#title' => $this->t('Weight for row @number', array('@number' => $delta + 1)),
              '#title_display' => 'invisible',
              // Note: this 'delta' is the FAPI #type 'weight' element's property.
              '#delta' => $max,
              '#default_value' => $items[$delta]->_weight ?: $delta,
              '#weight' => 100,
            );
          }

          if (isset($element['#access']) && !$element['#access']) {
            $real_item_count--;
          }
          else {
            $elements[$delta] = $element;
          }
        }
      }
    }

    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['real_item_count'] = $real_item_count;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $entity_manager = \Drupal::entityTypeManager();
    $target_type = $this->getFieldSetting('target_type');

    $bundles = $this->getAllowedTypes();
    $access_control_handler = $entity_manager->getAccessControlHandler($target_type);

    $options = array();
    $access_options = array();

    $dragdrop_settings = $this->getSelectionHandlerSetting('target_bundles_drag_drop');
    foreach ($bundles as $machine_name => $bundle) {

      if ($dragdrop_settings || (!count($this->getSelectionHandlerSetting('target_bundles'))
        || in_array($machine_name, $this->getSelectionHandlerSetting('target_bundles')))) {
        $options[$machine_name] = $bundle['label'];

        if ($access_control_handler->createAccess($machine_name)) {
          $access_options[$machine_name] = $bundle['label'];
        }
      }
    }

    if ($real_item_count > 0) {
      $elements += array(
        '#theme' => 'field_multiple_value_form',
        '#field_name' => $field_name,
        '#cardinality' => $cardinality,
        '#cardinality_multiple' => $is_multiple,
        '#required' => $this->fieldDefinition->isRequired(),
        '#title' => $title,
        '#description' => $description,
        '#max_delta' => $max-1,
      );
    }
    else {
      $elements += [
        '#type' => 'container',
        '#theme_wrappers' => ['container'],
        '#field_name' => $field_name,
        '#cardinality' => $cardinality,
        '#cardinality_multiple' => TRUE,
        '#max_delta' => $max-1,
        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'strong',
          '#value' => $title,
        ],
        'text' => [
          '#type' => 'container',
          'value' => [
            '#markup' => $this->t('No @title added yet.', ['@title' => $this->getSetting('title')]),
            '#prefix' => '<em>',
            '#suffix' => '</em>',
          ]
        ],
      ];

      if ($description) {
        $elements['description'] = [
          '#type' => 'container',
          'value' => ['#markup' => $description],
          '#attributes' => ['class' => ['description']],
        ];
      }
    }

    // Add 'add more' button, if not working with a programmed form.
    if (($real_item_count < $cardinality || $cardinality == FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) && !$form_state->isProgrammed()) {
      // Hide the button when translating.
      $host = $items->getEntity();
      $this->initIsTranslating($form_state, $host);
      $elements['add_more'] = array(
        '#type' => 'container',
        '#theme_wrappers' => array('paragraphs_dropbutton_wrapper'),
        '#access' => !$this->isTranslating,
      );

      if (count($access_options)) {
        if ($this->getSetting('add_mode') == 'button' || $this->getSetting('add_mode') == 'dropdown') {
          $drop_button = FALSE;
          // Don't wrap buttons as dropdown, just print them as regular buttons that we are gonna override below
          // if (count($access_options) > 1 && $this->getSetting('add_mode') == 'dropdown') {
          //   $drop_button = TRUE;
          //   $elements['add_more']['#theme_wrappers'] = array('dropbutton_wrapper');
          //   $elements['add_more']['prefix'] = array(
          //     '#markup' => '<ul class="dropbutton">',
          //     '#weight' => -999,
          //   );
          //   $elements['add_more']['suffix'] = array(
          //     '#markup' => '</ul>',
          //     '#weight' => 999,
          //   );
          //   $elements['add_more']['#suffix'] = $this->t(' to %type', array('%type' => $title));
          // }
          foreach ($access_options as $machine_name => $label) {

            $markup = $this->t('Add - @type', array('@type' => $label));
            if($icon = $this->getIconForField($machine_name)){
              $markup = '<span class="block ncc-paragraph-icon"><img src="' . $this->iconsRoot . '/' . $icon . '"></span>
                        <span class="block clear">' . $markup . '</span>';
            }

            $elements['add_more']['add_more_button_' . $machine_name] = array(
              '#type' => 'submit',
              '#name' => strtr($id_prefix, '-', '_') . '_' . $machine_name . '_add_more',
              '#markup' => $markup,
              '#attributes' => array('class' => array('field-add-more-submit')),
              '#limit_validation_errors' => array(array_merge($parents, array($field_name, 'add_more'))),
              '#submit' => array(array(get_class($this), 'addMoreSubmit')),
              '#ajax' => array(
                'callback' => array(get_class($this), 'addMoreAjax'),
                'wrapper' => $wrapper_id,
                'effect' => 'fade',
              ),
              '#bundle_machine_name' => $machine_name,
            );
            $elements['add_more']['add_more_button_' . $machine_name]['#prefix'] = '<div class="ncc-paragraph-item text-center">';
            $elements['add_more']['add_more_button_' . $machine_name]['#suffix'] = '</div>';
          }
        }
        else {
          $elements['add_more']['add_more_select'] = array(
            '#type'    => 'select',
            '#options' => $options,
            '#title'   => $this->t('@title type', array('@title' => $this->getSetting('title'))),
            '#label_display' => 'hidden',
          );

          $text = $this->t('Add @title', array('@title' => $this->getSetting('title')));

          if ($real_item_count > 0) {
            $text = $this->t('Add another @title', array('@title' => $this->getSetting('title')));
          }

          $elements['add_more']['add_more_button'] = array(
            '#type' => 'submit',
            '#name' => strtr($id_prefix, '-', '_') . '_add_more',
            '#value' => $text,
            '#attributes' => array('class' => array('field-add-more-submit')),
            '#limit_validation_errors' => array(array_merge($parents, array($field_name, 'add_more'))),
            '#submit' => array(array(get_class($this), 'addMoreSubmit')),
            '#ajax' => array(
              'callback' => array(get_class($this), 'addMoreAjax'),
              'wrapper' => $wrapper_id,
              'effect' => 'fade',
            ),
          );
          $elements['add_more']['add_more_button']['#suffix'] = $this->t(' to %type', array('%type' => $title));
        }
      }
      else {
        if (count($options)) {
          $elements['add_more']['info'] = array(
            '#type' => 'markup',
            '#markup' => '<em>' . $this->t('You are not allowed to add any of the @title types.', array('@title' => $this->getSetting('title'))) . '</em>',
          );
        }
        else {
          $elements['add_more']['info'] = array(
            '#type' => 'markup',
            '#markup' => '<em>' . $this->t('You did not add any @title types yet.', array('@title' => $this->getSetting('title'))) . '</em>',
          );
        }
      }
    }

    $elements['#attached']['library'][] = 'paragraphs/drupal.paragraphs.admin';
    $elements['#attached']['library'][] = 'utilities/ncc-paragraphs';

    return $elements;
  }

  private function getIconForField(String $field_name){

    $icon = NULL;

    $cid = implode(':', [__METHOD__, $field_name]);
    if ($cache = \Drupal::cache()->get($cid)) {
      $icon = $cache->data;
    }else{
      foreach ($this->icons as $key => $value) {
        if(in_array($field_name, $value)){
          $icon = $key;
          \Drupal::cache()->set($cid, $icon);
          break;
        }
      }
    }

    return $icon;
  }
}
