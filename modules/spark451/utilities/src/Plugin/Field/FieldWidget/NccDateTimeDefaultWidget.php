<?php

namespace Drupal\utilities\Plugin\Field\FieldWidget;

use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeWidgetBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Plugin NCC implementation of the 'datetime_default' widget.
 *
 * @FieldWidget(
 *   id = "ncc_datetime_default",
 *   label = @Translation("NCC Date and time"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class NccDateTimeDefaultWidget extends DateTimeWidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The date format storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityStorageInterface $date_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->dateStorage = $date_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity.manager')->getStorage('date_format')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Identify the type of date and time elements to use.
    switch ($this->getFieldSetting('datetime_type')) {
      case DateTimeItem::DATETIME_TYPE_DATE:
        $date_type = 'text';
        $time_type = 'none';
        $date_format = $this->getSetting('date_format');
        $time_format = '';
        break;

      default:
        $date_type = 'date';
        $time_type = 'time';
        $date_format = $this->getSetting('date_format');
        $time_format = $this->getSetting('time_format');
        break;
    }

    $element['value'] += array(
      '#date_date_format' => $date_format,
      '#date_date_element' => $date_type,
      '#date_date_callbacks' => array(),
      '#date_time_format' => $time_format,
      '#date_time_element' => $time_type,
      '#date_time_callbacks' => array()
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'date_format' => 'm/d/Y',
      'time_format' => 'H:i:s'
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['date_format'] = array(
      '#type' => 'textfield',
      '#title' => t('Date format'),
      '#default_value' => $this->getSetting('date_format'),
      '#description' => t("Check for proper date format <a target='_blank' href='http://php.net/manual/en/function.date.php'>here</a>")
    );

    if ($this->getFieldSetting('datetime_type') == 'datetime') {
      $element['time_format'] = array(
        '#type' => 'textfield',
        '#title' => t('Time format'),
        '#default_value' => $this->getSetting('time_format'),
        '#description' => t("Check for proper time format <a target='_blank' href='http://php.net/manual/en/function.date.php'>here</a>")
      );
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();

    $summary[] = t('Date format: @date_format', array('@date_format' => $this->getSetting('date_format')));
    if ($this->getFieldSetting('datetime_type') == 'datetime') {
      $summary[] = t('Time format: @time_format', array('@time_format' => $this->getSetting('time_format')));
    }
    return $summary;
  }

}
