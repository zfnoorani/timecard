<?php

namespace Drupal\utilities\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation for custom 'link' widget.
 *
 * @FieldWidget(
 *   id = "ncc_link",
 *   label = @Translation("NCC Link"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class NCCLinkWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['title']['#weight'] = -10;
    $element['title']['#title'] = t('BUTTON TEXT');
    $element['title']['#description'] = t('Word or phrase saying what the button does or the content is about. E.g. Contact Admissions, Faculty Profile, Study Abroad');
    unset($element['uri']['#description']);
    return $element;
  }
}