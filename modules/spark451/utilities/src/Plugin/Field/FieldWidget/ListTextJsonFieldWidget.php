<?php

namespace Drupal\utilities\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'list_text_json_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "list_text_json_field_widget",
 *   label = @Translation("List (Text) from JSON Widget"),
 *   field_types = {
 *     "list_string_json"
 *   }
 * )
 */
class ListTextJsonFieldWidget extends OptionsSelectWidget {

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Attach library
    $form['#attached']['library'][] = 'utilities/icon-drop-down';

    return parent::formElement($items, $delta, $element, $form, $form_state);
  }

  public static function validateElement(array $element, FormStateInterface $form_state) {
    // Execute parent validation.
    parent::validateElement($element, $form_state);
    // If value is different to none, then apply it.
    if($element['#value'] != '_none') {
      // Setup value to current element.
      $form_state->setValueForElement($element, array($element['#key_column'] => $element['#value']));
    }
  }

}
