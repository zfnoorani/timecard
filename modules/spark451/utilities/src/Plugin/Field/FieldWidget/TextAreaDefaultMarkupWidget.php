<?php

/**
 * @file
 * Contains \Drupal\utilities\Plugin\Field\FieldWidget\TextAreaDefaultMarkupWidget.
 */

namespace Drupal\utilities\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'text_area_default_markup_widget' widget.
 *
 * @FieldWidget(
 *   id = "text_area_default_markup_widget",
 *   label = @Translation("Text area (Default markup)"),
 *   field_types = {
 *     "text_long"
 *   }
 * )
 */
class TextAreaDefaultMarkupWidget extends TextareaWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // In order to get the $route you probably should use the $route_match
    $route = \Drupal::routeMatch()->getRouteObject();
    $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);

    if($is_admin) {
      return parent::formElement($items, $delta, $element, $form, $form_state);
    }

    // Append all items.
    $elements = [];
    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()
    foreach ($items as $delta => $item) {
      $elements[$delta] = array(
        '#type' => 'processed_text',
        '#text' => $item->value,
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      );
    }

    return $elements;
  }

}
