<?php

namespace Drupal\utilities\Plugin\Field\FieldType;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;
use GuzzleHttp\Exception\RequestException;

/**
 * Plugin implementation of the 'list_string_json' field type.
 *
 * @FieldType(
 *   id = "list_string_json",
 *   label = @Translation("List (text) from JSON"),
 *   description = @Translation("List (Text) based on values from JSON resource"),
 *   category = @Translation("Text"),
 *   default_widget = "list_text_json_field_widget",
 *   default_formatter = "list_default"
 * )
 */
class ListStringJsonItem extends ListStringItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'json_url' => '',
      'allowed_values_function' => 'utilities_callback_list_string_json_item',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['json_url'] = [
      '#type' => 'textfield',
      '#title' => t('JSON URL'),
      '#default_value' => $this->getSetting('json_url'),
      '#required' => TRUE,
      '#description' => t('An absolute valid URL to get JSON Resource.'),
    ];

    return $element;
  }

  /**
   * Callback function to retrieve allowed values.
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
   *   Field definition.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|NULL $entity
   *   Entity.
   * @param boolean $cacheable
   *   Flag to know if it is cacheable.
   *
   * @return array
   *   Values allowed to use.
   */
  public static function callbackAllowedValues(FieldStorageDefinitionInterface $definition, FieldableEntityInterface $entity = NULL, $cacheable) {
    //
    $values = [];
    //
    if ($cacheable) {
      //
      $cid = __METHOD__ . $definition->uuid();
      $data = NULL;
      if ($cache = \Drupal::cache()->get($cid)) {
        $data = $cache->data;
      }
      else {
        $data = self::callbackAllowedValues($definition, $entity, FALSE);
        \Drupal::cache()->set($cid, $data);
      }
      return $data;
    }
    //
    try {
      //
      $json_url = $definition->getSetting('json_url');
      if(strpos($json_url, '/') === 0) {
        $json_url = \Drupal::request()->getSchemeAndHttpHost() . $json_url;
      }
      //
      $response = \Drupal::httpClient()
        ->get($json_url);
      //
      $data = $response->getBody()->getContents();
      $values = \GuzzleHttp\json_decode($data, TRUE);
    } catch (RequestException $e) {
      watchdog_exception(__FUNCTION__, $e);
    }
    //
    return array_reduce($values, function ($icons, $pair) {
      //
      $value = reset($pair);
      //
      $icons[$value] = $value;
      return $icons;
    }, []);
  }
}
