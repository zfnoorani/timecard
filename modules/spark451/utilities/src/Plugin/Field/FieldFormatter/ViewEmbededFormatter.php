<?php

namespace Drupal\utilities\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'view_embeded_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "view_embeded_formatter",
 *   label = @Translation("View embeded formatter"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ViewEmbededFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      // Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(
      // Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    //
    foreach ($items as $delta => $item) {
      //
      if($item->entity->field_view_content_related->count()) {
        //
        $elements[$delta] = [
          '#type' => 'view',
          '#name' => $item->entity->field_view_content_related->first()->getValue()['target_id'],
          '#display_id' => 'block',
          '#arguments' => [],
        ];
      }
    }

    return $elements;
  }

}
