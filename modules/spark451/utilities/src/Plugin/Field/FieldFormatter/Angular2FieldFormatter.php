<?php

namespace Drupal\utilities\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'angular2field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "angular2field_formatter",
 *   label = @Translation("Angular2 Component"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class Angular2FieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'components_settings' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [
      '#type' => 'details',
      '#title' => 'Component Settings',
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $options = array_reduce(\Drupal::service('pdb.component_discovery')
      ->getComponents(), function ($options, $component) {
      if ('ng2' == $component->info['presentation'] &&
        array_key_exists('entity_display', $component->info) &&
        'field_formatter' == $component->info['entity_display']
      ) {
        $options[$component->info['machine_name']] = $component->info['name'];
      }
      return $options;
    });
    $elements['components_settings'] = [
      '#type' => 'radios',
      '#title' => 'Angular2 Component',
      '#default_value' => $this->getSetting('components_settings'),
      '#options' => $options,
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $message = t('Undefined Angular2 Component.');
    if ($components_settings = $this->getSetting('components_settings')) {
      $component = \Drupal::service('ng2_entity.ng2_view_display')
        ->getComponentByMachineName($components_settings);
      $message = t('Rendered by @component component', ['@component' => $component['name']]);
    }
    $summary[] = $message;
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    //
    if ($machineName = $this->getSetting('components_settings')) {
      //
      $components = array_map(function ($item) {
        return $item->view();
      }, $items->getIterator()->getArrayCopy());
      //
      $elements = [
        '#theme' => 'angular2_stack',
        '#component' => \Drupal::service('ng2_entity.ng2_view_display')
          ->getComponentByMachineName($machineName),
        '#ng_content' => \Drupal::service('renderer')->render($components),
      ];
    }
    return $elements;
  }
}
