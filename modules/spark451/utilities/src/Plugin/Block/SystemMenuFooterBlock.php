<?php

namespace Drupal\utilities\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Plugin\Block\SystemMenuBlock;

/**
 * Provides a 'SystemMenuFooterBlock' block.
 *
 * @Block(
 *  id = "system_menu_footer_block",
 *  admin_label = @Translation("System menu footer block"),
 * )
 */
class SystemMenuFooterBlock extends SystemMenuBlock {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Execute parent implementation.
    $form = parent::blockForm($form, $form_state);
    // Disable inputs to avoid modifications
    $form['menu_levels']['level']['#disabled'] = TRUE;
    $form['menu_levels']['depth']['#disabled'] = TRUE;

    // Retrieve configuration.
    $config = $this->configuration;
    
    // Define max links per group to show.
    $form['menu_levels']['max_links'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum links per group'),
      '#description' => $this->t('The maximum number of links to show on each group.'),
      '#default_value' => $config['max_links'],
      '#min' => 1,
      '#max' => 5,
      '#required' => TRUE,
    ];

    // Define max links to show.
    $form['menu_levels']['max_groups'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum groups'),
      '#description' => $this->t('The maximum number of group allowed to show on footer.'),
      '#default_value' => $config['max_groups'],
      '#min' => 1,
      '#max' => 3,
      '#required' => TRUE,
    ];

    // Define address group elements.
    $form['footer_address'] = [
      '#type' => 'details',
      '#title' => $this->t('Address'),
      '#open' => TRUE,
      '#process' => [[get_class(), 'processMenuLevelParents']],
    ];
    // Define Address field.
    $form['footer_address']['address'] = [
      '#type' => 'text_format',
      '#base_type' => 'textfield',
      '#title' => $this->t('Address'),
      '#default_value' => $config['address']['value'],
      '#format' => $config['address']['format'],
      '#required' => TRUE,
    ];
    // Define phone field.
    $form['footer_address']['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#default_value' => $config['phone'],
      '#required' => TRUE,
    ];
    // Define email field.
    $form['footer_address']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => $config['email'],
      '#required' => FALSE,
    ];

    // Define social network group elements.
    $form['social_networks'] = [
      '#type' => 'details',
      '#title' => $this->t('Social networks'),
      '#open' => TRUE,
    ];

    // Retrieve defaults.
    $defaults = $this->defaultConfiguration();
    // Exposes social networks availables.
    foreach ($defaults['social_networks'] as $key => $social_network) {
      // Define current social media value.
      $default_value = ($config['social_networks'][$key] != $defaults['social_networks'][$key]) ?
        $config['social_networks'][$key] : '';

      // Define social network URL.
      $form['social_networks'][$key] = [
        '#type' => 'url',
        '#title' => $this->t($social_network),
        '#default_value' => $default_value,
        '#description' => $this->t('Valid URL to @s page', ['@s' => $social_network]),
      ];
    }

    $form['copyright'] = [
      '#type' => 'details',
      '#title' => $this->t('Copyright'),
      '#open' => TRUE,
    ];

    $form['copyright']['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Copyright text'),
      '#default_value' => $config['copyright']['text'],
      '#description' => $this->t('Text for copyright link'),
      '#required' => FALSE,
    ];

    $form['copyright']['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Copyright link'),
      '#default_value' => $config['copyright']['link'],
      '#description' => $this->t('Absolute or relative link to copyright page.'),
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Call parent implementation.
    parent::blockSubmit($form, $form_state);
    // Setup new config based on submission values.
    $this->configuration['max_links'] = $form_state->getValue('max_links');
    $this->configuration['max_groups'] = $form_state->getValue('max_groups');
    $this->configuration['address'] = $form_state->getValue('address');
    $this->configuration['phone'] = $form_state->getValue('phone');
    $this->configuration['email'] = $form_state->getValue('email');
    $this->configuration['social_networks'] = $form_state->getValue('social_networks');
    $this->configuration['copyright'] = $form_state->getValue('copyright');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // Retrieve parent config.
    $config = parent::defaultConfiguration();
    // Setup default configuration.
    $config['max_links'] = 5;
    $config['max_groups'] = 3;
    $config['address'] = "30 North Brainard Street<br />Naperville, IL 60540";
    $config['phone'] = '630.637.5100';
    $config['email'] = 'info@noctrl.edu';
    $config['social_networks'] = [
      'facebook' => 'Facebook',
      'twitter-bird' => 'Twitter',
      'youtube-clip' => 'Youtube',
      'instagram' => 'Instagram',
    ];
    $config['copyright'] = [
      'text' => '2016 North Central College',
      'link' => '/copyright'
    ];
    return $config;
  }
}
