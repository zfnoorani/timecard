<?php

/**
 * Modifyed the class name to align with cancelled events
 * Modifyed proccessItem() to call deleteEvent()
 * RT: #411249
 * maholmes, jdust 31 May 2017
 */

namespace Drupal\utilities\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\utilities\Entity;
use Drupal\utilities\Entity\Events;

/**
 * Removes a feed's items.
 *
 * @QueueWorker(
 *   id = "cron_cancelled_event_feeds",
 *   title = @Translation("Cancelled Event feeds"),
 *   cron = {"time" = 30}
 * )
 *
 */
class CronCancelledEventFeeds extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
	$entity = new Events();
	$entity->deleteEvent($data);
  }

}
