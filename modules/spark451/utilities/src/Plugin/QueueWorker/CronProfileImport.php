<?php
/**
 * Created by PhpStorm.
 * User: gustavorf
 * Date: 1/4/17
 * Time: 10:30 AM
 */

namespace Drupal\utilities\Plugin\QueueWorker;


use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\utilities\Entity\Events;
use Drupal\utilities\Entity\UserProfileData;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "cron_profile_import",
 *   title = @Translation("Profile Import"),
 *   cron = {"time" = 30}
 * )
 *
 */
class CronProfileImport extends QueueWorkerBase {
  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $entity = new UserProfileData();
    $entity->save($data);
  }
}

