<?php

namespace Drupal\utilities\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\utilities\Entity\Events;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "cron_event_feeds",
 *   title = @Translation("Event feeds"),
 *   cron = {"time" = 30}
 * )
 *
 */
class CronEventFeeds extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $entity = new Events();
    $entity->save($data);
  }
}
