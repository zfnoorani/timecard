<?php

namespace Drupal\utilities\Plugin\views\row;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\search_api\Plugin\views\row\SearchApiRow;
use Drupal\search_api\SearchApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a row plugin for displaying a result as a rendered item.
 *
 * @ViewsRow(
 *   id = "ncc_search_api",
 *   title = @Translation("NCC Search Rendered entity"),
 *   help = @Translation("Displays entity using view mode of the matching search API item"),
 * )
 *
 */
class NCCSearchApiRow extends SearchApiRow {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $row */
    $row = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $row->setRenderer($container->get('renderer'));
    return $row;
  }

  /**
   * Define renderer attribute.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function setRenderer(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    $datasource_id = $row->search_api_datasource;

    if (!($row->_object instanceof ComplexDataInterface)) {
      $context = array(
        '%item_id' => $row->search_api_id,
        '%view' => $this->view->storage->label(),
      );
      $this->getLogger()->warning('Failed to load item %item_id in view %view.', $context);
      return '';
    }

    if (!$this->index->isValidDatasource($datasource_id)) {
      $context = array(
        '%datasource' => $datasource_id,
        '%view' => $this->view->storage->label(),
      );
      $this->getLogger()->warning('Item of unknown datasource %datasource returned in view %view.', $context);
      return '';
    }
    // Always use the default view mode if it was not set explicitly in the
    // options.
    $view_mode = 'default';
    $bundle = $this->index->getDatasource($datasource_id)->getItemBundle($row->_object);
    if (isset($this->options['view_modes'][$datasource_id][$bundle])) {
      $view_mode = $this->options['view_modes'][$datasource_id][$bundle];
    }

    try {
      $view = $this->index->getDatasource($datasource_id)->viewItem($row->_object, $view_mode);
      return ['item' => $this->renderer->render($view)];
    }
    catch (SearchApiException $e) {
      watchdog_exception('search_api', $e);
      return '';
    }
  }
}
