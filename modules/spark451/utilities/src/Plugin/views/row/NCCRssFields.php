<?php

namespace Drupal\utilities\Plugin\views\row;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\row\RowPluginBase;

/**
 * Provides a row plugin for displaying a result as a rendered item.
 *
 * @ViewsRow(
 *   id = "ncc_rss_fields",
 *   title = @Translation("NCC RSS Fields"),
 *   help = @Translation("Displays entity using view mode"),
 *   theme = "ncc_views_view_row_rss",
 *   display_types = {"feed"}
 * )
 *
 */
class NCCRssFields extends RowPluginBase {

  /**
   * @var bool
   */
  protected $usesFields = TRUE;

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['nid_field'] = array('default' => '');
    $options['title_field'] = array('default' => '');
    $options['link_field'] = array('default' => '');
    $options['description_field'] = array('default' => '');
    $options['date_field'] = array('default' => '');
    $options['image_field'] = array('default' => '');
    $options['tags_field'] = array('default' => '');
    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $initial_labels = array('' => $this->t('- None -'));
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);

    $form['nid_field'] = array(
      '#type' => 'select',
      '#title' => $this->t('NID field'),
      '#description' => $this->t('The field that is going to be used as the RSS item NID for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['nid_field'],
      '#required' => TRUE,
    );
    $form['title_field'] = array(
      '#type' => 'select',
      '#title' => $this->t('Title field'),
      '#description' => $this->t('The field that is going to be used as the RSS item title for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['title_field'],
      '#required' => TRUE,
    );
    $form['link_field'] = array(
      '#type' => 'select',
      '#title' => $this->t('Link field'),
      '#description' => $this->t('The field that is going to be used as the RSS item link for each row. This must be a drupal relative path.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['link_field'],
      '#required' => TRUE,
    );
    $form['description_field'] = array(
      '#type' => 'select',
      '#title' => $this->t('Description field'),
      '#description' => $this->t('The field that is going to be used as the RSS item description for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['description_field'],
      '#required' => TRUE,
    );
    $form['date_field'] = array(
      '#type' => 'select',
      '#title' => $this->t('Publication date field'),
      '#description' => $this->t('The field that is going to be used as the RSS item pubDate for each row. It needs to be in RFC 2822 format.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['date_field'],
      '#required' => TRUE,
    );
    $form['image_field'] = array(
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#description' => $this->t('The field that is going to be used as the RSS item image for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['image_field'],
      '#required' => FALSE,
    );
    $form['tags_field'] = array(
      '#type' => 'select',
      '#title' => $this->t('Tags field'),
      '#description' => $this->t('The field that is going to be used as the RSS item tags for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['tags_field'],
      '#required' => FALSE,
    );
  }

  public function validate() {
    $errors = parent::validate();
    $required_options = array('nid_field', 'title_field', 'link_field', 'description_field', 'date_field');
    foreach ($required_options as $required_option) {
      if (empty($this->options[$required_option])) {
        $errors[] = $this->t('NCC Row style plugin requires specifying which views fields to use for RSS item.');
        break;
      }
    }
    return $errors;
  }

  public function render($row) {
    static $row_index;
    if (!isset($row_index)) {
      $row_index = 0;
    }
    if (function_exists('rdf_get_namespaces')) {
      // Merge RDF namespaces in the XML namespaces in case they are used
      // further in the RSS content.
      $xml_rdf_namespaces = array();
      foreach (rdf_get_namespaces() as $prefix => $uri) {
        $xml_rdf_namespaces['xmlns:' . $prefix] = $uri;
      }
      $this->view->style_plugin->namespaces += $xml_rdf_namespaces;
    }

    // Create the RSS item object.
    $item = new \stdClass();
    $item->nid = $this->getField($row_index, $this->options['nid_field']);
    $item->publication_date = $this->getField($row_index, $this->options['date_field']);
    $item->title = $this->getField($row_index, $this->options['title_field']);

    $field = $this->getField($row_index, $this->options['description_field']);
    $item->description = is_array($field) ? $field : ['#markup' => $field];

    $item->image = $this->getField($row_index, $this->options['image_field']);

    // @todo Views should expect and store a leading /. See:
    //   https://www.drupal.org/node/2423913
    $item->link = Url::fromUserInput('/' . $this->getField($row_index, $this->options['link_field']))->setAbsolute()->toString();

    $item->tags = $this->getField($row_index, $this->options['tags_field']);

    $row_index++;

    return array(
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#row' => $item,
      '#field_alias' => isset($this->field_alias) ? $this->field_alias : '',
    );
  }

  public function getField($index, $field_id) {
    if (empty($this->view->style_plugin) || !is_object($this->view->style_plugin) || empty($field_id)) {
      return '';
    }
    return $this->view->style_plugin->getField($index, $field_id);
  }
}
