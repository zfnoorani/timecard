<?php

namespace Drupal\utilities\Plugin\views\row;

use Drupal\views\Plugin\views\row\RowPluginBase;


/**
 * Provides a row plugin for displaying a result as a rendered item.
 *
 * @ViewsRow(
 *   id = "ncc_registration_export",
 *   title = @Translation("NCC Registration Export Rendered entity"),
 *   help = @Translation("Displays entity using view mode"),
 * )
 *
 */
class NCCRegistrationRow  extends RowPluginBase  {

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    // Get the entity of the row
    $entity_manager = \Drupal::entityTypeManager();
    // get the registrant
    $registrant = $entity_manager->getStorage('registration')->load($row->id);
    $row_values = [];
    // Iterate for all entity field
    foreach ($registrant->getFields() as $field) {
      // get the name with the name with word field
      if (stripos($field->getName(), "field") !== false) {
        // Get the label of the field
        $field_label = $field->getFieldDefinition()->get('label');

        if ($field_label) {
          // build the new array of data
          $row_values[$field_label] = $field->getValue();
        }
      }
    }
    return $row_values;
  }

}
