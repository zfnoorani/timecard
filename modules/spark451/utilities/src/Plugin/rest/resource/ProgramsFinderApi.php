<?php

namespace Drupal\utilities\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "utilities_programs_api",
 *   label = @Translation("Programs Finder Api"),
 *   uri_paths = {
 *     "canonical" = "/api/programs",
 *   }
 * )
 */
class ProgramsFinderApi extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;


  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('utilities'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    // Retrieve results from Cache API.
    $cid = __METHOD__;
    if ($cache = \Drupal::cache()->get($cid)) {
      $result = $cache->data;
    }
    else {
      $result = [
        'categories' => $this->getCategories(),
        'programs' => $this->getPrograms(),
      ];
      \Drupal::cache()->set($cid, $result, (60 * 60));
    }
    return new JsonResponse($result);
  }

  /**
   * Get and format the terms for api
   * @param $vocabulary
   * @return array
   */
  public function getTermsByVocabulary($vocabulary) {
    $tree = array();
    $vocabularies = \Drupal\taxonomy\Entity\Vocabulary::loadMultiple($vocabulary);

    foreach ($vocabularies as $vocabulary) {
      $vid = $vocabulary->get('vid');
      // Load all terms of the vocabulary
      $terms = \Drupal::service('entity_type.manager')
        ->getStorage("taxonomy_term")
        ->loadTree($vid, $parent = 0, $max_depth = NULL,
          $load_entities = FALSE);

      foreach ($terms as &$term) {

        $color = '#b20818';
        if ($term->vid === 'areas_of_study') {
          $taxonomy = Term::load($term->tid);
          $color = $taxonomy->get('field_color')->getValue();
          if (!empty($color[0])) {
            $color = $taxonomy->get('field_color')->getValue()[0]['value'];
          }
          else {
            $color = '#b20818';
          }
        }

        $term = array(
          'tid' => $term->tid,
          'filter' => $term->vid,
          'title' => $term->name,
          'color' => $color
        );
      }

      $tree[] = array(
        'name' => $vocabulary->get('name'),
        'choices' => $terms
      );
    }

    return $tree;
  }

  /**
   * Get Filters by node
   * @param $node
   * @return array
   */
  public function getFiltersByNode($node) {
    $filters_list = array();

    // Get the values of all filters in programs
    $filters_program_type = $node->field_program_type->getValue();
    $filters_majors = $node->field_majors_minors->getValue();
    $filters_schools = $node->field_schools->getValue();
    $filters_department = $node->field_academic_department->getValue();
    $filters_areas_study = $node->field_program_categories->getValue();
    $filters_careers = $node->field_academic_careers->getValue();
    $filters_graduate = $node->field_graduate_type->getValue();
    $filters_features = $node->field_features->getValue();
    $filters_experiential_undergraduate = $node->field_experiential_undergraduate->getValue();
    $filters_international_studies = $node->field_international_studies_type->getValue();

    // Merge all filters in one array
    $filters = array_merge(
      $filters_features,
      $filters_graduate,
      $filters_majors,
      $filters_careers,
      $filters_schools,
      $filters_department,
      $filters_areas_study,
      $filters_program_type,
      $filters_experiential_undergraduate,
      $filters_international_studies
    );

    // clean the array with only ids
    $filters = $this->setFormatProgramFilters($filters);
    // Load all terms by ids
    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($filters);

    foreach ($terms as &$term) {
      $color = '#b20818';
      $level = '';
      $vid = $term->get('vid')->getValue()[0]['target_id'];
      $taxonomy = taxonomy_term_load($term->tid->value);
      if ($vid === 'areas_of_study') {
        $color = $taxonomy->get('field_color')->getValue();
        if (!empty($color[0])) {
          $color = $taxonomy->get('field_color')->getValue()[0]['value'];
        }
        else {
          $color = '#b20818';
        }
      }

      if (isset($taxonomy->field_abbreviation)) {
        $level = $taxonomy->get('field_abbreviation')->getValue();

        if (!empty($level)) {
          $level = $taxonomy->get('field_abbreviation')->getValue()[0]['value'];
        }
      }

      // Create the format to return
      $filters_list[] = array(
        'tid' => $term->tid->value,
        'filter' => $vid,
        'title' => $term->name->value,
        'color' => $color,
        'level' => $level
      );
    }

    return $filters_list;
  }

  /**
   * Make the format for the filters. Delete all levels nad return the tid
   * @param $filters
   * @return array
   */
  public function setFormatProgramFilters($filters) {
    $list = array();
    foreach ($filters as $filter) {
      $list[] = $filter['target_id'];
    }
    return $list;

  }

  /**
   * Get all categories with format
   * @return array
   */
  public function getCategories() {
    //Majors/Minors
    $vacabulary_majors_ids = array(
      'majors_minor_type',
      'areas_of_study',
      'careers',
      'features',
    );
    $vocabulary_majors = $this->getTermsByVocabulary($vacabulary_majors_ids);

    // Experimental - Undergraduate
    $vacabulary_experimental_ids = array(
      'experiential_undergraduate_type',
    );
    $vocabulary_experimental = $this->getTermsByVocabulary($vacabulary_experimental_ids);

    // Graduate
    $vacabulary_graduate_ids = array(
      'graduate_type',
    );
    $vocabulary_graduate = $this->getTermsByVocabulary($vacabulary_graduate_ids);

    // International Studies
    $vacabulary_international_studies_ids = array(
      'international_studies_type',
    );
    $vocabulary_international_studies = $this->getTermsByVocabulary($vacabulary_international_studies_ids);


    // TODO: Remove code commented.
    // International Studies
//    $vacabulary_non_degree_ids = array(
//      'non_degree_type',
//    );
//    $vocabulary_non_degree = $this->getTermsByVocabulary($vacabulary_non_degree_ids);

    $categories = array(
      [
        'name' => $this->t('Program Types'),
        'filters' => $this->getTermsByVocabulary(['program_type']),
      ],
      array(
        'name' => 'Majors/Minors',
        'filters' => $vocabulary_majors,
      ),
      array(
        'name' => 'Experiential - Undergraduate',
        'filters' => $vocabulary_experimental,
      ),
      array(
        'name' => 'Graduate',
        'filters' => $vocabulary_graduate,
      ),
      array(
        'name' => 'International Studies',
        'filters' => $vocabulary_international_studies,
      ),
      // TODO: Remove code commented.
//      array(
//        'name' => 'Non-Degree',
//        'filters' => $vocabulary_non_degree,
//      ),
    );

    return $categories;

  }

  /**
   * Get all programs with format
   * @return array
   */
  public function getPrograms() {
    $programs = array();

    // Get all programs
    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'program')
      ->condition('status', 1)
      ->sort('title','ASC')
      ->execute();
    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($nids);

    $color = '#b20818';

    foreach ($nodes as $node) {
      $filters = $this->getFiltersByNode($node);
      $level = '';
      $count = 0;
      foreach ($filters as $filter) {
        if ($filter['filter'] === 'areas_of_study') {
          $color = $filter['color'];
          break;
        }
        if (!empty($filter['level'])) {
          if ($count > 0) {
            $level = $level . ', ' . $filter['level'];
          }
          else {
            $level = $level . $filter['level'];
          }
          $count++;
        }
      }

      // Create the format
      $programs[] = array(
        'nid' => $node->get('nid')->value,
        'title' => $node->get('title')->value,
        'url' => \Drupal::service('path.alias_manager')
          ->getAliasByPath('/node/' . $node->get('nid')->value),
        'level' => $level,
        'summary' => $node->get('field_program_summary')->value,
        'stack' => $this->getNccCard($node),
        'color' => $color,
        'filters' => $this->getFiltersByNode($node),
      );

    }

    return $programs;

  }

  /**
   * Get the NCCCard Info
   * @param $node
   * @return string
   */
  function getNccCard($node) {
    $url = \Drupal::service('path.alias_manager')
      ->getAliasByPath('/node/' . $node->get('nid')->value);

    $viewDisplayId = sprintf('%s.%s.%s',
      $node->getEntityTypeId(),
      $node->getType(),
      'default');
    // Load entity view display.
    $entityViewDisplay = \Drupal::service('entity_type.manager')
      ->getStorage('entity_view_display')
      ->load($viewDisplayId);
    // Setup field name.
    $field = 'field_program_stack';
    // Load field's component.
    $component = $entityViewDisplay->getComponent($field);

    $field_stack = $node->field_program_stack->view($component);

    $cards_stack = (string) $field_stack['#ng_content'];

    $cards_stack = str_replace('[isActive]="true"', '[isActive]="false"', $cards_stack);
    $cards_stack = str_replace('[isPeekAdjacent]="true"', '[isPeekAdjacent]="false"', $cards_stack);
    $cards_stack = $this->str_replace_limit('[isPeekAdjacent]="false"', '[isPeekAdjacent]="true"', $cards_stack);

    $card = '<ncc-card [isActive]="true" [isPeekAdjacent]="false" #card' . $node->nid->value . '>

                <ncc-card-program
                  [cardState]="card' . $node->nid->value . '.cardState"
                  title="' . htmlspecialchars($node->title->value) . '"
                  description= "' . htmlspecialchars($node->field_program_summary->value). '"
                  linkUrl="' . $url . '">
                </ncc-card-program>
            </ncc-card>';
    $card = $card . $cards_stack;

    $cards = '<ncc-stack>' . $card . '</ncc-stack>';

    return $cards;
  }

  /**
   * Replace string just one
   * @param $search
   * @param $replace
   * @param $string
   * @param int $limit
   * @return mixed
   */
  function str_replace_limit($search, $replace, $string, $limit = 1) {
    $pos = strpos($string, $search);

    if ($pos === FALSE) {
      return $string;
    }
    $searchLen = strlen($search);
    for ($i = 0; $i < $limit; $i++) {
      $string = substr_replace($string, $replace, $pos, $searchLen);
      $pos = strpos($string, $search);
      if ($pos === FALSE) {
        break;
      }
    }
    return $string;

  }


}
