<?php

namespace Drupal\utilities\Api;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Yaml\Exception\RuntimeException;

/**
 * Class ProgramAcalog
 * @package Drupal\utilities\Api
 */
class ProgramAcalog {

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Symfony\Component\Serializer\Serializer definition.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;


  protected $xmlResponseContent;



  /**
   * Spark451ProgramAcalog constructor.
   * @param \GuzzleHttp\Client $http_client
   * @param \Symfony\Component\Serializer\Serializer $serializer
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   */
  public function __construct(Client $http_client, Serializer $serializer, ConfigFactory $config_factory) {
    $this->httpClient = $http_client;
    $this->serializer = $serializer;
    $this->config = $config_factory->get('utilities.ProgramAcalogSettings');
  }

  /**
   * Consume endpoint to retrieve resource based on ID.
   *
   * @param $catalog_id int Catalog ID.
   * @param $program_id int Program ID.
   * @return array|null
   *   Full Resource from service.
   */
  public function getResourceByIDs($catalog_id, $program_id, $type = 'programs') {
    // Build cache id.

    $cid = implode(':', [__METHOD__, $catalog_id, $program_id, $type]);
    // Retrieve data from cache or consuming service.
    $resource = NULL;
    if ($cache = \Drupal::cache()->get($cid)) {
      $resource = $cache->data;
    }
    else {
      // Build query string.
      $query = [
        'format' => 'xml',
        'key' => $this->config->get('api_key'),
        'catalog' => $catalog_id,
        'method' => 'getItems',
        'type' => $type,
        'ids' => [
          $program_id,
        ],
      ];
        
      // Define endpoint URL.
        $url = $this->config->get('api_url') . '?' . http_build_query($query);

        $xmlResponseUrl = $url;
        try {
        // Consume service.getResourceByIDs
        $response = $this->httpClient->get($url);
        if ($response->getStatusCode() == 200) {
          // Retrieve XML response content.
          $resource = $response->getBody()->getContents();
          // Add to cache.
          \Drupal::cache()->set($cid, $resource);
        }
        else {
          throw New RuntimeException('Response status code: ' . $response->getStatusCode());
        }
      } catch (\RuntimeException $e) {
        // Show error message and log error message.
        watchdog_exception(__METHOD__, $e);
        drupal_set_message(New TranslatableMarkup('Program Acalog Service failed.'), 'error');
      }
    }
    // Create resource based on XML data.
    return $resource;
  }

  //
  protected function parseCoreChildren($core_children, $catalog, $xmlChildren) {
    //
    if (empty($core_children)) {
      return [];
    }
    if (!empty($core_children['@id'])) {
      $core_children = [$core_children];
    }
    //
    $children = [];
    foreach ($core_children as $key => $core_child) {
      //
      $xmlChild = $xmlChildren[$key];
      $coursesList = XMLAcalogParser::getCoursesAsList($xmlChild);
      //
      $child = [
        'name' => XMLAcalogParser::getXMLContent($xmlChild, 'a:title', FALSE),
        'description' => XMLAcalogParser::getXMLContent($xmlChild),
        'courses' => $this->parseCoursesList($coursesList, $catalog),
      ];
      //
      $child['children'] = $this->buildCoreChildren($core_child, $xmlChild, $catalog);
      //
      
      $children[] = $child;
    }
    //
    return $children;
  }

  //
  protected function buildCoreChildren($core, $xmlCore, $catalog) {
    //
    $childrenKeys = ['children', 'core'];
    if($children = NestedArray::getValue($core, $childrenKeys)) {
      //

      $xmlChildren = XMLAcalogParser::getElement($xmlCore, $childrenKeys);
      return $this->parseCoreChildren($children, $catalog, $xmlChildren);
    }
    //
    $collection = [];
    // Check children values.
    $children = (!empty($core['children'])) ?
      $core['children'] : [];
    // Walk through all children.
    foreach (array_keys($children) as $key) {
      //
      $childrenKeys = ['children', $key, 'core'];
      $children = NestedArray::getValue($core, $childrenKeys);
      $xmlChildren = XMLAcalogParser::getElement($xmlCore, $childrenKeys);
      //
      $parsedChildren = $this->parseCoreChildren($children, $catalog, $xmlChildren);

      $collection[] = array_shift($parsedChildren);
    }
    return $collection;
  }

  //
  protected function parseCoursesList($coursesList, $catalog) {
    //
    //
    $catalog_id = str_replace('acalog-catalog-', '', $catalog);
    return array_map(function ($course) use ($catalog_id) {
      //
      if (!empty($course['trayContent'])) {
        // Setup tray content based on course data.

        $this->setUpCourseData($course, $course['pointer'], $catalog_id);
      }
      return $course;
    }, $coursesList);
  }

  //
  protected function setUpCourseData(&$build, $xpointer, $catalog_id) {
    // Load course id.
    
    if ($course_id = preg_match("/\[@id='acalog-course-(\d+)'\]/", $xpointer, $matches)) {
      $course_id = array_pop($matches);
    }
    // Load course resource.
    if (!$xmlContent = $this->getResourceByIDs($catalog_id, $course_id, 'courses')) {
      return;
    }
    $resource = $this->serializer->decode($xmlContent, 'xml');
    //
    $course = NestedArray::getValue($resource, [
      'courses',
      'course'
    ], $key_exists);
    if (!$key_exists) {
      return;
    }
    //
    $course = (!empty($course['a:content']['field'])) ? $course : reset($course);
    //
    if (empty($course['a:content']['field'])) {
      return;
    }
    //
    $xml = simplexml_load_string($xmlContent);
    $xmlCourse = XMLAcalogParser::getElement($xml, ['courses', 'course']);
    //echo "<pre>";
    //print_r($xmlCourse->xpath('a:content')[0]->field);
    //exit(0);
    foreach ($xmlCourse->xpath('a:content')[0]->field as $field) {
        //print_r($field);
        //print_r($field);
        //print_r($field->data->xpath('*'));     
        //
      switch ((string) $field->attributes()->{'type'}) {
        case 'acalog-field-365':
        case 'acalog-field-526':
        case 'acalog-field-589':
        case 'acalog-field-613':
        case 'acalog-field-676':
          $build['trayContent']['subtitle'] = (string) $field->data . ' credit hours';
          break;
        case 'acalog-field-363':
        case 'acalog-field-489':
        case 'acalog-field-524':
        case 'acalog-field-587':
        case 'acalog-field-611': 
        case 'acalog-field-674':
          $data = $field->data->xpath('*');
          if(!empty($data)) {
            $output = $data[0]->asXML();
            $build['trayContent']['description'] = XMLAcalogParser::removeXMLNamespaceH($output);
          }
          break;
        case 'acalog-field-364':
        case 'acalog-field-490':
        case 'acalog-field-525':
        case 'acalog-field-588':
        case 'acalog-field-612': 
        case 'acalog-field-675':                     
          $data = $field->data->xpath('*');
          if(!empty($data)) {
            $output = $data[0]->asXML();
            $build['trayContent']['prerequisite'] = XMLAcalogParser::removeXMLNamespaceH($output);
          }
          break;
        case 'acalog-field-366':
        case 'acalog-field-527':
        case 'acalog-field-614':  
          $data = (string) $field->data;
          if(!empty($data)) {
            $build['trayContent']['iai'] = $data;
          }
          break;
        case 'acalog-field-374':
        case 'acalog-field-535':
        case 'acalog-field-622':  
          $build['trayContent']['core'] = (string) $field->data;
          break;
      }
    }
  }

  //
  /**
   * Transfer the XML data to the website
   */
  protected function parseProgramRequirements($paths, $resource, $xml) {

    $requirements = [];
    $tempArray = [];

    //Temp array to hold data for CPA exam
    $tempData = [];

    array_map(function ($parents) use ($resource, &$requirements, $xml, &$tempData) {
      $cores = NestedArray::getValue($resource, $parents);   
      
      //These are the two node-ID's that need to be reordered. 
      $nodeIDs = array("acalog-core-8079", "acalog-core-8083");

      if(empty($cores)) {
        return;
      }
      $xmlCores = XMLAcalogParser::getElement($xml, $parents);
      $cores = (array_key_exists('@id', $cores)) ? [$cores] : $cores;

      foreach ($cores as $key => $core) {
 
    
        if (empty($core['a:title'])) {
          return;
        } 

        $xmlCore = $xmlCores[$key];

        $coursesList = XMLAcalogParser::getCoursesAsList($xmlCore);

        //if 'xi:include' exists set $include variable
        if(isset($resource['programs']['program']['cores']['xi:include']))
        {
          $include = $resource['programs']['program']['cores']['xi:include'];
        }
        //Else set null
        else
        {
          $include = [];
        }
        
        if(array_key_exists('@xi:xpointer', $include)){
          //Parsing for a Major
          $requirement = [
            'pointer' => $include,
            'name' => XMLAcalogParser::getXMLContent($xmlCore, 'a:title', FALSE),
            'description' => XMLAcalogParser::getXMLContent($xmlCore),
            'courses' => $this->parseCoursesList($coursesList, $resource['@id']),
          ];
        } 
        //Parsing for a Minor
        else {
          $requirement = [
          'name' => XMLAcalogParser::getXMLContent($xmlCore, 'a:title', FALSE),
          'description' => XMLAcalogParser::getXMLContent($xmlCore),
          'courses' => $this->parseCoursesList($coursesList, $resource['@id']),

          ];
        }

        if(array_key_exists('pointer', $requirement)){
          $pattern = "/acalog-core-(\d+)/";
          preg_match($pattern, $requirement['pointer']['@xi:xpointer'], $id);

          if($id[0] != $core['@id']){ //responsible for printing Reccomendations for the C.P.A examination
            foreach ($requirement as $key => $element) {
              if($key == 'pointer'){
                $key = 'children';
                $element = $core;
              }
              $tempArray[$key] = $element;
            }
            
          } 
          else{ // goes here then and prints the Common Buisness core
            $newArray['children'] = $this->buildCoreChildren($core, $xmlCore, $resource['@id']);
            $tempArray['children'] = $newArray['children'];
            $requirement = $tempArray;       
          }
        }
        else{
          $requirement['children'] = $this->buildCoreChildren($core, $xmlCore, $resource['@id']);
        }
         //Want to determine if id is within array, if so store in $tempData and wait to put in at the end
       if (in_array($core['@id'], $nodeIDs)) {
          //store current parsed XML for later use
          $tempData = $requirement; 
          return;  
        }
        $requirements[] = $requirement;

      }
    }, $paths);  
    $requirements[] = $tempData; //re-adds the XML from $tempData. Adds it to bottom of the array. 
    return $requirements;
  }

  //
  public function buildProgramVariablesTheme($content) {
    // Get resource decoded.
    $resource = $this->serializer->decode($content, 'xml');

       // Get XML object.
    $xml = simplexml_load_string($content);


    // Retrieve program title and description.
    $program = XMLAcalogParser::getElement($xml, ['programs', 'program']);
    $title = XMLAcalogParser::getXMLContent($program, 'a:title', FALSE);
    $description = XMLAcalogParser::getXMLContent($program);
    
    // Build requirements.
    $requirements = $this->parseProgramRequirements([
      ['programs', 'program', 0, 'cores', 'core'],
      ['programs', 'program', 'cores', 'core'],
      ['sharedcores', 'core'],
    ], $resource, $xml);

    return [
      'title' => $title,
      'description' => $description,
      'requirements' => $requirements,
    ];
  }
}
