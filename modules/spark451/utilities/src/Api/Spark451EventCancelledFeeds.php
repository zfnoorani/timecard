<?php

/**
 * Modifyed the class name to align with cancelled events
 * Modifyed connect() to retrieve the cancelled events
 * RT: #411249
 * maholmes, jdust 31 May 2017
 */

namespace Drupal\utilities\Api;


use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Serializer\Serializer;

class Spark451EventCancelledFeeds {

  /**
   * Name of key for state
   * Changed 10/25/17 - given unique key to seperate from event importer
   */
  const KEY_NAME = 'utilities.events_cancelled_feed_last';

  /**
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;


  /**
   * @var StateInterface
   */
  protected $state;

  /**
   * @var QueueFactory
   */
  protected $queue;

  /**
   * @var QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * @var ConfigFactory
   */
  protected $config;

  /**
   * @var
   */
  protected $events;

  /**
   * Spark451EventFeeds constructor.
   * @param Client $http_client
   * @param Serializer $serializer
   * @param StateInterface $state
   * @param ConfigFactory $config_factory
   * @param QueueFactory $queue
   * @param QueueWorkerManagerInterface $queue_manager
   */
  public function __construct(Client $http_client, Serializer $serializer, StateInterface $state, ConfigFactory $config_factory, QueueFactory $queue, QueueWorkerManagerInterface $queue_manager) {
    $this->httpClient = $http_client;
    $this->serializer = $serializer;
    $this->state = $state;
    $this->config = $config_factory->get('utilities.events_feed_settings');
    $this->queue = $queue;
    $this->queueManager = $queue_manager;
  }

  /**
   * Open connection and get events
   *
   * @return bool
   */
  private function connect() {
    try {
      // Get URL defined in config
      $url = $this->config->get('cancelled_events_api_url');
      // Open connection and get events
      $request = $this->httpClient->get($url);
      // Decode events
      $events = $this->serializer->decode($request->getBody(), 'json');
      // Assign events
      $this->setEvents($events);
    } catch (ClientException $exception) {
      watchdog_exception(__METHOD__, $exception);
    }
  }

  /**
   * @return mixed
   */
  public function getEvents() {
    return $this->events;
  }

  /**
   * Update only events that were updated 30 minutes ago
   *
   * @param $updatedAt
   * @return bool
   */
  private function checkTimeInterval($updatedAt) {
    // Set the correct date format and get the timestamp
    $this->formattedDate($updatedAt)->getTimeStamp($updatedAt);
    // Get timestamp of the current time
    $now = 'now'; 
    $this->getTimeStamp($now);
    // Get the last time of execution
    $lastExecution = $this->state->get(self::KEY_NAME, date('U'));
    /*	Commented out following line of code to stop processing a timestamp compliant string
	Ticket: #448750; maholmes; 10/11/17
    */
    //$this->getTimeStamp($lastExecution);
    // The time of the last update should be equal or less than time of last execution
    $timeOfLastUpdate = $this->getDiffInMinutes($updatedAt, $now);
    $timeOfTheLastExecution = $this->getDiffInMinutes($lastExecution, $now);

    return $timeOfLastUpdate <= $timeOfTheLastExecution;
  }

  /**
   * Get the difference in minutes between two dates
   *
   * @param $timeOne
   * @param $timeTwo
   * @return float
   */
  private function getDiffInMinutes($timeOne, $timeTwo) {
    $interval = abs($timeOne - $timeTwo);
    $minutes = round($interval / 60);

    return $minutes;
  }

  /**
   * Get time with time zone of New York
   *
   * @param $time
   * @return $this
   */
  private function getTimeStamp(&$time) {
    $timeStamp = new \DateTime($time, new \DateTimeZone('UTC'));
    $time = $timeStamp->getTimestamp();

    return $this;
  }

  /**
   * Get a string like this 2016:10:19:12:00:00 and return a formatted date
   * using DrupalDateTime::fromArrayToISO()
   *
   * @param $date string
   * @return $this
   */
  private function formattedDate(&$date) {

    $stringToArray = array();

    list(
      $stringToArray['year'],
      $stringToArray['month'],
      $stringToArray['day'],
      $stringToArray['hour'],
      $stringToArray['minute'],
      $stringToArray['second']
      ) = explode(':', $date);

    $date = DrupalDateTime::arrayToISO($stringToArray);

    return $this;
  }

  /**
   * @param mixed $events
   */
  public function setEvents($events) {
    $this->events = $events;
  }

  /**
   * This is called from the form or cron when applicable. The cron hook is found in the
   * utilities.module file.
   * @param string $deport
   * @return $this
   */
  public function feedsEventCancelledQueue($deport = NULL) {
    $queue = $this->queue->get('cron_cancelled_event_feeds');

    // Open connection and set events
    $this->connect();
	
    // Append items to the queue
    foreach ($this->getEvents() as $event) {
      if (NULL !== $deport && 'all' === $deport) {
        $queue->createItem($event);
      } else {
        if ($this->checkTimeInterval($event['updated_at'])) {
          $queue->createItem($event);
        }
      }
    }

    return $this;
  }

  /**
   * Process queue events
   *
   * @return $this
   */
  public function processQueue() {
    $queue = $this->queue->get('cron_cancelled_event_feeds');
    $queueWorker = $this->queueManager->createInstance('cron_cancelled_event_feeds');

    while($item = $queue->claimItem()) {
      try {
        $queueWorker->processItem($item->data);
        $queue->deleteItem($item);
      } catch (SuspendQueueException $exception) {
        $queue->releaseItem($item);
        break;
      } catch (\Exception $exception) {
        watchdog_exception(__METHOD__, $exception);
      }
    }

    return $this;
  }

  /**
   * Save time of last execution
   *
   * @return $this
   */
  public function saveLastRunTime() {
    $now = new \DateTime('now', new \DateTimeZone('UTC'));
    $this->state->set(self::KEY_NAME, $now->getTimestamp());

    return $this;
  }
}
