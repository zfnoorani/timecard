<?php

namespace Drupal\utilities\Api;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use GuzzleHttp\Client;
use Symfony\Component\Serializer\Serializer;

/**
 * Class Service Profile Import.
 *
 * Class Spark451ProfileImport
 * @package Drupal\utilities\Api
 */
class Spark451ProfileImport {

  /**
   * @var QueueFactory
   */
  protected $queue;

  /**
   * @var \GuzzleHttp\Client
   */
  protected $http_client;

  /**
   * @var QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * @var \Drupal\utilities\Api\ConfigFactory
   */
  protected $config_factory;

  protected $serializer;

  /**
   * Spark451ProfileImport constructor.
   * @param \Drupal\utilities\Api\QueueFactory $queue
   * @param \Drupal\utilities\Api\QueueWorkerManagerInterface $queueManager
   */
  public function __construct(Client $http_client, Serializer $serializer,  ConfigFactory $config_factory, QueueFactory $queue, QueueWorkerManagerInterface $queueManager) {
    $this->http_client = $http_client;
    $this->queue = $queue;
    $this->queueManager = $queueManager;
    $this->config_factory = $config_factory->get('utilities.Spark451ProfileImportForm');
    $this->serializer = $serializer;
  }

  public function getData() {
    // Get URL defined in config

    $url = $this->config_factory->get('api_url');

    $request = $this->http_client->get($url);
    $request= $this->serializer->decode($request->getBody(), 'csv');

    return $request;
  }

  /**
   * Process the queue of import.
   *
   * @param array $data
   *    Data profiles.
   */
  public function processProfileImport(array $data) {

    $queue = $this->queue->get('cron_profile_import');
    // Append items to the queue.
    foreach ($data as $item) {
      $queue->createItem($item);
    }
  }

  /**
   * Process queue profiles
   *
   * @return $this
   */
  public function processQueue() {
    $queue = $this->queue->get('cron_profile_import');
    $queueWorker = $this->queueManager->createInstance('cron_profile_import');

    while ($item = $queue->claimItem()) {
      try {
        $queueWorker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (SuspendQueueException $exception) {
        $queue->releaseItem($item);
        break;
      }
      catch (\Exception $exception) {
        watchdog_exception(__METHOD__, $exception);
      }
    }

    return $this;
  }

  /**
   * Execute the import by cron.
   */
  public function profileImportQueue() {

    $user_data = $this->config_factory->get('user_data');
    $values['fields'] = $this->config_factory->get('fields');
    $values['fields'][] = [
      'attribute' => $user_data['field_username'],
      'profile_type' => 'main',
      'field_name' => 'username'
    ];
    $values['fields'][] = [
      'attribute' => $user_data['field_email'],
      'profile_type' => 'main',
      'field_name' => 'mail'
    ];
    $data = $this->getData();


    if (!empty($data)) {

      foreach ($data as $key => $row) {
        foreach ($row as $item) {
          $row_data[] = $item;
        }
        $profiles['data'][] = $row_data;
        $headers = array_keys($row);
        $row_data = NULL;
      }
      $profiles['headers'] = $headers;

      $mapped_fields = $this->mapFields($profiles, $values);

      if (!empty($mapped_fields)) {
        $this->processProfileImport($mapped_fields);
        $this->processQueue();
      }
    }
  }

  /**
   * Map All fields with form fields.
   *
   * @param $data
   * @param $values
   * @return array
   */
  private function mapFields($data, $values) {

    $fields = $values['fields'];
    $data_profile = [];
    foreach ($data['data'] as $delta => $row) {
      foreach ($row as $index => $value) {

        $header = $data['headers'][$index];
        foreach ($fields as $field) {
          if (!empty($field['attribute']) && $field['attribute'] === $header) {
            $data_profile[$delta][$field['profile_type']][$field['field_name']] = $value;
          }
        }
      }
    }

    return $data_profile;
  }

}