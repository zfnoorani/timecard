<?php

namespace Drupal\utilities\Api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\profile\Entity\Profile;
use Drupal\simplesamlphp_auth\Service\SimplesamlphpAuthManager;
use Drupal\user\UserInterface;
use SimpleSAML_Auth_Simple;
use SimpleSAML_Configuration;

/**
 * Class Spark451SimpleSAMLphpAuth.
 *
 * @package Drupal\utilities
 */
class Spark451SimpleSAMLphpAuth extends SimplesamlphpAuthManager {

  /**
   * A configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $configMapping;

  /**
   * Profile storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $profileStorage;

  /**
   * {@inheritdoc}
   *
   * Spark451SimpleSAMLphpAuth constructor.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \SimpleSAML_Auth_Simple|NULL $instance
   * @param \SimpleSAML_Configuration|NULL $config
   */
  public function __construct(ConfigFactoryInterface $config_factory, SimpleSAML_Auth_Simple $instance = NULL, SimpleSAML_Configuration $config = NULL) {
    parent::__construct($config_factory, $instance, $config);
    $this->configMapping = $config_factory->get('utilities.Spark451SAMLMapFieldsForm');
    $this->profileStorage = \Drupal::entityTypeManager()->getStorage('profile');
  }

  /**
   * {@inheritdoc}
   */
  public function externalAuthenticate() {
    $this->instance->requireAuth([
      'ReturnTo' => \Drupal\Core\Url::fromRoute('<current>')->setAbsolute()->toString(),
    ]);
  }

  /**
   * @param \Drupal\user\UserInterface $account
   * @param $attributes
   * @return bool|\Drupal\user\UserInterface
   */
  public function mapAuthUserAttributes(UserInterface $account, $attributes) {
    //
    if(!$fields = $this->configMapping->get('fields')) {
      $fields = [];
    }
    //
    array_map(function($field) use ($account, $attributes) {
      // Load profile based on account id.
      if (!$profile = $this->profileStorage->loadByUser($account, $field['profile_type'])) {
        $profile = Profile::create([
          'type' => $field['profile_type'],
          'uid' => $account->id(),
        ]);
      }
      if(empty($attributes[$field['attribute']])) {
        return;
      }
      //
      if(!$profile->hasField($field['field_name'])) {
        return;
      }
      //
      $value = reset($attributes[$field['attribute']]);
      //
      $profile->{$field['field_name']}->value = $value;
      //
      $profile->save();
    }, $fields);
    //
    if(!$roles = $this->configMapping->get('roles')) {
      $roles = [];
    }
    //
    $roles = array_reduce($roles, function($carry, $role){
      $carry[$role['attribute']] = $role['role'];
      return $carry;
    }, []);
    //
    array_map(function($key) use ($attributes, $account, $roles) {
      //
      $value = (!empty($attributes[$key])) ?
        reset($attributes[$key]) : FALSE;
      if(!$value) {
        return;
      }
      //
      if(array_key_exists($value, $roles)) {
        //
        $account->addRole($roles[$value]);
      } else {
        //
        \Drupal::logger(__METHOD__)->warning('Role NOT mapped: ' . $value);
      }
    }, ['urn:oid:1.3.6.1.4.1.5923.1.1.1.5', 'urn:oid:2.16.840.1.113730.3.1.4']);
    //
    return $account;
  }
}
