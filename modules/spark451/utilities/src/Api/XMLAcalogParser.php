<?php

namespace Drupal\utilities\Api;

/**
 * Class XMLAcalogParser
 * @package Drupal\utilities\Api
 */
class XMLAcalogParser {

  public static function removeXMLNamespaceH($output) {
    $output = str_replace('<h:', '<', $output);
    $output = str_replace('</h:', '</', $output);
    return str_replace('xmlns:h="http://www.w3.org/1999/xhtml"', '', $output);
  }

  public static function getXMLContent($xml, $path = 'a:content/h:*', $asXML = TRUE) {
    $output = '';
    if (!$elements = $xml->xpath($path)) {
      return $output;
    }
    //
    foreach ($elements as $element) {
      //
      $output .= ($asXML) ? $element->asXML() : (string) $element;
    }
    if ($asXML) {
      $output = self::removeXMLNamespaceH($output);
    }
    return $output;
  }

  public static function getElement($xml, $keys) {
    //
    foreach ($keys as $key) {
      //
      if (!empty($xml->{$key})) {
        $xml = $xml->{$key};
      }
    }
    return $xml;
  }

  public static function getCoursesAsList($xml) {
    //
    $coursesXML = $xml->courses->asXML();
    $coursesXML = str_replace('xmlns:xi="http://www.w3.org/2001/XInclude"', '', $coursesXML);
    $coursesXML = str_replace('<xi:', '<', $coursesXML);
    $coursesXML = str_replace('</xi:', '</', $coursesXML);
    //
    $list = [];
    $courses = @simplexml_load_string($coursesXML);
    foreach ($courses->children() as $course) {
      $item = [];
      //
      switch ($course->getName()) {
        case 'include':
          //
          $item['pointer'] = (string) $course->attributes()->{'xi:xpointer'};
          $item['title'] = self::getXMLContent($course->fallback, 'a:title', FALSE);
          $item['trayContent'] = [
            'id' => rand(),
            'title' => $item['title'],
            'type' => 'tray-curriculum-course',
          ];
          break;
        case 'adhoc':
          //
          $item['title'] = self::getXMLContent($course);
          if ('right' == (string) $course->attributes()->{'position'}) {
            $idx = count($list) - 1;
            $list[$idx]['append'] = substr($item['title'], 4, -4);
            unset($item);
          }
          break;
      }
      //
      if (!empty($item)) {
        $list[] = $item;
      }
    }
    return $list;
  }
}
