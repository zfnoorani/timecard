<?php

namespace Drupal\utilities\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\utilities\Controller\UserProfileController;
use Drupal\utilities\Form\Spark451LocalSettingsFormAlter;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class Spark451RouterSubscriber.
 *
 * @package Drupal\utilities\Routing
 * Listens to the dynamic route events.
 */
class Spark451RouterSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Retrieve profile canonical router.
    if ($router = $collection->get('entity.profile.canonical')) {
      // Overwrite path.
      $router->setPath('/profile_type/{profile}');
      // Overwrite controller.
      $router->setDefault('_controller', UserProfileController::class . '::profileTypeRedirect');
    }
    // Retrieve "user.pass" router.
    if ($router = $collection->get('user.pass')) {
      // Retrieve defaults.
      $defaults = $router->getDefaults();
      // Remove default "_form".
      unset($defaults['_form']);
      // Setup "_controller" new controller behavior.
      $defaults['_controller'] = UserProfileController::class . '::userRedirectToResetPassword';
      // Overwrite default values.
      $router->setDefaults($defaults);
    }
    // Retrieve "simplesamlphp_auth.admin_settings_local" router.
    if ($router = $collection->get('simplesamlphp_auth.admin_settings_local')) {
      // Overwrite "_form" class value.
      $router->setDefault('_form', Spark451LocalSettingsFormAlter::class);
    }
    // Define router names to make admin type.
    $names = [
      'entity.profile.type.faculty.user_profile_form',
      'entity.profile.edit_form',
      'entity.profile.delete_form',
      'entity.user.edit_form',
      'rng.event.node.event',
      'rng.event.node.access',
      'rng.event.node.messages',
      'rng.event.node.messages.add',
      'rng.event.node.group.list',
      'entity.registration.edit_form',
      'entity.registration.delete_form',
      'entity.registration.canonical',
      'entity.registration.registrants',
      'rng.event.node.access.reset',
      'entity.rng_rule_component.canonical',
      'rng.event.node.access.reset',
      'entity.rng_rule_component.canonical',
      'entity.rng_rule.delete_form',
      'rng.event.node.group.add',
      'entity.registration_group.delete_form',
      'view.marketing_submissions.page',
    ];
    // Walk trough all router names.
    array_map(function ($name) use ($collection) {
      // Retrieve router based on name.
      if ($router = $collection->get($name)) {
        // Define as admin path.
        $router->setOption('_admin_route', TRUE);
      }
    }, $names);
    // Setup title dynamically to router based on main menu link.
    array_map(function ($route_name) use ($collection) {
      // Load router by machine name.
      if ($router = $collection->get($route_name)) {
        // Retrieve menu link title based on given path.
        if ($title = \Drupal::database()
          ->select('menu_link_content_data', 'm')
          ->fields('m', ['title'])
          ->condition('menu_name', 'main')
          ->condition('link__uri', 'internal:' . $router->getPath())
          ->execute()
          ->fetchField()
        ) {
          // Define retrieve title.
          $router->setDefault('_title', $title);
        }
      }
    }, [
      'utilities.program_finder_controller',
      'utilities.calendar_controller',
      'view.search_content.page'
    ]);
  }
}
