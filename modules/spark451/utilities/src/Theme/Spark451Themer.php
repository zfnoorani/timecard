<?php

namespace Drupal\utilities\Theme;

use Doctrine\Common\Collections\ArrayCollection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\utilities\Entity\UserProfile;
use Drupal\utilities\Menu\Spark451MenuHandler;
use Drupal\utilities\Ng2\Ng2SocialCard;
use Drupal\video_embed_field\ProviderManager as VideoEmbedProviderManager;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Template\Attribute;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\Core\Theme\ThemeManager;
use Drupal\Core\Render\Element;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Component\Utility\NestedArray;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Drupal\yamlform\Entity\YamlFormSubmission;

/**
 * Class Spark451Themer.
 *
 * @package Drupal\utilities
 */
class Spark451Themer implements Spark451ThemerInterface {

  /**
   * @var \Doctrine\Common\Collections\ArrayCollection Shared values.
   */
  protected $collection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $entityStorage;

  /**
   * @var \Drupal\utilities\Menu\Spark451MenuHandler
   */
  protected $spark451MenuHandler;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\Core\Theme\ThemeManager
   */
  protected $themeManager;

  /**
   * @var \Drupal\Core\Theme\Registry
   */
  protected $themeRegistry;

  /**
   * @var \Drupal\video_embed_field\ProviderManager
   */
  protected $videoEmbededProviderManager;


  /**
   * Spark451Themer constructor.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\utilities\Menu\Spark451MenuHandler $spark451_menu_handler
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\Core\Theme\ThemeManager $theme_manager
   * @param \Drupal\Core\Theme\Registry $theme_registry
   * @param \Drupal\video_embed_field\ProviderManager $provider_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Spark451MenuHandler $spark451_menu_handler, RendererInterface $renderer,
                              ThemeManager $theme_manager, Registry $theme_registry, VideoEmbedProviderManager $provider_manager) {
    //TODO: Remove dependency of "Drupal\Core\Theme\Registry" class.
    $this->collection = new ArrayCollection();
    $this->entityStorage = $entity_type_manager->getStorage('entity_view_display');
    $this->spark451MenuHandler = $spark451_menu_handler;
    $this->renderer = $renderer;
    $this->themeManager = $theme_manager;
    $this->themeRegistry = $theme_registry;
    $this->videoEmbededProviderManager = $provider_manager;
    $this->profileStorage = $entity_type_manager->getStorage('profile');

  }

  /**
   * @return \Drupal\Core\Render\RendererInterface
   */
  public function getRenderer() {
    return $this->renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function invokePreprocess(array &$vars) {
    // Retrieve first variables set.
    if (isset($vars['#theme']) && $vars['#theme'] == 'field' && !empty($vars[0])) {
      $vars = $vars[0];
    }
    // Define preprocess type.
    $preprocess_type = '';
    $preprocess_themes = ['#theme', '#type'];
    // Get through preprocess themes.
    foreach ($preprocess_themes as $type) {
      // Check preprocess theme.
      if (isset($vars[$type])) {
        // Remove hash tag.
        $preprocess_type = $vars[$type];
        break;
      }
    }
    // Check if preprocess function already exists.
    if (function_exists('template_preprocess_' . $preprocess_type)) {
      // Remove pound key from all keys.
      $keys = array_map(function ($key) {
        return str_replace('#', '', $key);
      }, array_keys($vars));
      // Create variables to preprocess function.
      $vars = array_combine($keys, array_values($vars));
      // Execute preprocess.
      call_user_func_array('template_preprocess_' . $preprocess_type, [&$vars]);
    }
    // Check last element.
    $last = end($vars);
    // Get through preprocess themes.
    foreach ($preprocess_themes as $type) {
      // Check preprocess theme.
      if (is_array($last) && array_key_exists($type, $last)) {
        // Rewrite $vars as $last element.
        $vars = $last;
        // Re-invoke preprocess.
        $this->invokePreprocess($vars);
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setupBasePath(array &$vars) {
    // Make global available base path variable for twig templates.
    if (!isset($vars['base_path'])) {
      // Setup base path using core function.
      $vars['base_path'] = base_path();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fieldImageView(ContentEntityBase $entity, ImageItem $image, $view_mode = 'default') {
    // Concat the entity view display id.
    $viewDisplayId = sprintf('%s.%s.%s',
      $entity->getEntityTypeId(),
      $entity->getType(),
      $view_mode);
    // Load entity view display.
    $entityViewDisplay = $this->entityStorage
      ->load($viewDisplayId);
    // Setup field name.
    $field = $image->getParent()->getName();
    // Load field's component.
    $component = $entityViewDisplay->getComponent($field);
    // Build image render array.
    return $image->view($component ?: []);
  }

  /**
   * Load all menu items from content variable.
   * @param array $vars Given values to check.
   * @return array
   *   Menu items from content.
   */
  public function loadMenuItems(array $vars) {
    // Retrieve menu items from content.
    return $this->spark451MenuHandler->loadMenuItems($vars);
  }

  /**
   * Build main menu based on given items.
   * @param array $vars Given variables to check.
   * @return array
   *   Main menu links hierarchy.
   */
  public function buildMainMenu(array $vars) {
    // Build hierarchical menu.
    return $this->spark451MenuHandler->buildMainMenu($vars);
  }

  /**
   * @param $menu_name
   * @return array
   */
  public function getMenuItems($menu_name) {
    //
    return $this->spark451MenuHandler->getMenuItems($menu_name);
  }

  /**
   * {@inheritdoc}
   */
  public function &getSharedCollection() {
    return $this->collection;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveTheme() {
    return $this->themeManager->getActiveTheme();
  }

  /**
   * {@inheritdoc}
   */
  public function getCompleteThemeRegistry() {
    // Called on ncc child theme - prints nothing related to ncc_child
    return $this->themeRegistry->get();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuickEditAttributes($field) {
    // Setup variables based on field definition.
    $vars = (array_key_exists('element', $field)) ?
      $field : ['element' => $field['#object']];
    // Execute quick edit preprocess.
    quickedit_preprocess_field($field);
    // Retrieve attributes.
    $attributes = (!empty($vars['attributes'])) ? $vars['attributes'] : [];
    return new Attribute($attributes);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsMargins($vars) {
    // Init collection.
    $collection = [];
    // Check if settings margins was defined.
    if (array_key_exists('field_settings_margins', $vars['elements'])) {
      // Retrieve settings margin field.
      $fieldSettingsMargins = $vars['elements']['field_settings_margins'];
      // Get through all children elements.
      $collection = array_reduce(Element::children($fieldSettingsMargins), function (&$collection, $child) use ($fieldSettingsMargins) {
        // Retrieve paragraph entity.
        $paragraph = $fieldSettingsMargins[$child]['#paragraph'];
        // Define fields to grab.
        $fields = [
          'top' => 'field_cb_top_margin',
          'bottom' => 'field_cb_bottom_margin',
        ];
        // Map field settings.
        $collection = array_map(function ($field) use ($paragraph) {
          // Check if field exists and no empty.
          if ($paragraph->hasField($field) && !$paragraph->{$field}->isEmpty()) {
            // Retrieve renderable array.
            return $paragraph->{$field}->view();
          }
          return NULL;
        }, $fields);
        return $collection;
      });
    }
    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function getTabsSection($items) {
    $tabs = array_reduce($items, function ($tabs, $item) {
      // Setup tab based on content variable.
      $tab = [
        'id' => $item['content']['#paragraph']->id(),
        'label' => $item['content']['#paragraph']->field_cb_title->value,
        'icon' => FALSE,
        'content' => $item['content'],
      ];
      // Check if any value was choose.
      if ($item['content']['#paragraph']->field_ncc_icon->count()) {
        // Define icon value.
        $tab['icon'] = $item['content']['#paragraph']->field_ncc_icon->value;
      }
      // Append tab to carry value.
      $tabs[] = $tab;
      return $tabs;
    }, []);

    return $tabs;
  }

  /**
   * {@inheritdoc}
   */
  public function getOverviewTab($node, $overview_field, $stack_field) {

    $hasHighlight = FALSE;

    // Set Overview tabs defaults
    $overviewTab = [
      'id' => "#program-overview",
      'label' => "Overview",
      'icon' => 'icon-flag-1',
      'highlight' => ''
    ];

    // If Highlight stack is persent render it and display it
    if (!empty($node) && $node->hasField($stack_field) && !$node->{$stack_field}->isEmpty()) {
      $hasHighlight = TRUE;
      // Get Highlights stak view
      $field = $node->{$stack_field}->view();
      // Get cards within Highlight Stack
      $components = array_map(function ($child) use ($field) {
        return $field[$child];
      }, Element::children($field));
      // Set object for rendering components in Highlight stack
      $highlightsStack = [
        '#theme' => 'angular2_stack',
        '#component' => \Drupal::service('ng2_entity.ng2_view_display')
          ->getComponentByMachineName('ng2-ncc-stack'),
        '#ng_content' => $this->renderer->render($components),
      ];
      // Render Highligh stack
      $overviewTab['highlight'] = [
        '#markup' => $this->renderer->render($highlightsStack),
      ];
    }

    // If Overview is present, display it
    if ($node->hasField($overview_field) && !$node->{$overview_field}->isEmpty()) {
      $overviewTab['content'] = [
        '#prefix' => '',
        '#markup' => $node->get($overview_field)->value,
        '#suffix' => ''
      ];
    }

    return $overviewTab;
  }

  //
  public function renderOverviewTab($node, $overview_field, $stack_field) {

    $output = '';
    $overviewTab = $this->getOverviewTab($node, $overview_field, $stack_field);
    // Check if overivew tab has content
    if (isset($overviewTab['content']) && isset($overviewTab['content']['#markup'])) {

      // Prerender overview tab content #markup
      $overviewTab['content'] = $this->renderer->render($overviewTab['content']);
      // Define parameters for rendering tabs template
      $raw_input = [
        '#type' => 'inline_template',
        '#template' => "{% include '@ncc/partials/tabs.html.twig'  %}",
        '#attached' => [
          'library' => [
            'ncc/cbpfwtabs',
          ],
        ],
        '#context' => [
          'base_path' => base_path(),
          'directory' => $this->themeManager->getActiveTheme()->getPath(),
          'tabs' => [$overviewTab],
        ],
      ];

      // Render tabs template
      $output = $this->renderer->render($raw_input);
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getNegativeComponentSpacing($vars) {
    // Load the current node.
    if (!$node = $this->loadCurrentNode('page')) {
      return;
    }
    //
    $sections = new \ArrayIterator();
    //
    if ($node->hasField('field_page_sections') && !$node->field_page_sections->isEmpty()) {
      $sections = $node->field_page_sections->getIterator();
    }
    // Init collection.
    $margins = [
      'additionalTopSpacing' => FALSE,
      'additionalBottomSpacing' => FALSE
    ];
    //
    $paragraph = $vars['paragraph'];
    //
    while ($sections->valid()) {
      //
      if ('stack_group' == $sections->current()->entity->bundle()) {
        //
        if ('ncc-stack-bottom-negative-margin' == $sections->current()->entity->field_stack_negative_margin->value) {
          //
          $next = NULL;
          $key = $sections->key() + 1;
          //
          if ($sections->offsetExists($key)) {
            $next = $sections->offsetGet($key);
          }
          //
          if ($next && $next->entity->id() == $paragraph->id()) {
            $margins['additionalTopSpacing'] = TRUE;
          }
        }
        elseif ('ncc-stack-top-negative-margin' == $sections->current()->entity->field_stack_negative_margin->value) {
          $offset = ($sections->key() - 1);
          if ($sections->offsetExists($offset)) {
            $previous = $sections->offsetGet($offset);
            if ($previous && $previous->entity->id() == $paragraph->id()) {
              $margins['additionalBottomSpacing'] = TRUE;
            }
          }
        }
      }
      //
      $sections->next();
    }

    return $margins;
  }

  /**
   * {@inheritdoc}
   */
  public function setUpMediaGalleryContent(Paragraph $paragraph, $field_name) {
    $content = [];

    $attributes = [
      'class' => [
        'object-fit-cover',
        'img-full-width',
      ],
      'width' => NULL,
      'height' => NULL,
    ];

    if ($gallery = $paragraph->get($field_name)->first()) {
      // Load first image from entity related.
      if (!$image = $gallery->entity->field_gallery_image->first()) {
        return $content;
      }
      // Define media based on field render array.
      // $content['media'] = $this->fieldImageView($gallery->entity, $image, 'teaser');
      $content['media'] = $this->getResponsiveImageByStyle('teaser', $image->entity->getFileUri(), ['class' => ['img-responsive']]);
      // Define media attributes and image url settings.
      $settings = [
        'media' => [
          '#item_attributes' => $attributes,
          '#attached' => [
            'library' => [
              'ncc/ncc-modal',
              'ncc/swiper',
              'ncc/object-fit-images'
            ],
          ]
        ],
        'imageUrl' => $image->entity->url(),
        'fullImageUrl' => $this->getImageByStyle('card_image_2x', $image->entity->getFileUri())
      ];
      // Merge within smart teaser variable.
      $content = NestedArray::mergeDeep($content, $settings);
      // Define modal content.
      $content['modal'] = [
        'id' => 'modal-' . $gallery->entity->uuid(),
        'type' => 'gallery',
        'content' => $this->renderGallerySlider($gallery->entity),
      ];
    }

    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function setUpMediaVideoContent(Paragraph $paragraph, $field_name) {
    // Init content variable.
    $content = [];
    // Load first video.
    if ($video = $paragraph->get($field_name)->first()) {
      // Retrieve node entity.
      $node = ($video->entity) ? $video->entity : FALSE;
      // Build media video content based on node.
      $content = $this->buildVideoContent($node);
    }
    return $content;
  }

  /**
   * Build media video content based on node entity.
   *
   * @param \Drupal\node\Entity\Node $node Given entity to parse.
   * @return array
   *   Renderable media content.
   */
  public function buildVideoContent(Node $node) {
    // Init variables.
    $content = [];
    $image_url = NULL;
    // Set common settings for attributes and attached libs
    $attributes = [
      'class' => [
        'img-responsive',
        'img-full-width',
      ],
      'width' => NULL,
      'height' => NULL
    ];
    $attached = [
      'library' => [
        'ncc/ncc-modal',
      ],
    ];
    // Define video as type.
    $content['type'] = 'video';
    // Get cover image object
    $image = ($node && !$node->field_image->isEmpty()) ? $node->field_image->first() : NULL;
    // Load plugin provider.
    $provider = $this->videoEmbededProviderManager
      ->loadProviderFromInput($node->field_video_link->value);
    if ($image) {
      // get cover image url
      $image_url = $image->entity->url();
      // Set media image
      $content['media'] = $this->getResponsiveImageByStyle('teaser', $image->entity->getFileUri(), [
        'class' => [
          'object-fit-cover',
          'img-full-width'
        ]
      ]);
      // $this->fieldImageView($paragraph, $image);
      // Define media attributes and image url settings.
      $settings = [
        'media' => [
          '#item_attributes' => $attributes,
          '#attached' => $attached,
          '#responsive_image_style_id' => 'teaser'
        ],
        'imageUrl' => $image_url
      ];
      // Merge within smart teaser variable.
      $content = NestedArray::mergeDeep($content, $settings);
    }
    // If image_url is not set, pull generic thumbnail from video_embed_field
    elseif ($provider && $node) {
      // get thumbnail from provider
      $provider->downloadThumbnail();
      // Set generic video thumbnail grabbed from provider
      $image_url = $this->getResponsiveImageByStyle('teaser', $provider->getLocalThumbnailUri());
      // Define render image array.
      $content['media'] = [
        '#theme' => 'responsive_image',
        '#responsive_image_style_id' => 'teaser',
        '#uri' => $provider->getLocalThumbnailUri(),
        '#alt' => $node->title->value,
        '#title' => $node->title->value,
        '#attributes' => $attributes,
        '#attached' => $attached
      ];
    }
    //
    if ($provider && $node) {
      // Define modal content.
      $content['modal'] = [
        'id' => 'modal-' . $node->uuid(),
        'type' => 'video',
        'content' => $provider->renderEmbedCode(NULL, NULL, FALSE),
        'imageUrl' => $image_url,
      ];
    }
    //
    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function setUpMediaContent(Paragraph $paragraph) {
    // Init content.
    $content = [];
    // Check media field and retrieve item entity.
    if ($paragraph->hasField('field_cb_media') && ($item = $paragraph->field_cb_media->first())) {
      // Load media paragraph.
      $media = $item->get('entity')->getTarget()->getValue();
      // Define field name.
      $field = 'field_' . $media->getType();
      // Define media teaser type.
      $content['type'] = str_replace('cb_media_', '', $media->getType());
      // Setup default image attributes.
      $attributes = [
        'class' => [
          'img-responsive',
          'img-full-width',
        ],
        'width' => NULL,
        'height' => NULL,
      ];
      // Check field type.
      switch ($content['type']) {
        // When it's an image type.
        case 'image':
          // Load image field.
          if ($image = $media->get($field)->first()) {
            // Define media based on field render array.
            $content['media'] = $this->fieldImageView($media, $image);
            // Overwrite current item attributes.
            $content['media']['#item_attributes'] = $attributes;
            // Setup image url based on entity file.
            $content['imageUrl'] = ($image->entity) ? $image->entity->url() : '';
          }
          break;
        case 'video':
          // Retrieve content pre-populated.
          $content += $this->setUpMediaVideoContent($media, 'field_cb_media_video');
          break;
        case 'gallery':
          // Load gallery field.
          $content += $this->setUpMediaGalleryContent($media, $field);
          break;
        case 'stack_highlight':
          //
          $fieldStack = 'field_stack_highlight_card';
          if (!$media->hasField($fieldStack)) {
            break;
          }
          // Define short name to type value.
          $content['type'] = 'stack';
          // Get all components rendered.
          $components = array_map(function ($item) {
            return $item->view('angular2_component');
          }, $media->{$fieldStack}->getIterator()->getArrayCopy());
          // Setup media as theme.
          $content['media'] = [
            '#theme' => 'angular2_stack',
            '#component' => \Drupal::service('ng2_entity.ng2_view_display')
              ->getComponentByMachineName('ng2-ncc-stack'),
            '#ng_content' => $this->renderer->render($components),
          ];
          break;
      }
    }
    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function renderGallerySlider(Node $node) {
    // Define inline template.
    $output = [
      '#type' => 'inline_template',
      '#template' => "{% include '@ncc/partials/gallery.html.twig'  %}",
      '#context' => [
        'base_path' => base_path(),
        'directory' => $this->themeManager->getActiveTheme()->getPath(),
        'gallery' => [],
      ],
    ];
    // Init gallery.
    $gallery = [];
    // Check if gallery image field exists.
    if ($node->hasField('field_gallery_image') && ($iterator = $node->field_gallery_image->getIterator())) {
      // Walk through all items.

      $thumbAttributes = [
        'class' => [
          'object-fit-cover'
        ],
        'width' => NULL,
        'height' => NULL
      ];

      $attributes = [
        'class' => [
          'object-fit-cover',
          'position-absolute',
          'top-0',
          'left-0'
        ],
        'width' => NULL,
        'height' => NULL,
      ];

      do {
        // Retrieve current image.
        $image = $iterator->current();
        // Setup URL, thumbnail and description.
        if ($image->entity) {
          $slide = $this->fieldImageView($node, $image);
          $slide['#item_attributes'] = $attributes;
          $gallery[] = [
             'image' => $slide,
             'thumbnail' => $this->getResponsiveImageByStyle('gallery_thumbnail', $image->entity->getFileUri(), $thumbAttributes),
              'alt' => $image->alt,
              'description' => $image->entity->get('field_image_description')->value
           ];
        }
        // Go to next element.
        $iterator->next();
      } while ($iterator->valid());
      // Setup gallery context.
      $output['#context']['gallery'] = $gallery;
    }
    return $output;
  }

  //
  public function shouldWeHideTitle($node, $fields) {
    $counter = array_reduce($fields, function ($counter, $field) use ($node) {
      // Check if current field name exist and if it is empty.
      if ($node->hasField($field) && $node->{$field}->isEmpty()) {
        // Increment counter.
        $counter++;
      }
      return $counter;
    });
    // Check if counter is different than fields quantity.
    return ($counter != count($fields));
  }

  //
  public function setUpBillboardFieldImage(array $field_image) {
    //
    $field_image['#attached']['library'][] = 'ncc/object-fit-images';
    //
    foreach (Element::children($field_image) as $child) {
      $field_image[$child]['#item_attributes'] = [
        'class' => [
          'object-fit-cover',
          'position-absolute',
          'top-0'
        ],
        'width' => NULL,
        'height' => NULL,
      ];
    }
    //
    unset($field_image['#printed']);
    return $field_image;
  }

  //
  public function getSliderSlide($node, $field_name) {
    $attributes = [
      'class' => [
        'object-fit-cover'
      ],
      'width' => NULL,
      'height' => NULL,
    ];

    $attached = [
      'library' => [
        'ncc/object-fit-images',
      ],
    ];

    $bgImage = $this->fieldImageView($node, $node->get($field_name)->first());
    $bgImage['#item_attributes'] = $attributes;
    $bgImage['#attached'] = $attached;

    return $bgImage;
  }

  //
  public function getImageByStyle($style, $fileUri) {
    return ImageStyle::load($style)->buildUrl($fileUri);
  }

  //
  public function getResponsiveImageByStyle($style, $uri, $attributes = [], $alt = '', $title = '', $attached = []) {

    $responsiveImage = [
      '#theme' => 'responsive_image',
      '#responsive_image_style_id' => $style,
      '#uri' => $uri
    ];

    if ($alt) {
      $responsiveImage['#alt'] = $alt;
    }
    if ($title) {
      $responsiveImage['#title'] = $title;
    }
    if ($attributes) {
      $responsiveImage['#attributes'] = $attributes;
    }
    if ($attached) {
      $responsiveImage['#attached'] = $attached;
    }

    return $responsiveImage;
  }

  /**
   * Implements hook_preprocess_HOOK() for video-embed-iframe.html.twig.
   */
  public function hookPreprocessVideoEmbedIframe(&$variables) {
    $variables['query'] += [
      'color' => 'b20818',
      'title' => '0',
      'byline' => '0',
      'portrait' => '0',
    ];
    $provider = array_filter(explode('__', $variables['theme_hook_original']), function ($value) {
      return (strstr($value, 'video_embed_iframe') === FALSE);
    });
    if ($provider) {
      $variables['attributes']->addClass($provider);
    }
  }

  /**
   * Implements hook_ENTITY_TYPE_view_alter() for Paragraph Edit Form.
   */
  public function hookParagraphViewAlter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
    // Check preview view mode and spacer paragraph.
    if ('preview' == $build['#view_mode'] && 'cb_spacer' == $entity->bundle()) {
      // Load children from build rendeable array.
      $children = Element::children($build);
      // Check if children was retrieved.
      if ((!empty($children))) {
        // Override theme.
        $build['#theme'] = 'paragraph_admin_preview';
        // Define field name.
        $build['#field_name'] = reset($children);
        // Remove last element form parents.
        array_pop($build['#parents']);
      }
    }
  }

  /**
   * Implements hook_preprocess_HOOK() for paragraph-admin-preview.html.twig.
   */
  public function hookPreprocessParagraphAdminPreview(&$variables) {
    // Load field name.
    $field = $variables['field_name'];
    // Check current field exists into entity.
    if ($variables['paragraph']->hasField($field)) {
      // Define content value based on field view.
      $variables['content'] = $variables['paragraph']->{$field}->view();
    }
    // Setup parent field.
    $collection = array_filter($variables['parents'], function ($parent) {
      // Return only value that match with regex.
      return (preg_match('/^(field_cb_)(.*)(_margin)$/', $parent) === 1);
    });
    // If parent field is an string.
    if (!empty($collection)) {
      $parent_field = reset($collection);
      // Define replacements.
      $replacements = [
        'field_cb' => '',
        '_' => ' ',
      ];
      //
      array_walk($replacements, function ($value, $key) use (&$parent_field) {
        $parent_field = str_replace($key, $value, $parent_field);
      });
      // Upper case each word and expose parent field.
      $variables['parent_field'] = ucwords($parent_field);
    }
  }

  //
  public function hookPreprocessViewContentRelatedBlock(&$variables) {
    //
    $featured = (count($variables['rows']) >= 3) ? 2 : 0;
    //
    $variables['rows'] = array_map(function ($row, $key) use ($featured) {
      /**
       * @var \Drupal\Core\Entity\EntityInterface;
       */
      $entity = $row['content']['#node'];
      // Build ng2 markup
      $content = $this->renderer->render($row['content']);
      // Check component and entity variables.
      if (empty($row['content']['#component'])) {
        //
        $viewDisplayId = sprintf('%s.%s.%s',
          $entity->getEntityTypeId(),
          $entity->getType(),
          'angular2_component');
        // Load entity view display.
        $display = $this->entityStorage
          ->load($viewDisplayId);
        // Retrieve components settings from Third party settings.
        $machineName = $display->getThirdPartySetting('ng2_entity', 'components_settings');
        if (!$component = \Drupal::service('ng2_entity.ng2_view_display')
          ->getComponentByMachineName($machineName)
        ) {
          return NULL;
        }
        $row['content']['#component'] = $component;
      }
      //
      $component = $row['content']['#component'];
      //
      $cardState = array_reduce(explode('-', $component['machine_name']), function ($carry, $value) {
        $carry[] = ucfirst($value);
        return $carry;
      });
      //
      $cardState = implode($cardState) . $entity->id();
      //
      $isActive = ($featured == $key);
      $isPeekAdjacent = (++$featured == $key);
      // TODO: Refactor to use only once. @see Spark451Ng2Stack L90.
      $attributes = [
        '[isActive]' => (!empty($isActive)) ? '"true"' : '"false"',
        '[isPeekAdjacent]' => (!empty($isPeekAdjacent)) ? '"true"' : '"false"',
      ];
      $attributes = array_reduce(array_keys($attributes), function ($carry, $key) use ($attributes) {
        $carry[] = "{$key}={$attributes[$key]}";
        return $carry;
      }, []);
      $attributes = implode(' ', $attributes);
      //
      $componentWrapper = "<{$component['component_wrapper']} {$attributes} #{$cardState}>";
      $searchTag = "<{$component['machine_name']}";
      $ng2Tag = str_replace($searchTag, "{$searchTag} [cardState]='{$cardState}.cardState'", $content);
      //
      if ('article' == $entity->bundle() && 'story' == $entity->field_article_type->value) {
        //
        $ng2Tag = str_replace($component['machine_name'], 'ncc-card-story', $ng2Tag);
      }      
      //
      return $componentWrapper . $ng2Tag . "</{$component['component_wrapper']}>";
    }, $variables['rows'], array_keys($variables['rows']));
    //
    $ng_content = implode($variables['rows']);
    if (empty($ng_content)) {
      return;
    }
    //
    $variables['ng2_stack'] = [
      '#theme' => 'angular2_stack',
      '#component' => \Drupal::service('ng2_entity.ng2_view_display')
        ->getComponentByMachineName('ng2-ncc-stack'),
      '#ng_content' => $ng_content,
    ];
  }

  //
  public function hookFormNodeProgramAlter(&$form) {
    //
    $groups = [
      'group_contact' => t('Who should people get in touch with if they have questions?'),
      'group_categorization' => t('Categories correspond with the filters in the program finder where users explore majors, minors, degrees and other programs.'),
    ];
    //
    array_map(function ($key) use (&$form, $groups) {
      //
      if (!empty($form['#fieldgroups'][$key])) {
        $form[$key . '_description'] = [
          '#markup' => '<p>' . $groups[$key] . '</p>',
        ];
        $form['#group_children'][$key . '_description'] = $key;
      }
    }, array_keys($groups));
  }

  //Hook the main profile form to dynamically display degree fields
  //jldust 9/17/18
  public function hookFormProfileMainAlter(&$form) {
    $empType = $form['field_employment_type']['widget'][0]['value']['#default_value'];

    //hide empType from everyone's display
    $form['field_employment_type']['#access'] = false;

    //An array to hold Faculty types that we want to overwrite for eduction
    //Including: Full Time, Full Time Visiting, Half Time, Part Time Faculty 
    $empArray = array("FTF", "FVF", "HTF", "PTF");

    //If a facutly member hide other degree field
    if(in_array($empType, $empArray)){
              
      //hide faculty degrees from non-faculty 
      $form['field_profile_award']['#access'] = false;
    }      
    //Use non-facutly degree field & hide other degree field
    else{
      $form['field_education_degrees']['#access'] = false;
    }
    //Inform users they cannot edit personal data
    $groups = [
      'group_basic' => t('Changes to greyed fields must be done through Human Resources.'),
    ];
    //
    array_map(function ($key) use (&$form, $groups) {
      //
      if (!empty($form['#fieldgroups'][$key])) {
        $form[$key . '_description'] = [
          '#markup' => '<p>' . $groups[$key] . '</p>',
        ];
        $form['#group_children'][$key . '_description'] = $key;
      }
    }, array_keys($groups));
  }



  /**
   * @param \Drupal\user\Entity\User $user Drupal User entity | integer $user UID
   * @param string $profileType
   * @return Object
   */
  public function getProfilesByUser($user, $profileType = 'main') {
    // if an integer was passed (UID), load the user based on that
    if (is_int($user)) {
      $account = User::load($user);
      return $this->profileStorage->loadByUser($account, $profileType);
    }
    // if the User object was passed, get the profile directly from that
    return $this->profileStorage->loadByUser($user, $profileType);
  }

  /**
   * @param User $user Drupal User entity
   * @return Object
   */
  public function prepareBillBoardProfile(User $user) {
    //
    $title = NULL;
    $lastName = NULL;
    $firstName = NULL;
    $socialNetworks = [];
    // Load main profile.
    if ($mainProfile = $this->getProfilesByUser($user, 'main')) {
      $firstName = $mainProfile->get('field_profile_first_name')->value;
      if ($middleName = $mainProfile->get('field_profile_middle_name')->value) {
        $firstName .= ' ' . $middleName;
      }

      $lastName = $mainProfile->get('field_profile_last_name')->value;
      if ($suffix = $mainProfile->get('field_profile_suffix')->value) {
        $lastName .= ', ' . $suffix;
      }

      $title = $mainProfile->get('field_profile_title')->value;
      if ($department = $mainProfile->get('field_profile_department')->value) {
        $title .= ', ' . $department;
      }

      if ($google = $mainProfile->get('field_profile_googleplus')->value) {
        array_push($socialNetworks, array(
          'link' => $google,
          'icon' => 'icon-google'
        ));
      }
      if ($linkedin = $mainProfile->get('field_profile_linkedin')->value) {
        array_push($socialNetworks, array(
          'link' => $linkedin,
          'icon' => 'icon-linkedin'
        ));
      }
    }
    // Load faculty profile.
    $facultyProfile = $this->getProfilesByUser($user, 'faculty');
    // Setup billboard profile.
    $billboardProfile = [
      "isUserAuthenticated" => \Drupal::currentUser()->isAuthenticated(),
      "first_name" => $firstName,
      "last_name" => $lastName,
      "title" => $title,
      "phone" => $mainProfile ? $mainProfile->get('field_profile_phone')->value : NULL,
      "email" => $user->get('mail')->value,
      "office_location" => $mainProfile ? $mainProfile->get('field_profile_office_location')->value : NULL,
      "office_hours" => $facultyProfile ? $facultyProfile->get('field_profile_office_hours')->value : NULL,
      "socials" => $socialNetworks,
      "stack" => [],
      "show_office" => !in_array('faculty', $user->getRoles())
    ];
    //
    $cards = [];
    if ($user->hasField('user_picture') && $user->get('user_picture')->entity) {
      $profilePictureUri = $user->get('user_picture')->entity->getFileUri();
      $profilePicture = $this->getImageByStyle('card_image_2x', $profilePictureUri);
      $imageCard = new Ng2SocialCard();
      $imageCard->id = 'card' . uniqid();
      $imageCard->type = 'image';
      $imageCard->data = [
        'image' => $profilePicture
      ];
      $cards[] = $imageCard;
    }
    //
    if ($mainProfile && ($twitter = $mainProfile->get('field_profile_twitter')->value)) {
      $spark451SocialFeeds = \Drupal::service('spark451.socialfeeds');
      $twitterCard = $spark451SocialFeeds->getTwitterCard($twitter);
      $cards[] = $twitterCard;
    }
    //
    if ($mainProfile && ($facebook = $mainProfile->get('field_profile_facebook')->value)) {
      $spark451SocialFeeds = \Drupal::service('spark451.socialfeeds');
      $facebookCard = $spark451SocialFeeds->getFacebookCard($facebook);
      $cards[] = $facebookCard;
    }
    //
    $slides = $this->buildBillboardSlides($cards);
    if ($slides != "") {
      $billboardProfile['stack']['collection'] = [
        '#theme' => 'angular2_stack',
        '#component' => \Drupal::service('ng2_entity.ng2_view_display')
          ->getComponentByMachineName('ng2-ncc-stack'),
        '#ng_content' => $slides
      ];

      $field = [
        'element' => [
          '#object' => NULL,
          '#view_mode' => '_custom',
        ]
      ];
      /* 
       * @TODO
       * Fix quick edit on billboard profile - currently doesn't work/show on 8.3.7; breaks 8.4.4 profiles
       */
      //$billboardProfile['stack']['attr'] = $this->getQuickEditAttributes($field);
    }

    return $billboardProfile;
  }

  //
  protected function buildBillboardSlides($cards) {
    $result = "";
    foreach ($cards as $key => $card) {
      if ($card) {
        if ($key === 0) {
          $prefix = '<ncc-card [isActive]="true" [isPeekAdjacent]="false" #' . $card->id . '>';
        }
        else {
          $prefix = '<ncc-card [isActive]="false" [isPeekAdjacent]="true" #' . $card->id . '>';
        }
        $suffix = '</ncc-card>';
        $result .= $prefix . $card->build() . $suffix;
      }
    }
    return $result;
  }

  /**
   * @param User $user Drupal User entity
   * @return Object
   */
  public function prepareProfileTabs(User $user) {
    $facultyProfile = $this->getProfilesByUser($user, 'faculty');
    $mainProfile = $this->getProfilesByUser($user, 'main');

    $tabs = [];

    // Prepare Biography Tab
    if ($mainProfile) {
      $bioTabContent = $mainProfile->get('field_profile_bio')->value;

      if ($facultyProfile) {
        $cv = $facultyProfile->get('field_profile_cv');
        if ($cv && $downloadCv = $cv->entity) {
          $bioTabContent .= '<a class="ncc-btn ncc-btn-storm" href="' . $downloadCv->downloadUrl()
              ->toString() . '">Download CV</a>';
        }
      }

      $bioTab = [
        "id" => "profile-biography",
        "icon" => "icon-view-list",
        "label" => "Biography"
      ];

      // field_profile_highlights
      $highlights = $mainProfile->get('field_profile_highlights');
      if (!$highlights->isEmpty()) {
        // Get Highlights stak view
        $field = $highlights->view();
        // Get cards within Highlight Stack
        $components = array_map(function ($child) use ($field) {
          return $field[$child];
        }, Element::children($field));
        // Set object for rendering components in Highlight stack
        $bioTab['highlight'] = [
          '#theme' => 'angular2_stack',
          '#component' => \Drupal::service('ng2_entity.ng2_view_display')
            ->getComponentByMachineName('ng2-ncc-stack'),
          '#ng_content' => $this->renderer->render($components),
        ];
      }

      $bioTab["content"] = [
        '#prefix' => (!isset($bioTab['highlight'])) ? '<div class="container-full">' : '',
        '#markup' => $bioTabContent,
        '#suffix' => (!isset($bioTab['highlight'])) ? '</div>' : ''
      ];

      array_push($tabs, $bioTab);
    }

    if ($facultyProfile) {
      // Prepare Scholarship Tab if exists
      $scholarships = $facultyProfile->get('field_profile_scholarship');

      if ($scholarships->count()) {

        $scholarshipsMarkup = '<h2>' . $scholarships->getfieldDefinition()
            ->getLabel() . '</h2>';

        foreach ($scholarships as $scholarship) {
          $scholarshipsMarkup .= $scholarship->value;
        }

        $scholarshipsTab = [
          "id" => "profile-scholarship",
          "icon" => "icon-newspaper",
          "label" => "Selected Scholarship",
          "content" => [
            '#type' => 'inline_template',
            '#template' => '<div class="container-full">{{ scholarshipsMarkup | raw }}</div>',
            '#context' => [
              'scholarshipsMarkup' => $scholarshipsMarkup
            ]
          ]
        ];

        array_push($tabs, $scholarshipsTab);
      }

      // Prepare Courses Tab if exists
      $courses = $facultyProfile->get('field_profile_courses');
      if ($courses->count()) {

        $coursesMarkup = '<h2>' . $courses->getfieldDefinition()
            ->getLabel() . '</h2>';

        foreach ($courses as $scholarship) {
          $coursesMarkup .= '<p class="no-margin-bottom">' . $scholarship->value . '</p>';
        }

        $coursesTab = [
          "id" => "profile-courses",
          "icon" => "icon-home-1",
          "label" => "Courses Taught",
          "content" => [
            '#prefix' => '<div class="container-full">',
            '#markup' => $coursesMarkup,
            '#suffix' => '</div>'
          ]
        ];

        array_push($tabs, $coursesTab);
      }
    }

    $tags = ($mainProfile) ? $mainProfile->get('field_profile_related_content') : FALSE;
    if ($tags && $tags->count()) {

      $args = [];
      $view = Views::getView('profile_content_related');
      foreach ($tags->getValue() as $key => $value) {
        array_push($args, $value['target_id']);
      }
      $stringified = implode('+', $args);
      $view->setArguments(array($stringified));

      $view->setDisplay('default');
      $view->preExecute();
      $view->execute();

      // Loop over the results
      $listingBlocks = [];
      foreach ($view->result as $row) {
        $data = $this->prepareListingBlock($row->_entity);
        $listingBlock = [
          '#theme' => 'listing_block',
          '#listingBlock' => $data
        ];
        $listingBlocks[]['#markup'] = $this->renderer->render($listingBlock);
      }

      // Alternative approach
      $newsTab = [
        "id" => "profile-news",
        "icon" => "icon-newspaper",
        "label" => "News",
        "content" => [
          '#prefix' => '<div class="container-full">',
          '#suffix' => '</div>',
          '#markup' => $this->renderer->render($listingBlocks)
        ]
      ];
      array_push($tabs, $newsTab);
    }

    return $tabs;
  }

  //
  public function prepareListingBlock($item) {
    $type = 'thumbnail'; // set default type
    $date = FALSE; // set default date
    $typeData = FALSE;

    if ($item->getType() !== 'article') {
      $type = $item->getType();
    }

    $contentType = ucfirst($type);
    // prepare data based on content type
    switch ($type) {
      case 'thumbnail':
        if ($item->field_publication_date && $item->field_publication_date->value) {
          $timestamp = strtotime($item->field_publication_date->value);
          $date = \Drupal::service('date.formatter')
            ->format($timestamp, 'custom', 'F j, Y');
        }
        if ($item->field_article_type && $item->field_article_type->value) {
          $contentType = ucfirst($item->field_article_type->value);
        }
        //Determine if its a blog type
        if ($item->field_blog_type && $item->field_blog_type->value) {
          $contentType = ucfirst($item->field_blog_type->value);
        }
        if ($item->field_image && $item->field_image->entity) {
          $imageUrl = $this->getImageByStyle('list_thumbnail_2x', $item->field_image->entity->getFileUri());
          $typeData = '<img src="' . $imageUrl . '" class="img-responsive" />';
        }
        break;
      case 'event':
        // Retrieve event date time raw value.
        $dateTime = $this->eventDateTimeByKey($item);
        $typeData = $dateTime->format('d');
        // Build start/end date from event content.
        $date = $this->buildEventStartEndDate($item);
        break;
      case 'video':
        $typeData = 'icon-control-play';
        break;
      case 'gallery':
        $typeData = 'icon-picture-3';
        break;
    }

    $result = [
      'type' => $type,
      'typeData' => $typeData,
      'date' => $date,
      'title' => $item->getTitle(),
      'titleHref' => \Drupal::service('path.alias_manager')
        ->getPathByAlias('/node/' . $item->nid->value),
      'content' => strip_tags(text_summary($item->body->value)),
      'contentType' => $contentType
    ];
    return $result;
  }

  /**
   * Retrieve timestamp from event date time field.
   * @param \Drupal\Core\Entity\ContentEntityBase $entity Entity to retrieve value.
   * @param string $key Given key to check.
   *
   * @return null|\DateTime
   *   DateTime object based on field event date time and key, otherwise NULL.
   */
  public function eventDateTimeByKey(ContentEntityBase $entity, $key = 'value') {
    // Check event date time field.
    if ($entity->hasField('field_event_date_time') &&
      !$entity->field_event_date_time->isEmpty()
    ) {
      // Retrieve event date time raw value.
      $field_event_date_time = $entity->field_event_date_time->getValue();
      $event_date_time = reset($field_event_date_time);
      // Check given key.
      if (array_key_exists($key, $event_date_time)) {
        // Retrieve datetime object using storage timezone.
        $dateTime = new \DateTime($event_date_time[$key],
          New \DateTimeZone(DATETIME_STORAGE_TIMEZONE));
        // Setup proper datetime zone.
        $dateTime->setTimezone(new \DateTimeZone(drupal_get_user_timezone()));
        return $dateTime;
      }
    }
    return NULL;
  }

  /**
   * Parse start/end datetime field value based on event content.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   Given entity to parse start/end datetime values.
   *
   * @return
   *   Proper start/end datetime parsed.
   */
  public function buildEventStartEndDate(ContentEntityBase $entity) {
    // Check entity bundle to be sure work with event CT.
    if ('event' != $entity->bundle()) {
      return NULL;
    }
    // Validate start/end date fields exist.
    if ($entity->field_event_date_time->isEmpty()) {
      return NULL;
    }
    // Retrieve start datetime object.
    $startDateTime = $this->eventDateTimeByKey($entity, 'value');
    $startDate = $startDateTime->format('F j, Y');
    $startTime = $startDateTime->format('g:i A');

    // Retrieve end datetime object.
    $endDateTime = $this->eventDateTimeByKey($entity, 'end_value');
    $endDate = $endDateTime->format('F j, Y');
    $endTime = $endDateTime->format('g:i A');

    // If start and end date are equals return it.
    if ($startDate === $endDate) {
      return sprintf('%s, %s - %s', $startDate, $startTime, $endTime);
    }
    // Otherwise add end date time to output.
    return sprintf('%s to %s, %s - %s', $startDate, $endDate, $startTime, $endTime);
  }

  /**
   * @param User $user Drupal User entity
   * @return Object
   * Updated 9/17/18 jldust
   * Changed to use degree field for specificly faculty and non-faculty
   * Modified $facultyProfile foreach to only display credentials if not empty
   */
  public function prepareProfileAdditionalData(User $user) {

    $additionalData = [];

    if ($facultyProfile = $this->getProfilesByUser($user, 'faculty')) {
      $fields = [
        'field_profile_credentials',
        'field_profile_interests'
      ];

      foreach ($fields as $field) {
        //determine if field has data & display if true
        if($facultyProfile->get($field)->value){
          $item = $facultyProfile->get($field);
          if ($item->count()) {
            $itemMarkup = ['items' => []];
            $itemMarkup['title'] = $item->getfieldDefinition()->getLabel();
            foreach ($item as $col) {
              array_push($itemMarkup['items'], $col->value);
            }
            array_push($additionalData, $itemMarkup);
          }
        }
      }
    }

    if ($mainProfile = $this->getProfilesByUser($user, 'main')) {
      $awards = $mainProfile->get('field_profile_award');
      $degrees = $mainProfile->get('field_education_degrees');
      $empType = $mainProfile->get('field_employment_type')->value;

      //An array to hold Faculty types that we want to overwrite for eduction
      //Including Full Time, Full Time Visiting, Half Time, Part Time Faculty 
      $empArray = array("FTF", "FVF", "HTF", "PTF");
      //If a facutly member & $degree has data display faculty degree field
      if((in_array($empType, $empArray)) and ($degrees->value)){
        $awardsMarkup = [
          'items' => ['#markup' => $degrees->value],
          'title' => $degrees->getfieldDefinition()->getLabel(),
          'raw' => TRUE
        ];
        array_push($additionalData, $awardsMarkup);
      }else{
        //Use non-facutly degree field if not in the array and the field has data
      if ($awards->value) {
        $awardsMarkup = [
          'items' => ['#markup' => $awards->value],
          'title' => $degrees->getfieldDefinition()->getLabel(),
          'raw' => TRUE
        ];
        array_push($additionalData, $awardsMarkup);
        }
      }
      $links = $mainProfile->get('field_profile_links');
      if ($links->count()) {
        $linksMarkup = ['items' => []];
        $linksMarkup['title'] = $links->getfieldDefinition()->getLabel();
        foreach ($links as $link) {
          $enternal = $link->isExternal() ? 'target="_blank"' : '';
          array_push($linksMarkup['items'], ['#markup' => '<a href="' . $link->uri . '" ' . $enternal . '>' . $link->title . '</a>']);
        }
        array_push($additionalData, $linksMarkup);
      }
    }

    return $additionalData;
  }

  //
  public function addContactPrefix(&$variables) {
    $prefix = '<div class="ncc-label">Questions?</div>';
    foreach ($variables['items'] as $i => $item) {
      $variables['items'][$i]['content']['#prefix'] = $prefix;
    }
  }

  //
  public function addContactSuffix(&$variables) {
    $suffix = '<div class="ncc-spacer-sm"></div>';
    foreach ($variables['items'] as $i => $item) {
      $variables['items'][$i]['content']['#suffix'] = $suffix;
    }
  }

  // Prepare the variables needed for profile listing
  public function prepareProfile($user) {
    $profile = $this->getProfilesByUser($user, 'main');

    // break if the user doesn't have the necessary profile
    if (!$profile) {
      return FALSE;
    }

    // build the name variable
    $name = '';
    if ($profile->field_profile_first_name && trim($profile->field_profile_first_name->value)) {
      $name .= $profile->field_profile_first_name->value . ' ';
    }
    if ($profile->field_profile_middle_name && trim($profile->field_profile_middle_name->value)) {
      $name .= $profile->field_profile_middle_name->value . ' ';
    }
    if ($profile->field_profile_last_name && trim($profile->field_profile_last_name->value)) {
      $name .= $profile->field_profile_last_name->value;
    }
    if ($profile->field_profile_suffix && trim($profile->field_profile_suffix->value)) {
      $name .= ', ' . $profile->field_profile_suffix->value;
    }
    $result['name'] = $name;

    $result['email'] = $user->mail->value;

    if ($profile->field_profile_title) {
      $result['title'] = $profile->field_profile_title->value;
    }

    if ($profile->field_profile_phone) {
      $result['phone'] = $profile->field_profile_phone->value;
    }

    // Check picture field and first entity attached it.
    if (!$user->user_picture->isEmpty() &&
      ($picture = $user->user_picture->first()) &&
      $picture->entity
    ) {
      // Define photo as renderable array.
      $result['photo'] = $this->getResponsiveImageByStyle('list_thumbnail', $picture->entity->getFileUri());
    }

    $result['url'] = \Drupal::service('path.alias_manager')
      ->getPathByAlias('/user/' . $user->uid->value);

    if ($profile->field_profile_office_location) {
      $result['address'] = $profile->field_profile_office_location->value;
    }

    if ($profile->field_profile_department) {
      $result['faculty'] = $profile->field_profile_department->value;
    }

    // It hide address for all cards
    $result['show_location'] = FALSE;

    return $result;
  }

  //
  public function getActiveAlert() {
    $alert = FALSE;

    // Get alerts
    $view = Views::getView('alert');
    if ($view) {
      // $view->setDisplay('display_alert');
      $view->execute();
     
      //Check to see if the view exists and there is data
      if ($view->result && !(is_null($view->result[0]->_entity))) {
        //If not null store entity for alert
        $entity = $view->result[0]->_entity;

        $title = $entity->getTitle();
        $body = $entity->get('body')->value;
        $subtitle = $entity->get('field_alert_subtitle')->value;
        $type = strtolower($entity->get('field_alert_type')->value);

        switch ($type) {
          case 'green':
            $class = 'ncc-alert-success';
            break;
          case 'red':
            $class = 'ncc-alert-warning';
            break;
          default:
            $class = '';
            break;
        }

        $alert = [
          "nodeID" => $entity->uuid(),
          "type" => [
            '#markup' => ($subtitle) ? $subtitle : '&nbsp;'
          ],
          "class" => $class,
          "title" => $title,
          "description" => $body,
          "cta" => $entity->field_alert_call_to_action->view()
        ];
      }
    }

    return $alert;
  }

  //
  public function hookFormEventRegisterAlter(&$form, FormStateInterface &$form_state, $form_id) {
    $form['actions']['submit']['#submit'][] = self::class . '::eventRegisterFormSubmit';
  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public static function eventRegisterFormSubmit(&$form, FormStateInterface $form_state) {
    //
    $current_path = \Drupal::service('path.current')->getPath();
    $url_object = \Drupal::service('path.validator')
      ->getUrlIfValid($current_path);
    $route_name = $url_object->getRouteName();
    $node_id = \Drupal::routeMatch()->getRawParameter('node');
    unset($_SESSION['messages']['status'][0]);
    drupal_set_message('Your registration is complete.');
    //
    $form_state->setRedirect($route_name, array('node' => $node_id));
  }

  //
  public function hookFormUserFormAlter(&$form, FormStateInterface &$form_state, $form_id) {
    // Grab current user id.
    $uid = \Drupal::currentUser()->id();
    // Load full current user account.
    $account = User::load($uid);
    // Check "administrator" role.
    $isAdmin = $account->hasRole('administrator');
    // Setup password field access.
    $form['account']['pass']['#access'] = $isAdmin;
    // If current account is administrator
    // and it doesn't have password defined
    // while check its own account.
    if ($isAdmin &&
      is_null($account->getPassword()) &&
      $form['account']['name']['#default_value'] == $account->getAccountName()
    ) {
      // Disable password fields.
      $form['account']['pass']['#access'] = FALSE;
      $form['account']['current_pass']['#access'] = FALSE;
    }
    // If current administrator account is not defining reset password.
    elseif ($isAdmin && !$form_state->get('user_pass_reset')) {
      $form['account']['current_pass']['#access'] = TRUE;
    }
  }

  //
  public function loadCurrentNode($bundle = NULL) {
    $node = \Drupal::routeMatch()->getParameter('node');
    if (!is_object($node) && !is_null($node)) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($node);
    }

    if ($node && !is_null($bundle) && $bundle != $node->bundle()) {
        return NULL;
    }
    return $node;
  }

  //
  public function setUpListingBlockByUser(User $user, UserProfile $profile = NULL) {
    //
    $listingBlock = [
      'type' => 'thumbnail',
      'titleHref' => '/user/' . $user->id(),
      'contentType' => 'Profile',
    ];
    //
    $profile = is_null($profile) ?
      $this->getProfilesByUser($user) : $profile;
    //
    if ($profile) {
      $listingBlock += \Drupal::service('spark451.themer')
        ->setUpListingBlockByProfile($profile);
    }
    //
    if (empty($listingBlock['title'])) {
      $listingBlock['title'] = $user->mail->value;
    }
    else {
      $listingBlock['title'] .= sprintf(' (%s)', $user->mail->value);
    }
    //
    if (!$user->user_picture->isEmpty()) {
      //
      $imageUrl = \Drupal::service('spark451.themer')->getImageByStyle('list_thumbnail_2x', $user->user_picture->entity->getFileUri());
      $listingBlock['typeData'] = '<img src="' . $imageUrl . '" class="img-responsive" />';
    }
    //
    return $listingBlock;
  }

  //
  public function setUpListingBlockByProfile(UserProfile $profile) {
    //
    $field_values = array_map(function ($field) use ($profile) {
      if ($profile->hasField($field)) {
        return $profile->{$field}->value;
      }
      return NULL;
    }, ['field_profile_title', 'field_profile_department']);
    //
    $field_values = array_filter($field_values);
    //
    $glue = (count($field_values) > 1) ? ' at ' : '';
    $content = implode($glue, $field_values);
    //
    return [
      'title' => UserProfile::buildProfileFullName($profile),
      'content' => $content,
    ];
  }

  public function convertPhpDateFormatToJavascript($php_format){

    $SYMBOLS_MATCHING = array(
        // Day
        'l' => 'dddd',
        'D' => 'ddd',
        'd' => 'dd',
        'j' => 'd',
        'N' => '',
        'S' => '',
        'w' => '',
        'z' => '',
        // Week
        'W' => '',
        // Month
        'F' => 'MMMM',
        'M' => 'MMM',
        'm' => 'MM',
        'n' => 'M',
        't' => '',
        // Year
        'L' => '',
        'o' => '',
        'Y' => 'yyyy',
        'y' => 'yy',
        // Time
        'a' => '',
        'A' => '',
        'B' => '',
        'g' => '',
        'G' => '',
        'h' => '',
        'H' => '',
        'i' => '',
        's' => '',
        'u' => ''
    );
    $jqueryui_format = "";
    $escaping = false;
    for($i = 0; $i < strlen($php_format); $i++){
      $char = $php_format[$i];
      if($char === '\\'){
        $i++;
        if($escaping) $jqueryui_format .= $php_format[$i];
        else $jqueryui_format .= '\'' . $php_format[$i];
        $escaping = true;
      }
      else{
        if($escaping) { $jqueryui_format .= "'"; $escaping = false; }
        if(isset($SYMBOLS_MATCHING[$char]))
          $jqueryui_format .= $SYMBOLS_MATCHING[$char];
        else
          $jqueryui_format .= $char;
      }
    }
    return $jqueryui_format;
  }

  /**
   * Setup datetime value to database UTC format.
   *
   * @param string $value Given raw datetime value.
   * @param array $time Given time as an array.
   * @return string
   *   Proper datetime database format.
   */
  protected function setUpDateTimeFormatAsUTC($value, $time = [0, 0, 0]) {
    // Create dateTime object.
    if(!$dateTime = \DateTime::createFromFormat('Y-m-d', $value)) {
      return $value;
    }
    // Parse given time.
    list($hour, $minute, $second) = $time;
    // If it was created successful then setup time and timezone.
    $dateTime->setTime($hour, $minute, $second)->setTimezone(new \DateTimeZone( 'UTC' ));
    // Split to define database format.
    $dateTimePieces = explode(' ', $dateTime->format('Y-m-d H:i:s'));
    return implode('T', $dateTimePieces);
  }

  /**
   * Implememts hook_views_query_alter().
   * Blog view implementation added to dynamically hook into the Generic Blog View
   * 10/8/18 jldust
   */
  public function hookViewsQueryAlter(ViewExecutable $view, QueryPluginBase $query) {
    /**
     * This portion of the hook specifically is utilize to dynamically change the 
     * generic view for blog landing pages.
     * 10/8/18 jldust
     */
     // Check view name and its display id.
     if ('generic_blog' == $view->id() && 'default' == $view->current_display) {

       //Use page type to determine which blog view to dynamically display
        switch ($view->element['#arguments'][0]) {
          //UniquelyNC Blog
          case 'UniquelyNC':
            $givenBlog = 'blog';
            break;
          //Shimer Blog
          case 'Shimer Great Books Blog':
            $givenBlog = 'shimer_blog';
            break;
          //Engineering Blog
          case 'The 21st Century Engineer':
            $givenBlog = 'engineering_blog';
            break;
          //If blog doesn't exist 
          default:
            $givenBlog = '';
            break;
        }
        //If the blog exists and is set
        if(isset($givenBlog)){
          //Setup configuration for blog filter view
          $configuration = [
            'type'       => 'LEFT',
            'table'      => 'node__field_blog_type',
            'field'      => 'entity_id',
            'left_table' => 'node_field_data',
            'left_field' => 'nid',
            'operator'   => '=',
          ];
          //Join tables and create relationship
          $join_obj = \Drupal\views\Views::pluginManager('join')->createInstance('standard', $configuration);
          $rel = $query->addRelationship('node__field_blog_type', $join_obj, 'node_field_data');   
          $query->addTable('node__field_blog_type', $rel, $join_obj, 'field_blog_type_value');
          //Where the blog type matches the current page
          $query->addWhere('', 'node__field_blog_type.field_blog_type_value', $givenBlog, '=');
        }
      }

    /**
    * This portion of the hook specifically is utilize to dynamically change the 
    * related blog posts that display on each blog page by checking blog type and taxonomy terms
    * jldust 10/23/18
    */
    // Check view name and its display id.
    if ('related_blog_posts' == $view->id() && 'default' == $view->current_display) {
      //Use page type passed as an argument to determine which blog view 
      //to dynamically display, this data is set in ncc.theme
      switch ($view->args[0]) {
        //UniquelyNC Blog
        case 'UniquelyNC':         
          $givenBlog = 'blog_categories';
          break;
        //Shimer Blog
        case 'Shimer Great Books Blog':
          $givenBlog = 'shimer_blog_categories';;
          break;
        //Engineering Blog
        case 'The 21st Century Engineer':
          $givenBlog = 'engineering_blog_categories';
        break;
        //If blog doesn't exist 
        default:
          $givenBlog = '';
          break;
      }
      //If the blog exists and is set
      if(isset($givenBlog)){
        //Setup configuration for blog filter view
        $configuration = [
          'table'      => 'taxonomy_term_field_data',
          'field'      => 'tid',
          'left_table' => 'taxonomy_index',
          'left_field' => 'tid',
          'operator'   => '=',
        ];
        //Join tables and create relationship
        $join_obj = \Drupal\views\Views::pluginManager('join')->createInstance('standard', $configuration);
        $rel = $query->addRelationship('taxonomy_term_field_data', $join_obj, 'taxonomy_index');   
        $query->addTable('taxonomy_term_field_data', $rel, $join_obj, 'vid');
        //Where the blog type matches the current page
        $query->addWhere('', 'taxonomy_term_field_data.vid', $givenBlog, '=');
        
        //Remove passed name prior to loop for taxonomy terms
        unset($view->args[0]);

        //Check for each selected term and the posts related to them
        foreach($view->args as $terms){
          //Add query group named "OR", and condition looking for given taxonomy term
          $query->addWhere("OR", 'taxonomy_term_field_data.tid', $terms, '=');

          //Set query conditional from "AND" to "OR" so we look for multiple taxonomy terms
          $query->where["OR"]["type"] = "OR";
        }

        //Remove the current page from the listed
        $node = \Drupal::routeMatch()->getParameter('node');
        if ($node instanceof \Drupal\node\NodeInterface) {
          //Get current node id to filter it out
          $nid = $node->id();
        }
        //Remove the current page from the the listed view
        $query->addWhere('', 'node_field_data.nid', $nid, 'NOT IN');
      }
    }

    /**
    * This portion of the hook specifically is utilized to dynamically change the 
    * specific blog categories that displayed on blog landing pages
    * jldust 10/23/18
    */
    // Check view name and its display id.
    if ('blog_categories' == $view->id() && 'default' == $view->current_display) {
      //Use page type passed as an argument to determine which blog categories 
      //to dynamically display, this data is set in ncc.theme
      switch ($view->args[0]) {
        //UniquelyNC Blog
        case 'UniquelyNC':
          $givenBlog = 'blog_categories';
          break;
        //Shimer Blog
        case 'Shimer Great Books Blog':
          $givenBlog = 'shimer_blog_categories';
          break;
        //Engineering Blog
        case 'The 21st Century Engineer':
          $givenBlog = 'engineering_blog_categories';
          break;
        //If blog doesn't exist 
        default:
          $givenBlog = '';
          break;
      }

      //If the blog exists and is set
      if(isset($givenBlog)){
        //Where the blog type matches the required categories
        $query->addWhere('', 'taxonomy_term_field_data.vid', $givenBlog, 'in');
      }  
      //jldust 6-13-19
      //Removed Events API Hook from Spark due to Views API Chnage
      //All query info is now handedl in Events API View under FILTER CRITERIA

    }

    // Get only future events
    if ('event_content_related' === $view->id() && in_array($view->current_display, ['block_paragraph', 'block'])) {
      // Add condition
      $now = new \DateTime('now', new \DateTimeZone('UTC'));
      $query->addWhere('AND', 'node__field_event_date_time.field_event_date_time_value', $now->format('Y-m-d\TH:i:s'), '>=');
    }
  }

  /**
   * Set "visually-hidden" class.
   * @param array $vars Given variables.
   */
  public function hideNodeTitleField(&$vars) {
    // Hide title field will apply only to full view mode.
    if ('full' !== $vars['element']['#view_mode']) {
      return;
    }
    // Hide current title field.
    $vars['attributes']['class'][] = 'visually-hidden';
  }

  /**
   * Build link based on reference form and save it into main form.
   *
   * @param string $text
   * @param \Drupal\yamlform\Entity\YamlFormSubmission $main
   * @param \Drupal\yamlform\Entity\YamlFormSubmission $reference
   */
  public function saveSubmissionNoteLink($text, YamlFormSubmission $main, YamlFormSubmission $reference) {
    /** @var \Drupal\Core\Url $url */
    $url = Url::fromRoute('entity.yamlform_submission.canonical', [
      'yamlform' => $reference->bundle(),
      'yamlform_submission' => $reference->id(),
    ]);
    /** @var \Drupal\Core\GeneratedLink $link */
    $link = \Drupal::linkGenerator()
      ->generate($text, $url);
    // Save note into main form.
    $main->setNotes($link->getGeneratedLink())->save();
  }

  /**
   * Callback submission function to process main marketing form.
   * @param array $form Given form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state Current form state.
   */
  public static function formMarketingMainSubmitAlter(&$form, FormStateInterface $form_state) {
    // Define session value to check when main form was submitted it for first time.
    // @see ncc_preprocess_field_group_html_element__node__marketing__group_header().
    $_SESSION['marketing_main_form_executed'] = TRUE;
  }

  /**
   * Callback submission function to process modal marketing form.
   * @param array $form Given form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state Current form state.
   */
  public static function formMarketingModalSubmitAlter(&$form, FormStateInterface $form_state) {
    // Grab token from query string.
    if(!$token = \Drupal::request()->get('token')) {
      return;
    }
    /** @var \Drupal\node\Entity\Node $node */
    if (!$node = \Drupal::service('spark451.themer')
      ->loadCurrentNode('marketing')) {
      return;
    }
    /** @var \Drupal\paragraphs\Entity\Paragraph $mainForm */
    $mainForm = $node->field_form_reference->entity;
    $entities = \Drupal::entityTypeManager()
      ->getStorage('yamlform_submission')
      ->loadByProperties([
        'yamlform_id' => $mainForm->field_form_reference->target_id,
        'token' => $token,
      ]);
    if(!empty($entities)) {
      /** @var \Drupal\yamlform\Entity\YamlFormSubmission $mainSubmission */
      $mainSubmission = reset($entities);
      /** @var \Drupal\yamlform\Entity\YamlFormSubmission $modalSubmission */
      $modalSubmission = $form_state->getFormObject()->getEntity();
      // Save note link into main form.
      \Drupal::service('spark451.themer')
        ->saveSubmissionNoteLink('Following-up form', $mainSubmission, $modalSubmission);
      // Save note link into reference form.
      \Drupal::service('spark451.themer')
        ->saveSubmissionNoteLink('Main form', $modalSubmission, $mainSubmission);
      // Define session value to check when modal form was submitted for first time.
      // @see ncc_preprocess_field_group_html_element__node__marketing__group_header().
      $_SESSION['marketing_modal_form_executed'] = TRUE;
    }
  }
}
