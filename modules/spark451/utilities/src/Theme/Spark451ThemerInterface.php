<?php

namespace Drupal\utilities\Theme;

use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\image\Plugin\Field\FieldType\ImageItem;

/**
 * Interface Spark451ThemerInterface.
 *
 * @package Drupal\utilities
 */
interface Spark451ThemerInterface {

  /**
   * Execute preprocess function of fiven element.
   *
   * @param array $vars
   */
  public function invokePreprocess(array &$vars);

  /**
   * Define base path variable.
   *
   * @param array $vars
   */
  public function setupBasePath(array &$vars);

  /**
   * Build theme image array based on field settings.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $image
   * @param string $view_mode
   * @return array Theme image array.
   */
  public function fieldImageView(ContentEntityBase $entity, ImageItem $image, $view_mode = 'default');

  /**
   * Return attribute reference to manage shared values.
   *
   * @return \Doctrine\Common\Collections\ArrayCollection Shared collection.
   */
  public function &getSharedCollection();

  /**
   * Returns the active theme object.
   *
   * @return \Drupal\Core\Theme\ActiveTheme
   */
  public function getActiveTheme();

  /**
   * Returns the complete theme registry from cache or rebuilds it.
   *
   * @return array
   *   The complete theme registry data array.
   *
   * @see Registry::$registry
   */
  public function getCompleteThemeRegistry();

  /**
   * Define QuickEdit attributes based on field.
   * @param $field array Field render array.
   * @return array
   *   Attributes defined by QuickEdit module.
   */
  public function getQuickEditAttributes($field);

  /**
   * Parse given variables to retrieve settings margins.
   * @param $vars array Given variables to check.
   * @return array
   *   Settings margins.
   */
  public function getSettingsMargins($vars);

  /**
   * Build media video content based on paragraph.
   * @param \Drupal\paragraphs\Entity\Paragraph Paragraph to parse.
   * @return array
   *   Renderable media array.
   */
  public function setUpMediaVideoContent(Paragraph $paragraph, $field_name);

  /**
   * Build media array based on media field.
   * @param \Drupal\paragraphs\Entity\Paragraph Paragraph to parse.
   * @return array
   *   Renderable media array.
   */
  public function setUpMediaContent(Paragraph $paragraph);

  /**
   * Build gallery theme with inline template.
   * @param \Drupal\node\Entity\Node $node Gallery entity.
   * @return array
   *   Gallery renderable array.
   */
  public function renderGallerySlider(Node $node);

}
