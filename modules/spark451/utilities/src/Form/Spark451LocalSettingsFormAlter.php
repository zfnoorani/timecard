<?php

namespace Drupal\utilities\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\simplesamlphp_auth\Form\LocalSettingsForm;

/**
 * Class Spark451LocalSettingsFormAlter
 * @package Drupal\utilities\Form
 */
class Spark451LocalSettingsFormAlter extends LocalSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['authentication']['reset_password_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL to reset password'),
      '#default_value' => $this->config('simplesamlphp_auth.settings')
        ->get('reset_password_url'),
      '#required' => TRUE,
      '#description' => $this->t('Specify a URL for users to go to in order to reset their password.'),
    );
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    //
    $this->config('simplesamlphp_auth.settings')
      ->set('reset_password_url', $form_state->getValue('reset_password_url'))
      ->save();
  }
}