<?php

namespace Drupal\utilities\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SearcMenuOption.
 *
 * @package Drupal\utilities\Form
 */
class SearchMenuOption extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'utilities.search_menu_option',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_menu_option';
  }

  /**
   * Retrieves all custom blocks
   */
  private function getCustomBlocks(){
    $blockManager = \Drupal::service('plugin.manager.block');
    $contextRepository = \Drupal::service('context.repository');
    // Get blocks definition
    $definitions = $blockManager->getDefinitionsForContexts($contextRepository->getAvailableContexts());
    foreach($definitions as $bk){
      if($bk['id'] == 'block_content'){
        $cd = $bk['config_dependencies']['content'][0];
        $in = explode(':',$cd);
        if($in[1] == 'html_block'){
          $rt[$bk['admin_label']] = $bk['admin_label'];
        }
      }
    }
    return $rt;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('utilities.search_menu_option');
    $form['options'] = array(
        '#type'  => 'details',
        '#title' => t('Search Options'),
        '#open' => TRUE,
      );
    $form['options']['status'] = array(
        '#type' => 'radios',
        '#title' => t('Modify search UI options'),
        '#required' => TRUE,
        '#default_value' => $config->get('status'),
        '#options' => array('true' => t('On'), 'false' => t('Off'), 'replace' => t('Replace')),
        '#description' => t('Option to turn the search on/off or replace with a block.'),
      );
    $form['replace'] = array(
        '#type'  => 'details',
        '#title' => t('Search Replacement'),
        '#open' => $config->get('status') == 'replace',
      );
    $form['replace']['block'] = [
        '#type' => 'select',
        '#title' => $this->t('Custom Replacement Block'),
        '#required' => $config->get('status') == 'replace',
        '#options' => self::getCustomBlocks(),
        '#description' => $this->t('Select a custom HTML block to replace the search function.'),
        '#default_value' => $config->get('block'),
      ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('utilities.search_menu_option')
      ->set('status', $form_state->getValue('status'))
      ->set('block', $form_state->getValue('block'))
      ->save();
  }

}
