<?php

namespace Drupal\utilities\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SocialFeedsSettingsForm
 * @package Drupal\utilities\Form
 */
class SocialFeedsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'utilities.SocialFeedSettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_feeds_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //
    $config = $this->config('utilities.SocialFeedSettings');
    //
    $form['facebook'] = [
      '#type' => 'details',
      '#title' => $this->t('Facebook'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['facebook']['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook App ID'),
      '#default_value' => $config->get('facebook.app_id'),
    ];
    $form['facebook']['app_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Secret'),
      '#default_value' => $config->get('facebook.app_secret'),
    ];
    $form['facebook']['graph_version'] = [
      '#type' => 'select',
      '#title' => $this->t('Facebook Graph version'),
      '#options' => ['v2.7' => 'v2.7'],
      '#default_value' => $config->get('facebook.graph_version'),
    ];
    $form['facebook']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Access Token'),
      '#default_value' => $config->get('facebook.access_token'),
    ];
    //
    $form['twitter'] = [
      '#type' => 'details',
      '#title' => $this->t('Twitter'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['twitter']['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter ID'),
      '#default_value' => $config->get('twitter.app_id'),
    ];
    $form['twitter']['app_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter API Key'),
      '#default_value' => $config->get('twitter.app_key'),
    ];
    $form['twitter']['app_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter API Secret'),
      '#default_value' => $config->get('twitter.app_secret'),
    ];
    $form['twitter']['app_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter Username'),
      '#default_value' => $config->get('twitter.app_username'),
    ];
    $form['twitter']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter Access Token'),
      '#default_value' => $config->get('twitter.access_token'),
    ];
    $form['twitter']['access_token_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter Access Token Secret'),
      '#default_value' => $config->get('twitter.access_token_secret'),
    ];
    //
    $form['instagram'] = [
      '#type' => 'details',
      '#title' => $this->t('Instagram'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['instagram']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram Token'),
      '#default_value' => $config->get('instagram.access_token'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    //
    $this->config('utilities.SocialFeedSettings')
      ->set('facebook', $form_state->getValue('facebook'))
      ->set('twitter', $form_state->getValue('twitter'))
      ->set('instagram', $form_state->getValue('instagram'))
      ->save();
  }
}