<?php

namespace Drupal\utilities\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class Spark451EventFeedsSettingsForm.
 *
 * @package Drupal\utilities\Form
 */
class Spark451EventFeedsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'utilities.events_feed_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spark451_event_feeds_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('utilities.events_feed_settings');

    $form['events_auto_import'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Auto Import (Cron Execution)'),
      '#default_value' => $config->get('events_auto_import'),
    ];

    $form['events_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Url'),
      '#description' => $this->t('API Url for Events'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('events_api_url'),
    ];

    $form['events_all'] = [
      '#title' => $this->t('Import All Events'),
      '#type' => 'link',
      '#url' => Url::fromRoute('utilities.spark451_event_feeds_consumer_form', array('import' => 'all')),
      '#attributes' => array('class' => array('button'))
    ];

    $form['events_manually'] = [
      '#title' => $this->t('Execute Cron Importer'),
      '#type' => 'link',
      '#url' => Url::fromRoute('utilities.spark451_event_feeds_consumer_form'),
      '#attributes' => array('class' => array('button'))
    ];

    $form['cancelled_events_auto_deport'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Auto Deport (Cron Execution)'),
      '#default_value' => $config->get('cancelled_events_auto_deport'),
    ];

    $form['cancelled_events_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Url'),
      '#description' => $this->t('API Url for Cancelled Events'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('cancelled_events_api_url'),
     ];

     $form['cancelled_events_all'] = [
      '#title' => $this->t('Remove Cancelled Events'),
      '#type' => 'link',
      '#url' => Url::fromRoute('utilities.spark451_event_feeds_remover_form', array('deport' => 'all')),
      '#attributes' => array('class' => array('button'))
     ];

     $form['cancelled_events_manually'] = [
      '#title' => $this->t('Execute Cron Remover'),
      '#type' => 'link',
      '#url' => Url::fromRoute('utilities.spark451_event_feeds_remover_form'),
      '#attributes' => array('class' => array('button'))
     ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('utilities.events_feed_settings')
      ->set('events_api_url', $form_state->getValue('events_api_url'))
      ->set('events_auto_import', $form_state->getValue('events_auto_import'))
      ->set('cancelled_events_api_url', $form_state->getValue('cancelled_events_api_url'))
      ->set('cancelled_events_auto_deport', $form_state->getValue('cancelled_events_auto_deport'))
      ->save();
  }

}
