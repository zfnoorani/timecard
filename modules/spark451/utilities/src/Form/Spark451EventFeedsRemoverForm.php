<?php

namespace Drupal\utilities\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\utilities\Controller\Spark451EventsCancelledBatch as Batch;
use GuzzleHttp\Exception\ClientException;

/**
 * Class Spark451EventFeedsRemoverForm.
 *
 * @package Drupal\utilities\Form
 */
class Spark451EventFeedsRemoverForm extends ConfirmFormBase  {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spark451_event_feeds_remover_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want execute the events remover?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will execute the events remover');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('utilities.spark451_event_feeds_settings_form');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $deport = \Drupal::request()->get('deport');

      \Drupal::service('spark451.event_cancelled.feeds')
        ->feedsEventCancelledQueue($deport)->saveLastRunTime();

      $batch = [
        'title' => $this->t('Deporting events'),
        'operations' => Batch::operations(),
        'finished' => '\Drupal\utilities\Controller\Spark451EventsCancelledBatch::finishedCallBack',
      ];

      batch_set($batch);
      $form_state->setRedirect('utilities.spark451_event_feeds_settings_form');

    } catch (ClientException $exception) {
      watchdog_exception(__METHOD__, $exception);
      drupal_set_message($exception->getMessage());
      $form_state->setRedirect('utilities.spark451_event_feeds_settings_form');
    }
  }
}
