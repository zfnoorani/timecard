<?php

namespace Drupal\utilities\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\profile\Entity\ProfileType;
use Drupal\user\Entity\Role;
use Drupal\user\RoleStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\field\FieldConfigInterface;

/**
 * Class Spark451SAMLMapFieldsForm.
 *
 * @package Drupal\utilities\Form
 */
class Spark451SAMLMapFieldsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityFieldManager definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityFieldManager $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->entityFieldManager = $entity_field_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'utilities.Spark451SAMLMapFieldsForm',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spark451_saml_map_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //
    $config = $this->config('utilities.Spark451SAMLMapFieldsForm');
    $fieldsConfig = $config->get('fields');
    $rolesConfig = $config->get('roles');
    //
    $form['fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fields'),
      '#prefix' => '<div id="fields-wrapper">',
      '#suffix' => '</div>',
    ];
    //
    if (!$max_fields = $form_state->get('max_fields')) {
      //
      if (!$max_fields = $config->get('max_fields')) {
        $max_fields = 1;
      }
      $form_state->set('max_fields', $max_fields);
    }
    //
    $profiles = array_map(function ($profile) {
      return $profile->label();
    }, ProfileType::loadMultiple());
    //
    for ($delta = 0; $delta < $max_fields; $delta++) {
      //
      $form['fields'][$delta]['attribute'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Attribute'),
        '#prefix' => '<div class="field-row-wrapper clearfix">',
        '#default_value' => $fieldsConfig[$delta]['attribute'],
      ];
      //
      $form['fields'][$delta]['profile_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Profile Type'),
        '#options' => $profiles,
        '#empty_option' => $this->t('- Select -'),
        '#ajax' => [
          'callback' => [$this, 'loadProfileFieldsAjax'],
          'wrapper' => "field-name-wrapper-{$delta}",
        ],
        '#default_value' => $fieldsConfig[$delta]['profile_type'],
      ];
      //
      $form['fields'][$delta]['field_name'] = [
        '#type' => 'select',
        '#title' => $this->t('Field Name'),
        '#options' => $this->loadProfileFields($form_state->getValue([
          'fields',
          $delta,
          'profile_type'
        ], $fieldsConfig[$delta]['profile_type'])),
        '#empty_option' => $this->t('- Select -'),
        '#prefix' => "<div id='field-name-wrapper-{$delta}'>",
        '#suffix' => '</div></div>',
        '#default_value' => $fieldsConfig[$delta]['field_name'],
      ];
    }
    //
    $form['fields']['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => [
          'field-row-wrapper',
          'clearfix',
        ],
      ],
    ];
    //
    $form['fields']['actions']['add_field_wrapper'] = [
      '#theme_wrappers' => ['container'],
      'add_field' => [
        '#type' => 'submit',
        '#value' => t('Add another field'),
        '#submit' => ['::addRowSubmit'],
        '#ajax' => [
          'callback' => [$this, 'addRowAjax'],
          'wrapper' => 'fields-wrapper',
        ],
      ],
    ];
    //
    if ($max_fields > 1) {
      //
      $form['fields']['actions']['remove_field_wrapper'] = [
        '#theme_wrappers' => ['container'],
        'remove_field' => [
          '#type' => 'submit',
          '#value' => t('Remove last field'),
          '#submit' => ['::removeRowSubmit'],
          '#ajax' => [
            'callback' => '::addRowAjax',
            'wrapper' => 'fields-wrapper',
          ],
        ],
      ];
    }
    //
    $form['roles'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Roles'),
      '#prefix' => '<div id="roles-wrapper">',
      '#suffix' => '</div>',
    ];
    //
    if (!$max_roles = $form_state->get('max_roles')) {
      //
      if (!$max_roles = $config->get('max_roles')) {
        $max_roles = 1;
      }
      $form_state->set('max_roles', $max_roles);
    }
    //
    $roles = array_map(function ($profile) {
      return $profile->label();
    }, Role::loadMultiple(['faculty', 'staff', 'student']));
    //
    for ($delta = 0; $delta < $max_roles; $delta++) {
      //
      $form['roles'][$delta]['attribute'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Attribute'),
        '#default_value' => $rolesConfig[$delta]['attribute'],
        '#prefix' => '<div class="field-row-wrapper clearfix">',
      ];
      //
      $form['roles'][$delta]['role'] = [
        '#type' => 'select',
        '#title' => $this->t('Role'),
        '#options' => $roles,
        '#default_value' => $rolesConfig[$delta]['role'],
        '#suffix' => '</div>',
      ];
    }
    //
    $form['roles']['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => [
          'field-row-wrapper',
          'clearfix',
        ],
      ],
    ];
    //
    $form['roles']['actions']['add_role_wrapper'] = [
      '#theme_wrappers' => ['container'],
      'add_role' => [
        '#type' => 'submit',
        '#value' => t('Add another role'),
        '#submit' => ['::addRowSubmit'],
        '#ajax' => [
          'callback' => [$this, 'addRowAjax'],
          'wrapper' => 'roles-wrapper',
        ],
      ],
    ];
    //
    if ($max_roles > 1) {
      //
      $form['roles']['actions']['remove_role_wrapper'] = [
        '#theme_wrappers' => ['container'],
        'remove_role' => [
          '#type' => 'submit',
          '#value' => t('Remove last role'),
          '#submit' => ['::removeRowSubmit'],
          '#ajax' => [
            'callback' => '::addRowAjax',
            'wrapper' => 'roles-wrapper',
          ],
        ],
      ];
    }
    //
    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'utilities/saml-map-fields';
    return parent::buildForm($form, $form_state);
  }

  //
  public function removeRowSubmit(array $form, FormStateInterface $form_state) {
    //
    $triggered = $form_state->getTriggeringElement();
    $key = 'max_' . current($triggered['#parents']);
    //
    $max = $form_state->get($key);
    $form_state->set($key, --$max);
    $form_state->setRebuild();
  }

  //
  public function addRowSubmit(array $form, FormStateInterface $form_state) {
    //
    $triggered = $form_state->getTriggeringElement();
    $key = 'max_' . current($triggered['#parents']);
    //
    $max = $form_state->get($key);
    $form_state->set($key, ++$max);
    $form_state->setRebuild();
  }

  //
  public function addRowAjax(array $form, FormStateInterface $form_state) {
    //
    $triggered = $form_state->getTriggeringElement();
    $key = current($triggered['#parents']);
    return $form[$key];
  }

  /**
   * Implements callback for Ajax event on profile type selection.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   *   Color selection section of the form.
   */
  public function loadProfileFieldsAjax(array &$form, FormStateInterface $form_state) {
    //
    $triggered = $form_state->getTriggeringElement();
    //
    $last = end($triggered['#array_parents']);
    //
    $parents = array_map(function ($parent) use ($last) {
      //
      if ($last === $parent) {
        return 'field_name';
      }
      return $parent;
    }, $triggered['#array_parents']);
    //
    $element = NestedArray::getValue($form, $parents);
    //
    $element['#options'] += $this->loadProfileFields($triggered['#value']);
    //
    return $element;
  }

  //
  protected function loadProfileFields($profile_type) {
    //
    if (!$profile_type) {
      return [];
    }
    //
    $instances = array_filter($this->entityFieldManager->getFieldDefinitions('profile', $profile_type), function ($field_definition) {
      return $field_definition instanceof FieldConfigInterface;
    });
    //
    return array_map(function ($instance) {
      return $instance->getLabel();
    }, $instances);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    //
    $config = $this->config('utilities.Spark451SAMLMapFieldsForm');
    array_map(function ($key) use ($config, $form_state) {
      //
      $max = 'max_' . $key;
      //
      $values = $form_state->getValue($key);
      unset($values['actions']);
      //
      $config->set($key, $values)
        ->set($max, $form_state->get($max));
    }, ['fields', 'roles']);
    $config->save();
  }

}
