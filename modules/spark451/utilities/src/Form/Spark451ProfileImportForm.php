<?php

namespace Drupal\utilities\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\field\FieldConfigInterface;
use Drupal\profile\Entity\ProfileType;
use Drupal\Component\Utility\NestedArray;
use Drupal\utilities\Controller\Spark451ProfileImportBatch as Batch;
use Drupal\Core\Config\ConfigFactoryInterface;
use League\Csv\Reader;

/**
 * Class Spark451ProfileImportForm.
 *
 * @package Drupal\utilities\Form
 */
class Spark451ProfileImportForm extends ConfigFormBase {
  /**
   * Drupal\Core\Entity\EntityFieldManager definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Spark451ProfileImportForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityFieldManager $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Create function container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *    Container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'utilities.Spark451ProfileImportForm',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spark451_profile_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the form configuration from yml file.
    $config = $this->config('utilities.Spark451ProfileImportForm');
    $fieldsConfig = $config->get('fields');
    $userConfig = $config->get('user_data');
    // Get profiles.
    $profiles = $this->getProfiles();

    $validators = array(
      'file_validate_extensions' => array('csv'),
    );

    $form['url_zone'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Url'),
      '#prefix' => '<div id="fields-url-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['url_zone']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url'),
      '#description' => $this->t('API url provides data to import'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_url'),
    ];
    $form['url_zone']['cron_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable auto import (Cron execution)'),
      '#default_value' => $config->get('profiles_auto_import'),
    ];

    $form['file_zone'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CSV File'),
      '#prefix' => '<div id="fields-file-wrapper">',
      '#suffix' => '</div>',

    ];
    $form['file_zone']['file_upload'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload a CSV File'),
      '#upload_location' => 'public://imports/csv',
      '#description' => array(
        '#theme' => 'file_upload_help',
        '#description' => $this->t('The CSV file must include a header'),
        '#upload_validators' => $validators,
      ),
      '#upload_validators' => $validators,
    ];
    $form['user_data'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User Information Columns'),
      '#prefix' => '<div id="fields-user-wrapper">',
      '#suffix' => '</div>',


    ];
    $form['user_data']['field_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username Column Name'),
      '#default_value' => $userConfig['field_username'],
    ];
    $form['user_data']['field_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Column Name'),
      '#default_value' => $userConfig['field_email'],
    ];

    $form['fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fields Mapping'),
      '#prefix' => '<div id="fields-wrapper">',
      '#suffix' => '</div>',
    ];
    // Set the max of the fields permitted.
    if (!$max_fields = $form_state->get('max_fields')) {
      if (!$max_fields = $config->get('max_fields')) {
        $max_fields = 1;
      }
      $form_state->set('max_fields', $max_fields);
    }
    for ($delta = 0; $delta < $max_fields; $delta++) {
      $form['fields'][$delta]['attribute'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Column Name'),
        '#prefix' => '<div class="field-row-wrapper clearfix">',
        '#default_value' => $fieldsConfig[$delta]['attribute'],
      ];
      $form['fields'][$delta]['profile_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Profile Type'),
        '#options' => $profiles,
        '#empty_option' => $this->t('- Select -'),
        '#ajax' => [
          'callback' => [$this, 'loadProfileFieldsAjax'],
          'wrapper' => "field-name-wrapper-{$delta}",
        ],
        '#default_value' => $fieldsConfig[$delta]['profile_type'],
      ];
      $form['fields'][$delta]['field_name'] = [
        '#type' => 'select',
        '#title' => $this->t('Field Name'),
        '#options' => $this->loadProfileFields($form_state->getValue([
          'fields',
          $delta,
          'profile_type',
        ], $fieldsConfig[$delta]['profile_type'])),
        '#empty_option' => $this->t('- Select -'),
        '#prefix' => "<div id='field-name-wrapper-{$delta}'>",
        '#suffix' => '</div></div>',
        '#default_value' => $fieldsConfig[$delta]['field_name'],
      ];
    }

    $form['fields']['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => [
          'field-row-wrapper',
          'clearfix',
        ],
      ],
    ];

    $form['fields']['actions']['add_field_wrapper'] = [
      '#theme_wrappers' => ['container'],
      'add_field' => [
        '#type' => 'submit',
        '#value' => t('Add another field'),
        '#submit' => ['::addRowSubmit'],
        '#ajax' => [
          'callback' => [$this, 'addRowAjax'],
          'wrapper' => 'fields-wrapper',
        ],
      ],
    ];
    if ($max_fields > 1) {
      $form['fields']['actions']['remove_field_wrapper'] = [
        '#theme_wrappers' => ['container'],
        'remove_field' => [
          '#type' => 'submit',
          '#value' => t('Remove last field'),
          '#submit' => ['::removeRowSubmit'],
          '#ajax' => [
            'callback' => '::addRowAjax',
            'wrapper' => 'fields-wrapper',
          ],
        ],
      ];
    }
    $form['#tree'] = TRUE;
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    );
    $form['#attached']['library'][] = 'utilities/profile-import-fields';

    return $form;
  }

  /**
   * Get exist profiles created.
   *
   * @return array
   *     variable profiles
   */
  public function getProfiles() {

    $profiles = array_map(function ($profile) {
      return $profile->label();
    }, ProfileType::loadMultiple());
    return $profiles;
  }

  /**
   * Load fields by profile.
   *
   * @param string $profile_type
   *    Profile Type.
   *
   * @return array
   */
  protected function loadProfileFields($profile_type) {
    if (!$profile_type) {
      return [];
    }
    $instances = array_filter(
      $this->entityFieldManager->getFieldDefinitions('profile', $profile_type),
      function ($field_definition) {
        return $field_definition instanceof FieldConfigInterface;
      }
    );
    return array_map(function ($instance) {
      return $instance->getLabel();
    }, $instances);
  }

  /**
   * Load profiles by ajax.
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return mixed
   */
  public function loadProfileFieldsAjax(array &$form,
                                        FormStateInterface $form_state) {
    $triggered = $form_state->getTriggeringElement();
    $last = end($triggered['#array_parents']);
    $parents = array_map(function ($parent) use ($last) {
      if ($last === $parent) {
        return 'field_name';
      }
      return $parent;
    }, $triggered['#array_parents']);
    $element = NestedArray::getValue($form, $parents);
    $element['#options'] += $this->loadProfileFields($triggered['#value']);
    return $element;
  }

  /**
   * Add a new row by ajax.
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return mixed
   */
  public function addRowAjax(array $form, FormStateInterface $form_state) {
    $triggered = $form_state->getTriggeringElement();
    $key = current($triggered['#parents']);
    return $form[$key];
  }

  /**
   * Add new row of fields.
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addRowSubmit(array $form, FormStateInterface $form_state) {
    $triggered = $form_state->getTriggeringElement();
    $key = 'max_fields';
    $max = $form_state->get($key);
    $form_state->set($key, ++$max);
    $form_state->setRebuild();
  }

  /**
   * Remove Row fields.
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function removeRowSubmit(array $form, FormStateInterface $form_state) {

    $triggered = $form_state->getTriggeringElement();
    $key = 'max_fields';
    $max = $form_state->get($key);
    $form_state->set($key, --$max);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $this->saveConfig($form_state);
    $data = [];
    $fid = $form_state->getValue('file_zone');
    $fid = (isset($fid['file_upload'][0]) ? $fid['file_upload'][0] : NULL);
    $user_data = $form_state->getValue('user_data');
    $values['fields'][] = [
      'attribute' => $user_data['field_username'],
      'profile_type' => 'main',
      'field_name' => 'username'
    ];
    $values['fields'][] = [
      'attribute' => $user_data['field_email'],
      'profile_type' => 'main',
      'field_name' => 'mail'
    ];
    $data_url = \Drupal::service('spark451.profile.import')
      ->getData();

    if ($fid) {
      $data = $this->getDataFromCsvFile($fid, NULL);
    }
    elseif ($data_url) {
      $data = $this->getDataFromCsvFile(NULL, $data_url);
    }
    if (!empty($data)) {
      $data_mapped = $this->mapFields($data, $values);
      \Drupal::service('spark451.profile.import')
        ->processProfileImport($data_mapped);

      try {
        $batch = [
          'title' => $this->t('Importing Profiles'),
          'operations' => Batch::operations(),
          'finished' => '\Drupal\utilities\Controller\Spark451ProfileImportBatch::finishedCallBack',
        ];
        batch_set($batch);
        $form_state->setRedirect('utilities.profile_import');
      }
      catch (ClientException $exception) {
        watchdog_exception(__METHOD__, $exception);
        drupal_set_message($exception->getMessage());
        $form_state->setRedirect('utilities.profile_import');
      }
    }
  }

  /**
   * Get all data from csv file.
   *
   * @param $fid
   * @return array
   */
  private function getDataFromCsvFile($fid = NULL, $data_url = NULL) {
    $profiles = [];
    if ($fid) {
      $file = \Drupal::entityTypeManager()
        ->getStorage('file')->load($fid);
      if ($file) {
        $uri = $file->getFileUri();
        $handle = fopen($uri, "r");
        // Read the csv file.
        $csv = Reader::createFromPath($uri);
        $headers = $csv->fetchOne();
        $data = $csv->fetchAll();
        unset($data[0]);
        $profiles['headers'] = $headers;
        $profiles['data'] = $data;
        fclose($handle);
      }
    }
    elseif ($data_url) {
      // Read the csv File form route.
      foreach ($data_url as $key => $row) {
        foreach ($row as $item) {
          $row_data[] = $item;
        }
        $profiles['data'][] = $row_data;
        $headers = array_keys($row);
        $row_data = NULL;
      }
      $profiles['headers'] = $headers;
    }

    return $profiles;
  }

  /**
   * Map All fields with form fields.
   *
   * @param $data
   * @param $values
   * @return array
   */
  private function mapFields($data, $values) {

    $fields = $values['fields'];
    $data_profile = [];
    foreach ($data['data'] as $delta => $row) {
      foreach ($row as $index => $value) {

        $header = $data['headers'][$index];
        foreach ($fields as $field) {
          if (!empty($field['attribute']) && $field['attribute'] === $header) {
            $data_profile[$delta][$field['profile_type']][$field['field_name']] = $value;
          }
        }
      }
    }

    return $data_profile;
  }

  /**
   * Function save field values into the configuration.
   *
   * @param  $form_state
   *    Form state values.
   */
  private function saveConfig($form_state) {
    $config      = $this->config('utilities.Spark451ProfileImportForm');
    $values      = $form_state->getValues();
    $fields      = (isset($values['fields']) ? $values['fields'] : NULL);
    $userdata    = (isset($values['user_data']) ? $values['user_data'] : NULL);
    $urldata     = (isset($values['url_zone']) ? $values['url_zone'] : NULL);
    $cron_import = (isset($urldata['cron_check']) ? $urldata['cron_check'] : NULL);
    $api_url     = (isset($urldata['url']) ? $urldata['url'] : NULL);

    unset($fields['actions']);

    $config->set('fields', $fields);
    $config->set('user_data', $userdata);
    $config->set('api_url', $api_url);
    $config->set('profiles_auto_import', $cron_import);
    $config->set('max_fields', $form_state->get('max_fields'));
    $config->save();
  }

}
