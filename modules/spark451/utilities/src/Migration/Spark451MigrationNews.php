<?php

namespace Drupal\utilities\Migration;

use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate_plus\Event\MigrateEvents as MigratePlusEvents;
use Drupal\migrate_plus\Event\MigratePrepareRowEvent;
use Drupal\utilities\File\Spark451Images;
use Drupal\migrate\Event\MigratePreRowSaveEvent;


class Spark451MigrationNews  implements EventSubscriberInterface  {

  const FILE_DESTINATION = 'public://articles';
  const FILE_SOURCE = 'http://northcentralcollege.edu/';
  use Spark451Images;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    $events[MigrateEvents::PRE_ROW_SAVE] = ['onPreRowSave'];
    $events[MigratePlusEvents::PREPARE_ROW] = ['onPrepareRow'];

    return $events;
  }

  /**
   * Implement function onPreRowSave
   * @param \Drupal\migrate\Event\MigratePreRowSaveEvent $event
   */
  public function onPreRowSave(MigratePreRowSaveEvent $event) {

    if ('d6_node_news_beta_1' === $event->getMigration()->id()) {
      // Get the row data
      $row = $event->getRow();
      $query_user = null;

      $timestamp = strtotime('01-09-2012');

      if ($timestamp <= $row->getSourceProperty('created')) {

        // get the current image in the node
        // Uncomment this line for add images
        //$image_fid = $row->getSourceProperty('field_newsimage')[0]['fid'];

        // Connect to td6 database
        $connection = \Drupal\Core\Database\Database::setActiveConnection('migrate');
        //get the file
        // Uncomment this line for add images
        //$query_file = db_query("select filepath, filename from files where fid='" . $image_fid . "'");

        if ( $user_uid = $row->getSourceProperty('node_uid')) {
          $query_user = db_query("select name from users where uid ='" . $user_uid . "' ");
        }

        // Connect to the d8 database
        $connection = \Drupal\Core\Database\Database::setActiveConnection();

        // Uncomment this line for add images
        //if ($image = $this->prepare_images($query_file)) {
          // Set the new image created at the d8 site
          //$row->setDestinationProperty('field_image', $image);
        //}

        if ($query_user) {
          $user_new_uid = 1;
          foreach ($query_user as $result) {

            $name = $result->name;
            // load user
            $user = user_load_by_name($name);
            if ($user) {
              //get the new user uid
              $user_new_uid = $user->uid->getValue()[0]['value'];
            }

          }
          //set user to the node
          $row->setDestinationProperty('uid', $user_new_uid);
        }

        $body = array(
          'value' => $row->getSourceProperty('body'),
          'format' => 'full_html'
        );
        $row->setDestinationProperty('body', array($body));

      }
    }
  } // end function

  /**
   * React to a new row.
   *
   * @param \Drupal\migrate_plus\Event\MigratePrepareRowEvent $event
   *   The prepare-row event.
   *
   */
  public function onPrepareRow(MigratePrepareRowEvent $event) {

    if ('d6_node_news_beta_1' === $event->getMigration()->id()) {
      $row = $event->getRow();
      // Set time validation
      $timestamp = strtotime('01-09-2012');


      if ($timestamp <= $row->getSourceProperty('created')) {
        $row->setDestinationProperty('type', 'article');
        $image_fid = $row->getSourceProperty('field_newsimage')[0]['fid'];

        if (!empty($image_fid)) {
          // Connection to Drupal 6 database
          $connection = \Drupal\Core\Database\Database::setActiveConnection('migrate');
          // Query for files
          $query_file = db_query("select filepath, filename from files where fid='" . $image_fid . "'");
          // Query for tag terms
          $query_tags = db_query("SELECT term_node.nid, term_data.name, term_node.tid, term_data.vid
                              FROM term_node
                              INNER JOIN term_data
                              ON term_node.tid=term_data.tid
                              AND term_node.nid = '" . $row->getSourceProperty('nid') . "'
                              AND ( term_data.vid = '4' OR term_data.vid = '3')
                             ");
          // Query for category terms
          $query_category = db_query("SELECT term_node.nid, term_data.name, term_node.tid, term_data.vid
                              FROM term_node
                              INNER JOIN term_data
                              ON term_node.tid=term_data.tid
                              AND term_node.nid = '" . $row->getSourceProperty('nid') . "'
                              AND term_data.vid = '6'
                             ");

          // Connection to Drupal 8 database
          $connection = \Drupal\Core\Database\Database::setActiveConnection();

          // Attach tags to the row
          if ($tags = $this->prepare_tags($query_tags, 'tags')) {
            $row->setDestinationProperty('field_tags', $tags);
          }
          // Attach categories to the row
          if ($categories = $this->prepare_categories($query_category, 'content_categories')) {
            $row->setDestinationProperty('field_content_categories', $categories);
          }
          \Drupal::logger(__METHOD__)
            ->debug("Imported nid = " . $row->getSourceProperty('nid'));
        }
      }
      else {

          // Skip the row.
            $row->setIdMap(array(MigrateIdMapInterface::STATUS_IGNORED));
            $row->rehash();
            return false;
      }
    }

  } // end function

  /**
   * Function transform the tags query to format tags
   * @param $query_tags
   *    Data result of query
   * @param $vocabulary_name
   *    Machine name of the vocabulary
   * @return $tags
   *    Formated array('target_id=> value')
   *
   */
  function prepare_tags($query_tags, $vocabulary_name) {

    $tags = array();
    if (!empty($query_tags)) {

      foreach ($query_tags as $result) {

        $tag_name = $result->name;
        $query = \Drupal::entityQuery('taxonomy_term');
        $query->condition('vid', $vocabulary_name);
        $query->condition('name', $tag_name);
        $tids = $query->execute();

        if (!empty($tids)) {

          $tags[] = array('target_id' => current($tids));

        }
        else {

          $term = \Drupal\taxonomy\Entity\Term::create(array(
            'parent' => array(),
            'name' => $tag_name,
            'vid' => 'tags',
          ))->save();
          $tags[] = array('target_id' => $term->tid);

        }
      }
    }

  return $tags;

  } // end function

  /**
   * Function transform the category terms query to format categories
   * @param $query_categories
   *    Data result of the query
   * @param $vocabulary_name
   *    Vocabulary name
   * @return $categories
   *    Formated list of categories
   */
  function prepare_categories($query_categories, $vocabulary_name) {

    $categories = array();

    if (!empty($query_categories)) {

      foreach ($query_categories as $result) {
        // Get tag name with new format
        $tag_name = $this->makeTitle($result->name);
        // get the tid of the term by name
        $query = \Drupal::entityQuery('taxonomy_term');
        $query->condition('vid', $vocabulary_name);
        $query->condition('name', $tag_name);
        $tids = $query->execute();

        if (!empty($tids)) {
          // ad the tid to the list
          $categories[] = array('target_id' => current($tids));

        }
      }
    }
    return $categories;
  }

  /**
   * Get files form the query and save them in the other site.
   * @param $query_file
   * @return array
   */
  function prepare_images($query_file) {
    $image = array();
    foreach ($query_file as $result) {
      $filepath = $result->filepath;
      if ($filepath) {
        // Save the new image to the d8 site
        if ($saved_file = $this->process(self::FILE_SOURCE . $filepath, self::FILE_DESTINATION)) {
          $image[] = array(
            'target_id' => $saved_file->id(),
            'alt' => '',
            'title' => '',
            'width' => '',
            'height' => ''
          );
        }
      }
    }

    return $image;

  } // end function

  /**
   * Function convert titles to words with first Capital letter. Example "Academics Test"
   * @param $title
   *    Word to be format
   * @return string
   */
  function makeTitle($title) {
    $str = ucwords($title);
    $exclude = 'a,an,the,for,and,nor,but,or,yet,so,such,as,at,around,by,after,along,for,from,of,on,to,with,without';
    $excluded = explode(",", $exclude);
    foreach ($excluded as $noCap) {
      $str = str_replace(ucwords($noCap), strtolower($noCap), $str);
    }
    return ucfirst($str);
  } // end function

}

