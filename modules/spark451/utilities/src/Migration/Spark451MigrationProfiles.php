<?php

namespace Drupal\utilities\Migration;


use Drupal\migrate\MigrateException;
use Drupal\profile\Entity\Profile;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate_plus\Event\MigrateEvents as MigratePlusEvents;
use Drupal\migrate_plus\Event\MigratePrepareRowEvent;


class Spark451MigrationProfiles implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    $events[MigrateEvents::POST_ROW_SAVE] = ['onPostRowSave'];
    $events[MigratePlusEvents::PREPARE_ROW] = ['onPrepareRow'];
    return $events;
  }

  /**
   * Implement function onPostRowSave
   * Create profiles and set some properties.
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   */
  public function onPostRowSave(MigratePostRowSaveEvent $event) {

    $row = $event->getRow();

    $user = \Drupal\user\Entity\User::load($event->getDestinationIdValues()[0]);
//    \Drupal::logger(__METHOD__)->debug("<pre>".print_r($row->getSourceProperty('keys')[0], 1). "</pre>");
    if (!empty($user)) {
      if ($row->getSourceProperty('keys')[0] == 'HRPER->XIT.FACSTAFF.MERLIN.LOGIN') {
        // Load profile based on account id.
        if (!$profile_main = \Drupal::entityTypeManager()
          ->getStorage('profile')
          ->loadByUser($user, 'main')
        ) {
          $profile_main = Profile::create([
            'type' => 'main',
            'uid' => $user->id(),
          ]);

          // Profile Properties
          $profile_main->field_profile_links->setValue([
            array(
              'uri' => $row->getSourceProperty('XFI.FACULTY.WEB.PAGE'),
              'title' => 'Website',
              'attributes' => array()
            )
          ]);
          $profile_main->field_profile_title->value = $row->getSourceProperty('HRPER->XIT.JOB.TITLE');
          $profile_main->field_profile_first_name->value = $row->getSourceProperty('PERSON->FIRST.NAME');
          $profile_main->field_profile_last_name->value = $row->getSourceProperty('PERSON->LAST.NAME');
          $profile_main->field_profile_department->value = $row->getSourceProperty('FAC.DEPTS');
          $profile_main->field_profile_middle_name->value = $row->getSourceProperty('PERSON->MIDDLE.NAME');
          $profile_main->field_profile_phone->value = $row->getSourceProperty('HRPER->HRP.PRI.CAMPUS.EXTENSION');
          $profile_main->field_profile_office_location->value = $row->getSourceProperty('HRPER->XIT.CBORD.PRI.BLDG.NAME') . ', ' . $row->getSourceProperty('HRPER->HRP.PRI.CAMPUS.OFFICE');

          $profile_main->save();
          $username = $row->getSourceProperty('HRPER->XIT.FACSTAFF.MERLIN.LOGIN');

          $image_data = file_get_contents('http://northcentralcollege.edu/sites/default/files/staff/'.$username. '.jpg');
          if (!empty($image_data)) {
            $image_file = file_save_data($image_data, 'public://faculty/' . $username . '.jpg', FILE_EXISTS_REPLACE);

            $user->user_picture =  array(
              'target_id' => $image_file->id(),
            );
          }
          $user->status->value = 1;
          $user->addRole('faculty');
          $user->save();
        }

        if (!$profile_faculty = \Drupal::entityTypeManager()
          ->getStorage('profile')
          ->loadByUser($user, 'faculty')
        ) {
          $profile_faculty = Profile::create([
            'type' => 'faculty',
            'uid' => $user->id(),
          ]);
          $profile_faculty->field_profile_credentials->setValue(['value' => $row->getSourceProperty('XFI.GET.FACCD')]);
          $profile_faculty->save();

        }
      }
      else {
        if ($row->getSourceProperty('keys')[0] === 'Username') {

          if (!$profile_staff = \Drupal::entityTypeManager()
              ->getStorage('profile')
              ->loadByUser($user, 'main') &&
            !$profile_staff = \Drupal::entityTypeManager()
              ->getStorage('profile')
              ->loadByUser($user, 'faculty')
          ) {

            $username = $row->getSourceProperty('Username');
            $image_data = file_get_contents('http://northcentralcollege.edu/sites/default/files/staff/'.$username. '.jpg');
            if (!empty($image_data)) {
              $image_file_staff = file_save_data($image_data, 'public://staff/' . $username . '.jpg', FILE_EXISTS_REPLACE);
              if (is_object($image_file_staff)) {
                $user->user_picture = array(
                  'target_id' => $image_file_staff->id(),
                );
              }
            }


            $user->status->value = 1;
            $user->addRole('staff');
            $user->save();

            $profile_staff = Profile::create([
              'type' => 'main',
              'uid' => $user->id(),
            ]);

            $profile_staff->field_profile_first_name->value = $row->getSourceProperty('First_Name');
            $profile_staff->field_profile_last_name->value = $row->getSourceProperty('Last_Name');
            $profile_staff->field_profile_department->value = $row->getSourceProperty('Dept_Desc');

            $profile_staff->save();

          }
        }
      }
    }

  }
  /**
   * React to a new row.
   *
   * @param \Drupal\migrate_plus\Event\MigratePrepareRowEvent $event
   *   The prepare-row event.
   *
   * @throws \Drupal\migrate\MigrateSkipRowException
   *
   */
  public function onPrepareRow(MigratePrepareRowEvent $event) {

    $row = $event->getRow();

    if ($row->getSourceProperty('type') === 'faculty_bio') {

      $mail = NULL;
      $username = $row->getSourceProperty('field_bio_fac_mem_id');
      $username = $username[0]['value'];

      \Drupal::logger(__METHOD__)->debug("<pre>".print_r($row,true)."/<pre>");
      if (!empty($username)) {
        $user_updated = user_load_by_name($username);
        if (!$user_updated) {
          $connection = \Drupal\Core\Database\Database::setActiveConnection('migrate');
          $query = db_query("select mail from users where name='" . $username . "'");

          foreach ($query as $result) {
            $mail = $result->mail;
          }
          $connection = \Drupal\Core\Database\Database::setActiveConnection();
          if (!empty($mail)) {

            $user = \Drupal\user\Entity\User::create();

            $image_data = file_get_contents('http://northcentralcollege.edu/sites/default/files/staff/'.$username. '.jpg');
            if (!empty($image_data)) {
              $image_file = file_save_data($image_data, 'public://faculty/' . $username . '.jpg', FILE_EXISTS_REPLACE);

              $user->user_picture =  array(
                'target_id' => $image_file->id(),
              );
            }

            $user->enforceIsNew();
            $user->setEmail($mail);
            $user->setUsername($username);
            $user->activate();
            $user->addRole('faculty');
            $user->save();
            \Drupal::logger(__METHOD__)->debug("User Created by Migrations");
            // MAIN PROFILE
            if (!$profile_main = \Drupal::entityTypeManager()
              ->getStorage('profile')
              ->loadByUser($user, 'main')
            ) {
              $profile_main = Profile::create([
                'type' => 'main',
                'uid' => $user->id(),
              ]);

              if (!empty($extension)) {
                $profile_main->field_profile_phone->value('630-637-' . $extension);
              }
              //$profile_main->field_profile_award->value = $degree;
              $profile_main->field_profile_department->value = $row->getSourceProperty('field_fac_bio_mem_dept')[0]['value'];
              $profile_main->field_profile_facebook->value = $row->getSourceProperty('field_fac_bio_pro_website')[0]['value'];
              $profile_main->field_profile_linkedin->value = $row->getSourceProperty('field_fac_bio_linkedin')[0]['value'];
              $profile_main->field_profile_twitter->value = $row->getSourceProperty('field_fac_bio_twitter')[0]['value'];
              $profile_main->field_profile_bio->value = $row->getSourceProperty('body');
              $profile_main->field_profile_links->setValue([
                array(
                  'uri' => $row->getSourceProperty('field_fac_bio_pro_website')[0]['value'],
                  'title' => 'Website',
                  'attributes' => array()
                )
              ]);
              $profile_main->save();
            }
            // FACULTY PROFILE
            if (!$profile_faculty = \Drupal::entityTypeManager()
              ->getStorage('profile')
              ->loadByUser($user, 'faculty')
            ) {
              $profile_faculty = Profile::create([
                'type' => 'faculty',
                'uid' => $user->id(),
              ]);

              $credentials = $row->getSourceProperty('field_fac_bio_credentials');
              if (!empty($credentials)) {
                $profile_faculty->field_profile_credentials->setValue($credentials);
              }
              else {
                $profile_faculty->field_profile_credentials->setValue('');
              }

              $scholarship = $row->getSourceProperty('field_fac_bio_select_scholarship');
              if (!empty($scholarship)) {
                  $profile_faculty->field_profile_scholarship->setValue($scholarship);
              }
              $area_interest = $row->getSourceProperty('field_fac_bio_area_interest');
              if (!empty($area_interest)) {
                  $profile_faculty->field_profile_interests->setValue($area_interest);
              }
              $courses = $row->getSourceProperty('field_fac_bio_course_taught');
              if (!empty($courses)) {
                $profile_faculty->field_profile_courses->setValue($courses);
              }
              $profile_faculty->save();
            }
          }
        }
        else {
          \Drupal::logger(__METHOD__)->debug("User Exist, Let update the user");

          $node_id = $row->getSourceProperty('nid');
          $image_fid = $row->getSourceProperty('field_fac_bio_image')[0]['fid'];
          $filepath = '';
          $degree = '';
          $extension = '';
          $title = '';

          $connection = \Drupal\Core\Database\Database::setActiveConnection('migrate');
          $query = db_query("select filepath from files where fid='" . $image_fid . "'");

          foreach ($query as $result) {
            $filepath = 'http://northcentralcollege.edu/'. $result->filepath;
          }

          $query = db_query("select * from feeds_data_faculty_merged where hrper_xit_facstaff_merlin_login='" . $username . "'");
          foreach ($query as $result) {
            $degree = $result->xfi_get_faccd;
            $extension = $result->hrper_hrp_pri_campus_extension;
            $title = $result->hrper_xit_job_title;
          }

          $connection = \Drupal\Core\Database\Database::setActiveConnection();

          if (empty($filepath)) {
            $filepath = 'http://northcentralcollege.edu/sites/default/files/staff/' . $username . '.jpg';
          }
          $image_data = file_get_contents($filepath);
          if (!empty($image_data)) {
            $image_file = file_save_data($image_data,
              'public://faculty/' . $username . '.jpg', FILE_EXISTS_REPLACE);

            $user_updated->user_picture = array(
              'target_id' => $image_file->id(),
            );
          }
          $user_updated->save();

          if ($profile_main = \Drupal::entityTypeManager()
            ->getStorage('profile')
            ->loadByUser($user_updated, 'main')
          ) {

              if (!empty($extension)) {
                $profile_main->field_profile_phone->value = '630-637-' . $extension;
              }
              $profile_main->field_profile_title->value = $title;
              $profile_main->field_profile_award->value = $degree;

              $profile_main->field_profile_department->value = $row->getSourceProperty('field_fac_bio_mem_dept')[0]['value'];

              $facebook = $row->getSourceProperty('field_fac_bio_facebook')[0]['value'];
              if (!strpos($facebook, 'http://')){
              $facebook = 'http://'. $facebook;
              }
              $profile_main->field_profile_facebook->value = $facebook;

              $linkedin = $row->getSourceProperty('field_fac_bio_linkedin')[0]['value'];
              if (!strpos($facebook, 'http://')){
                $linkedin = 'http://'. $linkedin;
              }
              $profile_main->field_profile_linkedin->value = $linkedin;

              $twitter = $row->getSourceProperty('field_fac_bio_twitter')[0]['value'];
              if (!strpos($facebook, 'http://')){
                $twitter = 'http://'. $twitter;
              }
              $profile_main->field_profile_twitter->value = $twitter;

              $profile_main->field_profile_bio->value = $row->getSourceProperty('body');
              $profile_main->field_profile_links->setValue([
                array(
                  'uri' => $row->getSourceProperty('field_fac_bio_pro_website')[0]['value'],
                  'title' => 'Website',
                  'attributes' => array()
                )
              ]);
              $profile_main->save();
            }
            if ($profile_faculty = \Drupal::entityTypeManager()
              ->getStorage('profile')
              ->loadByUser($user_updated, 'faculty')
            ) {

              $credentials = $row->getSourceProperty('field_fac_bio_credentials');
              if (!empty($credentials)) {
                $profile_faculty->field_profile_credentials->setValue($credentials);
              }
              else {
                $profile_faculty->field_profile_credentials->setValue('');
              }
              $scholarship = $row->getSourceProperty('field_fac_bio_select_scholarship');
              if (!empty($scholarship)) {
                $profile_faculty->field_profile_scholarship->setValue($scholarship);
              }
              $area_interest = $row->getSourceProperty('field_fac_bio_area_interest');
              if (!empty($area_interest)) {
                $profile_faculty->field_profile_interests->setValue($area_interest);
              }
              $courses = $row->getSourceProperty('field_fac_bio_course_taught');
              if (!empty($courses)) {
                $profile_faculty->field_profile_courses->setValue($courses);
              }
              $profile_faculty->save();
            }
          }
        }

    }

    if ($row->getSourceProperty('keys')[0] == 'HRPER->XIT.FACSTAFF.MERLIN.LOGIN') {
      $user = $row->getSourceProperty('HRPER->XIT.FACSTAFF.MERLIN.LOGIN');
       if (user_load_by_name($user)) {
         \Drupal::logger(__METHOD__)->debug("User exist");
         return false;
      }
    }
    if ($row->getSourceProperty('keys')[0] == 'Username') {
      $user = $row->getSourceProperty('Username');
      if (user_load_by_name($user)) {
        \Drupal::logger(__METHOD__)->debug("User exist");
        return false;
      }
    }
    return false;
  }

}

