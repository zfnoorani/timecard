<?php
/**
 * @file Contains Spark451TwigExtension class.
 */

namespace Drupal\utilities\TwigExtension;

use Drupal\utilities\Theme\Spark451ThemerInterface;
use Twig_SimpleTest;
use Drupal\Core\Template\TwigExtension;
use Drupal\Core\Render\RendererInterface;

/**
 * Class Spark451TwigExtension
 * @package Drupal\utilities\TwigExtension
 */
class Spark451TwigExtension extends TwigExtension {

  /**
   * @var \Drupal\utilities\Spark451ThemerInterface
   */
  protected $spark451Themer;

  /**
   * All theme templates from current theme.
   * @var array
   */
  protected $templates;

  /**
   * Setter injection to define "spark451.themer" service.
   * @param \Drupal\utilities\Theme\Spark451ThemerInterface $spark451_themer
   */
  public function setSpark451Themer(Spark451ThemerInterface $spark451_themer) {
    $this->spark451Themer = $spark451_themer;
    $this->templates = drupal_find_theme_templates($this->spark451Themer->getCompleteThemeRegistry(), '.html.twig', $this->spark451Themer->getActiveTheme()->getPath());
  }

  /**
   * Double check given value to be string.
   * @param $value string Given parameter to check
   * @return string
   *   String value.
   */
  protected function forceString($value) {
    // Value should be string value.
    return (is_string($value)) ? $value : '';
  }

  /**
   * Build template machine name based on template name.
   * @param $template string Given template name.
   * @return string
   *   Template machine name
   */
  protected function buildMachineName($template) {
    // Value should be string value.
    $template = $this->forceString($template);
    // Get template machine name.
    return str_replace('-', '_', $template);
  }

  /**
   * {@inheritdoc}
   */
  public function getTests() {
    return [
      new Twig_SimpleTest('ondisk', [$this, 'onDisk']),
    ];
  }

  /**
   * Verify if given template name exists on active theme.
   * @param $value String template name to check.
   * @return boolean
   *   TRUE when template already exists, otherwise false.
   */
  public function onDisk($template) {
    // Get template machine name.
    $key = $this->buildMachineName($template);
    // Check if current template exists.
    return array_key_exists($key, $this->templates);
  }

  /**
   * Get path to given template name.
   * @param $template string Template name to check.
   * @return string
   *   Path to template, otherwise NULL.
   */
  public function templatePath($template){
    // Check that current template already exists.
    if($this->onDisk($template)) {
      // Get template machine name.
      $key = $this->buildMachineName($template);
      // Return path template.
      return base_path() . $this->templates[$key]['path'] . '/';
    }
    // If it fails then return NULL.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions()
  {
    return [
      new \Twig_SimpleFunction('template_path', array($this, 'templatePath')),
      new \Twig_SimpleFunction('render_menu', array($this, 'renderMenu'))
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'spark451.twig.extension';
  }

  public function renderMenu($menuName) {
    /** @var $menuTree */
    $menuTree = \Drupal::menuTree();
    /** @var $treeParameters \Drupal\Core\Menu\MenuLinkTreeInterface */
    $treeParameters = $menuTree->getCurrentRouteMenuTreeParameters($menuName);
    /** @var $menu \Drupal\Core\Menu\MenuLinkTreeInterface */
    $menu = $menuTree->load($menuName, $treeParameters);
    /** @var $treeElementLink \Drupal\Core\Menu\MenuLinkTreeInterface */
    $treeElementLink = $menuTree->transform($menu, [
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ]);
    /** @var $toRender */
    $toRender = $menuTree->build($treeElementLink);

    return [
      '#markup' => \Drupal::service('renderer')->render($toRender)
    ];
  }
}
