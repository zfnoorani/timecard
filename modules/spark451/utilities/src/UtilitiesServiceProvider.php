<?php

/**
 * @file
 * Contains \Drupal\utilities\UtilitiesServiceProvider.
 */

namespace Drupal\utilities;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\utilities\Api\Spark451SimpleSAMLphpAuth;
use Drupal\utilities\Cron\Spark451Cron;
use Drupal\utilities\Entity\Spark451EntityOperations;

/**
 * Class UtilitiesServiceProvider
 * @package Drupal\utilities
 */
class UtilitiesServiceProvider extends ServiceProviderBase {

  /**
   * Overwrite class of given service name.
   */
  public function setUpClass(ContainerBuilder $container, $id, $class) {
    //
    if ($container->hasDefinition($id)) {
      //
      $container->getDefinition($id)
        ->setClass($class);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    //
    $this->setUpClass($container, 'simplesamlphp_auth.manager', Spark451SimpleSAMLphpAuth::class);
    //
    $this->setUpClass($container, 'cron', Spark451Cron::class);
    //
    $this->setUpClass($container, 'workbench_moderation.entity_operations', Spark451EntityOperations::class);
  }
}
