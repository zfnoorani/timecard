'use strict';

/** Regular npm dependendencies */
var del = require('del');
var gulp = require('gulp');
var replace = require('gulp-replace');

/** Cleaning theme folder */
gulp.task('clean-theme', function() {
  return del.sync([
    './themes/spark451/ncc/css/',
    './themes/spark451/ncc/css/**',
    './themes/spark451/ncc/icons/',
    './themes/spark451/ncc/icons/**',
    './themes/spark451/ncc/images/',
    './themes/spark451/ncc/images/**',
    './themes/spark451/ncc/js/vendor/',
    './themes/spark451/ncc/js/vendor/**',
    './themes/spark451/ncc/ng2_ncc/components/',
    './themes/spark451/ncc/ng2_ncc/components/**',
    './themes/spark451/ncc/ng2_ncc/program_finder/',
    './themes/spark451/ncc/ng2_ncc/program_finder/**',
    './themes/spark451/ncc/ng2_ncc/social_feed/',
    './themes/spark451/ncc/ng2_ncc/social_feed/**',
    './themes/spark451/ncc/ng2_ncc/stack/',
    './themes/spark451/ncc/ng2_ncc/stack/**',
    './themes/spark451/ncc/templates/partial/',
    './themes/spark451/ncc/templates/partials/**'
  ]);
});

/** Copy Styleguide Base */
gulp.task('copy-styleguide-base', ['clean-theme'], function() {
  return gulp.src([
      '!./themes/spark451/styleguide/drupal-theme/images/demo',
      '!./themes/spark451/styleguide/drupal-theme/images/demo/**',
      '!./themes/spark451/styleguide/drupal-theme/js/vendor/hammer.js',
      '!./themes/spark451/styleguide/drupal-theme/js/vendor/picturefill.js',
      '!./themes/spark451/styleguide/drupal-theme/js/vendor/picturefill.min.js',
      '!./themes/spark451/styleguide/drupal-theme/js/vendor/svg4everybody.js',
      '!./themes/spark451/styleguide/drupal-theme/layout/',
      '!./themes/spark451/styleguide/drupal-theme/layout/**',
      '!./themes/spark451/styleguide/drupal-theme/partials/',
      '!./themes/spark451/styleguide/drupal-theme/partials/**',
      '!./themes/spark451/styleguide/drupal-theme/README.md',
      './themes/spark451/styleguide/drupal-theme/**'
    ])
    .pipe(gulp.dest('./themes/spark451/ncc/'));
});

/** Copy Styleguide Partials */
gulp.task('copy-styleguide-partials', ['clean-theme'], function() {
  return gulp.src([
      './themes/spark451/styleguide/drupal-theme/partials/**'
    ])
    .pipe(gulp.dest('./themes/spark451/ncc/templates/partials/'));
});

/** Copy Styleguide Components */
gulp.task('copy-ng2-components', ['clean-theme'], function() {
  return gulp.src([
      './themes/spark451/styleguide/ng2_ncc/**'
    ])
    .pipe(replace('pdb-styleguide', 'pdb'))
    .pipe(gulp.dest('./themes/spark451/ncc/ng2_ncc/'));
});

/** The default task */
gulp.task('default', [
  'clean-theme',
  'copy-styleguide-base',
  'copy-styleguide-partials',
  'copy-ng2-components'
]);
