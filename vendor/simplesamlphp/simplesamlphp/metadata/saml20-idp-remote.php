<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */

/*
 * Guest IdP. allows users to sign up and register. Great for testing!
 */
$metadata['https://openidp.feide.no'] = array(
	'name' => array(
		'en' => 'Feide OpenIdP - guest users',
		'no' => 'Feide Gjestebrukere',
	),
	'description'          => 'Here you can login with your account on Feide RnD OpenID. If you do not already have an account on this identity provider, you can create a new one by following the create new account link and follow the instructions.',

	'SingleSignOnService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => 'c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb'
);

//
$metadata['https://pingone.com/idp/spark451'] = array (
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
	'name' => array(
		'en' => 'PingOne.com - Spark451 Inc',
	),
	'entityid' => 'https://pingone.com/idp/spark451',
	'contacts' =>
		array (
		),
	'metadata-set' => 'saml20-idp-remote',
	'SingleSignOnService' =>
		array (
			0 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
					'Location' => 'https://sso.connect.pingidentity.com/sso/idp/SSO.saml2?idpid=eb115ac4-aa99-46a4-9518-4b86fcc4ed7a',
				),
			1 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
					'Location' => 'https://sso.connect.pingidentity.com/sso/idp/SSO.saml2?idpid=eb115ac4-aa99-46a4-9518-4b86fcc4ed7a',
				),
		),
	'SingleLogoutService' =>
		array (
			0 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
					'Location' => 'https://sso.connect.pingidentity.com/sso/SLO.saml2',
				),
			1 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
					'Location' => 'https://sso.connect.pingidentity.com/sso/SLO.saml2',
				),
		),
	'ArtifactResolutionService' =>
		array (
		),
	'NameIDFormats' =>
		array (
		),
	'keys' =>
		array (
			0 =>
				array (
					'encryption' => false,
					'signing' => true,
					'type' => 'X509Certificate',
					'X509Certificate' => '
                        MIIDTjCCAjagAwIBAgIGAVZXt/neMA0GCSqGSIb3DQEBCwUAMGgxCzAJBgNVBAYTAlVTMQswCQYD
                        VQQIEwJDTzEPMA0GA1UEBxMGRGVudmVyMRYwFAYDVQQKEw1QaW5nIElkZW50aXR5MSMwIQYDVQQD
                        ExpodHRwc3BpbmdvbmVjb21pZHBzcGFyazQ1MTAeFw0xNjA4MDQyMjQxMzBaFw0xOTA4MDQyMjQx
                        MzBaMGgxCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDTzEPMA0GA1UEBxMGRGVudmVyMRYwFAYDVQQK
                        Ew1QaW5nIElkZW50aXR5MSMwIQYDVQQDExpodHRwc3BpbmdvbmVjb21pZHBzcGFyazQ1MTCCASIw
                        DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALTNf5KNg427IKLPfUUL7+cfU1qpiqvRtOa1sdhf
                        4PPXb+eLd5qFhnfX6jTsSzoy6lC7uW2pE3LP++YlzTbMMKCXptf53q5iC54qgKDa3CT4FUdhBjUL
                        oXk3JSUuev7v3tZGu/If48s5v1XxJij7L4qer5sUPihuDEOWtxzVLmykJL6CQa2tSp6B8z5ETFWv
                        htH1u7Pcw+ozV2RvywatJ3iCUHHGiX8n3nAcmPau+jeV0E3Tkb4omLGH3vPYz2isY+qS2vfYXLcB
                        P9rhIAVOU5IxD+vjSDn9eO4JgayijrBENN26kCAl5srdet3Gi4nt3x4JBuljMgWCSoTXkJkVX/0C
                        AwEAATANBgkqhkiG9w0BAQsFAAOCAQEAGfJJXk7UDuhflLQ0HL9Wttz44eWfnfp10IddS7UtNsd1
                        nZXVXLA/GJe97ltdm9yzOz2oDRSN+o56ND9kdHzSbfMr9vsLNnl8pohzKteryRcGkrPPa4h87HPF
                        SiQC7e1TNNcCHE2QOpUfJgdU409cXlsPW0sV8M+Uvkx0CtcgfBjLiEsYOO+C2XTZEbwOSGJz80lF
                        9obi8YRFJOj/qaLw68sCc7iENp2BOezkcs1ulkeYGp8Glzrb3FNKVCYeY4Evna02jhvL5LXl3RVm
                        HwTef7uLRzwqwwBLFS1jAqsEWLWQkNl7xF4m6rNuqO5mIb2AyolWzlSrd70Jldm7HytSJA==
                    ',
				),
		),
);

//
$metadata['https://login.noctrl.edu/idp/shibboleth'] = array (
	'name' => array(
		'en' => 'North Central College - Shibboleth',
	),
	'entityid' => 'https://login.noctrl.edu/idp/shibboleth',
	'contacts' =>
		array (
		),
	'metadata-set' => 'saml20-idp-remote',
	'SingleSignOnService' =>
		array (
			0 =>
				array (
					'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
					'Location' => 'https://login01.noctrl.edu/idp/profile/Shibboleth/SSO',
				),
			1 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
					'Location' => 'https://login01.noctrl.edu/idp/profile/SAML2/POST/SSO',
				),
			2 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
					'Location' => 'https://login01.noctrl.edu/idp/profile/SAML2/POST-SimpleSign/SSO',
				),
			3 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
					'Location' => 'https://login01.noctrl.edu/idp/profile/SAML2/Redirect/SSO',
				),
		),
	'SingleLogoutService' =>
		array (
		),
	'ArtifactResolutionService' =>
		array (
			0 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
					'Location' => 'https://login01.noctrl.edu:8443/idp/profile/SAML1/SOAP/ArtifactResolution',
					'index' => 1,
				),
			1 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
					'Location' => 'https://login01.noctrl.edu:8443/idp/profile/SAML2/SOAP/ArtifactResolution',
					'index' => 2,
				),
		),
	'NameIDFormats' =>
		array (
			0 => 'urn:mace:shibboleth:1.0:nameIdentifier',
			1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
		),
	'keys' =>
		array (
			0 =>
				array (
					'encryption' => false,
					'signing' => true,
					'type' => 'X509Certificate',
					'X509Certificate' => '
MIIDcDCCAligAwIBAgIUDV8GJ6YOSCEuxAf2JIzJI0FaQrYwDQYJKoZIhvcNAQEL
BQAwHTEbMBkGA1UEAwwSbG9naW4wMS5ub2N0cmwuZWR1MB4XDTE2MDMwODE3MjMz
NVoXDTM2MDMwODE3MjMzNVowHTEbMBkGA1UEAwwSbG9naW4wMS5ub2N0cmwuZWR1
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApfzKrazO96ItG61ezMX5
tbjUrEC4c16B2afYiu+GORxNJy5Za4Yd/RUCEHtkOLkxZgOGQDEpZUz+O5q5EcZt
9TJVptnWmzFE1+bei0VNKwouhkkDCqSXuekJER6rgdCy0wWBfwUilpBlZkbeW/Ki
z4pVjYqisfpnvcD9P2jJrRkPlChzia9kskro7QBrV1g8AGUg3juN/atf7ZbBGLql
vayiSgjORZ0SAmuzH+C7IVefqsFhiHcrQfogTEbxnh3Ypw7z5LzqyGUPFRTyksJa
0fkh8oKIHrFw5JY3CviuegwfZai/AnB6DktOxfe16o6kH+qd7QJMwCqGFREkYfKQ
pQIDAQABo4GnMIGkMB0GA1UdDgQWBBSQuqMbKon4vvWqSiII263NMnGlZDCBggYD
VR0RBHsweYISbG9naW4wMS5ub2N0cmwuZWR1ghBsb2dpbi5ub2N0cmwuZWR1ghJs
b2dpbjAyLm5vY3RybC5lZHWCFGxvZ2ludGVzdC5ub2N0cmwuZWR1hidodHRwczov
L2xvZ2luLm5vY3RybC5lZHUvaWRwL3NoaWJib2xldGgwDQYJKoZIhvcNAQELBQAD
ggEBAIhDQbGyuNEt2ZedFDczxXb+xP04EEoOAzDbaWN5OXunrh/sePYuNsiz2WHF
/P20ms1Bq+tI1hXBNH/SprwmiVSXn+cIbXjx3+CChpLUtXui0+gMy1qHysSv/6E5
OhMQu4fZtoaqIH3leDgaAjFBlMPnHqSrXHYeUTRKLIbvhQfRGjJbBEE7zUKSV3ZB
fxFiYHZnQVouW7RwHI+lsL/ygQ/Xlqni12nYEytkgOV3jrZsdSBfdELHCAZo909M
mL+jGInLkrssD037MAO5c5C6h/54Z8XpLyA+TJE9ZUbiBxkCFJqOLC0Ei8Xa0eIO
DpT0U1q92TvXzowHX0Oz0vq4Vlc=
                        ',
				),
			1 =>
				array (
					'encryption' => false,
					'signing' => true,
					'type' => 'X509Certificate',
					'X509Certificate' => '
MIIDcDCCAligAwIBAgIUWtsy0/6vZLwoDVXH78TY2qmhUNowDQYJKoZIhvcNAQEL
BQAwHTEbMBkGA1UEAwwSbG9naW4wMS5ub2N0cmwuZWR1MB4XDTE2MDMwODE1MTEx
MVoXDTM2MDMwODE1MTExMVowHTEbMBkGA1UEAwwSbG9naW4wMS5ub2N0cmwuZWR1
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmDOmzXwxhWxkD/ktqW/6
sjwodUuEHY4NFKp3VPMDxGmI8YUhKV6JEpVr0aGlzvL3m/GIk2FQyKKp2YQPmEy3
77MSJqIcAxpml1B3G7oKn6QsxA1bQxfbg0yvHc1NQqhbyyX617//Ol1ID778GR+q
dHDYnu32dsi78x92dS8lCgUJIiRDmZO0tQUAdKYKwQEXRk5VCmFqZEUx70ykLxgD
3ljbgpX9S/rIwLDBPoW1IU4IErBTcIUYvkskC2hbPckRJqtKDqNIc7F8+/ObhUNU
rKAkDtTKezLJWtt9GreAsNXnUlOjCbI75dJQay8w46n3VX2UqUG2I+TPixJsfE9y
GQIDAQABo4GnMIGkMB0GA1UdDgQWBBT1dPoo5z1xiGwI+5bcqVM1eACnNzCBggYD
VR0RBHsweYISbG9naW4wMS5ub2N0cmwuZWR1ghBsb2dpbi5ub2N0cmwuZWR1ghJs
b2dpbjAyLm5vY3RybC5lZHWCFGxvZ2ludGVzdC5ub2N0cmwuZWR1hidodHRwczov
L2xvZ2luLm5vY3RybC5lZHUvaWRwL3NoaWJib2xldGgwDQYJKoZIhvcNAQELBQAD
ggEBAAm4af+mYpASKfJM9hLrH60RPyD8yUdztCWdz4qxzD8bVsyR4W+gX/GLvAPX
Gfunsxo37/p4z2pLtKfkh6rjV0XpyjC8yUGdKc7k6glvRvmLFB1mrioseoeS5Lfl
gREeBIe+zBpqTHI/AlrTGzwMmZUjXAKvB1ycN/gdmDYYZGR1/EhCusIkv9i4tYjt
qh881l3oZL6Z5u+j6Bm5hfIFv+klBwnqqJN8ssng/GRUD/bxgZzkZBSrS+CMo5qk
WN7Jgu/G5ICTvWSk//bd3/BtRhPhSY3ORymf5lQUY6qDYXYgHXXWoryu3dUrLPyO
ueORC0fUYnUnRL8VxlfktQzVIkc=
                        ',
				),
			2 =>
				array (
					'encryption' => true,
					'signing' => false,
					'type' => 'X509Certificate',
					'X509Certificate' => '
MIIDcTCCAlmgAwIBAgIVAPYpa2HbfxknBF2HAwWMWhe1AtgnMA0GCSqGSIb3DQEB
CwUAMB0xGzAZBgNVBAMMEmxvZ2luMDEubm9jdHJsLmVkdTAeFw0xNjAzMDgxNzI0
MzZaFw0zNjAzMDgxNzI0MzZaMB0xGzAZBgNVBAMMEmxvZ2luMDEubm9jdHJsLmVk
dTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAK0sHQqX+sMs3K4eWR0k
7bt5K19I1fNgt7jfS/Y4/pPdrPUcDoZ016SBUChgNz1/Us7kwgDpTeInb19lgyCv
uAlG5eNJN0lc0HKA9fWAE9cnyBaTKVkTxQUidtkXbBQfnDC9nfIchBpSmAqHbNqD
aX8+9Qx54UlPqNvkdpJcoKShZnug3H4dcp88ClJ715kKSVlpgRgSv25pagwEFNOi
S9ELovQv8o5yOfbeKYxAsyWlQiZZEOTEbA1XUlbwIL0atXZPID45xjY8mTejXC3G
5tF4GK3Haes0r8VGx8NTO+8rH07mI1vRmVJPswDsl7iTppw1oCy1TJqk6D569dBb
LgcCAwEAAaOBpzCBpDAdBgNVHQ4EFgQUZyqgwVhdaUqpazRqEAvatudS3CowgYIG
A1UdEQR7MHmCEmxvZ2luMDEubm9jdHJsLmVkdYIQbG9naW4ubm9jdHJsLmVkdYIS
bG9naW4wMi5ub2N0cmwuZWR1ghRsb2dpbnRlc3Qubm9jdHJsLmVkdYYnaHR0cHM6
Ly9sb2dpbi5ub2N0cmwuZWR1L2lkcC9zaGliYm9sZXRoMA0GCSqGSIb3DQEBCwUA
A4IBAQCoKmX5RpsO19paSjkmq1MV73t/uxiaxwwhZXjlwT46nJL64CFLFadUAf6c
7fiDM3ZQn81QrJHwf7q6nR7w5iTzNtkMgRM+BfgXgcVbliQeNbSIUTpHqJ/M0kiG
bDyXiLwbCIu5y0EkT0jZuziv7+I+XhvTbIf24GMahAw9FaKWBtPHvRjFh85yKd48
P5G3eqZ3WtITm4CLZEKWgec9FGwvRE+xx3VANWU8m1KYqDpw8G/nW2+/jAbx/sUe
XVLhJJJhVADkaWuWuVBUwxMg4U1rGV8SN2Fl9L+T/0EjfJVsbGIGYbBO2AH0ipUg
gwFy6kMTC2Cw3/2Igijhqp3XHX0u
                        ',
				),
		),
	'scope' =>
		array (
			0 => 'noctrl.edu',
		),
);
